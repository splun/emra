<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/settings');

        $this->lang->load('admin/insurance');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_settings_insurance'), 'admin/settings/insurance');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();


            $this->data['items'] = $this->settings->get_items("insurance");

            /* Load Template */
            $this->template->admin_render('admin/settings/insurance/list', $this->data);
        }
    }

	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/settings/insurance/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->settings->get_items("insurance",array("id" => $id));

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
            		'discount' => $this->input->post('discount'),
            		'showDiscount' => $this->input->post('showDiscount'),
            		'status' => $this->input->post('status'),
				);

                if($this->settings->update_item("insurance",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/settings/insurance/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);

		$this->data['discount'] = array(
			'name'  => 'discount',
			'id'    => 'discount',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('discount', $item->discount)
		);

		$this->data['showDiscount'] = array(
			'name'  => 'showDiscount',
			'id'    => 'showDiscount',
			'type'  => 'checkbox',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('showDiscount', $item->showDiscount)
		);
		
        /* Load Template */
		$this->template->admin_render('admin/settings/insurance/edit', $this->data);
	}

	public function add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), 'admin/settings/insurance/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'discount' => $this->input->post('discount'),
	            'showDiscount' => $this->input->post('showDiscount')
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("insurance",$fields))
		{
            $this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/insurance/', 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['discount'] = array(
			'name'  => 'discount',
			'id'    => 'discount',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('discount')
		);

		$this->data['showDiscount'] = array(
			'name'  => 'showDiscount',
			'id'    => 'showDiscount',
			'type'  => 'checkbox',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('showDiscount')
		);
			
            /* Load Template */
            $this->template->admin_render('admin/settings/insurance/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		$item = $this->settings->get_items("insurance",array("id" => $id));

		if($item->showDiscount == 1){
			$this->session->set_flashdata('message', lang("actions_cannot_delete"));
			redirect('admin/settings/insurance/', 'refresh');
		}else{
			if ($this->settings->delete_item("insurance",$id))
			{
            	$this->session->set_flashdata('message', lang("actions_success"));
				redirect('admin/settings/insurance/', 'refresh');
			}
		}

	}

	public function activate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'status' => 1
	    );

		if ($this->settings->update_item("insurance",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/insurance/', 'refresh');
		}

	}

	public function deactivate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'showDiscount' => 0
	    );

		if ($this->settings->update_item("insurance",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/insurance/', 'refresh');
		}

	}

	public function showDiscountOn($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'showDiscount' => 1
	    );

		if ($this->settings->update_item("insurance",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/insurance/', 'refresh');
		}

	}

	public function showDiscountOff($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'showDiscount' => 0
	    );

		if ($this->settings->update_item("insurance",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/insurance/', 'refresh');
		}

	}
}
?>