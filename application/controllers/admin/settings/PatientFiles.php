<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PatientFiles extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/settings');

        $this->lang->load('admin/patient_files');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');


	    $this->data['type'] = "";

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index($section)
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        $this->page_title->push(lang('section_'.$section));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/'.$section);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->data['section_id'] = $section;
        $this->data['items'] = $this->settings->get_patient_file($section);

        /* Load Template */
        $this->template->admin_render('admin/settings/patient_files/list', $this->data);
        }
    }

    public function ddl($section)
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        $this->page_title->push(lang('section_ddl_'.$section));
        $this->data['pagetitle'] = $this->page_title->show();
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/ddl/'.$section);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->data['section_id'] = $section;
        $this->data['type'] = "ddl";
        $this->data['items'] = $this->settings->get_patient_file_ddl($section);

        /* Load Template */
        $this->template->admin_render('admin/settings/patient_files/list', $this->data);
        }
    }

	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}


        /* Data */
		$item = $this->settings->get_patient_file(0,$id);

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
					'category' => $this->input->post('category')
				);

                if($this->settings->update_item("patient_file",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/settings/patient_files/'.$item->section, 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		$this->data['categories'] = $this->settings->get_items("patient_file_categories",array("section"=>$item->section));

		$this->data['section_id'] = $item->section;

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/'.$item->section);
        $this->breadcrumbs->unshift(2, lang('pagetitle_edit'), 'admin/settings/patient_files/edit/'.$id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_'.$item->section));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Load Template */
		$this->template->admin_render('admin/settings/patient_files/edit', $this->data);
	}

	public function edit_ddl($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}


        /* Data */
		$item = $this->settings->get_patient_file_ddl(0,$id);

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->settings->update_item("patient_file_ddl",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/settings/patient_files/ddl/'.$item->section, 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		$this->data['section_id'] = "ddl_".$item->section;
		
		$this->data['categories'] = $this->settings->get_items("patient_file_ddl_categories",array("section"=>$item->section));

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/ddl/'.$item->section);
        $this->breadcrumbs->unshift(2, lang('pagetitle_edit'), 'admin/settings/patient_files/edit_ddl/'.$id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_ddl_'.$item->section));
        $this->data['pagetitle'] = $this->page_title->show();

        $this->data['type'] = "ddl";
        /* Load Template */
		$this->template->admin_render('admin/settings/patient_files/edit', $this->data);
	}

	public function add($section_id)
	{
		$section_id = (int) $section_id;

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/'.$section_id);
        $this->breadcrumbs->unshift(2, lang('pagetitle_add'), 'admin/settings/patient_files/add/'.$section_id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_'.$section_id));
        $this->data['pagetitle'] = $this->page_title->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		
		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'category' => $this->input->post('category'),
	            'section' => $section_id,
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_item_create"));
			redirect('admin/settings/patient_files/'.$section_id, 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

        $this->data['categories'] = $this->settings->get_items("patient_file_categories",array("section"=>$section_id));

        $this->data['section_id'] = $section_id;

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

            /* Load Template */
            $this->template->admin_render('admin/settings/patient_files/create', $this->data);
        }
	}

	public function add_ddl($section_id)
	{
		$section_id = (int) $section_id;

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/ddl/'.$section_id);
        $this->breadcrumbs->unshift(2, lang('pagetitle_add'), 'admin/settings/patient_files/add_ddl/'.$section_id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_ddl_'.$section_id));
        $this->data['pagetitle'] = $this->page_title->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		
		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'category' => $this->input->post('category'),
	            'section' => $section_id,
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file_ddl", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_item_create"));
			redirect('admin/settings/patient_files/ddl/'.$section_id, 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

        $this->data['categories'] = $this->settings->get_items("patient_file_ddl_categories",array("section"=>$section_id));

        $this->data['section_id'] = $section_id;

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);
		$this->data['type'] = "ddl";
            /* Load Template */
            $this->template->admin_render('admin/settings/patient_files/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		$item = $this->settings->get_patient_file(0,$id);

		if ($this->settings->delete_item("patient_file",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_item_delete"));
			redirect('admin/settings/patient_files/'.$item->section, 'refresh');
		}

	}

	public function delete_ddl($id)
	{
		$id = (int) $id;

		$item = $this->settings->get_patient_file_ddl(0,$id);

		if ($this->settings->delete_item("patient_file_ddl",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_item_delete"));
			redirect('admin/settings/patient_files/ddl/'.$item->section, 'refresh');
		}

	}

	public function category_add($section_id)
	{

		$section_id = (int) $section_id;

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/'.$section_id);
        $this->breadcrumbs->unshift(2, lang('actions_cat_create_edit'), 'admin/settings/patient_files/category_add/'.$section_id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_'.$section_id));
        $this->data['pagetitle'] = $this->page_title->show();
		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'section' => $section_id
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file_categories", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_category_create"));
			redirect('admin/settings/patient_files/category_add/'.$section_id, 'refresh');
		}
		else
		{

        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

 		$this->data['section_id'] = $section_id;

 		$this->data['categories'] = $this->settings->get_items("patient_file_categories",array("section"=>$section_id));

        /* Load Template */
        $this->template->admin_render('admin/settings/patient_files/category_create', $this->data);
        }
	}

	public function ddl_category_add($section_id)
	{

		$section_id = (int) $section_id;

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/patient_files/ddl/'.$section_id);
        $this->breadcrumbs->unshift(2, lang('actions_cat_create_edit'), 'admin/settings/patient_files/ddl_category_add/'.$section_id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('section_'.$section_id));
        $this->data['pagetitle'] = $this->page_title->show();
		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'section' => $section_id
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file_ddl_categories", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_category_create"));
			redirect('admin/settings/patient_files/ddl_category_add/'.$section_id, 'refresh');
		}
		else
		{

        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

 		$this->data['section_id'] = $section_id;

 		$this->data['categories'] = $this->settings->get_items("patient_file_ddl_categories",array("section"=>$section_id));

 		$this->data['type'] = "ddl";

        /* Load Template */
        $this->template->admin_render('admin/settings/patient_files/category_create', $this->data);
        }
	}
	public function category_edit($id)
	{
        $id = (int) $id;


        $category = $this->settings->get_items("patient_file_categories",array("id"=>$id));

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->settings->update_item("patient_file_categories",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_category_update"));
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : $this->data['message']);

		redirect('admin/settings/patient_files/category_add/'.$category->section, 'refresh');
	}

	public function ddl_category_edit($id)
	{
        $id = (int) $id;


        $category = $this->settings->get_items("patient_file_ddl_categories",array("id"=>$id));

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->settings->update_item("patient_file_ddl_categories",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_category_update"));
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : $this->data['message']);

		redirect('admin/settings/patient_files/ddl_category_add/'.$category->section, 'refresh');
	}

	public function category_delete($category_id)
	{
		$category_id = (int) $category_id;

		$category = $this->settings->get_items("patient_file_categories",array("id"=>$category_id));

		$items = $this->settings->get_items("patient_file",array("category"=>$category_id));

		if(count($items) > 0){
			$this->session->set_flashdata('message', lang("actions_cannot_delete_category"));
			redirect('admin/settings/patient_files/category_add/'.$category->section, 'refresh');
		}else{
			if ($this->settings->delete_item("patient_file_categories",$category_id))
			{
            	$this->session->set_flashdata('message', lang("actions_category_delete"));
				redirect('admin/settings/patient_files/category_add/'.$category->section, 'refresh');
			}
		}

	}

	public function ddl_category_delete($category_id)
	{
		$category_id = (int) $category_id;

		$category = $this->settings->get_items("patient_file_ddl_categories",array("id"=>$category_id));

		$items = $this->settings->get_items("patient_file_ddl",array("category"=>$category_id));

		if(count($items) > 0){
			$this->session->set_flashdata('message', lang("actions_cannot_delete_category"));
			redirect('admin/settings/patient_files/ddl_category_add/'.$category->section, 'refresh');
		}else{
			if ($this->settings->delete_item("patient_file_ddl_categories",$category_id))
			{
            	$this->session->set_flashdata('message', lang("actions_category_delete"));
				redirect('admin/settings/patient_files/ddl_category_add/'.$category->section, 'refresh');
			}
		}

	}
}
?>