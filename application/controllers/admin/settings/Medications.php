<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class medications extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/settings');

        $this->lang->load('admin/medications');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/medications');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $this->data['items'] = $this->settings->get_patient_file(100);

        /* Load Template */
        $this->template->admin_render('admin/settings/medications/list', $this->data);
        }
    }
	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_edit'), 'admin/settings/medications/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->settings->get_patient_file(0,$id);

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
					'category' => $this->input->post('category')
				);

                if($this->settings->update_item("patient_file",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/settings/medications/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		$this->data['categories'] = $this->settings->get_patient_file_sections(100);

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);


        /* Load Template */
		$this->template->admin_render('admin/settings/medications/edit', $this->data);
	}

	public function add()
	{
		
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_add'), 'admin/settings/medications/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');
		
		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'category' => $this->input->post('category'),
	            'section' => 100,
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_item_create"));
			redirect('admin/settings/medications/', 'refresh');
		}
		else
		{
        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

        $this->data['categories'] = $this->settings->get_patient_file_sections(100);

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);


            /* Load Template */
            $this->template->admin_render('admin/settings/medications/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		$item = $this->settings->get_patient_file(0,$id);

		if ($this->settings->delete_item("patient_file",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_item_delete"));
			redirect('admin/settings/medications/', 'refresh');
		}

	}

	public function category_edit($id)
	{
        $id = (int) $id;

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_category_edit'), 'admin/settings/medications/category_edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->settings->update_item("patient_file_categories",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_category_update"));
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : $this->data['message']);

		redirect('admin/settings/medications/category_add', 'refresh');
	}

	public function category_add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('actions_cat_create_edit'), 'admin/settings/medications/category_add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'section' => 100
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("patient_file_categories", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_category_create"));
			redirect('admin/settings/medications/category_add', 'refresh');
		}
		else
		{

        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['categories'] = $this->settings->get_patient_file_sections(100);


        /* Load Template */
        $this->template->admin_render('admin/settings/medications/category_create', $this->data);
        }
	}

	public function category_delete($category_id)
	{
		$category_id = (int) $category_id;

		$items = $this->settings->get_patient_file($category_id);

		if(count($items) > 0){
			$this->session->set_flashdata('message', lang("actions_cannot_delete_category"));
			redirect('admin/settings/medications/', 'refresh');
		}else{
			if ($this->settings->delete_item("patient_file_categories",$category_id))
			{
            	$this->session->set_flashdata('message', lang("actions_category_delete"));
				redirect('admin/settings/medications/category_add', 'refresh');
			}
		}

	}

	public function category($id)
    {
    	$category_id = (int) $id;

    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        $this->data['category'] = $this->settings->get_patient_file_sections(100,$category_id);

        /* Title Page :: Common */
	    $this->page_title->push($this->data['category']->name." ");
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, $this->data['category']->name, 'admin/settings/medications/category/'.$category_id);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->data['items'] = $this->settings->get_patient_file($category_id);

        /* Load Template */
        $this->template->admin_render('admin/settings/medications/list', $this->data);
        }
    }


}
?>