<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class invoice extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/settings');

        $this->lang->load('admin/invoice');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/settings/invoice');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $this->data['items'] = $this->settings->get_invoice_items();

        $this->data['func'] = $this;
        
        /* Load Template */
        $this->template->admin_render('admin/settings/invoice/list', $this->data);
        }
    }

    function parent_categories($parent_id)
    {
    	return $this->settings->get_items("invoice_categories",$parent_id);

    }

    function cild_categories($parent_id)
    {
    	return $this->settings->get_invoice_child_categories($parent_id);

    }

	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_edit'), 'admin/settings/invoice/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->settings->get_invoice_items($id);

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');
		$this->form_validation->set_rules('price', 'lang:price_label', 'numeric');
		$this->form_validation->set_rules('net_price', 'lang:net_price_label', 'numeric');
		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
					'price' => $this->input->post('price'),
					'net_price' => $this->input->post('net_price'),
					'category' => $this->input->post('category')
				);

                if($this->settings->update_item("invoice",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/settings/invoice/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		$this->data['categories'] = $this->settings->get_items("invoice_categories");

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);

		$this->data['price'] = array(
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'number',
			'min'   => 0,
			'step'  => 0.01,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('price', $item->price)
		);

		$this->data['net_price'] = array(
			'name'  => 'net_price',
			'id'    => 'net_price',
			'type'  => 'number',
			'min'   => 0,
			'step'  => 0.01,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('net_price', $item->net_price)
		);

		$this->data['func'] = $this;

        /* Load Template */
		$this->template->admin_render('admin/settings/invoice/edit', $this->data);
	}

	public function add()
	{
		
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_add'), 'admin/settings/invoice/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');
		$this->form_validation->set_rules('price', 'lang:price_label', 'numeric');
		$this->form_validation->set_rules('net_price', 'lang:net_price_label', 'numeric');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
				'price' => $this->input->post('price'),
				'net_price' => $this->input->post('net_price'),
	            'category' => $this->input->post('category')
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("invoice", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_item_create"));
			redirect('admin/settings/invoice/', 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

        $this->data['categories'] = $this->settings->get_items("invoice_categories");

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['price'] = array(
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'number',
			'min'   => 0,
			'step'  => 0.01,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('price')
		);

		$this->data['net_price'] = array(
			'name'  => 'net_price',
			'id'    => 'net_price',
			'type'  => 'number',
			'min'   => 0,
			'step'  => 0.01,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('net_price')
		);

		$this->data['func'] = $this;

            /* Load Template */
            $this->template->admin_render('admin/settings/invoice/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		if ($this->settings->delete_item("invoice",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_item_delete"));
			redirect('admin/settings/invoice/', 'refresh');
		}

	}

	public function category_edit($id)
	{
        $id = (int) $id;

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->settings->update_item("invoice_categories",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_category_update"));
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

			redirect('admin/settings/invoice/category_add/', 'refresh');

		}

	}

	public function category_add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('actions_cat_create_edit'), 'admin/settings/invoice/category_add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'parent' =>  $this->input->post('parent')
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->settings->insert_item("invoice_categories", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_category_create"));
			redirect('admin/settings/invoice/category_add', 'refresh');
		}
		else
		{
        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

        /* Data */
		$this->data['categories'] = $this->settings->get_items("invoice_categories");

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['func'] = $this;
        
        /* Load Template */
        $this->template->admin_render('admin/settings/invoice/category_create', $this->data);
        }
	}

	public function category_delete($category_id)
	{
		$category_id = (int) $category_id;

		$items = $this->settings->get_items("invoice", array("category" => $category_id));

		if(count($items) > 0){
			$this->session->set_flashdata('message', lang("actions_cannot_delete_category"));
			redirect('admin/settings/invoice/category_add', 'refresh');
		}else{
			if ($this->settings->delete_item("invoice_categories",$category_id))
			{
            	$this->session->set_flashdata('message', lang("actions_category_delete"));
				redirect('admin/settings/invoice/category_add', 'refresh');
			}
		}

	}

	public function activate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'flag_status' => 1
	    );

		if ($this->settings->update_item("invoice",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/invoice/', 'refresh');
		}

	}

	public function deactivate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'flag_status' => 0
	    );

		if ($this->settings->update_item("invoice",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/settings/invoice/', 'refresh');
		}

	}

}
?>