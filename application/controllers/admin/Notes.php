<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('admin/users');


        /* Title Page :: Common */
	    $this->page_title->push(lang('notes_title'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('notes_title'), 'admin/notes');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index(){
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();


            if($this->ion_auth->is_admin() || $this->ion_auth->user()->row()->id == 5){

                //$this->data['notes'] = $this->core_model->get_items("notes",array("done_flag"=>0),"id","DESC");
                $this->data['notes'] = $this->NotesModel->get_notes();
            }else{
                
                $this->data['notes'] = $this->NotesModel->get_notes("owner",$this->ion_auth->user()->row()->id);
            }

            /* Load Template */
            $this->template->admin_render('admin/users/notes', $this->data);
        }
    }

    public function activate($id)
    {
        $id = (int) $id;

        $fields = array(
            'done_flag' => 1
        );

        if ($this->core_model->update_item("notes",$fields,array("id"=>$id)))
        {
            $this->session->set_flashdata('message', lang("actions_success"));
            redirect('admin/notes', 'refresh');
        }

    }

    public function deactivate($id)
    {
        $id = (int) $id;

        $fields = array(
            'done_flag' => 0
        );

        if ($this->core_model->update_item("notes",$fields,array("id"=>$id)))
        {
            $this->session->set_flashdata('message', lang("actions_success"));
            redirect('admin/notes', 'refresh');
        }

    }

    public function save(){
    	
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Validate form input */
            $this->form_validation->set_rules('note_id', 'Unknown note id', 'required');

            if ($this->form_validation->run() == TRUE)
            {
                $fields = array(
                    'txt_notesReplay' => $this->input->post('txt_notesReplay')
                );

                $id = $this->input->post('note_id');

                if($this->core_model->update_item("notes",$fields,array("id"=>$id)))
                {
                    $this->session->set_flashdata('message', lang("actions_item_update"));
                }
                else
                {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                }

                redirect('admin/notes', 'refresh');
            }

        }
    }

    public function delete($id)
    {
        $id = (int) $id;

        if ($this->core_model->delete_item("notes",$id))
        {
            $this->session->set_flashdata('message', lang("actions_success"));
            redirect('admin/notes/', 'refresh');
        }

    }

    public function insertNotesFromJson(){
        $file = file_get_contents('upload/notes.json');
        $json = json_decode($file);
        $c=0;

        foreach($json->notes as $note){
            if ($loadItems = $this->core_model->insert_item("notes",$note))
            {
                echo 'Success! Record - '.$note->id.' loaded <br />';
            }else{
                echo 'Error! Record - '.$note->id.' not loaded <br />';
            }

            $c++;
        }

        echo 'Load: '.$c.' records';
    }
}