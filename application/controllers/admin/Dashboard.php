<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');

        $this->load->model('admin/dashboard_model');
        $this->load->model('admin/settings');
        $this->lang->load('admin/dashboard');
        $this->lang->load('admin/patients');
        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            
            /****Main Counts*****/
            /*$this->data['count_users']       = $this->dashboard_model->get_count_record('users');
            $this->data['count_patients']       = $this->dashboard_model->get_count_record('patients');
            $this->data['count_groups']      = $this->dashboard_model->get_count_record('groups');
            $this->data['disk_totalspace']   = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
            $this->data['disk_freespace']    = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
            $this->data['disk_usespace']     = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
            $this->data['disk_usepercent']   = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
            $this->data['memory_usage']      = $this->dashboard_model->memory_usage();
            $this->data['memory_peak_usage'] = $this->dashboard_model->memory_peak_usage(TRUE);
            $this->data['memory_usepercent'] = $this->dashboard_model->memory_usepercent(TRUE, FALSE);*/


            $this->data['notes_patient_id'] = array(
                'name'  => 'notes_patient_id',
                'id'    => 'notes_patient_id',
                'type'  => 'text',
                'placeholder'=>lang("notes_patientid_label"),
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('notes_patient_id')
            );

            $this->data['notes_text'] = array(
                'name'  => 'notes_text',
                'id'    => 'notes_text',
                'type'  => 'text',
                'placeholder'=>lang("notes_text_label"),
                'rows'  => "5",
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('notes_text')
            );

            $this->data['task_title'] = array(
                'name'  => 'task_title',
                'id'    => 'task_title',
                'type'  => 'text',
                'placeholder'=>lang("task_title_label"),
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('task_title')
            );

            $this->data['task_date'] = array(
                'name'  => 'task_date',
                'id'    => 'task_date',
                'type'  => 'date',
                'placeholder'=>lang("task_date_label"),
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('task_date')
            );

            $this->data['task_text'] = array(
                'name'  => 'task_text',
                'id'    => 'task_text',
                'type'  => 'text',
                'placeholder'=>lang("task_text_label"),
                'rows'  => "5",
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('task_text')
            );

            $this->data['task_users'] = $this->ion_auth->users()->result();

            /*****Stock*******/
            //$this->data['stock_items_toexprire'] = $this->core_model->get_items("stock_items","expire_day  < (NOW() + INTERVAL 120 DAY) AND expire_day != '0000-00-00' AND qty > 0","expire_day","ASC",5);

            //$this->data['stock_items_qtylow'] = $this->core_model->get_items("stock_items","qty < 10 AND qty > 0","qty","ASC",5);

            //$this->data['stock_items_units'] = $this->core_model->get_items("units");

            /*****Financy*******/
            //$filter_doctor = 0;
            //if(!$this->ion_auth->is_admin()){
                //$filter_doctor = $this->ion_auth->user()->row()->id;
            //}
            
            //$this->data['today_finance_info'] = $this->dashboard_model->get_finance_info("day",$filter_doctor);
            //$this->data['week_finance_info'] = $this->dashboard_model->get_finance_info("week",$filter_doctor);
            //$this->data['month_finance_info'] = $this->dashboard_model->get_finance_info("month",$filter_doctor);
            
            

            /********Appointments***********/
            $this->data['doctors'] = $this->ion_auth->users(array('doctors','nurses'))->result();

            $date = date("Y-m-d");
            $doctor_id = 0;


            if(!isset($_GET['doctor']) && $this->data['is_doctor'] || ($this->data['is_doctor'] && $this->data['is_admin'])){
                $doctor_id = $this->ion_auth->user()->row()->id;
            }

            if(isset($_GET['doctor']) && $_GET['doctor'] !== 0){

                $doctor_id = (int)$this->input->get('doctor');

            }

            if($this->input->get('date_appointments')){
                $date = date("Y-m-d",strtotime($this->input->get('date_appointments')));
            }

            $this->data["current_doctor_id"] = $doctor_id;
            $this->data["current_date"] = $date;
            $this->data["appointments"] = $this->appointmentsModel->get_appointments($date,$doctor_id,"all",3000,TRUE);
            
            $this->data["func"] = $this;

            /* Load Templates From groups */
            if($this->data['is_admin'] || $this->data['is_doctor']){

                if(!empty($this->data["appointments"])){

                    $this->data["count_out_come"] = $this->appointmentsModel->get_count_outcome($date,$doctor_id);

                    $this->data["active_app"] = ($this->input->get('appointment')) ? $this->input->get('appointment') : $this->data["appointments"][0]->event_id;
                }

                $this->template->admin_render('admin/dashboard/admin_dashboard', $this->data);
            }

            if($this->data['is_nurse']){
                $this->data['rooms'] = $this->core_model->get_items("rooms");
                $this->data['types'] = $this->core_model->get_items("appointments_type",array("status"=>"1"),"name","ASC");

                $this->template->admin_render('admin/dashboard/nurse_dashboard', $this->data);
            }

            if($this->data['is_reception']){
                $this->lang->load('admin/appointments');
                $this->data["patients"] = $this->PatientsModel->get_patients();
                $this->data['rooms'] = $this->core_model->get_items("rooms");
                $this->data['types'] = $this->core_model->get_items("appointments_type",array("status"=>"1"),"name","ASC");
                $this->template->admin_render('admin/dashboard/reception_dashboard', $this->data);
            }
        }
	}


    public function add_notes()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Validate form input */
            //$this->form_validation->set_rules('notes_patient_id', 'lang:notes_patientid_label', 'required');
            $this->form_validation->set_rules('notes_text', 'lang:notes_text_label', 'required');

            if ($this->form_validation->run()){

                if($this->input->post('notes_users') && !empty($this->input->post('notes_users'))){
                    $allocate = serialize($this->input->post('notes_users'));
                }else{
                    $allocate = "";
                }

                $fields = array(
                    'patient_id'=>$this->input->post('notes_patient_id'),
                    'notes' => $this->input->post('notes_text'),
                    'for_user' => $allocate,
                    'originate_date' => date("Y-m-d h:i:s"),
                    'create_user'=>$this->ion_auth->user()->row()->id
                );
            }

            if ($this->form_validation->run() == TRUE && $this->core_model->insert_item("notes",$fields))
            {   
                
                $this->session->set_flashdata('message', lang("actions_success"));
  
            }else{
                $this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
            }

            redirect("/admin/dashboard/", 'refresh');
        }
    }

    public function add_task()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Validate form input */
            $this->form_validation->set_rules('task_title', 'lang:task_title_label', 'required');
            $this->form_validation->set_rules('task_date', 'lang:task_date_label', 'required');

            $this->form_validation->set_rules('task_users[]', 'lang:task_to_label', 'required');

            if ($this->form_validation->run()){

                $fields = array(
                    'title'=>$this->input->post('task_title'),
                    'taskDate' => $this->input->post('task_date'),
                    'taskendDate' => ($this->input->post('task_end_date')) ? $this->input->post('task_end_date') : $this->input->post('task_date'),
                    'txt_description' => $this->input->post('task_text'),
                    'status_flag' => 0,
                    'create_date' => date("Y-m-d h:i:s"),
                    'chk_Repeaton' => ($this->input->post('task_repeaton')) ? $this->input->post('task_repeaton') : 0,
                    'chk_Neverend' => ($this->input->post('task_neverend')) ? $this->input->post('task_neverend') : 0,
                    'rdo_Priority' => $this->input->post('task_priority'),
                    'rdo_Status' => $this->input->post('task_status'),
                    'calendarDate' => $this->input->post('task_date'),
                    'create_user'=>$this->ion_auth->user()->row()->id
                );
            }




            if ($this->form_validation->run() == TRUE && $insert_id = $this->core_model->insert_item("tasks",$fields))
            {                   

                foreach($this->input->post('task_users') as $u=>$val){
                    $this->core_model->insert_item("task_users",array("user_id"=>$val, "task_id"=>$insert_id));
                }

                $this->session->set_flashdata('message', lang("actions_success"));
                redirect("/admin/dashboard/", 'refresh');
                 
            }else{
                $this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
            }
        }
    }


    public function add_checkboxpage_records(){

        /* Validate form input */
        $this->form_validation->set_rules('item[]', 'lang:bhv_problem_field', 'required');
        $this->form_validation->set_rules('patient_id', 'unknown patient id', 'required');


        if ($this->form_validation->run()){

            $fields = array();
            $items = $this->input->post('item');
            $description = ($this->input->post('description')) ? $this->input->post('description') : "";
            $section = ($this->input->post('section')) ? $this->input->post('section') : "";

            $this->PatientsModel->delete_items("patient_checkboxpage",array("date_add"=>$this->input->post('date_appointment'),"section"=>$section,"patient_id"=>$this->input->post('patient_id')));

            for($i=1;$i<=5;$i++){
                if(isset($items[$i]) && $items[$i] !== "Select"){
                    $fields[] = array(
                        'patient_id'=>$this->input->post('patient_id'),
                        'patient_file_id'=>$items[$i],
                        'patient_file_description'=>(isset($description[$i])) ? $description[$i] : "",
                        'status'=>1,
                        'date_add' => $this->input->post('date_appointment'),
                        'section'=> ($this->input->post('section')) ? $this->input->post('section') : 0
                    );
                }
            }

            if($this->input->post('desc') && $this->input->post('section')){


                $share_field_section = $this->PatientsModel->get_patient_results_history($this->input->post('patient_id'),0,$this->input->post('date_appointment'));

                $share_field_section_id = ($share_field_section) ? $share_field_section->id : false;

                //$share_field_section_id = $this->input->post('share_field_section');

                $share_fields = array(
                    'patient_id' => $this->input->post('patient_id'),
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => ($this->input->post('section')) ? $this->input->post('section') : 0,
                    'date_test' => $this->input->post('date_appointment'),
                    'ddl_id' => 0
                );

                if(!empty($share_field_section_id)){
                    $this->PatientsModel->update_item("share_field_section",$share_fields,$share_field_section_id);
                }else{
                    $this->PatientsModel->insert_item("share_field_section",$share_fields);
                }
            }

            if($this->core_model->insert_all_items("patient_checkboxpage",$fields)){
                echo json_encode(array("status"=>"success","message"=>lang("actions_success")));
            }else{
                echo json_encode(array("status"=>"error","message"=>lang("actions_error")));
            }

        }else{
            echo json_encode(array("status"=>"error","message"=>validation_errors()));
        }
    }

    public function add_treatments_records(){

        /* Validate form input */
        $this->form_validation->set_rules('item[]', 'lang:bhv_problem_field', 'required');
        $this->form_validation->set_rules('patient_id', 'unknown patient id', 'required');


        if ($this->form_validation->run()){

            $fields = array();

            $items = $this->input->post('item');
            $description = ($this->input->post('description')) ? $this->input->post('description') : "";
            $complaint = $this->input->post('complaint');
            $dc = $this->input->post('dc');
            $weeks = $this->input->post('weeks');

            for($i=1;$i<=5;$i++){
                if(isset($items[$i]) && $items[$i] !== ""){

                    $patient_files = $this->settings->get_items("patient_file",array("id"=>$items[$i]));

                    $fields[] = array(
                        'patient_id'=>$this->input->post('patient_id'),
                        'patient_file_id'=>$items[$i],
                        'patient_file_category_id' => ($patient_files) ? $patient_files->category : 0,
                        'patient_file_description'=>$description[$i],
                        'complaint' => (isset($complaint)) ? $complaint[$i] : 0,
                        'dc' => (isset($dc)) ? $dc[$i] : 0,
                        'weeks' => (isset($weeks)) ? $weeks[$i] : 0,
                        'date_add' => $this->input->post('date_appointment'),
                    );
                }
            }

            if($this->core_model->insert_all_items("treatments",$fields)){
                echo json_encode(array("status"=>"success","message"=>lang("actions_success")));
            }else{
                echo json_encode(array("status"=>"error","message"=>lang("actions_error")));
            }

        }else{
            echo json_encode(array("status"=>"error","message"=>validation_errors()));
        }
    }

    public function add_sharefields_records(){

        /* Validate form input */
        $this->form_validation->set_rules('item', 'lang:bhv_problem_field', 'required');
        $this->form_validation->set_rules('patient_id', 'unknown patient id', 'required');

        if ($this->form_validation->run()){

            $fields = array(
                'patient_id' => $this->input->post('patient_id'),
                'desc' => $this->input->post('description'),
                'chk_normal' => 1,
                'section' => $this->input->post('section'),
                'date_test' => $this->input->post('date_appointment'),
                'ddl_id' => $this->input->post('item')
            );
            
            if($this->input->post('record_id')){
                $action = $this->core_model->update_item("share_field_section",$fields,array("id"=>$this->input->post('record_id')));
            }else{
                $action = $this->core_model->insert_item("share_field_section",$fields);
            }

            if($action){
                echo json_encode(array("status"=>"success","message"=>lang("actions_success")));
            }else{
                echo json_encode(array("status"=>"error","message"=>lang("actions_error")));
            }

        }else{
            echo json_encode(array("status"=>"error","message"=>validation_errors()));
        }
    }

    public function add_sickl_records(){

        /* Validate form input */
        $this->form_validation->set_rules('diagnosis', 'lang:patient_diagnosis_tab', 'required');
        $this->form_validation->set_rules('leave_days', 'lang:leave_days', 'required');
        $this->form_validation->set_rules('destination', 'lang:destination', 'required');
        $this->form_validation->set_rules('patient_id', 'unknown patient id', 'required');

        if ($this->form_validation->run()){

            $fields = array(
                'patient_id' => $this->input->post('patient_id'),
                'date_add' => $this->input->post('date_appointment'),
                'diagnosis' => $this->input->post('diagnosis'),
                'leave_days' => $this->input->post('leave_days'),
                'destination' => $this->input->post('destination'),
                'extra_notes' => $this->input->post('extra_notes')
            );
            
            if($this->input->post('record_id')){
                $action = $this->core_model->update_item("patients_sick_leave",$fields,array("id"=>$this->input->post('record_id')));
            }else{
                $action = $this->core_model->insert_item("patients_sick_leave",$fields);
            }

            if($action){
                echo json_encode(array("status"=>"success","message"=>lang("actions_success")));
            }else{
                echo json_encode(array("status"=>"error","message"=>lang("actions_error")));
            }

        }else{
            echo json_encode(array("status"=>"error","message"=>validation_errors()));
        }
    }

}
