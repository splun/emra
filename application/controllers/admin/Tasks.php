<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('admin/users');


        /* Title Page :: Common */
	    $this->page_title->push(lang('tasks_title'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('tasks_title'), 'admin/tasks');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index(){
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();


            $this->data['tasks'] = $this->TasksModel->get_tasks($this->ion_auth->user()->row()->id,false);

            /* Load Template */
            $this->template->admin_render('admin/users/tasks', $this->data);
        }
    }

    public function complete($task_id){
    	
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	if(!empty($task_id)){

	            if ($this->core_model->update_item("task_users",array('task_status'=> 1),array("task_id" => $task_id,"user_id" => $this->ion_auth->user()->row()->id)))
	            {   
	                $this->session->set_flashdata('message', lang("actions_success"));
	  
	            }else{
	                $this->session->set_flashdata('message', "Unknown task id");
	            }

	            redirect("/admin/tasks/", 'refresh');  
	        }

        }
    }

    public function start($task_id){
    	
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	if(!empty($task_id)){

				if ($this->core_model->update_item("tasks",array('rdo_Status'=> 2),array("id" => $task_id)))
	            {   
	                $this->session->set_flashdata('message', lang("actions_success"));
	  
	            }else{
	                $this->session->set_flashdata('message', "Unknown task id");
	            }

	            redirect("/admin/tasks/", 'refresh');  
	        }

        }
    }
}