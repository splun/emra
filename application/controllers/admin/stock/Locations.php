<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/stock');

        $this->lang->load('admin/stock');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('locations_pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_settings_locations'), 'admin/stock/locations');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();


            $this->data['items'] = $this->stock->get_items("stock_locations");

            $this->data['func'] = $this;
            /* Load Template */
            $this->template->admin_render('admin/stock/locations/list', $this->data);
        }
    }



	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('locations_pagetitle'), 'admin/stock/locations/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->stock->get_items("stock_locations",array('id' => $id));

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
            		'notes' => $this->input->post('notes')
				);

                if($this->stock->update_item("stock_locations",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/stock/locations/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);

		$this->data['notes'] = array(
			'name'  => 'notes',
			'id'    => 'notes',
			'type'  => 'textarea',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes', $item->notes)
		);

		
        /* Load Template */
		$this->template->admin_render('admin/stock/locations/edit', $this->data);
	}

	public function add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('locations_pagetitle'), 'admin/stock/locations/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'notes' => $this->input->post('notes')
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->stock->insert_item("stock_locations",$fields))
		{
            $this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/stock/locations/', 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['notes'] = array(
			'name'  => 'notes',
			'id'    => 'notes',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes')
		);

			
            /* Load Template */
            $this->template->admin_render('admin/stock/locations/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		$active_stock_item = $this->stock->get_items("stock_items",array("location_id"=>$id));

		if(!empty($active_stock_item)){
			$this->session->set_flashdata('message', lang("actions_cannot_delete_using_item"));
			redirect('admin/stock/locations/', 'refresh');
		}else{
			if ($this->stock->delete_item("stock_locations",$id))
			{
            	$this->session->set_flashdata('message', lang("actions_success"));
				redirect('admin/stock/locations/', 'refresh');
			}
		}

	}

	function get_active_stock_items($locations_id)
    {

       $items =  $this->stock->get_items("stock_items",array("location_id"=>$locations_id));

       if(!empty($items)){
       		return count($items);
       }else{
       		return 0;
       }

    }
}
?>