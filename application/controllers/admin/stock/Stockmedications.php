<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StockMedications extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/stock');

        $this->lang->load('admin/stock');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('stockmedications_pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('stockmedications_pagetitle'), 'admin/stock/stockmedications');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        /* Breadcrumbs */
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $this->data['items'] = $this->stock->get_stock_medications();
        $this->data['func'] = $this;
        /* Load Template */
        $this->template->admin_render('admin/stock/medications/list', $this->data);
        }
    }
	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('stockmedications_pagetitle_edit'), 'admin/stock/stockmedications/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->stock->get_stock_medications($id);

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');

		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
					'category' => $this->input->post('category'),
					'sc' => $this->input->post('sc'),
	            	'sl' => $this->input->post('sl')
				);

                if($this->stock->update_item("stock_medications",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/stock/stockmedications/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		$this->data['categories'] = $this->stock->get_stock_medications_categories();

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);


        /* Load Template */
		$this->template->admin_render('admin/stock/medications/edit', $this->data);
	}

	public function add()
	{
		
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('stockmedications_pagetitle_add'), 'admin/stock/stockmedications/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('category', 'lang:category_label', 'required');
		
		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name'),
	            'category' => $this->input->post('category'),
	            'sc' => ($this->input->post('sc')) ? $this->input->post('sc') : 0,
	            'sl' => ($this->input->post('sl')) ? $this->input->post('sl') : 0
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->stock->insert_item("stock_medications", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_item_create"));
			redirect('admin/stock/stockmedications/', 'refresh');
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

        $this->data['categories'] = $this->stock->get_stock_medications_categories();

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

            /* Load Template */
            $this->template->admin_render('admin/stock/medications/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		$active_stock_item = $this->stock->get_items("stock_items",array("medication_id"=>$id));

		if(!empty($active_stock_item)){

			$this->session->set_flashdata('message', lang("actions_cannot_delete_using_item"));
			redirect('admin/stock/stockmedications/', 'refresh');

		}else{
			if ($this->stock->delete_item("stock_medications",$id))
			{
	        	$this->session->set_flashdata('message', lang("actions_item_delete"));
				redirect('admin/stock/stockmedications/', 'refresh');
			}
		}

	}


	public function category_add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('actions_cat_create_edit'), 'admin/stock/stockmedications/category_add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
	            'name' => $this->input->post('name')
	        );
		}

		if ($this->form_validation->run() == TRUE && $this->stock->insert_item("stock_categories", $fields))
		{
            $this->session->set_flashdata('message', lang("actions_category_create"));
			redirect('admin/stock/stockmedications/category_add', 'refresh');
		}
		else
		{

        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name')
		);

		$this->data['categories'] = $this->stock->get_stock_medications_categories();
        /* Load Template */
        $this->template->admin_render('admin/stock/medications/category_create', $this->data);
        }
	}

	public function category_edit($id)
	{
        $id = (int) $id;


		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');

		if (isset($_POST) && ! empty($_POST))
		{

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name')
				);

                if($this->stock->update_item("stock_categories",$fields,$id))
			    {
                    $this->session->set_flashdata('message', lang("actions_category_update"));
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		redirect('admin/stock/stockmedications/category_add/', 'refresh');



	}

	public function category_delete($category_id)
	{
		$category_id = (int) $category_id;


		if ($this->stock->delete_item("stock_categories",$category_id))
		{
        	$this->session->set_flashdata('message', lang("actions_category_delete"));
			redirect('admin/stock/stockmedications/category_add', 'refresh');
		}

	}

	function get_active_stock_items($medication_id)
    {

       $items =  $this->stock->get_items("stock_items",array("medication_id"=>$medication_id));

       if(!empty($items)){
       		return count($items);
       }else{
       		return 0;
       }

    }


}
?>