<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instock extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/stock');

        $this->lang->load('admin/stock');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('instock_pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('instock_pagetitle'), 'admin/stock/instock');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
        	/* Breadcrumbs */

            
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Filter Data */
			$this->data['name'] = array(
				'name'  => 'name',
				'id'    => 'name',
				'type'  => 'text',
				'data-index'  => '1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('name',$this->input->post('name'))
			);
			$this->data['qty'] = array(
				'name'  => 'qty',
				'id'    => 'qty',
				'type'  => 'number',
				'step'  => '0.1',
				'data-index'  => '5',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('qty',$this->input->post('qty'))
			);

			$this->data['purchase_date_from'] = array(
				'name'  => 'purchase_date_from',
				'id'    => 'purchase_date_from',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('purchase_date_from', ($this->input->post('purchase_date_from')) ? $this->input->post('purchase_date_from') : date("d.m.Y"))
			);
			$this->data['purchase_date_to'] = array(
				'name'  => 'purchase_date_to',
				'id'    => 'purchase_date_to',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('purchase_date_to', ($this->input->post('purchase_date_to')) ? $this->input->post('purchase_date_to') : date("d.m.Y"))
			);
			$this->data['price'] = array(
				'name'  => 'price',
				'id'    => 'price',
				'type'  => 'number',
				'data-index'  => '7',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('price',$this->input->post('price'))
			);
			$this->data['expire_day_from'] = array(
				'name'  => 'expire_day_from',
				'id'    => 'expire_day_from',
				'type'  => 'date',
				'data-index'  => '8',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('expire_day_from',$this->input->post('expire_day_from'))
			);
			
			$this->data['expire_day_to'] = array(
				'name'  => 'expire_day_to',
				'id'    => 'expire_day_to',
				'type'  => 'date',
				'data-index'  => '8',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('expire_day_to',$this->input->post('expire_day_to'))
			);


            $this->data['categories'] = $this->stock->get_items("stock_categories");
			$this->data['medications'] = $this->stock->get_items("stock_medications");
			$this->data['locations'] = $this->stock->get_items("stock_locations");

			$this->data['filter'] = ($this->input->post("filter")) ? $this->input->post() : $this->input->get();

			$filter = "";

            /* Get All Items */
		    
            /***Not Ajax***/

		    if($this->input->get("filter")){

				$filter = array();


				if(!empty($this->input->get('category_id'))){
					$filter += array('stock_items.category_id' => $this->input->get('category_id'));
				}
				if(!empty($this->input->get('medication_id'))){
					$filter += array('stock_items.medication_id' => $this->input->get('medication_id'));
				}
				
				if(!empty($this->input->get('location_id'))){
					$filter += array('stock_items.location_id' => $this->input->get('location_id'));
				}
				

				if(empty($this->input->get('outofstock'))){
					$filter += array('stock_items.qty >' => 0);
				}else{
					$curdate = date("Y-m-d");
					$or_where = "stock_items.qty <= 0 OR stock_items.expire_day < $curdate";
				}

			}

    		if(!empty($filter)){
    			$this->data['items'] = $this->stock->get_stock_items($filter);
    		}else{

            	$this->data['items'] = $this->stock->get_stock_items(array("stock_items.qty >"=>0,"expire_day >"=>date("Y-m-d")));
            }

            if(!empty($this->input->get('outofstock'))){
            	$this->data['curent_out_of_stock'] = 1;
            }else{
            	$this->data['curent_out_of_stock'] = 0;
            }

            /* Load Template */
            $this->data['func'] = $this;
            $this->template->admin_render('admin/stock/instock/list', $this->data);
        }
    }


    public function filter(){

    	$filter = 0;
    	$or_where = 0;

	    if($this->input->post()){

			$filter = array();

			if(!empty($this->input->post('name'))){
				$filter += array('stock_items.name LIKE' => '%'.$this->input->post('name').'%');
			}

			if(!empty($this->input->post('category_id'))){
				$filter += array('stock_items.category_id' => $this->input->post('category_id'));
			}
			if(!empty($this->input->post('medication_id'))){
				$filter += array('stock_items.medication_id' => $this->input->post('medication_id'));
			}
			if(!empty($this->input->post('qty'))){
				$filter += array('stock_items.qty' => $this->input->post('qty'));
			}
			if(!empty($this->input->post('price'))){
				$filter += array('stock_items.price' => $this->input->post('price'));
			}
			if(!empty($this->input->post('location_id'))){
				$filter += array('stock_items.location_id' => $this->input->post('location_id'));
			}
			if(!empty($this->input->post('expire_day_from'))){
				$filter += array(
					'stock_items.expire_day >=' => $this->input->post('expire_day_from')
				);
			}
			if(!empty($this->input->post('expire_day_to'))){
				$filter += array(
					'stock_items.expire_day <=' => $this->input->post('expire_day_to')
				);
			}
			if(!empty($this->input->post('purchase_date_from'))){
				$filter += array(
					'stock_items.purchase_date >=' => $this->input->post('purchase_date_from')
				);
			}
			if(!empty($this->input->post('purchase_date_to'))){
				$filter += array(
					'stock_items.purchase_date <=' => $this->input->post('purchase_date_to')
				);
			}

			if(!empty($this->input->post('sc')) && !empty($this->input->post('sl'))){

				$filter += array('stock_items.type !=' => "Other");

			}else{

				if(!empty($this->input->post('sc'))){
					$filter += array('stock_items.type' => $this->input->post('sc'));
				}

				if(!empty($this->input->post('sl'))){
					$filter += array('stock_items.type' => $this->input->post('sl'));
				}
			}

			if(empty($this->input->post('outofstock'))){
				$filter += array('stock_items.qty >' => 0);
			}else{
				$curdate = date("Y-m-d");
				$or_where = "stock_items.qty <= 0 OR stock_items.expire_day < '{$curdate}'";
			}

		}


	    /* Get All Items */
	    $items = $this->stock->get_stock_items($filter,$or_where);


	    /* Ajax Response */
	    echo json_encode($items);

    }

	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('instock_pagetitle_edit'), 'admin/stock/instock/edit/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
		$item = $this->stock->get_items("stock_items",array('id' => $id));

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('qty', 'lang:qty_label', 'required');
		$this->form_validation->set_rules('price', 'lang:price_label', 'required');
		$this->form_validation->set_rules('startQty', 'lang:start_qty_label', 'required');
		
		if (isset($_POST) && ! empty($_POST))
		{
            if ($id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			if ($this->form_validation->run() == TRUE)
			{
				$fields = array(
					'name' => $this->input->post('name'),
            		'category_id' => $this->input->post('category_id'),
            		'type' => $this->input->post('type'),
            		'medication_id' => $this->input->post('medication_id'),
            		'qty' => $this->input->post('qty'),
            		'purchase_date' => $this->input->post('purchase_date'),
            		'price' => $this->input->post('price'),
            		'currency' => $this->input->post('currency'),
            		'expire_day' => $this->input->post('expire_day'),
            		'location_id' => $this->input->post('location_id'),
            		'startQty' => $this->input->post('startQty'),
            		'unit' => $this->input->post('unit'),
            		'info' => $this->input->post('info')
				);

				if($this->input->post('category_id') == 1){
	        		$fields += array('vile'=>$this->input->post('vile'));
	        	}

                if($this->stock->update_item("stock_items",$fields,$id))
			    {	
			    	if($this->input->post('category_id') == 3){
						$invoice_fields = array(
							'name'=>$this->input->post('name'),
							//'price' => $this->input->post('price'),
							'flag_status' => 1,
							'selling_item_id'=>$id,
							'category' => 1
						);

						$invoice_item = $this->stock->get_items("invoice",array("selling_item_id"=>$id));

						if(isset($invoice_item[0])){
							$this->stock->update_item("invoice",$invoice_fields,$invoice_item[0]->id);
						}
					}

                    $this->session->set_flashdata('message', lang("actions_item_update"));

					redirect('admin/stock/instock/', 'refresh');
			    }
			    else
			    {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
			    }
			}

		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : "");

		// pass the user to the view
		$this->data['item']  = $item;
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name', $item->name)
		);
		$this->data['qty'] = array(
			'name'  => 'qty',
			'id'    => 'qty',
			'type'  => 'number',
			'step'  => '0.1',
			'min' => '0',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('qty', $item->qty)
		);
		$this->data['purchase_date'] = array(
			'name'  => 'purchase_date',
			'id'    => 'purchase_date',
			'type'  => 'date',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('purchase_date', $item->purchase_date)
		);
		$this->data['price'] = array(
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('price', $item->price)
		);
		$this->data['expire_day'] = array(
			'name'  => 'expire_day',
			'id'    => 'expire_day',
			'type'  => 'date',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('expire_day', $item->expire_day)
		);
		$this->data['startQty'] = array(
			'name'  => 'startQty',
			'id'    => 'startQty',
			'type'  => 'number',
			'step'  => '0.1',
			'min' => '0',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('startQty', $item->startQty)
		);
		$this->data['info'] = array(
			'name'  => 'info',
			'id'    => 'info',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('info', $item->info)
		);

		$this->data['categories'] = $this->stock->get_items("stock_categories");
		$this->data['medications'] = $this->stock->get_items("stock_medications");
		$this->data['locations'] = $this->stock->get_items("stock_locations");
		$this->data['units'] = $this->stock->get_items("units");
		$this->data['currencies'] = $this->stock->get_items("currencies");

        /* Load Template */
		$this->template->admin_render('admin/stock/instock/edit', $this->data);
	}

	public function add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('instock_pagetitle_add'), 'admin/stock/instock/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('name', 'lang:name_label', 'required');
		$this->form_validation->set_rules('price', 'lang:price_label', 'required');
		$this->form_validation->set_rules('qty', 'lang:qty_label', 'required');
		$this->form_validation->set_rules('expire_day', 'lang:expire_date_label', 'required');

		if ($this->form_validation->run()){
	        $fields = array(
					'name' => $this->input->post('name'),
            		'category_id' => $this->input->post('category_id'),
            		'type' => $this->input->post('type'),
            		'medication_id' => $this->input->post('medication_id'),
            		'qty' => $this->input->post('qty'),
            		'purchase_date' => $this->input->post('purchase_date'),
            		'price' => $this->input->post('price'),
            		'currency' => $this->input->post('currency'),
            		'expire_day' => $this->input->post('expire_day'),
            		'location_id' => $this->input->post('location_id'),
            		'startQty' => $this->input->post('qty'),
            		'unit' => $this->input->post('unit'),
            		'info' => $this->input->post('info')
			);

	        if($this->input->post('category_id') == 1){
	        	$fields += array('vile'=>$this->input->post('vile'));
	        }

	        $insert_id = $this->stock->insert_item("stock_items",$fields);
	        if($insert_id){

				if($this->input->post('category_id') == 3){
					$invoice_fields = array(
						'name'=>$this->input->post('name'),
						'price' => $this->input->post('price'),
						'flag_status' => 1,
						'selling_item_id'=>$insert_id,
						'category' => 1
					);
					$this->stock->insert_item("invoice",$invoice_fields);
				}

            	$this->session->set_flashdata('message', lang("actions_success"));
				redirect('admin/stock/instock/', 'refresh');
			}
		}
		else
		{
         $this->data['message'] = (validation_errors()  ? validation_errors() : "");

		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('name',$this->input->post('name'))
		);
		$this->data['qty'] = array(
			'name'  => 'qty',
			'id'    => 'qty',
			'type'  => 'number',
			'step'  => '0.1',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('qty',$this->input->post('qty'))
		);
		$this->data['purchase_date'] = array(
			'name'  => 'purchase_date',
			'id'    => 'purchase_date',
			'type'  => 'date',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('purchase_date', ($this->input->post('purchase_date')) ? $this->input->post('purchase_date') : date("d.m.Y"))
		);
		$this->data['price'] = array(
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('price',$this->input->post('price'))
		);
		$this->data['expire_day'] = array(
			'name'  => 'expire_day',
			'id'    => 'expire_day',
			'type'  => 'date',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('expire_day',$this->input->post('expire_day'))
		);

		$this->data['info'] = array(
			'name'  => 'info',
			'id'    => 'info',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('info',$this->input->post('info'))
		);

		$this->data['categories'] = $this->stock->get_items("stock_categories");
		$this->data['medications'] = $this->stock->get_items("stock_medications");
		$this->data['locations'] = $this->stock->get_items("stock_locations");
		$this->data['units'] = $this->stock->get_items("units");
		$this->data['currencies'] = $this->stock->get_items("currencies");
			
            /* Load Template */
            $this->template->admin_render('admin/stock/instock/create', $this->data);
        }
	}

	public function delete($id)
	{
		$id = (int) $id;

		if ($this->stock->delete_item("stock_items",$id))
		{
			$invoice_item = $this->stock->get_items("invoice",array("selling_item_id"=>$id));


			if(isset($invoice_item[0])){
				$this->stock->delete_item("invoice",$invoice_item[0]->id);
			}

        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/stock/instock/', 'refresh');
		}

	}

	public function activate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'showDiscount' => 1
	    );

		if ($this->stock->update_item("stock_items",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/stock/instock/', 'refresh');
		}

	}

	public function deactivate($id)
	{
		$id = (int) $id;

	    $fields = array(
	        'showDiscount' => 0
	    );

		if ($this->stock->update_item("stock_items",$fields,$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/stock/instock/', 'refresh');
		}

	}

	public function medication_by_cat(){

		$category_id = $this->input->post('category_id');

		$medications = $this->stock->get_items("stock_medications",array("category"=>$category_id));


		if(!$medications) return false;

		$output = '<select name="medication_id" class="form-control">';
		    foreach ($medications as $medication) {
		        $output .= '<option value="'.$medication->id.'">'.$medication->name.'</option>';
		    }
		$output .= '</select>';

		echo $output;

	}
}
?>