<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/patientsModel');
        $this->load->model('admin/settings');

        $this->lang->load('admin/patients');
        $this->lang->load('admin/reports');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    //$this->page_title->push(lang('pagetitle_reports'));
	    //$this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('pagetitle_reports'), 'admin/reports/advanced_user_search');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function advanced_user_search()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        $this->data['patient_name'] = array(
			'name'  => 'patient_name',
			'id'    => 'patient_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name',$this->input->post('patient_name'))
		);

		$this->data['parents_name'] = array(
			'name'  => 'parents_name',
			'id'    => 'parents_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('parents_name',$this->input->post('parents_name'))
		);
        
		$this->data['civil_id'] = array(
			'name'  => 'civil_id',
			'id'    => 'civil_id',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('civil_id',$this->input->post('civil_id'))
		);

		$this->data['file_no'] = array(
			'name'  => 'file_no',
			'id'    => 'file_no',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file_no',$this->input->post('file_no'))
		);
		$this->data['telephone'] = array(
			'name'  => 'telephone',
			'id'    => 'telephone',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('telephone',$this->input->post('telephone'))
		);

		$this->data['age_from'] = array(
			'name'  => 'age_from',
			'id'    => 'age_from',
			'type'  => 'number',
			'min' 	=> 0,
			'max' 	=>18,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('age_from',$this->input->post('age_from'))
		);

		$this->data['age_to'] = array(
			'name'  => 'age_to',
			'id'    => 'age_to',
			'type'  => 'number',
			'min' 	=> 0,
			'max' 	=>18,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('age_to',$this->input->post('age_to'))
		);

		$this->data['month_from'] = array(
			'name'  => 'month_from',
			'id'    => 'month_from',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>36,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('month_from',$this->input->post('month_from'))
		);

		$this->data['month_to'] = array(
			'name'  => 'month_to',
			'id'    => 'month_to',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>36,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('month_to',$this->input->post('month_to'))
		);


		$this->data['skin_testing_from'] = array(
			'name'  => 'skin_testing_from',
			'id'    => 'skin_testing_from',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>40,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('skin_testing_from',$this->input->post('skin_testing_from'))
		);

		$this->data['skin_testing_to'] = array(
			'name'  => 'skin_testing_to',
			'id'    => 'skin_testing_to',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>40,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('skin_testing_to',$this->input->post('skin_testing_to'))
		);

		$this->data['spirometr_from'] = array(
			'name'  => 'spirometr_from',
			'id'    => 'spirometr_from',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>20,
			'step' => 0.1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('spirometr_from',$this->input->post('spirometr_from'))
		);

		$this->data['spirometr_to'] = array(
			'name'  => 'spirometr_to',
			'id'    => 'spirometr_to',
			'type'  => 'number',
			'min' 	=> 1,
			'max' 	=>20,
			'step' => 0.1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('spirometr_to',$this->input->post('spirometr_to'))
		);

		$this->data['weight_from'] = array(
			'name'  => 'weight_from',
			'id'    => 'weight_from',
			'type'  => 'number',
			'min' 	=> 1,
			'step' => 0.1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('weight_from',$this->input->post('weight_from'))
		);

		$this->data['weight_to'] = array(
			'name'  => 'weight_to',
			'id'    => 'weight_to',
			'type'  => 'number',
			'min' 	=> 1,
			'step' => 0.1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('weight_to',$this->input->post('weight_to'))
		);

		$this->data['height_from'] = array(
			'name'  => 'height_from',
			'id'    => 'height_from',
			'type'  => 'number',
			'min' 	=> 1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('height_from',$this->input->post('height_from'))
		);

		$this->data['height_to'] = array(
			'name'  => 'height_to',
			'id'    => 'height_to',
			'type'  => 'number',
			'min' 	=> 1,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('height_to',$this->input->post('height_to'))
		);

		$this->data['referral'] = array(
			'name'  => 'referral',
			'id'    => 'referral',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('referral',$this->input->post('referral'))
		);
		$this->data['immuno'] = array(
			'name'  => 'immuno',
			'id'    => 'immuno',
			'type'  => 'checkbox',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('immuno')
		);

		$this->data['doctors'] = $this->ion_auth->users('doctors')->result();
		$this->data['insurances'] = $this->patientsModel->get_items("insurance",array("status"=>1));

		$this->data['complaints'] = $this->settings->get_patient_file(1);
		$this->data['complaints_other'] = $this->settings->get_patient_file_ddl(1);
		$this->data['complaints'] = $this->settings->get_patient_file(1);
		$this->data['medications'] = $this->settings->get_patient_file(100);
		$this->data['asthma'] = $this->settings->get_patient_file(5);
		$this->data['family_history'] = $this->settings->get_patient_file(4);
		$this->data['environments'] = $this->settings->get_patient_file(3);
		
		$this->data['birth_vaccination'] = $this->settings->get_patient_file(11);
		$this->data['diets'] = $this->settings->get_patient_file(12);
		$this->data['developments'] = $this->settings->get_patient_file(10);
		$this->data['immunity'] = $this->settings->get_patient_file(9);
		$this->data['diagnosis'] = $this->settings->get_patient_file(2);
		$this->data['diagnosis_other'] = $this->settings->get_patient_file_ddl(2);
		$this->data['eczema'] = $this->settings->get_patient_file(6);
		$this->data['rhino_conjunctivitis'] = $this->settings->get_patient_file(7);
		$this->data['food_allergy'] = $this->settings->get_patient_file(29);
		$this->data['food_questions'] = $this->settings->get_patient_file_ddl(29);
       	
       	$this->data['immunoteraphy_items'] = $this->settings->get_items("immunotherapy_diagnosis");

		$this->data['skin_testing'] = $this->settings->get_patient_file(16);
		$this->data['spirometers'] = $this->settings->get_patient_file(18);
		$this->data['rhinoscopy'] = $this->settings->get_patient_file(17);
		$this->data['immunoCAP'] = $this->settings->get_patient_file(15);
		$this->data['physicals'] = $this->settings->get_patient_file(13);
		$this->data['investigations'] = $this->settings->get_patient_file(25);
		$this->data['prescriptions'] = $this->settings->get_patient_file(100);
		$this->data['drug_allergy'] = $this->settings->get_patient_file(30);
		$this->data['drug_questions'] = $this->settings->get_patient_file_ddl(30);

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('advanced_search'), 'admin/patients/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('advanced_search'));
	    $this->data['pagetitle'] = $this->page_title->show();
       

        /* Load Template */
        $this->template->admin_render('admin/reports/advanced_search', $this->data);
        }
    }

	public function filter(){

    	$filter = 0;
    	$join = array();

	    if($this->input->post()){

			$filter = array();

			if(!empty($this->input->post('patient_name'))){
				$filter += array('patients.patient_name LIKE' => '%'.$this->input->post('patient_name').'%');
			}

			if(!empty($this->input->post('parents_name'))){
				$filter += array('patients.parents_name LIKE' => '%'.$this->input->post('parents_name').'%');
			}

			if(!empty($this->input->post('civil_id'))){
				$filter += array('patients.civil_id' => $this->input->post('civil_id'));
			}
			if(!empty($this->input->post('file_no'))){
				$filter += array('patients.id' => $this->input->post('file_no'));
			}
			if(!empty($this->input->post('doctor'))){
				$filter += array('patients.doctor' => $this->input->post('doctor'));
			}
			if(!empty($this->input->post('telephone'))){
				$filter += array('patients.telephone' => $this->input->post('telephone'));
			}

			if(!empty($this->input->post('insurance'))){
				$filter += array('patients.ddl_insurance' => $this->input->post('insurance'));
			}

			if(!empty($this->input->post('referral'))){
				$filter += array('patients.hear LIKE' => '%'.$this->input->post('referral').'%');
			}

			if($this->input->post('immuno') && $this->input->post('immuno') == 1){
				$filter += array("immuno" => 1);
			}

			if(!empty($this->input->post('date_appointments'))){

				$date = explode("-",$this->input->post('date_appointments'));

				$start_date = date("Y-m-d",strtotime($date[0]));
                $end_date = date("Y-m-d",strtotime($date[1]));

				$join[] = array('table'=>'appointments','on'=>'appointments.event_name = patients.id');

				$filter += array("DATE(appointments.start_date) >="=>$start_date);
			}


			if(!empty($this->input->post('age_from'))){

				$start_age = $this->input->post('age_from');

				$filter += array("TIMESTAMPDIFF(YEAR,dob,CURDATE()) >="=>$start_age);

			}

			if(!empty($this->input->post('age_to'))){

				$end_age = $this->input->post('age_to');

				$filter += array("TIMESTAMPDIFF(YEAR,dob,CURDATE()) <="=>$end_age);

			}

			if(!empty($this->input->post('month_from'))){

				$start_month = $this->input->post('month_from');

				$filter += array("TIMESTAMPDIFF(MONTH,dob,CURDATE()) >="=>$start_age);

			}

			if(!empty($this->input->post('month_to'))){

				$end_month = $this->input->post('month_to');

				$filter += array("TIMESTAMPDIFF(MONTH,dob,CURDATE()) <="=>$end_month);

			}

			if(!empty($this->input->post('weight_from')) || !empty($this->input->post('weight_to')) || !empty($this->input->post('height_from')) || !empty($this->input->post('height_to'))){
				$join[] = array('table'=>'nurse','on'=>'nurse.patient_id = patients.id');
			}

			if(!empty($this->input->post('complaints')) || !empty($this->input->post('complaints_other')) || !empty($this->input->post('asthma')) || !empty($this->input->post('family_history')) || !empty($this->input->post('environment')) || !empty($this->input->post('birth_vaccination')) || !empty($this->input->post('diet')) || !empty($this->input->post('development')) || !empty($this->input->post('immunity')) || !empty($this->input->post('diagnosis')) || !empty($this->input->post('diagnosis_other')) || !empty($this->input->post('eczema')) || !empty($this->input->post('rhino_conjunctivitis')) || !empty($this->input->post('skin_testing')) || !empty($this->input->post('skin_testing_from')) || !empty($this->input->post('skin_testing_to')) || !empty($this->input->post('spirometers')) || !empty($this->input->post('spirometr_from')) || !empty($this->input->post('spirometr_to')) || !empty($this->input->post('rhinoscopy')) || !empty($this->input->post('immunoCAP')) || !empty($this->input->post('physical_exam'))){ 

				$join[] = array('table'=>'patient_checkboxpage','on'=>'patient_checkboxpage.patient_id = patients.id');
			}

			if(!empty($this->input->post('food_allergy')) || !empty($this->input->post('drug_allergy')) || !empty($this->input->post('food_questions')) || !empty($this->input->post('drug_questions'))){
				$join[] = array('table'=>'allergy','on'=>'allergy.patient_id = patients.id');
			}

			if(!empty($this->input->post('medications'))){
				$join[] = array('table'=>'treatments','on'=>'treatments.patient_id = patients.id');
			}

			if(!empty($this->input->post('immunoteraphy'))){
				$join[] = array('table'=>'immunotherapy_procedure_plan','on'=>'immunotherapy_procedure_plan.patient_id = patients.id');
			}

			if(!empty($this->input->post('investigations'))){
				$join[] = array('table'=>'investigation','on'=>'investigation.patient_id = patients.id');
			}

			if(!empty($this->input->post('prescriptions'))){
				$join[] = array('table'=>'prescription','on'=>'prescription.patient_id = patients.id');
			}

			if(!empty($this->input->post('weight_from'))){
				$filter += array('nurse.weight >=' => $this->input->post('weight_from'));
			}

			if(!empty($this->input->post('weight_to'))){
				$filter += array('nurse.weight <=' => $this->input->post('weight_to'));
			}

			if(!empty($this->input->post('height_from'))){
				$filter += array('nurse.height >=' => $this->input->post('height_from'));
			}

			if(!empty($this->input->post('height_to'))){
				$filter += array('nurse.height <=' => $this->input->post('height_to'));
			}

			if(!empty($this->input->post('complaints'))){
				$filter += array("patient_checkboxpage.section"=>1,"patient_checkboxpage.patient_file_id"=> $this->input->post('complaints'));
			}

			if(!empty($this->input->post('complaints_other'))){
				$filter += array("patient_checkboxpage.section"=>1,"patient_checkboxpage.ddl_id"=> $this->input->post('complaints_other'));
			}

			if(!empty($this->input->post('medications'))){

				$filter += array("treatments.patient_file_id"=> $this->input->post('medications'));
			}

			if(!empty($this->input->post('asthma'))){
				$filter += array("patient_checkboxpage.section"=>5,"patient_checkboxpage.patient_file_id"=> $this->input->post('asthma'));
			}

			if(!empty($this->input->post('family_history'))){
				$filter += array("patient_checkboxpage.section"=>4,"patient_checkboxpage.patient_file_id"=> $this->input->post('family_history'));
			}

			if(!empty($this->input->post('environment'))){
				$filter += array("patient_checkboxpage.section"=>3,"patient_checkboxpage.patient_file_id"=> $this->input->post('environment'));
			}

			if(!empty($this->input->post('birth_vaccination'))){
				$filter += array("patient_checkboxpage.section"=>11,"patient_checkboxpage.patient_file_id"=> $this->input->post('birth_vaccination'));
			}

			if(!empty($this->input->post('diet'))){
				$filter += array("patient_checkboxpage.section"=>12,"patient_checkboxpage.patient_file_id"=> $this->input->post('diet'));
			}

			if(!empty($this->input->post('development'))){
				$filter += array("patient_checkboxpage.section"=>10,"patient_checkboxpage.patient_file_id"=> $this->input->post('development'));
			}
			
			if(!empty($this->input->post('immunity'))){
				$filter += array("patient_checkboxpage.section"=>9,"patient_checkboxpage.patient_file_id"=> $this->input->post('immunity'));
			}

			if(!empty($this->input->post('diagnosis'))){
				$filter += array("patient_checkboxpage.section"=>2,"patient_checkboxpage.patient_file_id"=> $this->input->post('diagnosis'));
			}

			if(!empty($this->input->post('diagnosis_other'))){
				$filter += array("patient_checkboxpage.section"=>2,"patient_checkboxpage.ddl_id"=> $this->input->post('diagnosis_other'));
			}

			if(!empty($this->input->post('eczema'))){
				$filter += array("patient_checkboxpage.section"=>6,"patient_checkboxpage.patient_file_id"=> $this->input->post('eczema'));
			}

			if(!empty($this->input->post('rhino_conjunctivitis'))){
				$filter += array("patient_checkboxpage.section"=>7,"patient_checkboxpage.patient_file_id"=> $this->input->post('rhino_conjunctivitis'));
			}

			if(!empty($this->input->post('food_allergy'))){
				$filter += array("allergy.section"=>29,"allergy.patient_file_id"=> $this->input->post('food_allergy'));
			}
			if(!empty($this->input->post('food_questions'))){
				$filter += array("allergy.section"=>29,'allergy.ddl_id LIKE' => '%'.$this->input->post('food_questions').'%');
			}
			if(!empty($this->input->post('drug_allergy'))){
				$filter += array("allergy.section"=>30,"allergy.patient_file_id"=> $this->input->post('drug_allergy'));
			}

			if(!empty($this->input->post('drug_questions'))){
				$filter += array("allergy.section"=>30,'allergy.ddl_id LIKE' => '%'.$this->input->post('drug_questions').'%');
			}

			if(!empty($this->input->post('immunoteraphy'))){
				$filter += array("immunotherapy_procedure_plan.diagnosis"=> $this->input->post('immunoteraphy'));
			}

			if(!empty($this->input->post('skin_testing'))){
				$filter += array("patient_checkboxpage.section"=>16,"patient_checkboxpage.patient_file_id"=> $this->input->post('skin_testing'));
			}

			if(!empty($this->input->post('skin_testing_from'))){
				$filter += array("patient_checkboxpage.section"=>16,"patient_checkboxpage.ddl_id >="=> $this->input->post('skin_testing_from'));
			}

			if(!empty($this->input->post('skin_testing_to'))){
				$filter += array("patient_checkboxpage.section"=>16,"patient_checkboxpage.ddl_id <="=> $this->input->post('skin_testing_to'));
			}
			
			if(!empty($this->input->post('spirometers'))){
				$filter += array("patient_checkboxpage.section"=>18,"patient_checkboxpage.patient_file_id"=> $this->input->post('spirometers'));
			}

			if(!empty($this->input->post('spirometr_from'))){
				$filter += array("patient_checkboxpage.section"=>18,"patient_checkboxpage.ddl_id >="=> $this->input->post('spirometr_from'));
			}

			if(!empty($this->input->post('spirometr_to'))){
				$filter += array("patient_checkboxpage.section"=>18,"patient_checkboxpage.ddl_id <="=> $this->input->post('spirometr_to'));
			}


			if(!empty($this->input->post('rhinoscopy'))){
				$filter += array("patient_checkboxpage.section"=>17,"patient_checkboxpage.patient_file_id"=> $this->input->post('rhinoscopy'));
			}

			if(!empty($this->input->post('immunoCAP'))){
				$filter += array("patient_checkboxpage.section"=>15,"patient_checkboxpage.patient_file_id"=> $this->input->post('immunoCAP'));
			}

			if(!empty($this->input->post('physical_exam'))){
				$filter += array("patient_checkboxpage.section"=>13,"patient_checkboxpage.patient_file_id"=> $this->input->post('physical_exam'));
			}

			if(!empty($this->input->post('investigations'))){
				$filter += array("investigation.patient_file_id"=> $this->input->post('investigations'));
			}

			if(!empty($this->input->post('prescriptions'))){
				$filter += array("prescription.patient_file_id"=> $this->input->post('prescriptions'));
			}
			

		}

	    /* Get All Items */
		$items = $this->data['items'] = $this->patientsModel->advanced_search_patients($filter,$join);

	    /* Ajax Response */   
	    $output = ""; 		
	    foreach ($items as $item):

	        $output .= "<tr>
	            <td>".$item->id."</td>
	            <td>".htmlspecialchars($item->patient_name, ENT_QUOTES, 'UTF-8')."</td>
	            <td class='action-links'>
	            	<a href='/admin/patients/card/".$item->id."/#prescription-block' target='_blank'><img src='/assets/images/prescription_drugs.png' /></a>
	            	<a href='/admin/patients/card/".$item->id."/#diagnosis-block' target='_blank'><img src='/assets/images/diagnosie.jpg' /></a>
	                <a href='/admin/patients/card/".$item->id."/#history-block' target='_blank'><img  src='/assets/images/history-icon.gif' /></a>".anchor('admin/patients/card/'.$item->id, lang('view_patient_card'), array('target'=>'_blank'))."
	            </td>
	        </tr>";

       	endforeach;

	    echo $output;

    }


    public function invoice_report($tooday=0){
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        	if($this->ion_auth->in_group("Nurses")){
        		redirect('admin/dashboard', 'refresh');
        	}
        	if($this->ion_auth->in_group("Reception") && ($this->input->get('debt') != 1 || $this->input->post('debt'))){
        		redirect('admin/dashboard', 'refresh');
        	}

	        $this->data['patient_name'] = array(
				'name'  => 'patient_name',
				'id'    => 'patient_name',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('patient_name',$this->input->post('patient_name'))
			);

			$this->data['civil_id'] = array(
				'name'  => 'civil_id',
				'id'    => 'civil_id',
				'type'  => 'number',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('civil_id',$this->input->post('civil_id'))
			);

			$this->data['file_no'] = array(
				'name'  => 'file_no',
				'id'    => 'file_no',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('file_no',$this->input->post('file_no'))
			);
			$this->data['telephone'] = array(
				'name'  => 'telephone',
				'id'    => 'telephone',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('telephone',$this->input->post('telephone'))
			);
			$this->data['doctors'] = $this->ion_auth->users('doctors')->result();

			$this->data['debt'] = array(
				'name'  => 'debt',
				'id'    => 'debt',
				'type'  => 'checkbox',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('debt')
			);


			if(!empty($this->input->post())){

				$filter = "";
				$debt = 0;
				$doctor = 0;

				if($this->input->post('debt')){
					$debt = 1;
					$filter .= "patient_pays.`debt` != 0 ";
				}else{
					$filter .= "patient_balance.`id` != 0 ";
				}

				if(trim($this->input->post('patient_name'))!=""){
					$filter .= "AND patients.`patient_name` LIKE '%".$this->input->post('patient_name')."%' ";
				}

				if(trim($this->input->post('civil_id'))!=""){
					$filter .= "AND patients.`civil_id` = ".$this->input->post('civil_id')." ";
				}

				if(trim($this->input->post('file_no'))!=""){
					$filter .= "AND patients.`id` = ".$this->input->post('file_no')." ";
				}
	
				if(trim($this->input->post('telephone'))!=""){
					$filter .= "AND (patients.`telephone` =".$this->input->post('telephone')." OR patients.`mobile` =".$this->input->post('telephone').") ";
				}

				if((int)$this->input->post('insurance') !== 0){
					$filter .= "AND patients.`ddl_insurance` = ".$this->input->post('insurance')." ";
				}

				if(!empty($this->input->post('date_appointments'))){

					$date = explode("-",$this->input->post('date_appointments'));

					$start_date = date("Y-m-d",strtotime($date[0]));
	                $end_date = date("Y-m-d",strtotime($date[1]));
					
	                if($debt != 0){
	                	$filter .= "AND (DATE(`patient_pays`.`pay_day`) BETWEEN '".$start_date."' AND '".$end_date."') ";
	            	}else{
						$filter .= "AND (DATE(`patient_balance`.`invoice_date`) BETWEEN '".$start_date."' AND '".$end_date."') ";
	            	}
					
					
				}else{
					// if(trim($this->input->post('doctor_id'))!=""){
					// 	$filter .= "AND patients.`doctor` = ".$this->input->post('doctor_id')." ";

					// }
					//$start_date = date("Y-m-d");
				}

				if(trim($this->input->post('doctor_id'))!=""){
						
					// if($start_date > "2020-11-26" &&  $end_date > "2020-11-26"){
					// 	$filter .= "AND (`patient_pays`.`pay_day` BETWEEN '".$start_date."' AND '".$end_date."') AND patient_pays.`invoice_doctor_id` = ".$this->input->post('doctor_id')." ";
					// }else{
					// 	$filter .= "AND patients.`doctor` = ".$this->input->post('doctor_id')." ";
					// }
					//$filter .= "AND (`appointments`.`doctor_id` = ".$this->input->post('doctor_id')." ";
					$doctor = $this->input->post('doctor_id');
				}

				

				if(! empty($this->input->post('invoice_items')))

					{

						$count = 0;
						$inv_items = $this->input->post('invoice_items');

						$filter .= " AND ( ";

						foreach($inv_items as $inv)

						{ 

							if($count>0)

							{

								$filter .=	" OR ";	

							}

							$filter .=	"  patient_balance.invoice_item_id = ".$inv;

							$count++;

						}

						$filter .= " ) ";	

					}
				
				$items = $this->patientsModel->get_invoices($debt,$filter);
				echo $this->invoice_report_output($debt,$items,$doctor);
				
			}else{
				$this->data['doctors'] = $this->ion_auth->users('doctors')->result();
				$this->data['insurances'] = $this->patientsModel->get_items("insurance",array("status"=>1));

		        /* Breadcrumbs */
		        $this->breadcrumbs->unshift(2, lang('invoice_reports'), 'admin/reports/invoice_reports/');
		        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		        $this->page_title->push(lang('invoice_reports'));
			    $this->data['pagetitle'] = $this->page_title->show();

			    $filter_doctor = 0;
			    $this->data["current_doctor"] = "";

			    if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group("Reception")){
			    	$filter_doctor = $this->ion_auth->user()->row()->id;
			    	$this->data["current_doctor"] = $this->ion_auth->user()->row()->id;
			    }

		        if($this->input->get('debt') && (int)$this->input->get('debt') == 1){

		        	$items = $this->patientsModel->get_invoices(1,"patient_pays.`debt` != 0".$filter_doctor);

		        	$this->data['list'] = $this->invoice_report_output(1,$items);
		        }else{

		        	$items = $this->patientsModel->get_invoices(0,"DATE(patient_balance.`invoice_date`) = '".date('Y-m-d')."'",$filter_doctor);

		        	$this->data['list'] = $this->invoice_report_output(0,$items);
		        }
		        

		        $this->data['invoice_items'] = $this->core_model->get_items("invoice",array("flag_status !="=>0),"name","ASC");
	
		        //var_dump($this->data['invoice_items']);
		        /* Load Template */
		        $this->template->admin_render('admin/reports/invoice_reports', $this->data);
		    }
	    }
    }

    public function invoice_report_output($debt,$items,$doctor=0){

    	$html = "";
    	$totalInsurance = 0;
        $totalQuoted = 0;
        $totalCP = 0;
        $totalCash = 0;
        $totalKnet = 0;
        $totalPay = 0;
        $totalDebt = 0;
        $cpay = 0;
		$total_deduct = 0;

    	if($items):


            if($debt !== 0){
				$deduct = 0;
				
	            $i=1; foreach($items as $item):

					if($doctor){
						
						if((int)$item->invoice_doctor_id != 0 ){

							if((int)$item->invoice_doctor_id != $doctor){
								continue;
							}
							
						}else{
							$check_doctor = $this->patientsModel->check_doctor_appointment($item->pid,$doctor,$item->pay_day);
							if(!$check_doctor && $item->doctor !== $doctor){
								continue;
							}
						}

					}

	                $totalItemsList = $this->patientsModel->get_patient_invoice($item->pid,$item->pay_day);

	                $total_with_ins = 0;
	                $totalItemsSumm = 0;
	                $customer_pay = 0;
					$deduct_amount = 0;

	                foreach($totalItemsList as $invoiceItems){

	                    $price = 0;
						
						$deduct = $invoiceItems->net_price;

	                    if($invoiceItems->discount_type != 1){
	                        //$price = ($invoiceItems->total-($invoiceItems->total*$invoiceItems->discount/100))*$invoiceItems->qty;
	                        $price = ($invoiceItems->total-($invoiceItems->total*$invoiceItems->discount/100));
	                    }else{
	                        //$price = $invoiceItems->total*$invoiceItems->qty - $invoiceItems->discount;
	                        $price = $invoiceItems->total - $invoiceItems->discount;
	                    }
	                    

	                    if($invoiceItems->ins_flag == 1 AND $invoiceItems->ins_discount != 0){
	                        $total_with_ins += $price*$invoiceItems->ins_discount/100;
	                    }else{
	                        $total_with_ins += $price;
	                        $customer_pay += $price;
	                    }

	                    $totalItemsSumm += $price;

	                }

	                if($item->ddl_insurance<>0) {
						$insPay = round($totalItemsSumm - $total_with_ins,2);
					}else{
						$insPay = 0;
					}

					$cpay = round($customer_pay,2);
					$deduct_amount = round($insPay + $cpay,2)-$deduct;
					$final_income = $deduct_amount-$item->debt;

					$html.= "<tr>";
					$html.= "<td>".$i."</td>";
					$html.= "<td>".$item->pay_day."</td>";
					$html.= "<td>".$item->pid."</td>";
					$html.= "<td>".$item->patient_name." - ".$item->telephone."</td>";
					$html.= "<td>".$totalItemsSumm."KD </td>";
					$html.= "<td>".round($insPay + $cpay,2)." KD</td>";
					$html.= "<td>".$deduct."KD </td>";
					$html.= "<td>".$deduct_amount." KD</td>";
					$html.= "<td>".$insPay."KD </td>";
					$html.= "<td>".$cpay." KD</td>";
					$html.= "<td>".$item->knet." KD</td>";
					$html.= "<td>".$item->cash." KD</td>";
					$html.= "<td>".$item->debt." KD</td>";
					$html.= "<td>".$final_income." KD</td>";
					$html.= "<td class='action-links'><a href='/admin/patientsections/invoice/".$item->pid."/".$item->pay_day."/#invoice-block' target='_blank'><img src='/assets/images/doctor-icon.png' alt=''></a></td>";
					$html .= "</tr>";

					$totalInsurance += $insPay;
					$totalCP += $cpay;
					$totalCash += $item->cash;
					$totalKnet += $item->knet;
					$totalQuoted += $totalItemsSumm;
					$totalDebt += $item->debt;
					$totalPay +=  $insPay + $cpay;
					$total_deduct += $deduct;
					$i++;

	            endforeach;
    			
    		}else{
    			$curId = 0;
				$curdate = "";

				$i=1;
				$insPay = 0;
				$cashPayment = 0;
				$knetPayment = 0;
				$debtPayment = 0;
				$deduct_amount = 0;

				$i=1; foreach($items as $item):
					
					if($doctor){
						
						if((int)$item->invoice_doctor_id != 0 ){

							if((int)$item->invoice_doctor_id != $doctor){
								continue;
							}
							
						}else{
							$check_doctor = $this->patientsModel->check_doctor_appointment($item->pid,$doctor,$item->invoice_date);
							if(!$check_doctor){
								continue;
							}
						}
					}
					
					$totalItemsList = $this->patientsModel->get_patient_invoice($item->pid,$item->invoice_date);

					$getCashKnetPayment = 
					$this->patientsModel->get_patient_pays($item->pid,$item->invoice_date);
					
					$total_with_ins = 0;
					$totalItemsSumm = 0;
					$customer_pay = 0;
					$deduct = 0;

					foreach($totalItemsList as $invoiceItems){

						$price = 0;
						
						$deduct += $invoiceItems->net_price;

	                    if($invoiceItems->discount_type != 1){
	                        //$price = ($invoiceItems->total-($invoiceItems->total*$invoiceItems->discount/100))*$invoiceItems->qty;
	                        $price = ($invoiceItems->total-($invoiceItems->total*$invoiceItems->discount/100));
	                    }else{
	                        //$price = $invoiceItems->total*$invoiceItems->qty - $invoiceItems->discount;
	                        $price = $invoiceItems->total - $invoiceItems->discount;
	                    }
	                    

	                    if($invoiceItems->ins_flag == 1 AND $invoiceItems->ins_discount != 0){
	                        $total_with_ins += $price*$invoiceItems->ins_discount/100;
	                    }else{
	                        $total_with_ins += $price;
	                        $customer_pay += $price;
	                    }

	                    $totalItemsSumm += $price;
						
	                }

	                if($item->ddl_insurance<>0) {
						$insPay = round($totalItemsSumm - $total_with_ins,2);
					}else{
						$insPay = 0;
					}

					if($getCashKnetPayment){
						$cashPayment = $getCashKnetPayment->cash;
						$knetPayment = $getCashKnetPayment->knet;
						$debtPayment = $getCashKnetPayment->debt;

						//$insPay = $totalItemsSumm - $cpay;
	
					}

					$cpay = round($customer_pay,2);
					$deduct_amount = round($insPay + $cpay,2)-$deduct;
					$final_income = $deduct_amount-$debtPayment;

					$html.= "<tr>";
					$html.= "<td>".$i."</td>";
					$html.= "<td>".$item->invoice_date."</td>";
					$html.= "<td>".$item->pid."</td>";
					$html.= "<td>".$item->patient_name." - ".$item->telephone."</td>";
					$html.= "<td>".$totalItemsSumm." KD</td>";
					$html.= "<td>".round($insPay + $cpay,2)." KD</td>";
					$html.= "<td>".$deduct."KD </td>";
					$html.= "<td>".$deduct_amount." KD</td>";
					$html.= "<td>".$insPay." KD</td>";
					$html.= "<td>".$cpay." KD</td>";
					$html.= "<td>".$knetPayment." KD</td>";
					$html.= "<td>".$cashPayment." KD</td>";
					$html.= "<td>".$debtPayment." KD</td>";
					$html.= "<td>".$final_income." KD</td>";
					$html.= "<td class='action-links'><a href='/admin/patientsections/invoice/".$item->pid."/".$item->invoice_date."/#invoice-block' target='_blank'><img src='/assets/images/doctor-icon.png' alt=''></a></td>";
					$html .= "</tr>";

					$totalQuoted +=  $totalItemsSumm;
					$totalInsurance += $insPay;
					$totalCP += $cpay;
					$totalCash += $cashPayment;
					$totalKnet += $knetPayment;
					$totalPay +=  $insPay + $cpay;
					$totalDebt += $debtPayment;

					$total_deduct += $deduct;

					$i++;
				endforeach;

    		}

    	endif;

		$total_net_amount = $totalPay - $total_deduct;
		$total_final_income = $total_net_amount-$totalDebt;
    	$html .= "<tfoot>";
			$html .= "<tr style='background:#595959; color:#fff;'>";
		    $html .= "<td colspan='4' style='vertical-align:middle;'>".lang('all_pay_info')."</td>";
			$html .= "<td>".lang('inv_quoted')."<br />".$totalQuoted." KD</td>";
			$html .= "<td>".lang('inv_total')."<br />".$totalPay." KD</td>";
			$html .= "<td>".lang('inv_total_deductable')."<br />".$total_deduct." KD</td>";
			$html .= "<td>".lang('inv_total_net')."<br />".$total_net_amount." KD</td>";
			$html .= "<td>".lang('inv_insurance_pay')."<br />".$totalInsurance." KD</td>";
		    $html .= "<td>".lang('inv_customer_pay')."<br />".$totalCP." KD</td>";
		    $html .= "<td>".lang('inv_knet')."<br />".$totalKnet." KD</td>";
		    $html .= "<td>".lang('inv_cash')."<br />".$totalCash." KD</td>";
		    $html .= "<td>".lang('inv_debt')."<br />".$totalDebt." KD</td>";
		    $html .= "<td>".lang('total_net_amount_with_debt')."<br />".$total_final_income." KD</td>";
			$html .= "<td></td>";
			$html .= "</tr>";
		    $html .= "</tfoot>";

    		return $html;
    }


    public function sales_report($tooday=0){
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        	if($this->ion_auth->in_group("Nurses")){
        		redirect('admin/dashboard', 'refresh');
        	}
        	if($this->ion_auth->in_group("Reception") && ($this->input->get('debt') != 1 || $this->input->post('debt'))){
        		redirect('admin/dashboard', 'refresh');
        	}

			$this->data['doctors'] = $this->ion_auth->users('doctors')->result();


			if(!empty($this->input->post())){

				$filter = "";
				$filtered_doctor = 0;

				if(!empty($this->input->post('date_appointments'))){

					$date = explode("-",$this->input->post('date_appointments'));

					$start_date = date("Y-m-d",strtotime($date[0]));
	                $end_date = date("Y-m-d",strtotime($date[1]));

					$filter .= "(DATE(`patient_balance`.`invoice_date`) BETWEEN '".$start_date."' AND '".$end_date."') ";

				}

				if(trim($this->input->post('doctor_id'))!=""){
					// if($start_date > "2020-11-26" &&  $end_date > "2020-11-26"){
					// 	$filter .= "AND (`patient_pays`.`pay_day` BETWEEN '".$start_date."' AND '".$end_date."') AND patient_pays.`invoice_doctor_id` = ".$this->input->post('doctor_id')." ";
					// }else{
					// 	$filter .= "AND patients.`doctor` = ".$this->input->post('doctor_id')." ";
					// }
					$filtered_doctor = $this->input->post('doctor_id');
				}


				if(! empty($this->input->post('invoice_items')))
				{

					$count = 0;
					$inv_items = $this->input->post('invoice_items');

					$filter .= " AND ( ";

					foreach($inv_items as $inv)

					{ 

						if($count>0)

						{

							$filter .=	" OR ";	

						}

						$filter .=	"  patient_balance.invoice_item_id = ".$inv;

						$count++;

					}

					$filter .= " ) ";	

					//$selected_items = 1;

				}

				
				$items = $this->patientsModel->get_sales_per_item($filter);
				echo $this->sales_report_output($items,$filtered_doctor);
				
			}else{
				$this->data['doctors'] = $this->ion_auth->users('doctors')->result();

		        /* Breadcrumbs */
		        $this->breadcrumbs->unshift(2, lang('invoice_reports'), 'admin/reports/sales_report/');
		        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		        $this->page_title->push(lang('sales_report'));
			    $this->data['pagetitle'] = $this->page_title->show();

			    $filter_doctor = 0;
			    $this->data["current_doctor"] = "";
			    if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group("Reception")){
			    	$filter_doctor = $this->ion_auth->user()->row()->id;
			    	$this->data["current_doctor"] = $this->ion_auth->user()->row()->id;
			    }


	        	$items = $this->patientsModel->get_sales_per_item("DATE(patient_balance.`invoice_date`) = '".date('Y-m-d')."'",$filter_doctor);
	        	$this->data['list'] = $this->sales_report_output($items);
		        

		        $this->data['invoice_items'] = $this->core_model->get_items("invoice",array("flag_status !="=>0),"name","ASC");
	
		        //var_dump($this->data['invoice_items']);
		        /* Load Template */
		        $this->template->admin_render('admin/reports/sales_repot', $this->data);
		    }
	    }
    }


    public function sales_report_output($items,$filtered_doctor = 0){

  	$html = "";
  	$final_total = 0;
	$total_ins = 0;
	$total_qty = 0;
	$total_deduct = 0;

	if($items):
		
		$curId = 0;
		$curdate = "";
		$i=1;
		$deduct_price = 0;

		foreach($items as $item):

			if($item);

			if($filtered_doctor){
				if((int)$item->invoice_doctor_id !== 0 && $item->invoice_doctor_id == $filtered_doctor){
					$invoice_doctor = $this->patientsModel->get_items("users",array("id"=>$item->invoice_doctor_id));
				}else{	
					$check_appointment = $this->patientsModel->check_doctor_appointment($item->pid,$filtered_doctor,$item->invoice_date);
					if($check_appointment && $check_appointment->doctor_id == $filtered_doctor){
						$invoice_doctor = $this->patientsModel->get_items("users",array("id"=>$filtered_doctor));
					}else{
						continue;
					}
				}

			}else{
				if((int)$item->invoice_doctor_id !== 0){
					$invoice_doctor = $this->patientsModel->get_items("users",array("id"=>$item->invoice_doctor_id));
				}else{
					$check_appointment = $this->patientsModel->check_doctor_appointment($item->pid,0,$item->invoice_date);
					if($check_appointment){
						$invoice_doctor = $this->patientsModel->get_items("users",array("id"=>$check_appointment->doctor_id));
					}else{
						continue;
					}
				}
			}

			$item_price=0;

			$insFlag = FALSE;
            $item_ins = 0;
			$total_net_price = 0;
            $insur_field = $item->ins_flag;

            if($item->qty>1) $qtyFlag=true;

            if($insur_field>0) $insFlag=true;

			$total = $item->total;
			$deduct_price = $item->net_price;

            $discount = $item->discount;
            $discount_item =  ($item->discount == 0) ? $item->discount_item : $item->discount;
			
			if($discount_item>0) {
				$discountFlag=true;
			}else{
				$discount_item = 0;
			}

            $discount_type = $item->discount_type;


            if($discount_item > 0 && (int)$discount_item !== 100){

            	if($discount_type !== "0"){
					$item_price = $total + $discount_item;
            	}else{
            		$item_price = $total + ($total * $discount_item / (100-$discount_item));
            	}
            	
            }else{
            	$item_price = $total;
            }

            if(isset($qtyFlag)){
            	$item_price = $item_price / $item->qty;
            }

            $insDis=$item->ins_discount;

			$html.= "<tr>";
			$html.= "<td>".$i."</td>";
			$html.= "<td>".$item->invoice_date."</td>";
			$html.= "<td>".$item->patient_name." - ".$item->telephone."; ".lang('patient_file_no_label').$item->pid."</td>";
			if($invoice_doctor){
				$html.= "<td>Dr. ".$invoice_doctor->first_name." ".$invoice_doctor->last_name."</td>";
			}else{
				$html.= "<td>Dr. ".$item->main_doctor_first_name." ".$item->main_doctor_last_name."</td>";
			}
			$html.= "<td>".$item->item_name."</td>";
			$html.= "<td>".$item_price." KD</td>";
			$html.= "<td>".$item->qty."</td>";

			if((int)$discount_type !== 0){
				$html.= "<td>".$discount_item." KD</td>";
			}else{
				$html.= "<td>".$discount_item." %</td>";
			}

			if($insFlag == true AND $insDis > 0){ 
				$item_total_with_ins = $item->total-$item->total*($insDis/100);
				$item_ins = $item->total*($insDis/100);
				$insPAy = $item->total - $item_ins;
				$total_net_price = $item_total_with_ins-$item->net_price;

				$html.= "<td>".$insPAy." KD</td>";
				$html.= "<td>".$item_total_with_ins." KD</td>";
				$html.= "<td>".$deduct_price." KD</td>";
				$html.= "<td>".$total_net_price." KD</td>";


				$total_ins +=  $insPAy;
				$final_total += $item_total_with_ins;

			}else{

				$html.= "<td>0 KD</td>";

				if($item->total){
					$total_net_price = $item->total-$item->net_price;
					$html.= "<td>".$item->total." KD</td>";
					$html.= "<td>".$deduct_price." KD</td>";
					$html.= "<td>".$total_net_price." KD</td>";
				}else{
					$html.= "<td>0 KD</td>";
					$html.= "<td>".$deduct_price." KD</td>";
					$html.= "<td>0 KD</td>";
				}
				
				

				$total_ins +=  0; 
				$final_total += $item->total; 
				//$customer_pay += $item->total;
			}

			$total_qty += $item->qty;
			$total_deduct += $item->net_price;
			$html.= "<td class='action-links'><a href='/admin/patientsections/invoice/".$item->pid."/".$item->invoice_date."/#invoice-block' target='_blank'><img src='/assets/images/doctor-icon.png' alt=''></a></td>";
			$html .= "</tr>";
			

			$i++;
		endforeach;


	endif;
	$total_net_amount = $final_total - $total_deduct;
	$html .= "<tfoot>";
		$html .= "<tr style='background:#595959; color:#fff;'>";
		$html .= "<td colspan='6' style='vertical-align:middle;'>".lang('all_pay_info')."</td>";
	    $html .= "<td>".lang('inv_total_qty')."<br />".$total_qty."</td>";
	    $html .= "<td></td>";
		$html .= "<td>".lang('inv_insurance_pay')."<br />".$total_ins." KD</td>";
		$html .= "<td>".lang('inv_total')."<br />".$final_total." KD</td>";
		$html .= "<td>".lang('inv_total_deductable')."<br />".$total_deduct." KD</td>";
		$html .= "<td>".lang('inv_total_net')."<br />".$total_net_amount." KD</td>";
		$html .= "<td></td>";
	    $html .= "</tfoot>";

		return $html;
    }

}
