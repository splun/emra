<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PatientSections extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        /* Load :: Common */
        $this->load->model('admin/patientsModel');
        $this->load->model('admin/settings');

        $this->lang->load('admin/patients');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

        /* Title Page :: Common */
        $this->page_title->push("");
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_settings_patients'), 'admin/settings/patients');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");


    }

    public function main($section_slug, $patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        if($this->input->get("newwindow")){ 
            $newwindow = "?newwindow=1";
        }else{
            $newwindow = "";
        }

        $allergy_sections = array(
            "food_allergy" => 29,
            "drug_allergy" => 30,
        );

        if(isset($allergy_sections[$section_slug])) redirect('admin/patientsections/allergy/'.$section_slug.'/'.$patient_id."/".$date_appointment."/".$newwindow, 'refresh');

        $summary_sections = array(
            "summary_plan" => 28,
            "referral" => 27
        );

        if(isset($summary_sections[$section_slug])) redirect('admin/patientsections/summary/'.$section_slug.'/'.$patient_id."/".$date_appointment."/".$newwindow, 'refresh');

        $sections = array(
            "complaints" => 1,
            "diagnosis" => 2,
            "environment" => 3,
            "family_social_history" => 4,
            "asthma" => 5,
            "eczema" => 6,
            "rhino_conjunctivitis" => 7,
            "immunity" => 9,
            "development"=>10,
            "birth_vaccination" => 11,
            "diet" =>12,
            "physical"=>13,
            "immunoCAP" =>15,
            "skin_testing"=>16,
            "rhinoscopy" =>17,
            "spirometer" =>18,
            "exam_photo"=>55

        );

        if(!isset($sections[$section_slug])) redirect('admin/patientsections/'.$section_slug.'/'.$patient_id."/".$date_appointment."/".$newwindow, 'refresh');

        $section = $sections[$section_slug];


        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')[$section_slug]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/main/'.$section_slug.'/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $active_checkboxes = $this->patientsModel->get_patient_diagnosis($patient_id,$section,$date_appointment);

        //var_dump($active_checkboxes);
        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);


        if ($this->input->post('save')){

            $this->patientsModel->delete_items("patient_checkboxpage",array("date_add"=>$date_appointment,"section"=>$section,"patient_id"=>$patient_id));

            $chk = $this->input->post('checkbox');
            $chk_desc = $this->input->post('chk_description');


            if(!empty($chk) && is_array($chk)){
                $chk_fields = array();

                foreach($chk as $key=>$val){

                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'patient_file_id' => $val,
                        'patient_file_description' => ($chk_desc && isset($chk_desc[$val])) ? $chk_desc[$val] : "",
                        'status' => 1,
                        'date_add' => $date_appointment,
                        'section' => $section
                    ));
                }

                $this->patientsModel->insert_all_items("patient_checkboxpage",$chk_fields);

            }

            if(!empty($this->input->post('desc')) || $this->input->post('ddl_other')){

                $fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section)){
                    $this->patientsModel->update_item("share_field_section",$fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }

            if(!empty($this->input->post("delete_file"))){
                $path = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file");
                if(file_exists($path)){
                    unlink($path);
                }
                $this->patientsModel->delete_item("files",$this->input->post("delete_file"));
                $this->patientsModel->delete_items("files_to_patient",array("file_id"=>$this->input->post("delete_file")));
            }

            $file_name="";

            if($_FILES){
                $Attach = $_FILES['file']['name'];
                if($Attach[0] !== ""){
                    for($i=0;$i<count($Attach); $i++){

                        $file_name = date('Ymd').$patient_id.$_FILES["file"]["name"][$i]; 

                        $file_fields = array(
                            'name' => $file_name,
                            'section' => $section,
                        );

                        $fileId = $this->patientsModel->insert_item("files",$file_fields);

                        if($fileId && $file_name != ""){
                            move_uploaded_file($_FILES["file"]["tmp_name"][$i], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file_name); 

                            $file_to_patient_fields = array(
                                'patient_id' => $patient_id,
                                'file_id' => $fileId,
                                'date_file' => $date_appointment,
                                'section'=>$section
                            );

                            $this->patientsModel->insert_item("files_to_patient",$file_to_patient_fields);

                        }

                    }
                }
            }

            $this->session->set_flashdata('message', lang("actions_success"));

            if(!isset($_GET["newwindow"])){
                redirect('/admin/patientsections/main/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
            }else{
                redirect('/admin/patientsections/main/'.$section_slug.'/'.$patient_id.'/'.$date_appointment."#close", 'refresh');
            }
        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');


            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            if($section == 5 || $section == 6 ||  $section == 7 || $section == 13 || $section == 16){

                $section_group = array();
                foreach($this->settings->get_patient_file($section) as $group){
                    if($group->category_name == "") continue;
                    $section_group[$group->category_name][$group->id] = $group->name;
                }

                $this->data['group_checkboxes'] = $section_group;

            }else{

                $this->data['checkboxes'] = $this->settings->get_patient_file($section);

            }

            $this->data['active_checkboxes'] = array();
            $this->data['active_chk_description'] = array();
            $active_diagnosis = array();

            if($active_checkboxes){
                foreach($active_checkboxes as $active_chk){
                    $this->data['active_checkboxes'][] = $active_chk->patient_file_id;

                    $this->data['active_chk_description'][$active_chk->patient_file_id] = $active_chk->patient_file_description;

                    $active_diagnosis[] = $active_chk->name_diagnos;
                }

            }

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }
            

            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

            $main_ddl_list = $this->settings->get_patient_file_ddl($section);

            if(!empty($main_ddl_list) && $section == 1){
                $this->data['other_select'] = array(
                    'label'  => 'Other',
                    'name'  => 'ddl_other',
                    'id'    => 'ddl_other',
                    'options' => $main_ddl_list,
                    'active' => $active_option
                ); 
            }

            if($section == 10 || $section == 15 || $section == 17 || $section == 18 || $section == 55){
               $this->data['file'] = array(
                'name'  => 'file[]',
                'id'    => 'file',
                'multiple'    => 'multiple',
                'type'  => 'file',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('file')
                ); 

               $this->data['active_files'] = $this->patientsModel->get_files($patient_id,$date_appointment,$section);
            }

            if($section == 2){
                $pmh = "<span style='color:red'>".$date_appointment."</span> ";
                $pmh .= ($active_desc) ? $active_desc." " : "";
                $pmh .= (!empty($active_diagnosis)) ? implode(" , ",$active_diagnosis) : "";

                $this->data['pmh'] = array(
                    "label" => lang("pmh"),
                    "value" => $pmh
                );
            }

            $this->data['date_appointment'] = $date_appointment;
            $this->data['section'] = $section;
            $this->template->admin_render('admin/patients/sections/mainform', $this->data);
        }
       
    }


    public function medications($patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 19;
        $section_slug = "medications";

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["$section_slug"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/'.$section_slug.'/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);


        if ($this->input->post('save')){


            if($this->input->post('row')){

                $this->patientsModel->delete_items("treatments",array("date_add"=>$date_appointment,"patient_id"=>$patient_id));

                $chk_fields = array();

                $post_rows = $this->input->post('row');

                for($i=0;$i<count($post_rows);$i++){
                    if($post_rows[$i]["items"] == 0) continue;
                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'patient_file_id' => $post_rows[$i]["items"],
                        'patient_file_category_id' => (isset($post_rows[$i]["category_name"])) ? $post_rows[$i]["category_name"] : 0,
                        'patient_file_description' => (isset($post_rows[$i]["chk_description"])) ? $post_rows[$i]["chk_description"] : 0,
                        'complaint' => (isset($post_rows[$i]["complaint"])) ? $post_rows[$i]["complaint"] : 0,
                        'dc' => (isset($post_rows[$i]["dc"])) ? $post_rows[$i]["dc"] : 0,
                        'weeks' => (isset($post_rows[$i]["weeks"])) ? $post_rows[$i]["weeks"] : 0,
                        'date_add' => $date_appointment
                    ));
                }

                if(!empty($chk_fields)){
                  $this->patientsModel->insert_all_items("treatments",$chk_fields);
                }
                

            }

            if(!empty($this->input->post('desc')) || $this->input->post('ddl_other')){

                $share_fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section)){
                    $this->patientsModel->update_item("share_field_section",$share_fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$share_fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }


            $this->session->set_flashdata('message', lang("actions_success"));

            if(!isset($_GET["newwindow"])){
                redirect('/admin/patientsections/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
            }else{
                redirect('/admin/patientsections/'.$section_slug.'/'.$patient_id.'/'.$date_appointment."#close", 'refresh');
            }

        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');


            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $categories = $this->settings->get_patient_file_sections(100);

            $treatments = $this->patientsModel->get_patient_treatments($patient_id,$date_appointment);

            $items_by_category = array();

            if($treatments){ 
                foreach($treatments as $treatment){
                    $items_by_category[] = $this->patientsModel->get_items("patient_file",array("category"=>$treatment["patient_file_category_id"]));
                }
            }


            $this->data['selects'] = array(
                "category" => array(
                    "name"=>"category_name",
                    "class"=>"form-control category-select",
                    "label"=>"Category",
                    "onchange" => "itemBycategories(this)",
                    "active-db-index"=>3,
                    "options"=>$categories
                ),
                "items"=>array(
                    "name"=>"items",
                    "label"=>"Items",
                    "class"=>"form-control category-item",
                    "active-db-index"=>2,
                    "options"=>"",
                    "active_options"=>$items_by_category
                )
            );

            $this->data['chk_descriptions'] = array(
                "name"=>"chk_description",
                "label"=>"Description",
                "active-db-index"=>4
            );

            $this->data['checkboxes'] = array(
                "complaint" => array(
                    "name"=>"complaint",
                    "label"=>"Complaint",
                    "active-db-index"=>5
                ),
                "dc" => array(
                    "name"=>"dc",
                    "label"=>"DC",
                    "active-db-index"=>6
                )
            );


            $this->data['active_rows'] = $treatments;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }
            
            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

            $this->data['date_appointment'] = $date_appointment;
            $this->template->admin_render('admin/patients/sections/customform', $this->data);
        }
       
    }

    public function investigations($patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 25;
        $section_slug = "investigations";

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["$section_slug"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/'.$section_slug.'/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);


        if ($this->input->post('save')){

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            if($this->input->post('row')){

                if($this->input->post('dublicate') !== "1"){
                    $this->patientsModel->delete_items("investigation",array("date_add"=>$date_appointment,"patient_id"=>$patient_id,"section"=>$section));
                }


                $chk_fields = array();

                $post_rows = $this->input->post('row');

                for($i=0;$i<count($post_rows);$i++){
                    if($post_rows[$i]["items"] == 0) continue;
                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'patient_file_id' => $post_rows[$i]["items"],
                        'patient_file_category_id' => (isset($post_rows[$i]["category_name"])) ? $post_rows[$i]["category_name"] : 0,
                        'date_add' => $date_appointment,
                        'section' => $section
                    ));
                }

                if(!empty($chk_fields)){
                  $this->patientsModel->insert_all_items("investigation",$chk_fields);
                }
                

            }

            if(!empty($this->input->post('desc')) || $this->input->post('ddl_other')){

                $share_fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section)){
                    $this->patientsModel->update_item("share_field_section",$share_fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$share_fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }


            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');


            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $categories = $this->settings->get_patient_file_sections($section);

            $investigations = $this->patientsModel->get_patient_investigations($patient_id,$date_appointment);

            $items_by_category = array();

            if($investigations){ 
                foreach($investigations as $investigation){
                    $items_by_category[] = $this->patientsModel->get_items("patient_file",array("category"=>$investigation["patient_file_category_id"]));
                }
            }


            $this->data['selects'] = array(
                "category" => array(
                    "name"=>"category_name",
                    "class"=>"form-control category-select",
                    "label"=>"Category",
                    "onchange" => "itemBycategories(this)",
                    "active-db-index"=>3,
                    "options"=>$categories
                ),
                "items"=>array(
                    "name"=>"items",
                    "label"=>"Items",
                    "class"=>"form-control category-item",
                    "active-db-index"=>2,
                    "options"=>"",
                    "active_options"=>$items_by_category
                )
            );

            $this->data['active_rows'] = $investigations;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }
            
            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

            $main_ddl_list = $this->settings->get_patient_file_ddl($section);

            if(!empty($main_ddl_list) && $section == 1){
                $this->data['other_select'] = array(
                    'label'  => 'Other',
                    'name'  => 'ddl_other',
                    'id'    => 'ddl_other',
                    'options' => $main_ddl_list,
                    'active' => $active_option
                ); 
            }

            $this->data['date_appointment'] = $date_appointment;
            $this->data['clone_flag'] = true;
            $this->data['print_page'] = "investigations_print";

            if($this->input->post('dublicate') !== "1"){
                $this->patientsModel->delete_items("prescription", array("date_add"=>$date_appointment,"patient_id"=>$patient_id));
            }

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            $this->template->admin_render('admin/patients/sections/customform', $this->data);
        }
       
    }

    public function investigations_print($patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 25;

        /* Title */
        $this->page_title->push(lang("patientsection")["investigations"]);
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang("patientsection")["investigations"], 'admin/patientsections/investigations_print/'.$patient_id.'/'.$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));


        //$this->data['investigations'] = $this->patientsModel->get_patient_investigations($patient_id,$date_appointment);

        $this->data['categories'] = $this->patientsModel->get_patient_investigation_category($patient_id,$date_appointment);


        $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);
        $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
        $this->data['patient'] = $patient;
        $this->data['date_appointment'] = $date_appointment;

        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);

        $this->data["investigations_desc"] = ($share_field_section) ? $share_field_section->desc : "";

        $this->data['diagnosis'] = $this->patientsModel->get_patient_diagnosis($patient_id,2,$date_appointment);

        $this->data['diagnosis_desc'] = $this->patientsModel->get_patient_descriptions($patient_id,2,$date_appointment);

        $this->data['method'] = $this;
        $this->template->admin_render('admin/patients/sections/print_investigations', $this->data);
    }

    public function prescription($patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 20;
        $section_slug = "prescription";

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["$section_slug"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/'.$section_slug.'/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);


        if ($this->input->post('save')){

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            if($this->input->post('row')){

                if($this->input->post('dublicate') !== "1"){
                    $this->patientsModel->delete_items("prescription", array("date_add"=>$date_appointment,"patient_id"=>$patient_id));
                }

                $chk_fields = array();

                $post_rows = $this->input->post('row');


                for($i=0;$i<count($post_rows);$i++){
                    if($post_rows[$i]["items"] == 0) continue;
                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'patient_file_id' => $post_rows[$i]["items"],
                        'patient_file_category_id' => (isset($post_rows[$i]["category_name"])) ? $post_rows[$i]["category_name"] : 0,
                        'dose' => (isset($post_rows[$i]["dose"])) ? $post_rows[$i]["dose"] : 0,
                        'form' => (isset($post_rows[$i]["form"])) ? $post_rows[$i]["form"] : 0,
                        'freq' => (isset($post_rows[$i]["freq"])) ? $post_rows[$i]["freq"] : 0,
                        'route' => (isset($post_rows[$i]["route"])) ? $post_rows[$i]["route"] : 0,
                        'p_use' => (isset($post_rows[$i]["use"])) ? $post_rows[$i]["use"] : 0,
                        'length' => (isset($post_rows[$i]["length"])) ? $post_rows[$i]["length"] : 0,
                        'date_add' => $date_appointment
                    ));
                }

                if(!empty($chk_fields)){
                  $this->patientsModel->insert_all_items("prescription",$chk_fields);
                }
                

            }

            if(!empty($this->input->post('desc')) || $this->input->post('ddl_other')){

                $share_fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section) &&  $this->input->post('dublicate') !== "1"){
                    $this->patientsModel->update_item("share_field_section",$share_fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$share_fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }


            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');


            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $categories = $this->settings->get_patient_file_sections(100);

            $prescriptions = $this->patientsModel->get_patient_prescriptions($patient_id,$date_appointment);

            $items_by_category = array();

            if($prescriptions){ 
                foreach($prescriptions as $prescription){
                    $items_by_category[] = $this->patientsModel->get_items("patient_file",array("category"=>$prescription["patient_file_category_id"]));
                }
            }


            $this->data['selects'] = array(
                "category" => array(
                    "name"=>"category_name",
                    "class"=>"form-control category-select",
                    "label"=>"Category",
                    "onchange" => "itemBycategories(this)",
                    "active-db-index"=>3,
                    "options"=>$categories
                ),
                "items"=>array(
                    "name"=>"items",
                    "label"=>"Items",
                    "class"=>"form-control category-item",
                    "active-db-index"=>2,
                    "options"=>"",
                    "active_options"=>$items_by_category
                ),
            );

            $this->data['dose'] = array(
                'name'  => 'dose',
                'id'    => 'dose',
                'label' => 'Dose',
                'type'  => 'number',
                'step'  => '0.1',
                "active-db-index"=>4,
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('dose')
            );

            $med_forms = $this->patientsModel->get_items("prescription_forms");
            $med_freq = $this->patientsModel->get_items("prescription_frequency");
            $med_route = $this->patientsModel->get_items("prescription_route");
            $med_use = $this->patientsModel->get_items("prescription_use");

            $this->data['custom_selects'] = array(
                "form"=>array(
                    "name"=>"form",
                    "label"=>"Form",
                    "class"=>"form-control",
                    "active-db-index"=>10,
                    "options"=>$med_forms
                ),
                "freq"=>array(
                    "name"=>"freq",
                    "label"=>"Frequency",
                    "class"=>"form-control",
                    "active-db-index"=>5,
                    "options"=>$med_freq
                ),
                "route"=>array(
                    "name"=>"route",
                    "label"=>"Route",
                    "class"=>"form-control",
                    "active-db-index"=>6,
                    "options"=>$med_route
                ),
                "use"=>array(
                    "name"=>"use",
                    "label"=>"Use",
                    "class"=>"form-control",
                    "active-db-index"=>7,
                    "options"=>$med_use
                ),
            );

            $this->data['length'] = array(
                'name'  => 'length',
                'label' => 'Length/Days',
                'id'    => 'length',
                'type'  => 'number',
                'step'  => '1',
                'class' => 'form-control',
                "active-db-index"=>8,
                'value' => $this->form_validation->set_value('length')
            );

            $this->data['active_rows'] = $prescriptions;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }

            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

            $main_ddl_list = $this->settings->get_patient_file_ddl($section);

            if(!empty($main_ddl_list) && $section == 1){
                $this->data['other_select'] = array(
                    'label'  => 'Other',
                    'name'  => 'ddl_other',
                    'id'    => 'ddl_other',
                    'options' => $main_ddl_list,
                    'active' => $active_option
                ); 
            }

            $this->data['date_appointment'] = $date_appointment;
            $this->data['clone_flag'] = true;
            $this->data['print_page'] = "prescription_print";

            $this->template->admin_render('admin/patients/sections/customform', $this->data);
        }
       
    }
    public function prescription_print($patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 20;

        /* Title */
        $this->page_title->push(lang('prescription_print_title'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('prescription_print_title'), 'admin/patientsections/prescription_print/'.$patient_id.'/'.$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

        $this->data['prescriptions'] = $this->patientsModel->get_patient_prescriptions($patient_id,$date_appointment);

        $this->data['allergy'] = $this->patientsModel->get_patient_drug_allergy($patient_id,30,$date_appointment);

        $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);
        $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
        $this->data['patient'] = $patient;
        $this->data['date_appointment'] = $date_appointment;

        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);

        $this->data["prescription_desc"] = ($share_field_section) ? $share_field_section->desc : "";

        $this->data['diagnosis'] = $this->patientsModel->get_patient_diagnosis($patient_id,2,$date_appointment);

        $this->data['diagnosis_desc'] = $this->patientsModel->get_patient_descriptions($patient_id,2,$date_appointment);

        $this->template->admin_render('admin/patients/sections/print_prescription', $this->data);
    }

    public function allergy($section_slug, $patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $allergy_sections = array(
            "food_allergy" => 29,
            "drug_allergy" => 30,
        );


        $section = $allergy_sections[$section_slug];

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["$section_slug"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/allergy/'.$section_slug.'/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);


        if ($this->input->post('save')){


            if($this->input->post('row')){

                $this->patientsModel->delete_items("allergy",array("date_add"=>$date_appointment,"patient_id"=>$patient_id,"section"=>$section));

                $chk_fields = array();

                $post_rows = $this->input->post('row');

                for($i=0;$i<count($post_rows);$i++){
                    if($post_rows[$i]["items"] == 0) continue;
                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'patient_file_id' => $post_rows[$i]["items"],
                        'ddl_id' => (isset($post_rows[$i]["ddl_items"]) && !empty($post_rows[$i]["ddl_items"])) ? implode(",",$post_rows[$i]["ddl_items"]) : 0,
                        'date_add' => $date_appointment,
                        'section' => $section
                    ));
                }

                if(!empty($chk_fields)){
                  $this->patientsModel->insert_all_items("allergy",$chk_fields);
                }
                

            }

            if(!empty($this->input->post('desc')) || $this->input->post('ddl_other')){

                $share_fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section)){
                    $this->patientsModel->update_item("share_field_section",$share_fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$share_fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }


            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/allergy/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');


            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $items = $this->settings->get_patient_file($section);
            $ddl_items = $this->settings->get_patient_file_ddl($section);

            $allergy = $this->patientsModel->get_patient_drug_allergy($patient_id,$section,$date_appointment);

            $this->data['selects'] = array(
                "items"=>array(
                    "name"=>"items",
                    "label"=>"Items",
                    "class"=>"form-control",
                    "active-db-index"=>2,
                    "options"=>$items,
                ),
                "ddl_items" => array(
                    "name"=>"ddl_items",
                    "class"=>"form-control multiple-select",
                    "label"=>"Questions",
                    "active-db-index"=>3,
                    "type"=>"multiple",
                    "options"=>$ddl_items
                ),
            );

            $this->data['active_rows'] = $allergy;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }
            
            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

            $main_ddl_list = $this->settings->get_patient_file_ddl($section);

            if(!empty($main_ddl_list) && $section == 1){
                $this->data['other_select'] = array(
                    'label'  => 'Other',
                    'name'  => 'ddl_other',
                    'id'    => 'ddl_other',
                    'options' => $main_ddl_list,
                    'active' => $active_option
                ); 
            }

            $this->data['date_appointment'] = $date_appointment;

            $this->template->admin_render('admin/patients/sections/customform', $this->data);
        }
       
    }

    public function results($patient_id = 0 ,$date_appointment = 0,$delete_file = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        $section = 14;
        $section_slug = "results";

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')[$section_slug]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/<?php echo $section_slug;?>/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);

        if($delete_file !== 0){
            $path = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$delete_file;
            if(file_exists($path)){
                unlink($path);
            }
            $this->patientsModel->delete_item("files",$delete_file);
            $this->patientsModel->delete_items("files_to_patient",array("file_id"=>$delete_file));
        }

        if ($this->input->post('save')){

            if(!empty($this->input->post('desc'))){

                $fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section)){
                    $this->patientsModel->update_item("share_field_section",$fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }

            if($_FILES['file']){
                
                $Attach = $_FILES['file']['name'];

                foreach($Attach as $key=>$val){

                    if($val[0] !== ""){ $file_name="";
                        for($i=0;$i<count($val); $i++){

                            $file_name = date('Ymd').$patient_id.$val[$i]; 

                            $file_fields = array(
                                'name' => $file_name,
                                'section' => $key,
                            );

                            $fileId = $this->patientsModel->insert_item("files",$file_fields);

                            if($fileId && $file_name != ""){
                                move_uploaded_file($_FILES["file"]["tmp_name"][$key][$i], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file_name); 

                                $file_to_patient_fields = array(
                                    'patient_id' => $patient_id,
                                    'file_id' => $fileId,
                                    'date_file' => $date_appointment,
                                    'section'=>$key
                                );

                                $this->patientsModel->insert_item("files_to_patient",$file_to_patient_fields);

                            }

                        }
                    }
                }
            }

            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/results/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

            if(!$patient) redirect('admin/patients/', 'refresh');
            
            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id;  
            }
            
            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );

           $main_ddl_list = $this->settings->get_patient_file_ddl($section);

           $this->data['labels']= array(
                101 => "Bloods",
                102 => "Radiology",
                103 => "Culture",
                104 => "Immune",
                105 => "Allergy",
                106 => "Urine"
            );
    
            $this->data['date_appointment'] = $date_appointment;
            $this->data['func'] = $this;

            $this->template->admin_render('admin/patients/sections/resultsform', $this->data);
        }
       
    }

    public function summary($section_slug,$patient_id = 0 ,$date_appointment = 0){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')[$section_slug]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')[$section_slug], 'admin/patientsections/<?php echo $section_slug;?>/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $summary_sections = array(
            "summary_plan" => 28,
            "referral" => 27
        );

        $section = $summary_sections[$section_slug];

        $share_field_section = $this->patientsModel->get_patient_descriptions($patient_id,$section,$date_appointment);

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

        if(!$patient) redirect('admin/patients/', 'refresh');

        if ($this->input->post('save')){

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            if(!empty($this->input->post('desc')) || !empty($this->input->post('ddl_other'))){

                $fields = array(
                    'patient_id' => $patient_id,
                    'desc' => $this->input->post('desc'),
                    'chk_normal' => 1,
                    'section' => $section,
                    'date_test' => $date_appointment,
                    'ddl_id' => $this->input->post('ddl_other')
                );

                if(!empty($share_field_section) && !$this->input->post('dublicate')){
                    $this->patientsModel->update_item("share_field_section",$fields,$share_field_section->id);
                }else{
                    $this->patientsModel->insert_item("share_field_section",$fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }

            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/summary/'.$section_slug.'/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient'] = $patient;

            $active_desc = "";
            $active_option = "";

            if($share_field_section){
               $active_desc = $share_field_section->desc;
               $active_option = $share_field_section->ddl_id; 
            }

            if($section == 27){
                $this->data['referral'] = $this->settings->get_patient_file_ddl($section);
                $this->data['active_ddl_option'] = $active_option;

                $this->data['active_ddl_option_name'] = "";

                foreach($this->data['referral'] as $ref):
                    if($ref->id == $active_option){
                        $this->data['active_ddl_option_name'] = $ref->name;
                        break;
                    }

                endforeach;
            }
            
            $this->data['desc'] = array(
                'name'  => 'desc',
                'id'    => 'desc',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('desc',$active_desc)
            );


            $this->data['date_visit'] = date("l F d, Y",strtotime($date_appointment));

            $this->data['diagnosis'] = $this->patientsModel->get_patient_diagnosis($patient->id,2,$date_appointment);

            $this->data['diagnosis_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,2,$date_appointment);

            $this->data['medications'] = $this->patientsModel->get_patient_treatments($patient_id,$date_appointment);

            $this->data['complaints'] = $this->patientsModel->get_patient_diagnosis($patient_id,1,$date_appointment);

            $this->data['family_history'] = $this->patientsModel->get_patient_diagnosis($patient_id,4,$date_appointment);
            $this->data['family_history_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,4,$date_appointment);

            $this->data['environments'] = $this->patientsModel->get_patient_diagnosis($patient_id,3,$date_appointment);
            $this->data['environments_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,3,$date_appointment);
            
            $this->data['birth_vaccination'] = $this->patientsModel->get_patient_diagnosis($patient_id,11,$date_appointment);
            $this->data['birth_vaccination_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,11,$date_appointment);

            $this->data['developments'] = $this->patientsModel->get_patient_diagnosis($patient_id,10,$date_appointment);
            $this->data['developments_active_files'] = $this->patientsModel->get_files($patient_id,$date_appointment,10);
            $this->data['developments_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,10,$date_appointment);

            $this->data['immunity'] = $this->patientsModel->get_patient_diagnosis($patient_id,9,$date_appointment);
            $this->data['immunity_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,9,$date_appointment);

            $this->data['asthma'] = $this->patientsModel->get_patient_diagnosis($patient_id,5,$date_appointment);

            $this->data['eczema'] = $this->patientsModel->get_patient_diagnosis($patient_id,6,$date_appointment);

            $this->data['rhino_conjunctivitis'] = $this->patientsModel->get_patient_diagnosis($patient_id,7,$date_appointment);
            
            $this->data['food_allergy'] = $this->patientsModel->get_patient_drug_allergy($patient_id,29,$date_appointment);

            $this->data['drug_allergy'] = $this->patientsModel->get_patient_drug_allergy($patient_id,30,$date_appointment);
            $this->data['drug_allergy_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,30,$date_appointment);

            $this->data['diets'] = $this->patientsModel->get_patient_diagnosis($patient_id,12,$date_appointment);
            $this->data['diets_desc'] = $this->patientsModel->get_patient_descriptions($patient->id,12,$date_appointment);


            $this->data['nurse_records'] = $this->patientsModel->get_items("nurse",array("patient_id"=>$patient_id,"date_visit"=>$date_appointment));

            $this->data['physicals'] = $this->patientsModel->get_patient_diagnosis($patient_id,13,$date_appointment);

            $this->data['skin_testing'] = $this->patientsModel->get_patient_diagnosis($patient_id,16,$date_appointment);

            $this->data['spirometers'] = $this->patientsModel->get_patient_diagnosis($patient_id,18,$date_appointment);
            $this->data['spirometers_active_files'] = $this->patientsModel->get_files($patient_id,$date_appointment,18);

            $this->data['rhinoscopy'] = $this->patientsModel->get_patient_diagnosis($patient_id,17,$date_appointment);
            $this->data['rhinoscopy_active_files'] = $this->patientsModel->get_files($patient_id,$date_appointment,17);

            $this->data['results'] = $this->patientsModel->get_results_files($patient_id,$date_appointment);

            $this->data['immunoCAP'] = $this->patientsModel->get_patient_diagnosis($patient_id,15,$date_appointment);
            $this->data['immunoCAP_active_files'] = $this->patientsModel->get_files($patient_id,$date_appointment,15);

            $this->data['investigations'] = $this->patientsModel->get_patient_investigations($patient_id,$date_appointment);

            $this->data["investigations_desc"] = $this->patientsModel->get_patient_descriptions($patient_id,25,$date_appointment);;

            $this->data['prescriptions'] = $this->patientsModel->get_patient_prescriptions($patient_id,$date_appointment);

            $this->data['advice'] = $active_desc;
            $this->data['date_appointment'] = $date_appointment;
            $this->data['func'] = $this;

            $this->template->admin_render('admin/patients/sections/summary', $this->data);
        }
    }

    public function invoice($patient_id,$date_appointment){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["invoice"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')["invoice"], 'admin/patientsections/invoice/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

        $patient_pay = $this->patientsModel->get_patient_pays($patient_id,$date_appointment);

        if ($this->input->post('save')){

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            if($this->input->post('row')){

                if($this->input->post('dublicate') !== "1"){
                    $this->patientsModel->delete_items("patient_balance",array("invoice_date"=>$date_appointment,"patient_id"=>$patient_id));
                }

                $chk_fields = array();

                $post_rows = $this->input->post('row');
                $discount_per_item = 0;

                
                for($i=0;$i<count($post_rows);$i++){

                    if(!isset($post_rows[$i]) || $post_rows[$i]["items"] == 0) { 
                        continue;
                    }

                    array_push($chk_fields, array(
                        'patient_id' => $patient_id,
                        'invoice_item_id' => $post_rows[$i]["items"],
                        'invoice_date' => $date_appointment,
                        'discount' => $post_rows[$i]["discount"],
                        'discount_item' =>$post_rows[$i]["discount_item"],
                        'qty' => $post_rows[$i]["qty"],
                        'ins_discount' => $post_rows[$i]["ins_discount"],
                        'ins_flag' => (isset($post_rows[$i]["insurance"])) ? $post_rows[$i]["insurance"] : 0,
                        'total' => str_replace(" KD","",$post_rows[$i]["total"]),
                        'discount_type' => $post_rows[$i]["discount_type"]
                    ));

                    $item_info = $this->settings->get_items("invoice",array("id"=>$post_rows[$i]["items"]));

 
                   if($item_info->selling_item_id !== 0){
                        $recalculate_stock = $this->Recalculate_Stock_Items($item_info->selling_item_id,$post_rows[$i]["qty"],$patient_id);
                   }

                }

                if(!empty($chk_fields)){
                  $this->patientsModel->insert_all_items("patient_balance",$chk_fields);
                }
                

            }

            if(!empty($this->input->post('knet')) || !empty($this->input->post('cash')) || !empty($this->input->post('debt'))){

                $fields = array(
                    'patient_id' => $patient_id,
                    'pay_day' => $date_appointment,
                    'knet' => $this->input->post('knet'),
                    'cash' => $this->input->post('cash'),
                    'debt' => $this->input->post('debt')
                );

                if(!empty($patient_pay)){
                    $this->patientsModel->update_item("patient_pays",$fields,$patient_pay->id);
                }else{
                    $this->patientsModel->insert_item("patient_pays",$fields);
                }

            }else{
                if(!empty($share_field_section)){
                    $this->patientsModel->delete_item("share_field_section",$share_field_section->id);
                }
            }

            $this->session->set_flashdata('message', lang("actions_success"));

            redirect('/admin/patientsections/invoice/'.$patient_id.'/'.$date_appointment, 'refresh');
        }else{

            $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

            $this->data['gender'] = array(1=>'Male',2=>'Female');
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

            $this->data['patient_insurance'] = $this->patientsModel->get_items("insurance",array("id"=>$patient->ddl_insurance));

            $this->data['patient'] = $patient;

            $this->data['categories'] = $this->settings->get_items("invoice_categories");

            $this->data['invoice_items'] = $this->settings->get_invoice_selling_items();

            $this->data["active_items"] = $this->patientsModel->get_patient_invoice($patient_id,$date_appointment);

            $this->data["patient_pay"] = $patient_pay;

            $this->data['date_appointment'] =  $date_appointment;
            $this->data['func'] = $this;

            $this->template->admin_render('admin/patients/sections/invoice', $this->data);
        }
    }

    function Recalculate_Stock_Items($id, $qty=0,$patient){

        $check_history = $this->core_model->get_item("stock_recalculate_history",array("item_id"=>$id,"date_deduct"=>date("Y-m-d"),"patient" => $patient));
        
        $current_stock = $this->core_model->get_item("stock_items",array("id"=>$id));

        if($current_stock){
            $current_qty = $current_stock->qty;
        }else{
            $current_qty = 0;
        }

        if($check_history){
            

            $old_qty = $check_history->qty;
            $historyId = $check_history->id;

            //var_dump($old_qty+$current_qty); var_dump($current_qty);die();

            $Recalc = $this->patientsModel->update_item("stock_items",array("qty"=>$current_qty + $old_qty),$id);

            $HistoryUpdate = $this->patientsModel->update_item("stock_recalculate_history",array("qty"=>$qty),$historyId);

            $current_qty = $current_qty + $old_qty;


        }else{
            $history_insert_fields = array(
                "item_id"=>$id,
                "date_deduct"=>date("Y-m-d"),
                "qty"=>$qty,
                "patient"=>$patient
            ); 

            $HistoryAdd = $this->patientsModel->insert_item("stock_recalculate_history",$history_insert_fields);

        }


        $InsertQuery = $this->patientsModel->update_item("stock_items",array("qty"=>$current_qty - $qty),$id);

        if ($InsertQuery)

            return 1;

        else

            return 0;
    }

    public function print_invoice($patient_id,$date_appointment,$copy){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        /* Breadcrumbs */
        if($copy == "patient"){
            $this->page_title->push(" - ".lang('print_invoice_patient_copy'));
        }else{
            $this->page_title->push(" - ".lang('print_invoice_clinic_copy'));
        }
        $this->page_title->push(lang('print_invoice_title'));

        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('print_invoice_title'), 'admin/patientsections/print_invoice/'.$patient_id."/".$date_appointment);
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

        $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);

        $this->data['gender'] = array(1=>'Male',2=>'Female');
        $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

        $this->data['patient_insurance'] = $this->patientsModel->get_items("insurance",array("id"=>$patient->ddl_insurance));

        $this->data['patient'] = $patient;
        $this->data['copy'] = $copy;
        $this->data['date_appointment'] =  $date_appointment;
        

        $this->data['patient_pay'] = $this->patientsModel->get_patient_pays($patient_id,$date_appointment);
        $this->data["patient_invoice"] = $this->patientsModel->get_patient_invoice($patient_id,$date_appointment);

        $this->template->admin_render('admin/patients/sections/print_invoice', $this->data);
    }

    function invoice_parent_categories($parent_id)
    {
        return $this->settings->get_items("invoice_categories",$parent_id);

    }

    function invoice_child_categories($parent_id)
    {
        return $this->settings->get_invoice_child_categories($parent_id);

    }


    public function sickleave($patient_id,$date_appointment){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

        if($date_appointment == 0) $date_appointment = date("Y-m-d");

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["sickleave"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')["sickleave"], 'admin/patientsections/sickleave/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));

        $check_sickl_record = $this->core_model->get_item("patients_sick_leave",array("patient_id"=>$patient_id,"date_add"=>$date_appointment));

        /* Validate form input */
        $this->form_validation->set_rules('diagnosis', 'lang:patient_diagnosis_tab', 'required');
        $this->form_validation->set_rules('leave_days', 'lang:leave_days', 'required');
        $this->form_validation->set_rules('destination', 'lang:destination', 'required');

        if ($this->form_validation->run()){

            if($this->input->post('dublicate') == "1")
            {
                $date_appointment = date("Y-m-d");
            }

            $fields = array(
                'patient_id' => $patient_id,
                'date_add' => $date_appointment,
                'diagnosis' => $this->input->post('diagnosis'),
                'leave_days' => $this->input->post('leave_days'),
                'destination' => $this->input->post('destination'),
                'extra_notes' => $this->input->post('extra_notes')
            );
            
            if(!empty($check_sickl_record) && !$this->input->post('dublicate')){
                $action = $this->core_model->update_item("patients_sick_leave",$fields,array("id"=>$check_sickl_record->id));
            }else{
                $action = $this->core_model->insert_item("patients_sick_leave",$fields);
            }
            
            if($action){

                $this->session->set_flashdata('message', lang("actions_success"));

                redirect('/admin/patientsections/sickleave/'.$patient_id.'/'.$date_appointment, 'refresh');
            }

        }else{
            

        $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);
        $this->data['gender'] = array(1=>'Male',2=>'Female');
        $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
        $this->data['patient_insurance'] = $this->patientsModel->get_items("insurance",array("id"=>$patient->ddl_insurance));

        $this->data['patient'] = $patient;

        $this->data['date_appointment'] = $date_appointment;

        $this->data['check_sickl_record'] = $check_sickl_record;

        //$this->data['func'] = $this;

        $this->template->admin_render('admin/patients/sections/sickleave', $this->data);

        }
    }

    public function growth_chart($patient_id,$date_appointment){

        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        $patient_id = (int)$patient_id;

        if($patient_id == 0) redirect('admin/patients/', 'refresh');

         if($date_appointment == 0) $date_appointment = date("Y-m-d");

        /* Breadcrumbs */
        $this->page_title->push(lang('patientsection')["growth_chart"]);
        $this->data['pagetitle'] = $this->page_title->show();

        $this->breadcrumbs->unshift(2, lang('patientsection')["sickleave"], 'admin/patientsections/growth_chart/'.$patient_id."/".$date_appointment);

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $patientXData["headcircumference"] = array();
        $patientYData["headcircumference"] = array();

        $patientXData["weight"] = array();
        $patientYData["weight"] = array();

        $patientXData["height"] = array();
        $patientYData["height"] = array();

        $patient = $this->patientsModel->get_items("patients",array("id" => $patient_id));
        $getVital = $this->patientsModel->get_items("nurse",array("patient_id"=>$patient_id));


        array_push($patientXData["weight"], 0);
        array_push($patientYData["weight"], $patient->b_weight );

        foreach($getVital as $vital) 
        {

            $birthDate = new DateTime($patient->dob);
            $to = new DateTime($vital->date_visit);

            $years = (int)$birthDate->diff($to)->format("%Y");
            $months = (int)$birthDate->diff($to)->format("%m");
            $days = (int)$birthDate->diff($to)->format("%d");

            if((trim($vital->Circumference) != "") and (number_format($vital->Circumference) != 0)) {

                if(trim($days/30)>0)

                    array_push($patientXData["headcircumference"],number_format($months+($days/30),4));

                else

                    array_push($patientXData["headcircumference"],number_format($months));

                array_push($patientYData["headcircumference"],number_format($vital->Circumference,8));

            }

            if((trim($vital->weight) != "") and (number_format($vital->weight) != 0)) {

                if(trim($vital->weight)>0) {
                    array_push($patientXData["weight"],number_format($months+($days/30),4));

                    array_push($patientYData["weight"],number_format($vital->weight,8));

                }

            }

            if(trim($vital->height)>0) {

                if(trim($days/30)>0)

                    array_push($patientXData["height"],number_format($months+($days/30),4));

                else

                    array_push($patientXData["height"],number_format($months));

                array_push($patientYData["height"],number_format($vital->height,8));

            }

        }

        $this->data["headcircumference"]["xvals"] = join(',', $patientXData["headcircumference"]);
        $this->data["headcircumference"]["yvals"] = join(',', $patientYData["headcircumference"]);

        $this->data["weight"]["xvals"] = join(',', $patientXData["weight"]);
        $this->data["weight"]["yvals"] = join(',', $patientYData["weight"]);

        $this->data["height"]["xvals"] = join(',', $patientXData["height"]);
        $this->data["height"]["yvals"] = join(',', $patientYData["height"]);
        
        $this->data["patient"] = $patient;
        $this->data['patient_year'] = $this->calculate_age($patient->dob,$date_appointment);
        $this->data['gender'] = array(1=>'Male',2=>'Female');
        $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
        $this->data['patient_insurance'] = $this->patientsModel->get_items("insurance",array("id"=>$patient->ddl_insurance));
        $this->data['date_appointment'] = $date_appointment;

        $this->template->admin_render('admin/patients/sections/growth_chart', $this->data);

    }

    public function get_allergy_questions($section,$question_id){

        return $this->settings->get_patient_file_ddl($section,$question_id);
    }

    public function get_active_files_by_section($patient_id,$date_appointment,$section_id = 0){

        return $this->patientsModel->get_files($patient_id,$date_appointment,$section_id);
    }

    public function calculate_age($birthday,$date_appointment = 0) {
  
        $dob_day = date("d",strtotime($birthday));
        $dob_month = date("m",strtotime($birthday));
        $dob_year = date("Y",strtotime($birthday));

        if($date_appointment !== 0){
            $day = date("d",strtotime($date_appointment)) - $dob_day;
            $month = date("m",strtotime($date_appointment)) - $dob_month;
            $year = date("Y",strtotime($date_appointment)) - $dob_year;
        }else{
            $day = date("d") - $dob_day;
            $month = date("m") - $dob_month;
            $year = date("Y") - $dob_year;
        }

        if($day < 0)
        {
            $day = 30 + $day;
            $month = $month - 1;    
        }

        if($month < 0)
        {
            $month = 12 + $month;
            $year = $year - 1;  
        }

      return $year." year ".$month." month ".$day." days ";
    }

    public function invoice_items_by_cat() {

        $category_id =$this->input->post('category_id');

        if($category_id !== 0){
            $items = $this->settings->get_invoice_selling_items($category_id);
        }else{
            $items = $this->settings->get_invoice_selling_items();
        }

        if($items){
            echo json_encode($items);
        }else{
            echo 0;
        }

    }

    public function items_by_cat() {

        if(!$this->input->post('category_id')) return;

        $category_id =$this->input->post('category_id');

        $items = $this->patientsModel->get_items("patient_file",array("category"=>$category_id),"name","asc");

        if($items){
            echo json_encode($items);
        }else{
            echo 0;
        }

    }

    public function quick_add_results(){
        if ( ! $this->ion_auth->logged_in()) redirect('auth/login', 'refresh');

        /* Validate form input */
        $this->form_validation->set_rules('results_patient', 'lang:upload_result_select_patient', 'required');
        $this->form_validation->set_rules('results_date', 'lang:upload_result_date', 'required');
        $this->form_validation->set_rules('results_section', 'lang:upload_result_select_section', 'required');

        if ($this->form_validation->run()){

            if(!empty($this->input->post('desc'))){

                $fields = array(
                    'patient_id' => $this->input->post('results_patient'),
                    'desc' => $this->input->post('results_desc'),
                    'chk_normal' => 1,
                    'section' => $this->input->post('desc_section'),
                    'date_test' => $this->input->post('results_date'),
                    'ddl_id' => $this->input->post('ddl_other')
                );

                $this->patientsModel->insert_item("share_field_section",$fields);
            }


            if($_FILES['results_files']['name'][0] !== ""){   
                $Attach = $_FILES['results_files']['name'];

                foreach($Attach as $key=>$val){

                    $file_name = date('Ymd').$this->input->post('results_patient').$val; 

                    $file_fields = array(
                        'name' => $file_name,
                        'section' => $this->input->post('results_section'),
                    );

                    $fileId = $this->patientsModel->insert_item("files",$file_fields);

                    if($fileId && $file_name != ""){
                        move_uploaded_file($_FILES["results_files"]["tmp_name"][$key], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file_name); 

                        $file_to_patient_fields = array(
                            'patient_id' => $this->input->post('results_patient'),
                            'file_id' => $fileId,
                            'date_file' => $this->input->post('results_date'),
                            'section'=>$this->input->post('results_section')
                        );

                        $this->patientsModel->insert_item("files_to_patient",$file_to_patient_fields);

                    }

                }

                $this->session->set_flashdata('message', lang("actions_upload_success"));

                redirect('/admin/dashboard', 'refresh');

                //redirect('/admin/patientsections/results/'.$this->input->post('results_patient').'/'.$this->input->post('results_date'), 'refresh');
            }else{
                $this->session->set_flashdata('message', lang("actions_upload_error"));
                redirect('/admin/dashboard', 'refresh');
            }


        }else{
            //$this->session->set_flashdata('message', lang("actions_upload_error"));

            $this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
            redirect('/admin/dashboard', 'refresh');
        }

    }
    //TODO
    public function chart(){

        $style = null;
        $sex = null;
        $maxage = null;
        $width = null;
        $height = null;

        $url_style = $_GET["style"];
        $url_sex = $_GET["sex"];
        $url_maxage = $_GET["maxage"];
        $url_xvals = $_GET["xvals"];
        $url_yvals = $_GET["yvals"];

        // check for minimum required variables
        if (!isset($url_style) || !isset($url_sex) || !isset($url_maxage))

        {
            die('One or more expected request variables not present.');

        }

        $style = $url_style;
        // check sex value

        if (!is_numeric($url_sex) || $url_sex < 1 || $url_sex > 2)

        {
            die('Sex value out of range: ' . $url_sex);
        }

        $sex = $url_sex;

        // check maximum age value

        if (!is_numeric($url_maxage) || $url_maxage < 0)

        {
            die('Maximum age value out of range: ' . $url_maxage);
        }

        $maxage = $url_maxage;
        // check image width

        if (isset($url_width) && !is_numeric($url_width))

        {
            die('Image width value out of range: ' . $url_width);

        }



        $width = (isset($url_width) ? $url_width : 1000);
        // check image height

        if (isset($url_height) && !is_numeric($url_height))
        {

            die('Image height value out of range: ' . $url_height);

        }

        $height = (isset($url_height) ? $url_height :1000);
        $patientXarray = null;
        $patientYarray = null;
        // check patient data values

        // expecting comma-separated lists of x coordinates and y coordinates

        if (isset($url_xvals) && isset($url_yvals))
        {
            $paX = explode(',', $url_xvals);
            $paY = explode(',', $url_yvals);

            if (sizeof($paX) == sizeof($paY))
            {
                $okay = true;
                foreach ($paX as $value)
                {
                    if (!is_numeric($value))

                        $okay = false;
                }

                foreach ($paY as $value)
                {
                    if (!is_numeric($value))

                        $okay = false;
                }

                if ($okay)
                {
                    $patientXarray = $paX;

                    $patientYarray = $paY;

                }

            }

        }

        //TODO
        //$this->load->library('charts/GrowthChart');

        //$this->GrowthChart->render($style, $sex, $maxage, $width, $height, $patientXarray, $patientYarray);


    }
}