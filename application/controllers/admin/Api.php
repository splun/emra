<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Admin_Controller {

    public function __construct()
    {
    	parent::__construct();

    	$this->load->model('admin/settings');
    } 

    public function UpdateOldInvoiceBalansTotal()
    {
		$this->db->select('id,invoice_item_id,qty,discount_type,discount_item')->from('patient_balance');

        $oldTotal =  $this->db->get()->result(); 
        $invoice_id = 0;
        $qty = 0;

        foreach ($oldTotal as $value) {

        	$this->db->select('price')->from('invoice')->where("id = {$value->invoice_item_id}");

        	$prices = $this->db->get()->result();

        	$qty = $value->qty;
        	$invoice_id = $value->id;
            $discount_type = $value->discount_type;
            $discount_item = $value->discount_item;

        	$summ = 0;
            $price = 0;

        	foreach($prices as $inv_price){

                $price = $inv_price->price;

                if((int)$discount_type == 0){
                    $summ = $price - ($price*($discount_item/100));
                    $summ = $summ * $qty;
                }else{
        		    $summ = $price * $qty - $discount_item;
                }

        		$this->db->where('id',$invoice_id);
        		$this->db->update('patient_balance',array("total"=>$summ));
        		
        	}

        }
        
    	
    }

    public function MoveDDL()
    {
        $this->db->select('*')->from('patient_file_ddl')->where("section = 1 OR section = 2");

        $ddl = $this->db->get()->result();

        foreach($ddl as $item){

            $fields = array(
                "name"=>$item->name,
                "section"=>$item->section,
                "category"=>$item->category
            );

            $id = $this->settings->insert_item("patient_file",$fields);

            echo "Section:".$item->section." - item:".$id."<br />";
        }

        
    }

}