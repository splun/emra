<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointments extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/appointmentsModel');
        $this->load->model('admin/patientsModel');
        
        $this->lang->load('admin/patients');
        $this->lang->load('admin/appointments');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->data['pagetitle'] = "";

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_appointments'), 'admin/appointments/');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index(){
    	//$this->load->library('calendar');

    	 /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_calendar'), 'admin/appointments/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('pagetitle_calendar'));

        $this->data['pagetitle'] = $this->page_title->show();

        if(isset($_GET['doctor']) && $_GET['doctor'] != 0){
        	$this->data["appointments"] = $this->appointmentsModel->get_appointments(0,(int)$_GET['doctor']);
        }else{
        	$this->data["appointments"] = $this->appointmentsModel->get_appointments();
    	}
        
        
        $this->data["patients"] = $this->patientsModel->get_patients();
    	$this->data['doctors'] = $this->ion_auth->users(array('doctors','nurses'))->result();
    	$this->data['types'] = $this->core_model->get_items("appointments_type",array("status"=>"1"),"sort,name","ASC");
    	$this->data['rooms'] = $this->core_model->get_items("rooms");

   		$this->data["func"] = $this;

    	/* Load Template */
        $this->template->admin_render('admin/appointments/calendar', $this->data);

    }

    public function tooday(){
    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_tooday'), 'admin/appointments/tooday');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('pagetitle_tooday'));

        $this->data['pagetitle'] = $this->page_title->show();

        $date = date("Y-m-d");
        $doctor_id = 0;

        if($this->input->post('date_appointments')){
        	$date = explode("-",$this->input->post('date_appointments'));
        	$this->data["date_appointments"] = $this->input->post('date_appointments');
        }

        if($this->input->post('doctor_id')){
        	$doctor_id = $this->input->post('doctor_id');
        	$this->data["doctor_id"] = $doctor_id;
        }

        $this->data["appointments"] = $this->appointmentsModel->get_appointments($date,$doctor_id);


        $this->data['doctors'] = $this->ion_auth->users(array('doctors','nurses'))->result();

   		$this->data["func"] = $this;
    	/* Load Template */
        $this->template->admin_render('admin/appointments/list', $this->data);

    }

    public function pending(){
    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_pending'), 'admin/appointments/pending');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $this->page_title->push(lang('pagetitle_pending'));

        $this->data['pagetitle'] = $this->page_title->show();

        $date = 0;
        $doctor_id = 0;

        if($this->input->post('date_appointments')){
        	$date = explode("-",$this->input->post('date_appointments'));
        	$this->data["date_appointments"] = $this->input->post('date_appointments');
        }

        /*if($this->input->post('doctor_id')){
        	if($this->input->post('doctor_id') !== "ALL"){
        		$doctor_id = $this->input->post('doctor_id');
        	}else{
        		$doctor_id = 0;
        	}
        }else{
        	if($this->data['is_doctor'] && $this->data['is_admin']){
            	$doctor_id = $this->ion_auth->user()->row()->id;
	        }else{
	        	$doctor_id = 0;
	        }
        }*/

        if($this->input->post('doctor_id')){

        	if($this->input->post('doctor_id') !== "ALL"){
        		$doctor_id = $this->input->post('doctor_id');
        	}else{
        		$doctor_id = 0;
        	}

        }else{
        	$doctor_id = 5;
        }

        $this->data["doctor_id"] = $doctor_id;

        $this->data["appointments"] = $this->appointmentsModel->get_appointments(0,$doctor_id,0);

        $this->data['doctors'] = $this->ion_auth->users(array('doctors','nurses'))->result();

   		$this->data["func"] = $this;
    	/* Load Template */
        $this->template->admin_render('admin/appointments/list', $this->data);

    }

    public function out($appointment_id=0)
	{
		if($appointment_id == 0) return;

		$fields = array(
	        	'out_come_flag' => 1,
	            'out_come_time' => date("Y-m-d H:i:s"),
	        );
		if($this->appointmentsModel->update_item("appointments",$fields,$appointment_id)){
			$this->session->set_flashdata('message', lang("actions_success"));
			redirect('/admin/appointments/pending', 'refresh');
		}


	}

	public function done($appointment_id=0)
	{
		if($appointment_id == 0) return;

		$fields = array(
		        	'done_flag' => 1,
		            'done_flag_time' => date("Y-m-d H:i:s"),
		        );
		if($this->appointmentsModel->update_item("appointments",$fields,$appointment_id)){
			$this->session->set_flashdata('message', lang("actions_success"));
			redirect('/admin/appointments/pending', 'refresh');
		}


	}

    public function add_appointments(){

    	if(isset($_POST) && ! empty($_POST))
		{
	        /* Main Information validate form input */
			$this->form_validation->set_rules('date_appointment', 'lang:date_appointment', 'required');
			$this->form_validation->set_rules('start_time', 'lang:time_period', 'required');
			$this->form_validation->set_rules('end_time', 'lang:time_period', 'required');
			$this->form_validation->set_rules('details', 'Uncorrect details', 'trim');

			if((int)$this->input->post('event_name') == 0 && (empty($this->input->post('new-patient-name')) || empty($this->input->post('new-patient-telephone')))){

				echo json_encode(array("message"=>(validation_errors())  ? validation_errors() : "","data" => 0));
			}

			if ($this->form_validation->run()){

				$start_date = date("Y-m-d H:i:s", strtotime($this->input->post('date_appointment')." ".$this->input->post('start_time')));

				$end_date = date("Y-m-d H:i:s", strtotime($this->input->post('date_appointment')." ".$this->input->post('end_time')));
					
		        $fields = array(
		        	'start_date' => $start_date,
		            'end_date' => $end_date,
		            'event_name' => $this->input->post('event_name'),
		            'details' => str_replace(array("'",'"'),"",(trim($this->input->post('appointment_details')))),
		            'type' => $this->input->post('type'),
		            'room' => $this->input->post('room_id'),
		            'doctor_id'=>$this->input->post('doctor_id')
		        );

			}

			if(!empty($this->input->post('event_id')) && $this->input->post('event_id') !== 0){
				if ($this->form_validation->run() == TRUE && $this->appointmentsModel->update_item("appointments",$fields,$this->input->post('event_id')))
				{	
					echo json_encode(array("message"=>lang("actions_success"),"data" => 0));
		        }else{
		        	echo json_encode(array("message"=>(validation_errors())  ? validation_errors() : "","data" => 0));
				}
	

			}else{

				$insert_patient_id = 0;

				if(!empty($this->input->post('new-patient-name')) && !empty($this->input->post('new-patient-telephone'))){

					$insert_patient_id = $this->appointmentsModel->insert_item("patients",
						array(
							'patient_name'=>$this->input->post('new-patient-name'),
							'telephone'=>$this->input->post('new-patient-telephone'),
							'doctor'=>$this->input->post('doctor_id')
						)
					);

					if($insert_patient_id !== 0){
						$fields['event_name'] = $insert_patient_id;
					}
				}


				$insert_id = $this->appointmentsModel->insert_item("appointments",$fields);

				if ($this->form_validation->run() == TRUE && $insert_id)
				{	

					echo json_encode(array("message"=>lang("actions_success"),"data" => $insert_id,"new_patient_id"=>$insert_patient_id));

		        }else{
		        	echo json_encode(array("message"=>(validation_errors())  ? validation_errors() : "","data" => 0));
				}

			}


		}else{
			echo "Error!";
		}
    }


    public function calculate_age($birthday) {
	  	$dob_day = date("d",strtotime($birthday));
		$dob_month = date("m",strtotime($birthday));
		$dob_year = date("Y",strtotime($birthday));

		$day = date("d") - $dob_day;
		$month = date("m") - $dob_month;
		$year = date("Y") - $dob_year;

		if($day < 0)
		{
			$day = 30 + $day;
			$month = $month - 1;	
		}

		if($month < 0)
		{
			$month = 12 + $month;
			$year = $year - 1;	
		}

	  return $year." year ".$month." month ".$day." days ";
	}

	public function delete()
	{
		$id = (int) $this->input->post('id');

		if ($this->appointmentsModel->delete_item("appointments",$id))
		{
        	echo 1;
		}


	}

}
?>