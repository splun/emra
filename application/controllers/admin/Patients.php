<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patients extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->model('admin/patientsModel');
        $this->load->model('admin/AppointmentsModel');
        
        $this->lang->load('admin/patients');
        $this->lang->load('admin/settings');
        $this->lang->load('admin/errors');

		/* Title Page :: Common */
	    $this->page_title->push(lang('pagetitle'));
	    $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_settings_patients'), 'admin/settings/patients');

        $this->data['message'] = (!empty($this->session->flashdata('message')) ? $this->session->flashdata('message') : "");

    }

    public function index()
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

        $this->data['patient_name'] = array(
			'name'  => 'patient_name',
			'id'    => 'patient_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name',$this->input->post('patient_name'))
		);

		$this->data['civil_id'] = array(
			'name'  => 'civil_id',
			'id'    => 'civil_id',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('civil_id',$this->input->post('civil_id'))
		);

		$this->data['file_no'] = array(
			'name'  => 'file_no',
			'id'    => 'file_no',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file_no',$this->input->post('file_no'))
		);
		$this->data['telephone'] = array(
			'name'  => 'telephone',
			'id'    => 'telephone',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('telephone',$this->input->post('telephone'))
		);
		$this->data['referral'] = array(
			'name'  => 'referral',
			'id'    => 'referral',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('referral',$this->input->post('referral'))
		);
		$this->data['immuno'] = array(
			'name'  => 'immuno',
			'id'    => 'immuno',
			'type'  => 'checkbox',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('immuno')
		);

		$this->data['doctors'] = $this->ion_auth->users('doctors')->result();
		$this->data['insurances'] = $this->patientsModel->get_items("insurance",array("status"=>1));

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, lang('pagetitle'), 'admin/patients/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


        if($this->input->get('immuno') && $this->input->get('immuno') == 1){
        	$this->data['items'] = $this->patientsModel->get_patients(array("immuno" => 1));
        }else{


        	if($this->input->get('search') && $this->input->get('search') !==""){
        		$this->data['items'] = $this->patientsModel->search_patients(strtolower(strip_tags($this->input->get('search'))));
        	}else{
				$this->data['items'] = $this->patientsModel->get_patients();
        	}

        }
       
        $this->data["func"] = $this;
        /* Load Template */
        $this->template->admin_render('admin/patients/list', $this->data);
        }
    }

    function get_patient_last_immuno_visit($patient_id = 0){
    	if($patient_id == 0) return "---";
    	$visit_array = $this->patientsModel->get_immuno_procedure_last_record_by_patient($patient_id);
        if($visit_array){
            return $visit_array->date_vacc."<br /> Type:".$visit_array->type;
        }else{
            return "---";
        }
    }

	public function filter(){

    	$filter = 0;
    	$join = "";

	    if($this->input->post()){

			$filter = array();

			if(!empty($this->input->post('patient_name'))){
				$filter += array('patients.patient_name LIKE' => '%'.$this->input->post('patient_name').'%');
			}
			if(!empty($this->input->post('civil_id'))){
				$filter += array('patients.civil_id' => $this->input->post('civil_id'));
			}
			if(!empty($this->input->post('file_no'))){
				$filter += array('patients.id' => $this->input->post('file_no'));
			}
			if(!empty($this->input->post('doctor'))){
				$filter += array('patients.doctor' => $this->input->post('doctor'));
			}
			if(!empty($this->input->post('telephone'))){
				$filter += array('patients.telephone' => $this->input->post('telephone'));
			}

			if(!empty($this->input->post('insurance'))){
				$filter += array('patients.ddl_insurance' => $this->input->post('insurance'));
			}

			if(!empty($this->input->post('referral'))){
				$filter += array('patients.hear LIKE' => '%'.$this->input->post('referral').'%');
			}

			if($this->input->post('immuno') && $this->input->post('immuno') == 1){

				$filter += array("immuno" => 1);

				if($this->input->post('immuno_sc')){
					$filter += array("immunotherapy_procedure_plan.vaccine_sc !=" => "", "immunotherapy_procedure_plan.vaccine_sc !=" => "N;");
				}

				if($this->input->post('immuno_sl')){
					$filter += array("immunotherapy_procedure_plan.vaccine_sl !=" => "", "immunotherapy_procedure_plan.vaccine_sl !=" => "N;");
				}

			}

		}


	    /* Get All Items */
		$items = $this->patientsModel->get_patients($filter);

	    /* Ajax Response */
	    echo json_encode($items);

    }

	public function add()
	{

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('pagetitle_add'), 'admin/patients/add/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('patient_name', 'lang:patient_name_label', 'required');
		$this->form_validation->set_rules('telephone', 'lang:patient_phone_label', 'required');
		$this->form_validation->set_rules('doctor', 'lang:patient_doctor_label', 'required');

		if ($this->form_validation->run()){

			$file1="";
			$Attach1 = $_FILES['file01']['name']; 
			if(trim($Attach1) != ""){
			  $file1 = date('Ymd').$patient_id.$_FILES["file01"]["name"]; 
			}

			$file2="";
			$Attach2 = $_FILES['file02']['name']; 
			if(trim($Attach2) != ""){
			  $file2 = date('Ymd').$patient_id.$_FILES["file02"]["name"];
			}

			$file3="";
			$Attach3 = $_FILES['file03']['name']; 
			if(trim($Attach3) != ""){
			  $file3 = date('Ymd').$patient_id.$_FILES["file03"]["name"];
			}

	        $fields = array(
	        	'civil_id' => $this->input->post('civil_id'),
	            'patient_name' => $this->input->post('patient_name'),
	            'patient_name_ar' => $this->input->post('patient_name_ar'),
	            'dob' => $this->input->post('dob'),
	            'b_weight' => $this->input->post('b_weight'),
				'gender' => $this->input->post('gender'),
				'governance' => $this->input->post('governance'),
				'address' => $this->input->post('address'),
				'parents_name' => $this->input->post('parents_name'),
				'job' => $this->input->post('job'),
	            'telephone' => $this->input->post('telephone'),
	            'mobile' => $this->input->post('mobile'),
	            'mobile2' => $this->input->post('mobile2'),
	            'email' => $this->input->post('email'),
	            'ddl_insurance' => $this->input->post('insurance_company'),
	            'insurance_number' => $this->input->post('insurance_number'),
	            'file01' => $file1,
	            'file02' => $file2,
	            'file03' => $file3,
	            'hear' => $this->input->post('referral'),
	            'notes' => $this->input->post('notes'),
	            'doctor' => $this->input->post('doctor')
	        );
		}


		if ($this->form_validation->run() == TRUE )
		{	
			$insert_id = $this->patientsModel->insert_item("patients",$fields);
			if ($insert_id){
				if($file1 != ""){
					move_uploaded_file($_FILES["file01"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file1); 
				}

				if($file2 != ""){
				 	move_uploaded_file($_FILES["file02"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file2); 

				}

				if($file3 != ""){
				 	move_uploaded_file($_FILES["file03"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file3); 
				}

	            redirect('admin/patients/card/'.$insert_id, 'refresh');

        	}else{
				$this->data['message'] = (validation_errors()  ? validation_errors() : "");
        	}
		}
		else
		{

        $this->data['message'] = (validation_errors()  ? validation_errors() : "");

        $this->data['civil_id'] = array(
			'name'  => 'civil_id',
			'id'    => 'civil_id',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('civil_id')
		);

        $this->data['patient_name'] = array(
			'name'  => 'patient_name',
			'id'    => 'patient_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name')
		);

		$this->data['patient_name_ar'] = array(
			'name'  => 'patient_name_ar',
			'id'    => 'patient_name_ar',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name_ar')
		);

        $this->data['dob'] = array(
			'name'  => 'dob',
			'id'    => 'dob',
			'type'  => 'date',
			'data-index'  => '8',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('dob')
		);

        $this->data['b_weight'] = array(
			'name'  => 'b_weight',
			'id'    => 'b_weight',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('b_weight')
		);

        $this->data['address'] = array(
			'name'  => 'address',
			'id'    => 'address',
			'type'  => 'text',
			'rows'	=> 3,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('address')
		);

		$this->data['parents_name'] = array(
			'name'  => 'parents_name',
			'id'    => 'parents_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('parents_name')
		);

		$this->data['job'] = array(
			'name'  => 'job',
			'id'    => 'job',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('job')
		);

		$this->data['telephone'] = array(
			'name'  => 'telephone',
			'id'    => 'telephone',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('telephone')
		);

		$this->data['mobile'] = array(
			'name'  => 'mobile',
			'id'    => 'mobile',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('mobile')
		);

		$this->data['mobile2'] = array(
			'name'  => 'mobile2',
			'id'    => 'mobile2',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('mobile2')
		);

		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('email')
		);

		$this->data['insurance_number'] = array(
			'name'  => 'insurance_number',
			'id'    => 'insurance_number',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('insurance_number')
		);
		
		$this->data['file01'] = array(
			'name'  => 'file01',
			'id'    => 'file01',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file01')
		);
		$this->data['file02'] = array(
			'name'  => 'file02',
			'id'    => 'file02',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file02')
		);
		$this->data['file03'] = array(
			'name'  => 'file03',
			'id'    => 'file03',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file03')
		);

		$this->data['referral'] = array(
			'name'  => 'referral',
			'id'    => 'referral',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('referral')
		);

		$this->data['notes'] = array(
			'name'  => 'notes',
			'id'    => 'notes',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes')
		);

		$this->data['gender'] = array(1=>'Male',2=>'Female');
		$this->data['doctors'] = $this->ion_auth->users('doctors')->result();
		$this->data['insurances'] = $this->patientsModel->get_items("insurance",array("status"=>1));

        /* Load Template */
        $this->template->admin_render('admin/patients/create', $this->data);
        }
	}


	
	public function card($patient_id,$date_appointment = 0)
    {
        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

		if($date_appointment == 0){
			$date_appointment = date("Y-m-d");
		}

        /**********Patient Data***********/
		$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

        $this->data['civil_id'] = array(
			'name'  => 'civil_id',
			'id'    => 'civil_id',
			'type'  => 'number',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('civil_id',$patient->civil_id)
		);

        $this->data['patient_name'] = array(
			'name'  => 'patient_name',
			'id'    => 'patient_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name',$patient->patient_name)
		);
		$this->data['patient_name_ar'] = array(
			'name'  => 'patient_name_ar',
			'id'    => 'patient_name_ar',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('patient_name_ar',$patient->patient_name_ar)
		);

        $this->data['dob'] = array(
			'name'  => 'dob',
			'id'    => 'dob',
			'type'  => 'date',
			'data-index'  => '8',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('dob',$patient->dob)
		);

        $this->data['b_weight'] = array(
			'name'  => 'b_weight',
			'id'    => 'b_weight',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('b_weight',$patient->b_weight)
		);

        $this->data['address'] = array(
			'name'  => 'address',
			'id'    => 'address',
			'type'  => 'text',
			'rows'	=> 3,
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('address',$patient->address)
		);

		$this->data['parents_name'] = array(
			'name'  => 'parents_name',
			'id'    => 'parents_name',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('parents_name',$patient->parents_name)
		);

		$this->data['job'] = array(
			'name'  => 'job',
			'id'    => 'job',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('job',$patient->job)
		);

		$this->data['telephone'] = array(
			'name'  => 'telephone',
			'id'    => 'telephone',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('telephone',$patient->telephone)
		);

		$this->data['mobile'] = array(
			'name'  => 'mobile',
			'id'    => 'mobile',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('mobile',$patient->mobile)
		);

		$this->data['mobile2'] = array(
			'name'  => 'mobile2',
			'id'    => 'mobile2',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('mobile2',$patient->mobile2)
		);

		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('email',$patient->email)
		);

		$this->data['insurance_number'] = array(
			'name'  => 'insurance_number',
			'id'    => 'insurance_number',
			'type'  => 'text',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('insurance_number',$patient->insurance_number)
		);
		
		$this->data['file01'] = array(
			'name'  => 'file01',
			'id'    => 'file01',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file01')
		);
		$this->data['file02'] = array(
			'name'  => 'file02',
			'id'    => 'file02',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file02')
		);
		$this->data['file03'] = array(
			'name'  => 'file03',
			'id'    => 'file03',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file03')
		);

		$this->data['multiple_files'] = array(
			'name'  => 'multiple_files[]',
			'id'    => 'multiple_files',
			'type'  => 'file',
			'multiple' => "multiple",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('multiple_files')
		);

		$this->data['referral'] = array(
			'name'  => 'referral',
			'id'    => 'referral',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('referral',$patient->hear)
		);

		$this->data['notes'] = array(
			'name'  => 'notes',
			'id'    => 'notes',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes',$patient->notes)
		);

        $this->data['patient'] = $patient;
        $this->data['patient_year'] = $this->calculate_age($patient->dob);
		$this->data['gender'] = array(1=>'Male',2=>'Female');
		
		$this->data['doctors'] = $this->ion_auth->users('doctors')->result();
		$this->data['insurances'] = $this->patientsModel->get_items("insurance",array("status"=>1));

		$this->data['multiple_report_files'] = $this->patientsModel->get_multiple_reports_files($patient_id);

        /**********Nurse Record***********/
        $nurse_records = $this->patientsModel->get_items("nurse",array("patient_id"=>$patient_id));

        $this->data['nurse_records'] = $nurse_records;
		

		$appointment_current_doctor = $this->appointmentsModel->get_appointment($patient,date("Y-m-d",strtotime($date_appointment)));
		//$check_group = $this->ion_auth->get_users_groups($appointment_current_doctor->doctor_id)->row()->id;
		
		if($appointment_current_doctor && $appointment_current_doctor->doctor_id && $appointment_current_doctor->doctor_id != 8){
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$appointment_current_doctor->doctor_id));
        }else{
            $this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
        }

		/*********Prescription and Drug allergy***********/
		$this->data['prescriptions'] = $this->patientsModel->get_patient_prescriptions($patient->id);
		$this->data['drug_allergy'] = $this->patientsModel->get_patient_drug_allergy($patient->id,30);

		/*********Diagnosis****************/
		$this->data['diagnosis'] = $this->patientsModel->get_patient_diagnosis($patient->id,2);

		/*********Descriptions****************/
		$this->data['descriptions'] = $this->patientsModel->get_patient_descriptions($patient->id,2);

		/*********Exam Photos****************/
		$examphotos_records = $this->patientsModel->get_examphoto_files($patient_id);

		$this->data['examphotos'] = array();

		foreach($examphotos_records as $examphotos_record){

			$exfiles = array();

			$examphoto_files = $this->get_active_files_by_section($patient_id,$examphotos_record->date_file,55);

			foreach($examphoto_files as $file){
				$exfiles[] = $file->fname;
			}

			$exam_description = $this->patientsModel->get_patient_descriptions($patient_id,55,$examphotos_record->date_file);

			$this->data['examphotos'][] = array(
				"date_test" => $examphotos_record->date_file,
				"desc" => ($exam_description) ? $exam_description->desc : "",
				"files" => $exfiles
			);
		}

		/*********Spirometry****************/
		$spirometry_records = $this->patientsModel->get_patient_last_spirometry($patient_id);
		$spirometry_files =  $this->patientsModel->get_spirometry_files($patient_id);
		$this->data['spirometry'] = array();

		foreach($spirometry_records as $spirometry_record){

			$spirometers = "";
			$spirometer_items = $this->patientsModel->get_patient_diagnosis($patient_id,18,$spirometry_record->date_add);
			foreach($spirometer_items as $sp_items){
				$spirometers .= $sp_items->name_diagnos."; ";
			}

			$spfiles = array();
			$spirometry_files = $this->get_active_files_by_section($patient_id,$spirometry_record->date_add,18);
			foreach($spirometry_files as $file){
				$spfiles[] = $file->fname;
			}

			$spirometry_description = $this->patientsModel->get_patient_descriptions($patient_id,18,$spirometry_record->date_add);

			$this->data['spirometry'][] = array(
				"date_test" => $spirometry_record->date_add,
				"desc" => ($spirometry_description) ? $spirometry_description->desc : "",
				"spirometers" => $spirometers,
				"files" => $spfiles
			);
		}


		foreach($spirometry_files as $spirometry_file){

			$spfile = array();

			$spirometry_description = $this->patientsModel->get_patient_descriptions($patient_id,18,$spirometry_file->date_file);

			$spfile[0] = $spirometry_file->fname;

			$this->data['spirometry'][] = array(
				"date_test" => $spirometry_file->date_file,
				"desc" => ($spirometry_description) ? $spirometry_description->desc : "",
				"spirometers" => "",
				"files" => $spfile
			);
		}


		/*********Sick Leave****************/
		$this->data['sickleaves_records'] = $this->patientsModel->get_items("patients_sick_leave",array("patient_id"=>$patient_id),"date_add","DESC");

		/************Immunoteraphy************/
		$iplan = $this->patientsModel->get_immunoteraphy_plan($patient->id);

		$this->data['date_consented'] = array(
			'name'  => 'date_consented',
			'id'    => 'notes',
			'type'  => 'date',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes',($iplan) ? $iplan->date_consented : "")
		);

		$this->data['txt_remark'] = array(
			'name'  => 'txt_remark',
			'id'    => 'txt_remark',
			'type'  => 'text',
			'rows' 	=> "5",
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('notes', ($iplan) ? $iplan->txt_remark : "")
		);

		$this->data['immuno_file01'] = array(
			'name'  => 'file1',
			'id'    => 'file1',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file01')
		);
		$this->data['immuno_file02'] = array(
			'name'  => 'file2',
			'id'    => 'file2',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file02')
		);
		$this->data['immuno_file03'] = array(
			'name'  => 'file3',
			'id'    => 'file3',
			'type'  => 'file',
            'class' => 'form-control',
			'value' => $this->form_validation->set_value('file03')
		);

		$this->data['immuno_diagnosis'] = $this->patientsModel->get_items("immunotherapy_diagnosis");

		$this->data['iplan'] = $iplan;
		$this->data['enviropments'] = $this->patientsModel->get_patient_diagnosis($patient->id,3);
		
		$this->data['stock_medications'] = $this->patientsModel->get_items("stock_medications");

		$this->data['vaccines_sc'] = 0;
		$this->data['vaccines_sl'] = 0;

		if($iplan){
			$this->data['vaccines_sc'] = ($iplan->vaccine_sc) ? unserialize($iplan->vaccine_sc) : 0;

			$this->data['vaccines_sl'] = ($iplan->vaccine_sl) ? unserialize($iplan->vaccine_sl) : 0;

		/**********Immunoteraphy Procedure Records***********/

			$this->data['procedure_records'] = $this->patientsModel->get_immunotherapy_procedure_records($iplan->id);
		}else{
			$this->data['procedure_records'] = "";
		}

		/************Results************/
		//$this->data['result_files'] = $this->patientsModel->get_results_files($patient->id);
		$this->data['result_dates'] = $this->patientsModel->get_results_files_dates($patient->id);
		
	    $this->data['result_sections']= array(
            101 => "Bloods",
            102 => "Radiology",
            103 => "Culture",
            104 => "Immune",
            105 => "Allergy",
            106 => "Urine"
        );

		/************Visit History************/
		$this->data['history'] = $this->patientsModel->get_visit_history($patient->id);

		/************Referrals History************/
		$this->data['referrals_history'] = $this->patientsModel->get_last_referrals($patient->id);

		/************Referrals History************/
		$this->data['report_files'] = $this->patientsModel->get_patient_report_files($patient->id);

		/************Invoices************/
		$this->data['invoices'] = $this->patientsModel->get_invoices_groupby_date($patient->id);


        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, $this->data['patient']->patient_name, 'admin/patients/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		$this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['date_appointment'] = $date_appointment;
		$this->data['func'] = $this;

		$this->data['user'] = $this->patientsModel->get_items("users", array("id"=> $this->ion_auth->logged_in()));
        /* Load Template */
        $this->template->admin_render('admin/patients/card', $this->data);

        }
    }

    public function get_customer_pay_indate($patient_id,$date_appointment){
    	return $this->patientsModel->get_patient_pays($patient_id,$date_appointment);
	}
	
	public function get_invoice_doctor($doctor_id=0){
		$doctor_info = $this->patientsModel->get_items("users",array("id"=>$doctor_id));
		if($doctor_info){
			return "Dr. ".$doctor_info->first_name." ".$doctor_info->last_name;
		}else{
			return "";
		}
	}

    public function immunoteraphy_print($patient_id)
    {	
    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }

        /**********Patient Data***********/
		$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

    	$this->data['patient'] = $patient;
        $this->data['patient_year'] = $this->calculate_age($patient->dob);
		$this->data['gender'] = array(1=>'Male',2=>'Female');
		$this->data['insurance'] = $this->patientsModel->get_items("insurance",array("id"=>$patient->ddl_insurance));

		$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
		
		/************Immunoteraphy************/
		$iplan = $this->patientsModel->get_immunoteraphy_plan($patient->id);

		$this->data['diagnos'] = $this->patientsModel->get_items("immunotherapy_diagnosis",array("id"=>$iplan->diagnosis));

		$this->data['iplan'] = $iplan;
		$this->data['enviropments'] = $this->patientsModel->get_patient_diagnosis($patient->id,3);
		$this->data['stock_medications'] = $this->patientsModel->get_items("stock_medications");

		$this->data['vaccines_sc'] = 0;
		$this->data['vaccines_sl'] = 0;

		if($iplan){
			$this->data['vaccines_sc'] = ($iplan->vaccine_sc) ? unserialize($iplan->vaccine_sc) : 0;

			$this->data['vaccines_sl'] = ($iplan->vaccine_sl) ? unserialize($iplan->vaccine_sl) : 0;

		/**********Immunoteraphy Procedure Records***********/

			$this->data['procedure_records'] = $this->patientsModel->get_immunotherapy_procedure_records($iplan->id);
		}else{
			$this->data['procedure_records'] = "";
		}


        /* Breadcrumbs */
        $this->breadcrumbs->unshift(1, $this->data['patient']->patient_name, 'admin/patients/');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
		
		$this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		$this->data['func'] = $this;
		$this->data['user'] = $this->patientsModel->get_items("users", array("id"=> $this->ion_auth->logged_in()));

        /* Load Template */
        $this->template->admin_render('admin/patients/print_immunoteraphy', $this->data);

    }

    public function calculate_age($birthday,$date_appointment = 0) {
  
        $dob_day = date("d",strtotime($birthday));
        $dob_month = date("m",strtotime($birthday));
        $dob_year = date("Y",strtotime($birthday));

        if($date_appointment !== 0){
            $day = date("d",strtotime($date_appointment)) - $dob_day;
            $month = date("m",strtotime($date_appointment)) - $dob_month;
            $year = date("Y",strtotime($date_appointment)) - $dob_year;
        }else{
            $day = date("d") - $dob_day;
            $month = date("m") - $dob_month;
            $year = date("Y") - $dob_year;
        }

        if($day < 0)
        {
            $day = 30 + $day;
            $month = $month - 1;    
        }

        if($month < 0)
        {
            $month = 12 + $month;
            $year = $year - 1;  
        }

      return $year." year ".$month." month ".$day." days ";
    }

    public function main_edit($patient_id){

    	if(isset($patient_id) && isset($_POST) && ! empty($_POST))
		{
	        /* Main Information validate form input */
			$this->form_validation->set_rules('patient_name', 'lang:patient_name_label', 'required');
			$this->form_validation->set_rules('telephone', 'lang:patient_phone_label', 'required');
			$this->form_validation->set_rules('doctor', 'lang:patient_doctor_label', 'required');

			if ($this->form_validation->run()){


		        $fields = array(
		        	'civil_id' => $this->input->post('civil_id'),
		            'patient_name' => $this->input->post('patient_name'),
		            'patient_name_ar' => $this->input->post('patient_name_ar'),
		            'dob' => $this->input->post('dob'),
		            'b_weight' => $this->input->post('b_weight'),
					'gender' => $this->input->post('gender'),
					'governance' => $this->input->post('governance'),
					'address' => $this->input->post('address'),
					'parents_name' => $this->input->post('parents_name'),
					'job' => $this->input->post('job'),
		            'telephone' => $this->input->post('telephone'),
		            'mobile' => $this->input->post('mobile'),
		            'mobile2' => $this->input->post('mobile2'),
		            'email' => $this->input->post('email'),
		            'ddl_insurance' => $this->input->post('insurance_company'),
		            'insurance_number' => $this->input->post('insurance_number'),
		            'hear' => $this->input->post('referral'),
		            'notes' => $this->input->post('notes'),
		            'doctor' => $this->input->post('doctor')
		        );

			}

			if($this->input->post("delete_file_1")){
			$path1 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_1");
			if(file_exists($path1)){
				unlink($path1);
			}
			$fields += array('file01' => "");
			}
		

			$file1="";
			$Attach1 = $_FILES['file01']['name']; 
			if(trim($Attach1) != ""){
			  $file1 = date('Ymd').$patient_id.$_FILES["file01"]["name"];
			  $fields += array('file01' => $file1); 
			}

			if($this->input->post("delete_file_2")){
				$path2 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_2");
				if(file_exists($path2)){
					unlink($path2);
				}
				$fields += array('file02' => "");
			}

			$file2="";
			$Attach2 = $_FILES['file02']['name']; 
			if(trim($Attach2) != ""){
			  $file2 = date('Ymd').$patient_id.$_FILES["file02"]["name"];
			  $fields += array('file02' => $file2); 
			}

			if($this->input->post("delete_file_3")){
				$path3 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_3");
				if(file_exists($path3)){
					unlink($path3);
				}
				$fields += array('file03' => "");
			}

			$file3="";
			$Attach3 = $_FILES['file03']['name']; 
			if(trim($Attach3) != ""){
			  $file3 = date('Ymd').$patient_id.$_FILES["file03"]["name"];
			  $fields += array('file03' => $file3); 
			}


	        if($this->input->post("delete_multiple_files")){
	        	$delete_file = $this->input->post("delete_multiple_files");
	            $path = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_multiple_files");
	            if(file_exists($path)){
	                unlink($path);
	            }
	            $this->patientsModel->delete_item("files",$delete_file);
	            $this->patientsModel->delete_items("files_to_patient",array("file_id"=>$delete_file));
	        }


			if(!empty($_FILES['multiple_files']["name"][0])){

                $AttachMultiple = $_FILES['multiple_files']['name'];

                foreach($AttachMultiple as $key=>$val){

                    if($val !== ""){ $file_name="";
                	
                        $file_name = date('Ymd').$patient_id.$val; 

                        $file_fields = array(
                            'name' => $file_name,
                            'section' => 999,
                        );

                        $fileId = $this->patientsModel->insert_item("files",$file_fields);

                        if($fileId && $file_name != ""){
                            move_uploaded_file($_FILES["multiple_files"]["tmp_name"][$key], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file_name); 

                            $file_to_patient_fields = array(
                                'patient_id' => $patient_id,
                                'file_id' => $fileId,
                                'date_file' => date("Y-m-d"),
                                'section'=>999
                            );

                            $this->patientsModel->insert_item("files_to_patient",$file_to_patient_fields);

                        }

                    }
                }
            }


			if ($this->form_validation->run() == TRUE && $this->patientsModel->update_item("patients",$fields,$patient_id))
			{	
				if($file1 != ""){
					move_uploaded_file($_FILES["file01"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file1); 
				}
				if($file2 != ""){
				 	move_uploaded_file($_FILES["file02"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file2); 

				}
				if($file3 != ""){
				 	move_uploaded_file($_FILES["file03"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file3); 
				}
				
				$this->session->set_flashdata('message', lang("actions_success"));

	        }else{
	        	$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
			}


			redirect('admin/patients/card/'.$patient_id.'#main-block', 'refresh');
		}
    }

	public function nurse_record_edit($record_id){

		if ( ! $this->ion_auth->logged_in()){
            redirect('auth/login', 'refresh');
        }else{

        	if(!$record_id){
	  			redirect('admin/patient/card/'.$patient_id."/#nurse-block", 'refresh');
	  		}

	  		/* Main Information validate form input */
			//$this->form_validation->set_rules('weight', 'lang:nurse_records_weight', 'required');
			//$this->form_validation->set_rules('height', 'lang:nurse_records_height', 'required');

			$this->form_validation->set_rules('date_visit', 'lang:nurse_records_datev', 'required');

	        if($this->form_validation->run() && isset($_POST) && ! empty($_POST)){

		        $fields = array(
		        	'date_visit' => $this->input->post('date_visit'),
		        	'height' => $this->input->post('height'),
		            'weight' => $this->input->post('weight'),
		            'heart' => $this->input->post('heart'),
		            'blood' => $this->input->post('blood'),
					'respiratory' => $this->input->post('respiratory'),
					'temp' => $this->input->post('temp'),
					'Circumference' => $this->input->post('circumference'),
					'bmi' => $this->input->post('bmi'),
		            'sao' => $this->input->post('sao'),
		            'desc' => $this->input->post('comments')
		        );

				if ($this->form_validation->run() == TRUE && $this->patientsModel->update_item("nurse",$fields,$record_id))
				{	
					$this->session->set_flashdata('message', lang("actions_success"));
					redirect('admin/patients/card/'.$this->input->post('patient_id').'/#nurse-block', 'refresh');
		        }else{
		        	$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
		        	redirect('admin/patients/nurse_record_edit/'.$record_id, 'refresh');
				}

			}else{

				$nurse_record = $this->patientsModel->get_items("nurse",array("id"=>$record_id));

				if(!$nurse_record){
					redirect('admin/patients/card/'.$this->input->post('patient_id').'/#nurse-block', 'refresh');
				}

				$patient = $this->patientsModel->get_items("patients",array("id"=>$nurse_record->patient_id));

				$this->data['date_visit'] = array(
				'name'  => 'date_visit',
				'id'    => 'date_visit',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_visit',$nurse_record->date_visit)
				);
				$this->data['height'] = array(
				'name'  => 'height',
				'id'    => 'height',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('height',$nurse_record->height)
				);
				$this->data['weight'] = array(
				'name'  => 'weight',
				'id'    => 'weight',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('weight',$nurse_record->weight)
				);
				$this->data['heart'] = array(
				'name'  => 'heart',
				'id'    => 'heart',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('heart',$nurse_record->heart)
				);
				$this->data['blood'] = array(
				'name'  => 'blood',
				'id'    => 'blood',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('blood',$nurse_record->blood)
				);

				$this->data['respiratory'] = array(
				'name'  => 'respiratory',
				'id'    => 'respiratory',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('respiratory',$nurse_record->respiratory)
				);
				$this->data['temp'] = array(
				'name'  => 'temp',
				'id'    => 'temp',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('temp',$nurse_record->temp)
				);
				$this->data['circumference'] = array(
				'name'  => 'circumference',
				'id'    => 'circumference',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('circumference', $nurse_record->Circumference)
				);
				$this->data['bmi'] = array(
				'name'  => 'bmi',
				'id'    => 'bmi',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('bmi',$nurse_record->bmi)
				);
				
				$this->data['sao'] = array(
				'name'  => 'sao',
				'id'    => 'sao',
				'type'  => 'number',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('sao',$nurse_record->sao)
				);
				$this->data['comments'] = array(
				'name'  => 'comments',
				'id'    => 'comments',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('comments',$nurse_record->desc)
				);

				$this->data['record'] = $nurse_record;
				$this->data['patient'] = $patient;
				$this->data['gender'] = array(1=>'Male',2=>'Female');
				$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
		

				$this->data['pagetitle'] = '<h1>'.lang('nurse_record_title').'</h1>';

				/* Breadcrumbs */
				$this->breadcrumbs->unshift(1, lang('nurse_record_title'), 'admin/patients/');
		        $this->breadcrumbs->unshift(2, $this->data['patient']->patient_name, 'admin/patients/');

		        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

				/* Load Template */
				$this->template->admin_render('admin/patients/nurse_record_edit', $this->data);
			}
		}
    }

	public function nurse_record_add($patient_id,$date_appointment = 0){

		if ( ! $this->ion_auth->logged_in()){
            redirect('auth/login', 'refresh');
        }else{

        	/* Main Information validate form input */
			/*$this->form_validation->set_rules('weight', 'lang:nurse_records_weight', 'required');
			$this->form_validation->set_rules('height', 'lang:nurse_records_height', 'required');
			$this->form_validation->set_rules('heart', 'lang:nurse_records_heart', 'required');
			$this->form_validation->set_rules('temp', 'lang:nurse_records_temp', 'required');*/
			$this->form_validation->set_rules('date_visit', 'lang:nurse_records_datev', 'required');

	        if($this->form_validation->run() && isset($_POST) && ! empty($_POST)){


		        $fields = array(
		        	'patient_id' => $patient_id,
		        	'date_visit' => $this->input->post('date_visit'),
		        	'height' => $this->input->post('height'),
		            'weight' => $this->input->post('weight'),
		            'heart' => $this->input->post('heart'),
		            'blood' => $this->input->post('blood'),
					'respiratory' => $this->input->post('respiratory'),
					'temp' => $this->input->post('temp'),
					'Circumference' => $this->input->post('circumference'),
					'bmi' => $this->input->post('bmi'),
		            'sao' => $this->input->post('sao'),
		            'desc' => $this->input->post('comments')
		        );


				if ($this->form_validation->run() == TRUE && $this->patientsModel->insert_item("nurse",$fields))
				{	

					if($date_appointment !== 0){
						$this->AppointmentsModel->updateAppointmentInCome($date_appointment,$patient_id);
					}

					$this->session->set_flashdata('message', lang("actions_success"));
					redirect('admin/patients/card/'.$patient_id.'/#nurse-block', 'refresh');
		        }else{
		        	$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
		        	redirect('admin/patients/nurse_record_edit/'.$record_id, 'refresh');
				}

			}else{

				$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

				$this->data['date_visit'] = array(
				'name'  => 'date_visit',
				'id'    => 'date_visit',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_visit', ($date_appointment !== 0) ? date("Y-m-d",strtotime($date_appointment)) : "")
				);
				$this->data['height'] = array(
				'name'  => 'height',
				'id'    => 'height',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('height')
				);
				$this->data['weight'] = array(
				'name'  => 'weight',
				'id'    => 'weight',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('weight')
				);
				$this->data['heart'] = array(
				'name'  => 'heart',
				'id'    => 'heart',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('heart')
				);
				$this->data['blood'] = array(
				'name'  => 'blood',
				'id'    => 'blood',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('blood')
				);

				$this->data['respiratory'] = array(
				'name'  => 'respiratory',
				'id'    => 'respiratory',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('respiratory')
				);
				$this->data['temp'] = array(
				'name'  => 'temp',
				'id'    => 'temp',
				'type'  => 'number',
				'step'  => '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('temp')
				);
				$this->data['circumference'] = array(
				'name'  => 'circumference',
				'id'    => 'circumference',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('circumference')
				);
				$this->data['bmi'] = array(
				'name'  => 'bmi',
				'id'    => 'bmi',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('bmi')
				);
				
				$this->data['sao'] = array(
				'name'  => 'sao',
				'id'    => 'sao',
				'type'  => 'number',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('sao')
				);
				$this->data['comments'] = array(
				'name'  => 'comments',
				'id'    => 'comments',
				'type'  => 'text',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('comments')
				);

				$this->data['patient'] = $patient;
				$this->data['gender'] = array(1=>'Male',2=>'Female');
				$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

				$this->data['pagetitle'] = '<h1>'.lang('nurse_record_add').'</h1>';

				/* Breadcrumbs */
				$this->breadcrumbs->unshift(1, lang('nurse_record_add'), 'admin/patients/');
		        $this->breadcrumbs->unshift(2, $patient->patient_name, 'admin/patients/');

		        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		        $this->data['message'] = (validation_errors()  ? validation_errors() : $this->data['message']);

		        $this->data['date_appointment'] = $date_appointment;
				/* Load Template */
				$this->template->admin_render('admin/patients/nurse_record_add', $this->data);
			}
		}
    }

    public function nurse_record_skip($patient_id=0,$date_visit = 0){

		if ( ! $this->ion_auth->logged_in()){
            redirect('auth/login', 'refresh');
        }else{

	        if($date_visit !== 0 && $patient_id !== 0){


		        $fields = array(
		        	'patient_id' => $patient_id,
		        	'date_visit' => $date_visit,
		        );


				if ($this->patientsModel->insert_item("nurse",$fields))
				{	

					if($date_visit !== 0){
						$this->AppointmentsModel->updateAppointmentInCome($date_visit,$patient_id);
					}

					
					if($this->ion_auth->is_admin() || $this->data['is_reception']){

						$dokid = 0;

					}else{
						$dokid = $this->ion_auth->user()->row()->id;
					}
						
					$this->session->set_flashdata('message', lang("actions_success"));

					if($this->input->get('tooday')){

						redirect('admin/appointments/tooday', 'refresh');
					}else{
						redirect('admin/dashboard/?date_appointments='.date("d-m-Y",strtotime($date_visit)).'&doctor='.$dokid, 'refresh');
					}

		        }else{
		        	$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
		        	redirect('admin/patients/nurse_record_edit/'.$record_id, 'refresh');
				}

			}
		}
    }
    public function add_immunoteraphy_plan($patient_id){

    	//if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
    	if ( ! $this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}

		if(!$patient_id){
			redirect('admin/patients/', 'refresh');
		}

    	$patient_id = (int) $patient_id;

    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), '/admin/patients/add_immunoteraphy_plan/'.$patient_id );
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


		$file1="";
		$Attach1 = $_FILES['file1']['name']; 
		if(trim($Attach1) != ""){
		  $file1 = date('Ymd').$patient_id.$_FILES["file1"]["name"]; 
		}

		$file2="";
		$Attach2 = $_FILES['file2']['name']; 
		if(trim($Attach2) != ""){
		  $file2 = date('Ymd').$patient_id.$_FILES["file2"]["name"];
		}

		$file3="";
		$Attach3 = $_FILES['file3']['name']; 
		if(trim($Attach3) != ""){
		  $file3 = date('Ymd').$patient_id.$_FILES["file3"]["name"];
		}

        $fields = array(
           'patient_id' => $patient_id,
           'diagnosis' => $this->input->post('diagnosis'),
           'vaccine_sc' => serialize($this->input->post('vaccine_sc')),
           'vaccine_sl' => serialize($this->input->post('vaccine_sl')),
           'txt_remark' => $this->input->post('txt_remark'),
           'date_consented' => ($this->input->post('date_consented')) ? $this->input->post('date_consented') : date("Y-m-d"),
           'start_date' => date("Y-m-d"),
           'flag' => 26,
           'file1' => $file1,
           'file2' => $file2,
           'file3' => $file3
        );


		if ($this->patientsModel->insert_item("immunotherapy_procedure_plan",$fields))
		{
 
			if($file1 != "")
			{
				move_uploaded_file($_FILES["file1"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file1); 
			}

			if($file2 != "")
			{
			 	move_uploaded_file($_FILES["file2"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file2); 

			}

			if($file3 != "")
			{
			 	move_uploaded_file($_FILES["file3"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file3); 
			}

			$this->patientsModel->update_item("patients",array("notes"=>$this->input->post('notes')), $patient_id);

            $this->session->set_flashdata('message', lang("actions_success"));

		}else{

			$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
		}

		redirect('admin/patients/card/'.$patient_id.'#immunoteraphy-block', 'refresh');
		
    }

    public function edit_immunoteraphy_plan($plan_id,$patient_id){
    	//if ( ! $this->ion_auth->logged_in() OR !$this->ion_auth->is_admin() )
    	if ( ! $this->ion_auth->logged_in() )
		{
			redirect('auth', 'refresh');
		}

		if(!$plan_id || !$patient_id ){
			redirect('admin/patients/', 'refresh');
		}

    	$plan_id = (int) $plan_id;
		$patient_id = (int) $patient_id;

    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), '/admin/patients/add_immunoteraphy_plan/'.$patient_id );
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        $fields = array(
           'patient_id' => $patient_id,
           'diagnosis' => $this->input->post('diagnosis'),
           'vaccine_sc' => serialize($this->input->post('vaccine_sc')),
           'vaccine_sl' => serialize($this->input->post('vaccine_sl')),
           'txt_remark' => $this->input->post('txt_remark'),
           'date_consented' => ($this->input->post('date_consented')) ? $this->input->post('date_consented') : date("Y-m-d"),
           'start_date' => date("Y-m-d"),
           'flag' => 26
        );


		if($this->input->post("delete_file_1")){
			$path1 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_1");
			if(file_exists($path1)){
				unlink($path1);
			}
			$fields += array('file1' => "");
		}
	

		$file1="";
		$Attach1 = $_FILES['file1']['name']; 
		if(trim($Attach1) != ""){
		  $file1 = date('Ymd').$patient_id.$_FILES["file1"]["name"];
		  $fields += array('file1' => $file1); 
		}

		if($this->input->post("delete_file_2")){
			$path2 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_2");
			if(file_exists($path2)){
				unlink($path2);
			}
			$fields += array('file2' => "");
		}

		$file2="";
		$Attach2 = $_FILES['file2']['name']; 
		if(trim($Attach2) != ""){
		  $file2 = date('Ymd').$patient_id.$_FILES["file2"]["name"];
		  $fields += array('file2' => $file2); 
		}

		if($this->input->post("delete_file_3")){
			$path3 = $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$this->input->post("delete_file_3");
			if(file_exists($path3)){
				unlink($path3);
			}
			$fields += array('file3' => "");
		}
		$file3="";
		$Attach3 = $_FILES['file3']['name']; 
		if(trim($Attach3) != ""){
		  $file3 = date('Ymd').$patient_id.$_FILES["file3"]["name"];
		  $fields += array('file3' => $file3); 
		}

		$upFiles = "";
		$upVacDateSC = $this->input->post("upVacDateSC");
		$upVacDateSL = $this->input->post("upVacDateSL");

		if ($this->patientsModel->update_item("immunotherapy_procedure_plan",$fields,$plan_id))
		{
 			
			if(is_array($upVacDateSC)){

				foreach($upVacDateSC as $key=>$val){
					if(!empty($val)){
						$checkSCId = $this->patientsModel->check_immunotherapy_procedure_history($key,"SC",$plan_id);
						$historyFiledsSC = array(
							"`vaccine`" => $key,
							"`type`" => 'SC',
							"`date_app`" => $val,
							"`plan_id`" => $plan_id
						);
						if(!empty($checkSCId)){

							$this->patientsModel->update_item("immunotherapy_procedure_history",$historyFiledsSC,$checkSCId->id);
						}else{
							$this->patientsModel->insert_item("immunotherapy_procedure_history",$historyFiledsSC);
						}
					}
				}

			}

			if(is_array($upVacDateSL)){

				foreach($upVacDateSL as $key=>$val){
					if(!empty($val)){
						$checkSLId = $this->patientsModel->check_immunotherapy_procedure_history($key,"SL",$plan_id);

						$historyFiledsSL = array(
							"`vaccine`" => $key,
							"`type`" => 'SL',
							"`date_app`" => $val,
							"`plan_id`" => $plan_id
						);
						
						if(!empty($checkSLId)){

							$this->patientsModel->update_item("immunotherapy_procedure_history",$historyFiledsSL,$checkSLId->id);
						}else{
							$this->patientsModel->insert_item("immunotherapy_procedure_history",$historyFiledsSL);
						}
					}
				}

			}


			if($file1 != "")
			{
				move_uploaded_file($_FILES["file1"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file1); 
			}

			if($file2 != "")
			{
			 	move_uploaded_file($_FILES["file2"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file2); 

			}

			if($file3 != "")
			{
			 	move_uploaded_file($_FILES["file3"]["tmp_name"],$_SERVER["DOCUMENT_ROOT"]."/upload/patients/".$file3); 
			}

			$this->patientsModel->update_item("patients",array("notes"=>$this->input->post('notes')), $patient_id);

            $this->session->set_flashdata('message', lang("actions_success"));

		}else{
			$this->session->set_flashdata('message', (validation_errors()  ? validation_errors() : ""));
		}

		redirect('admin/patients/card/'.$patient_id.'#immunoteraphy-block', 'refresh');

    }

    public function add_immunoteraphy_record_sc($patient_id,$plan_id){
    	
    	if(!$patient_id){
			redirect('admin/patients/', 'refresh');
    	}
    	if(!$plan_id){
			redirect('admin/patients/card/'.$patient_id.'/#immunoteraphy-block', 'refresh');
    	}


    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('nurse_record_add'), 'admin/patients/add_immunoteraphy_record/'.$patient_id."/".$plan_id."/");
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('dose', 'lang:immuno_records_dose', 'required');
		$this->form_validation->set_rules('vile', 'lang:immuno_records_vile', 'required');
		$this->form_validation->set_rules('shedname', 'lang:immuno_records_shedname', 'required');
		$this->form_validation->set_rules('stock_vaccines', 'lang:immuno_records_vac', 'required');
		$this->form_validation->set_rules('date_vacc', 'lang:immuno_records_date', 'required');

		if ($this->form_validation->run()){

	        $fields = array(
	        	'patient_id' => $patient_id,
	        	'plan_id' => $plan_id,
	        	'vile' => $this->input->post('vile'),
	            'shedname' => $this->input->post('shedname'),
	            'dose' => $this->input->post('dose'),
	            'stock_vaccines' => $this->input->post('stock_vaccines'),
				'date_vacc' => $this->input->post('date_vacc'),
				'hand_rt' => $this->input->post('hand_rt'),
				'hand_lt' => $this->input->post('hand_lt'),
				'stTime' => $this->input->post('stTime'),
	            'edTime' => $this->input->post('edTime'),
	            'iComments' => $this->input->post('iComments'),
	            'lComments' => $this->input->post('lComments'),
	            'given' => $this->input->post('given'),
	            'paid' => $this->input->post('paid'),
	            'type' => 'SC'
	        );
		}


		if ($this->form_validation->run() == TRUE && $this->patientsModel->insert_item("immunotherapy_procedure_records",$fields)){

			if($this->input->post('stock_vaccines') && $this->input->post('dose') !== 0){
				$stock_item = $this->patientsModel->get_items("stock_items",array("id"=>$this->input->post('stock_vaccines')));

				if($stock_item){
					$recalculate_fields = array(
						"qty"=> $stock_item->qty - (float)$this->input->post('dose')
					);
					$this->patientsModel->update_item("stock_items",$recalculate_fields,$this->input->post('stock_vaccines'));
				}
			}

			redirect('admin/patients/card/'.$patient_id.'/#immunoteraphy-block', 'refresh');
	        $this->session->set_flashdata('message', lang("actions_success"));

		}else{
			$this->data['message'] = (validation_errors()  ? validation_errors() : "");	

			$iplan = $this->patientsModel->get_items("immunotherapy_procedure_plan",array("id"=>$plan_id));
			$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

			$lastRec = $this->patientsModel->get_immunotherapy_procedure_last_record($plan_id,"SC");

	        $this->data['dose'] = array(
				'name'  => 'dose',
				'id'    => 'dose',
				'type'  => 'number',
				'step' =>  '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('dose',($lastRec && $lastRec->dose) ? $lastRec->dose : 0)
			);

	        $this->data['date_vacc'] = array(
				'name'  => 'date_vacc',
				'id'    => 'date_vacc',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_vacc',date("Y-m-d"))
			);

	        $this->data['stTime'] = array(
				'name'  => 'stTime',
				'id'    => 'stTime',
				'type'  => 'time',
	            'class' => 'form-control',
				'value' => date("H:i")
			);

	        $this->data['edTime'] = array(
				'name'  => 'edTime',
				'id'    => 'edTime',
				'type'  => 'time',
	            'class' => 'form-control',
				'value' => date('H:i',strtotime(date('H:i').' + 15 min'))
			);

	        $this->data['iComments'] = array(
				'name'  => 'iComments',
				'id'    => 'iComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('iComments')
			);
			$this->data['lComments'] = array(
				'name'  => 'lComments',
				'id'    => 'lComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('lComments')
			);

			$this->data['patient'] = $patient;
	        $this->data['patient_year'] = $this->calculate_age($patient->dob);
			$this->data['gender'] = array(1=>'Male',2=>'Female');
			$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

			$this->data['vile'] = array(1=>'A',2=>'B',3=>'C',4=>'D');
			$this->data['shedname'] = array(1=>'ALEXOID',2=>'Alutek',3=>'General');
			
			$vaccines = unserialize($iplan->vaccine_sc);
			$medications = array();

			foreach($vaccines as $key=>$val){
				array_push($medications, $val);
			}

			$this->data['stock_items'] = $this->patientsModel->get_stock_items_by_medications($medications,"SC");

			$this->data['lastRec'] = $lastRec;

	       /* Load Template */
	       $this->template->admin_render('admin/patients/immunoteraphy_record_add', $this->data);
   		}
    }

    public function add_immunoteraphy_record_sl($patient_id,$plan_id){
    	
    	if(!$patient_id){
			redirect('admin/patients/', 'refresh');
    	}
    	if(!$plan_id){
			redirect('admin/patients/card/'.$patient_id.'/#immunoteraphy-block', 'refresh');
    	}


    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('nurse_record_add'), 'admin/patients/add_immunoteraphy_record/'.$patient_id."/".$plan_id."/");
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('dose', 'lang:immuno_records_dose', 'required');
		$this->form_validation->set_rules('stock_vaccines', 'lang:immuno_records_vac', 'required');
		$this->form_validation->set_rules('date_vacc', 'lang:immuno_records_date', 'required');

		if ($this->form_validation->run()){

	        $fields = array(
	        	'patient_id' => $patient_id,
	        	'plan_id' => $plan_id,
	            'dose' => $this->input->post('dose'),
	            'stock_vaccines' => $this->input->post('stock_vaccines'),
				'date_vacc' => $this->input->post('date_vacc'),
	            'iComments' => $this->input->post('iComments'),
	            'given' => $this->input->post('given'),
	            'paid' => $this->input->post('paid'),
	            'type' => 'SL'
	        );
		}


		if ($this->form_validation->run() == TRUE && $this->patientsModel->insert_item("immunotherapy_procedure_records",$fields)){

			if($this->input->post('stock_vaccines') && $this->input->post('dose') !== 0){
				$stock_item = $this->patientsModel->get_items("stock_items",array("id"=>$this->input->post('stock_vaccines')));

				if($stock_item){
					$recalculate_fields = array(
						"qty"=> $stock_item->qty - (float)$this->input->post('dose')
					);
					$this->patientsModel->update_item("stock_items",$recalculate_fields,$this->input->post('stock_vaccines'));
				}
			}

			redirect('admin/patients/card/'.$patient_id.'/#immunoteraphy-block', 'refresh');
	        $this->session->set_flashdata('message', lang("actions_success"));

		}else{
			$this->data['message'] = (validation_errors()  ? validation_errors() : "");	

			$iplan = $this->patientsModel->get_items("immunotherapy_procedure_plan",array("id"=>$plan_id));
			$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

			$lastRec = $this->patientsModel->get_immunotherapy_procedure_last_record($plan_id,"SL");

	        $this->data['dose'] = array(
				'name'  => 'dose',
				'id'    => 'dose',
				'type'  => 'number',
				'step' =>  '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('dose',($lastRec && $lastRec->dose) ? $lastRec->dose : 0)
			);

	        $this->data['date_vacc'] = array(
				'name'  => 'date_vacc',
				'id'    => 'date_vacc',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_vacc',date("Y-m-d"))
			);

	        $this->data['iComments'] = array(
				'name'  => 'iComments',
				'id'    => 'iComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('iComments')
			);

			$this->data['patient'] = $patient;
	        $this->data['patient_year'] = $this->calculate_age($patient->dob);
			$this->data['gender'] = array(1=>'Male',2=>'Female');
			$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

			$vaccines = unserialize($iplan->vaccine_sl);
			$medications = array();

			foreach($vaccines as $key=>$val){
				array_push($medications, $val);
			}


			$this->data['stock_items'] = $this->patientsModel->get_stock_items_by_medications($medications,"SL");


			$this->data['lastRec'] = $lastRec;

       		/* Load Template */
       		$this->template->admin_render('admin/patients/immunoteraphy_record_add_sl', $this->data);
   		}
    }

    public function edit_immunoteraphy_record_sc($record_id){
    	

    	if(!$record_id){
			redirect('admin/patients/', 'refresh');
    	}

    	$record = $this->patientsModel->get_items("immunotherapy_procedure_records",array("id"=>$record_id));

    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('nurse_record_add'), 'admin/patients/add_immunoteraphy_record/'.$record->patient_id."/".$record->plan_id."/");

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('dose', 'lang:immuno_records_dose', 'required');
		$this->form_validation->set_rules('vile', 'lang:immuno_records_vile', 'required');
		$this->form_validation->set_rules('shedname', 'lang:immuno_records_shedname', 'required');
		$this->form_validation->set_rules('stock_vaccines', 'lang:immuno_records_vac', 'required');
		$this->form_validation->set_rules('date_vacc', 'lang:immuno_records_date', 'required');

		if ($this->form_validation->run()){

	        $fields = array(
	        	'vile' => $this->input->post('vile'),
	            'shedname' => $this->input->post('shedname'),
	            'dose' => $this->input->post('dose'),
	            'stock_vaccines' => $this->input->post('stock_vaccines'),
				'date_vacc' => $this->input->post('date_vacc'),
				'hand_rt' => $this->input->post('hand_rt'),
				'hand_lt' => $this->input->post('hand_lt'),
				'stTime' => $this->input->post('stTime'),
	            'edTime' => $this->input->post('edTime'),
	            'iComments' => $this->input->post('iComments'),
	            'lComments' => $this->input->post('lComments'),
	            'given' => $this->input->post('given'),
	            'paid' => $this->input->post('paid'),
	            'type' => 'SC'
	        );
		}


		if ($this->form_validation->run() == TRUE && $this->patientsModel->update_item("immunotherapy_procedure_records",$fields,$record_id)){

			redirect('admin/patients/card/'.$record->patient_id.'/#immunoteraphy-block', 'refresh');

	        $this->session->set_flashdata('message', lang("actions_success"));

		}else{
			$this->data['message'] = (validation_errors()  ? validation_errors() : "");	

			$iplan = $this->patientsModel->get_items("immunotherapy_procedure_plan",array("id"=>$record->plan_id));

			$patient = $this->patientsModel->get_items("patients",array("id"=>$record->patient_id));


	        $this->data['dose'] = array(
				'name'  => 'dose',
				'id'    => 'dose',
				'type'  => 'number',
				'step' =>  '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('dose',($record && $record->dose) ? $record->dose : 0)
			);

	        $this->data['date_vacc'] = array(
				'name'  => 'date_vacc',
				'id'    => 'date_vacc',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_vacc',$record->date_vacc)
			);

	        $this->data['stTime'] = array(
				'name'  => 'stTime',
				'id'    => 'stTime',
				'type'  => 'time',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('stTime',$record->stTime)
			);

	        $this->data['edTime'] = array(
				'name'  => 'edTime',
				'id'    => 'edTime',
				'type'  => 'time',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('edTime',$record->edTime)
			);

	        $this->data['iComments'] = array(
				'name'  => 'iComments',
				'id'    => 'iComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('iComments',$record->iComments)
			);
			$this->data['lComments'] = array(
				'name'  => 'lComments',
				'id'    => 'lComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('lComments',$record->lComments)
			);

			$this->data['patient'] = $patient;
	        $this->data['patient_year'] = $this->calculate_age($patient->dob);
			$this->data['gender'] = array(1=>'Male',2=>'Female');
			$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));

			$this->data['vile'] = array(1=>'A',2=>'B',3=>'C',4=>'D');
			$this->data['shedname'] = array(1=>'ALEXOID',2=>'Alutek',3=>'General');
			
			$vaccines = unserialize($iplan->vaccine_sc);
			$medications = array();

			foreach($vaccines as $key=>$val){
				array_push($medications, $val);
			}

			$this->data['stock_items'] = $this->patientsModel->get_stock_items_by_medications($medications,"SC");

			//var_dump($record);
			$this->data['record'] = $record;

	       /* Load Template */
	       $this->template->admin_render('admin/patients/immunoteraphy_record_edit', $this->data);
   		}
    }

    public function edit_immunoteraphy_record_sl($record_id){
    	

    	if(!$record_id){
			redirect('admin/patients/', 'refresh');
    	}

    	$record = $this->patientsModel->get_items("immunotherapy_procedure_records",array("id"=>$record_id));

    	/* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('nurse_record_add'), 'admin/patients/add_immunoteraphy_record/'.$record->patient_id."/".$record->plan_id."/");

        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('dose', 'lang:immuno_records_dose', 'required');
		$this->form_validation->set_rules('stock_vaccines', 'lang:immuno_records_vac', 'required');
		$this->form_validation->set_rules('date_vacc', 'lang:immuno_records_date', 'required');

		if ($this->form_validation->run()){

	        $fields = array(
	            'dose' => $this->input->post('dose'),
	            'stock_vaccines' => $this->input->post('stock_vaccines'),
				'date_vacc' => $this->input->post('date_vacc'),
	            'iComments' => $this->input->post('iComments'),
	            'given' => $this->input->post('given'),
	            'paid' => $this->input->post('paid'),
	            'type' => 'SL'
	        );
		}


		if ($this->form_validation->run() == TRUE && $this->patientsModel->update_item("immunotherapy_procedure_records",$fields,$record_id)){

			redirect('admin/patients/card/'.$record->patient_id.'/#immunoteraphy-block', 'refresh');
			
	        $this->session->set_flashdata('message', lang("actions_success"));

		}else{
			$this->data['message'] = (validation_errors()  ? validation_errors() : "");	

			$iplan = $this->patientsModel->get_items("immunotherapy_procedure_plan",array("id"=>$record->plan_id));

			$patient = $this->patientsModel->get_items("patients",array("id"=>$record->patient_id));


	        $this->data['dose'] = array(
				'name'  => 'dose',
				'id'    => 'dose',
				'type'  => 'number',
				'step' =>  '0.1',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('dose',($record && $record->dose) ? $record->dose : 0)
			);

	        $this->data['date_vacc'] = array(
				'name'  => 'date_vacc',
				'id'    => 'date_vacc',
				'type'  => 'date',
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('date_vacc',$record->date_vacc)
			);

	        $this->data['iComments'] = array(
				'name'  => 'iComments',
				'id'    => 'iComments',
				'type'  => 'text',
				'rows'	=> 3,
	            'class' => 'form-control',
				'value' => $this->form_validation->set_value('iComments')
			);
			$this->data['patient'] = $patient;
	        $this->data['patient_year'] = $this->calculate_age($patient->dob);
			$this->data['gender'] = array(1=>'Male',2=>'Female');
			$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
			
			$vaccines = unserialize($iplan->vaccine_sl);
			$medications = array();

			if($vaccines){
				foreach($vaccines as $key=>$val){
					array_push($medications, $val);
				}
				$this->data['stock_items'] = $this->patientsModel->get_stock_items_by_medications($medications,"SL");
			}else{
				$this->data['stock_items'] = NULL;
			
			}

			//var_dump($record);
			$this->data['record'] = $record;

	       /* Load Template */
	       $this->template->admin_render('admin/patients/immunoteraphy_record_edit_sl', $this->data);
   		}
    }

    public function delete_immunoteraphy_record($id,$patient){
	$id = (int) $id;


		if ($this->patientsModel->delete_item("immunotherapy_procedure_records",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/patients/card/'.$patient.'#immunoteraphy-block', 'refresh');
		}
    }

	public function delete($id)
	{
		$id = (int) $id;


		if ($this->patientsModel->delete_item("patients",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/patients/', 'refresh');
		}


	}

	public function nurse_record_delete($patient_id,$id)
	{
		$id = (int) $id;
		$patient_id = (int) $patient_id;

		if ($this->patientsModel->delete_item("nurse",$id))
		{
        	$this->session->set_flashdata('message', lang("actions_success"));
			redirect('admin/patients/card/'.$patient_id."#nurse-block", 'refresh');
		}


	}

	public function get_active_files_by_section($patient_id,$date_appointment,$section_id = 0){

        return $this->patientsModel->get_files($patient_id,$date_appointment,$section_id);
    }


    public function appointmets_list($patient_id = 0){

    	if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');

        }else{

        	if($patient_id == 0 && !$this->input->post("patient_id")){
        		redirect('admin/patients', 'refresh');
        	}

        	if($this->input->post("patient_id")){
        		$patient_id = (int)$this->input->post("patient_id");
        	}

			$patient = $this->patientsModel->get_items("patients",array("id"=>$patient_id));

			/* Breadcrumbs */
			$this->breadcrumbs->unshift(1, "Patients", 'admin/patients');
        	$this->breadcrumbs->unshift(2, "Appointments", 'admin/patients/appointmets_list');
        	$this->data['breadcrumb'] = $this->breadcrumbs->show();

    		$this->data['pagetitle'] = "<h1>Appointments: ".$patient->patient_name."</h1>";

	        $date = 0;
	        $doctor_id = 0;

	        if($this->input->post('date_appointments')){
	        	$date = explode("-",$this->input->post('date_appointments'));
	        	$this->data["date_appointments"] = $this->input->post('date_appointments');
	        }

	        if($this->input->post('doctor_id') && $this->input->post('doctor_id') !== "ALL"){
	        	$doctor_id = $this->input->post('doctor_id');
	        	$this->data["doctor_id"] = $doctor_id;
	        }

	        if(!empty($patient)){

	        	$filter = array(
	        		"patient_id"=>$patient->id,
	        		"patient_name"=>$patient->patient_name,
	        		"patient_telephone"=>$patient->telephone

	        	);

	        	$this->data["appointments"] = $this->appointmentsModel->get_appointments_by_patient($filter,$date,$doctor_id);

	    	}else{
	    		$this->data["appointments"] = array();
	    	}

	    	$this->data['doctors'] = $this->ion_auth->users(array('doctors','nurses'))->result();

	    	$this->data['gender'] = array(1=>'Male',2=>'Female');
	    	$this->data['doctor'] = $this->patientsModel->get_items("users",array("id"=>$patient->doctor));
			$this->data["patient"] = $patient;

   			$this->data["func"] = $this;

    		/* Load Template */
        	$this->template->admin_render('admin/patients/appointments_list', $this->data);

			

		}

    }

    public function delete_appointment($id,$pid)
	{

		if ($this->appointmentsModel->delete_item("appointments",$id))
		{
        	redirect('admin/patients/appointmets_list/'.$pid.'/','refresh');
		}


	}

}
?>