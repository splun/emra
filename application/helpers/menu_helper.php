<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('active_link_controller'))
{
    function active_link_controller($controller)
    {
        $CI    =& get_instance();
        $class = $CI->router->fetch_class();

        if ($CI->uri->segment(2) == $controller || $CI->uri->segment(3) == $controller) {
            return 'active';
        } else {
            return ($class == $controller) ? 'active' : NULL;
        }
      
    }
}


if ( ! function_exists('active_link_function'))
{
    function active_link_function($controller,$type="")
    {
        $CI    =& get_instance();
        $class = $CI->router->fetch_method();

        if ($CI->uri->segment(4) == $controller) {
            return 'active';
        }else {
            return ($class == $controller) ? 'active' : NULL;   
        }
        
    }
}

if ( ! function_exists('active_link_subfunction'))
{
    function active_link_subfunction($controller,$type="")
    {
        $CI    =& get_instance();
        $class = $CI->router->fetch_method();

        if($CI->uri->segment(4) == $type && $CI->uri->segment(5) == $controller){
            return 'active';
        }else{
             return ($class == $controller) ? 'active' : NULL;   
        }
                   
    }
}

