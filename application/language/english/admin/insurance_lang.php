<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle'] = 'Insurance';
$lang['pagetitle_edit'] = 'Edit insurance';
$lang['pagetitle_add'] = 'Create insurance';

$lang['name_label'] = 'Company name';
$lang['discount_label'] = 'Discount (%)';
$lang['show_discount_label'] = 'Show discount?';
$lang['status_label'] = 'Invoice status';