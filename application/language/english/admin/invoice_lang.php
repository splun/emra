<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle'] = 'Invoice';
$lang['pagetitle_category'] = 'Invoice category';
$lang['pagetitle_list'] = 'Invoice list';
$lang['pagetitle_edit'] = 'Edit invoice item';
$lang['pagetitle_add'] = 'Create invoice item';

$lang['pagetitle_category_add'] = 'Create invoice category';
$lang['pagetitle_category_edit'] = 'Edit invoice category';

$lang['create_item'] = 'Create item';
$lang['name_label'] = 'Name';
$lang['category_label'] = 'Category';
$lang['category_label_parent'] = 'Parent category';
$lang['parent_categories_label'] = 'Parent categories';
$lang['price_label'] = 'Price (KD)';
$lang['net_price_label'] = 'Deductible price(KD)';//'NET amount(KD)';
