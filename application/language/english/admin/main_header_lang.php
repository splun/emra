<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['header_you_have']     = 'You have';

$lang['header_mainmenu_dashboard'] = 'Main <br />Dashboard';
$lang['header_mainmenu_calendar']  = 'View <br />Calendar';
$lang['header_mainmenu_patients']  = 'All <br />Patients';
$lang['header_mainmenu_immunotherapy'] = 'Immuno <br />therapy';
$lang['header_mainmenu_pendingcases']   = 'Pending <br />cases';
$lang['header_mainmenu_stock'] = 'Stock <br />items';
$lang['header_mainmenu_invoice'] = 'Invoice <br />reports';
$lang['header_mainmenu_pendingpayments'] = 'Pending <br />Payments';
$lang['header_mainmenu_addappointment'] = 'appointment';
$lang['header_mainmenu_upload'] = 'upload';

$lang['header_message']      = 'message';
$lang['header_notification'] = 'notification';
$lang['header_pending_notes'] = 'pending notes';
$lang['header_task']         = 'task';
$lang['header_view_all']     = 'View all';
$lang['header_complete']     = 'Complete';
$lang['header_member_since'] = 'Member since';
$lang['header_followers']    = 'Followers';
$lang['header_sales']        = 'Sales';
$lang['header_friends']      = 'Friends';
$lang['header_profile']      = 'Profile';
$lang['header_sign_out']     = 'Sign out';
