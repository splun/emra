<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle'] = 'Patients';
$lang['pagetitle_edit'] = 'Edit patient';
$lang['pagetitle_add'] = 'New patient';

$lang['patient_card'] = 'Patient card';
$lang['view_patient_card'] = 'View card';

$lang['patient_name_label'] = 'Patient name';
$lang['patient_name_ar_label'] = 'Patient name arabic';
$lang['patient_civil_id_label'] = 'Civil ID';
$lang['patient_dob_label'] = 'Date Of Birth';
$lang['patient_age_label'] = 'Age';
$lang['patient_b_weight_label'] = 'Birth Weight';
$lang['patient_gender_label'] = 'Gender';
$lang['patient_sex_label'] = 'Sex';
$lang['patient_governance_label'] = "Governance";
$lang['patient_governance'][0] = "Asemah";
$lang['patient_governance'][1] = "Ahmadi";
$lang['patient_governance'][2] = "Jahra";
$lang['patient_governance'][3] = "Hawally";
$lang['patient_governance'][4] = "Farwaniya";
$lang['patient_governance'][5] = "Mubarak AlKabeer";
$lang['patient_address_label'] = 'Address';
$lang['patient_parents_name_label'] = 'Parents name';
$lang['patient_job_label'] = 'Job type';
$lang['patient_phone_label'] = 'Mobile';
$lang['patient_mobile_label'] = 'Mobile(2nd)';
$lang['patient_mobile2_label'] = 'Mobile(3rd)';
$lang['patient_email_label'] = 'Email';
$lang['patient_insurance_company_label'] = 'Insurance company';
$lang['patient_insurance_number_label'] = 'Policy number';
$lang['patient_file01_label'] = 'Cid';
$lang['patient_file02_label'] = 'Insurance';
$lang['patient_file03_label'] = 'Report';
$lang['patient_othe_report_label'] = 'Multiple Reports';
$lang['patient_referral_label'] = 'How did you hear about us?';
$lang['patient_referral'][0] = "Friend";
$lang['patient_referral'][1] = "Web";
$lang['patient_referral'][2] = "Doctor";
$lang['patient_referral'][3] = "Other";
$lang['patient_notes_label'] = 'Notes';
$lang['patient_file_no_label'] = 'File No.';
$lang['patient_doctor_label'] = 'Doctor';
$lang['patient_treatingDR_label'] = 'Treating Dr.';
$lang['patient_immuno_label'] = 'Immunotherapy';
$lang['patient_card_title'] = 'Patient card';
$lang['patient_main_tab'] = 'Main information';
$lang['patient_nurse_tab'] = 'Nurse records';
$lang['patient_results_tab'] = 'Results';
$lang['patient_allergy_tab'] = 'Allergy test';
$lang['patient_examphoto_tab'] = 'Exam/Photos';
$lang['patient_spirometry_tab'] = 'Spirometry';
$lang['patient_sickleaves_tab'] = 'Sick Leaves';
$lang['patient_immunoteraphy_tab'] = 'Immunotherapy';
$lang['patient_diagnosis_tab'] = 'Diagnosis';
$lang['patient_prescription_tab'] = 'Prescription And Drug Allergy';
$lang['patient_visit_history_tab'] = 'Visit history';
$lang['patient_referral_tab'] = 'Referrals';
$lang['patient_reports_tab'] = 'Reports';
$lang['patient_invoice_tab'] = 'Invoices';


$lang['view_file_label'] = "View file";

$lang['nurse_record_title'] = 'Nurse record';
$lang['nurse_record_add'] = 'Add record';

$lang['nurse_records_datev'] = 'Date visit';
$lang['nurse_records_weight'] = 'Weight';
$lang['nurse_records_height'] = 'Height';
$lang['nurse_records_heart'] = 'Heart rate';
$lang['nurse_records_blood'] = 'Blood pressure';
$lang['nurse_records_respiratory'] = 'Respiratory rate';
$lang['nurse_records_temperature'] = 'Temperature';
$lang['nurse_records_circumference'] = 'Head Circumference';
$lang['nurse_records_bmi'] = 'BMI';
$lang['nurse_records_sao'] = 'SaO2';
$lang['nurse_records_comments'] = 'Comments';

$lang['patient_prescription'] = 'Prescription';
$lang['patient_drugallrgy'] = 'Drug allergy';
$lang['prescription_records_name'] = 'Medication Name';
$lang['prescription_records_cat_name'] = 'Category';
$lang['prescription_records_qty'] = 'Qty';
$lang['prescription_records_date'] = 'Date';
$lang['prescription_dublicate'] = 'Dublicate';
$lang['prescription_print_title'] = 'Prescription and Drug allergy';

$lang['diagnosis_description'] = 'Description';

$lang['immuno_shedule_title'] = 'Immunotherapy Shedule';
$lang['immuno_shedule_diagnos_label'] = 'Diagnosis:';
//$lang['immuno_shedule_remark_label'] = 'Remark/Medication:';
$lang['immuno_shedule_remark_label'] = 'Pay Plan:';
$lang['immuno_shedule_env_label'] = 'Environment:';
$lang['immuno_shedule_upload1_label'] = 'Skin test:';
$lang['immuno_shedule_upload2_label'] = 'Consent:';
$lang['immuno_shedule_upload3_label'] = 'Extra:';
$lang['immuno_shedule_vacc_sc_label'] = 'SCIT:';
$lang['immuno_shedule_vacc_sl_label'] = 'SLIT:';
$lang['immuno_shedule_date_label'] = 'Consent:';
$lang['immuno_shedule_notes_label'] = 'Notes:';
$lang['immuno_records_sc'] = "Sub Cutaneous";
$lang['immuno_records_sl'] = "Sublingual";
$lang['immuno_sc'] = "SC";
$lang['immuno_sl'] = "SL";

$lang['immuno_records_ville'] ="VILE";
$lang['immuno_records_schedule'] ="Schedule";
$lang['immuno_records_dose'] ="Dose(ml)";
$lang['immuno_records_date'] ="Date";
$lang['immuno_records_vac'] ="Vac.";
$lang['immuno_records_arm'] ="Arm";
$lang['immuno_records_time'] ="Time";
$lang['immuno_records_icomments'] ="Immediate Comments<br />(e.g Itch, pain, swelling)";
$lang['immuno_records_lcomments'] ="Late <br /> Comments";
$lang['immuno_records_comments'] ="Comments";
$lang['immuno_records_given'] ="Given";
$lang['immuno_records_paid'] ="Paid";
$lang['immuno_records_time'] ="Time";
$lang['immuno_records_handrt'] ="Rt.";
$lang['immuno_records_handlt'] ="Lt.";
$lang['immuno_records_timest'] ="St.";
$lang['immuno_records_sttime_label'] ="St. Time";
$lang['immuno_records_edtime_label'] ="Ed. Time";
$lang['immuno_records_timeed'] ="Ed.";
$lang['immuno_records_qty'] ="Q-ty";
$lang['immuno_records_nonstock'] ="NON Stock";
$lang['patient_immuno_last_visit'] ="Last visit";
$lang['doctor_record_edit&add'] = "Add/edit records";
$lang['pmh'] = "PMH";
$lang['date_visit'] = "Visit date";
$lang['advice'] = "Advise";
$lang['refer_to'] = "To";
$lang['sickleave_leave_days'] ="Leave days";
$lang['sickleave_leave_diag'] ="Diagnosis";
$lang['sickleave_destination'] ="To";
$lang['sickleave_patient_name'] = "Name";
$lang['sickleave_civil_id_label'] = "CID";
$lang['sickleave_file_no_label'] = 'File';
$lang['sickleave_signature'] = 'Signature';
$lang['sickleave_extra_notes'] ="Extra notes";

$lang['headcircumference'] = 'Head Circumference';
$lang['growth_weight'] = 'Weight';
$lang['growth_height'] = 'Height';
$lang['immunoteraphy_print'] ="Print";


$lang["patientsection"]['complaints'] = "Complaints";
$lang["patientsection"]['medications'] = "Medications";
$lang["patientsection"]['family_social_history'] = "Family-Social History";
$lang["patientsection"]['environment'] = "Environment";
$lang["patientsection"]['birth_vaccination'] = "PMH & Vaccination";
$lang["patientsection"]['diet'] = "Diet";
$lang["patientsection"]['growth_chart'] = "Growth Chart";
$lang["patientsection"]['development'] = "Development";
$lang["patientsection"]['immunity'] = "Immunity";
$lang["patientsection"]['diagnosis'] = "Diagnosis";
$lang["patientsection"]['asthma'] = "Asthma";
$lang["patientsection"]['eczema'] = "Eczema";
$lang["patientsection"]['rhino_conjunctivitis'] = "Rhino-Conjunctivitis";
$lang["patientsection"]['food_allergy'] = "Food Allergy";
$lang["patientsection"]['drug_allergy'] = "Drug Allergy";
$lang["patientsection"]['skin_testing'] = "Allergy test";
$lang["patientsection"]['spirometer'] = "Spirometer";
$lang["patientsection"]['rhinoscopy'] = "Rhinoscopy";
$lang["patientsection"]['results'] = "Results";
$lang["patientsection"]['exam_photo'] = "Exam/Photo";
$lang["patientsection"]['immunoCAP'] = "ImmunoCAP";
$lang["patientsection"]['physical'] = "Physical Exam";
$lang["patientsection"]['exam_photo'] = "Exam/Photo";
$lang["patientsection"]['investigations'] = "Investigations";
$lang["patientsection"]['prescription'] = "Prescription";
//$lang["patientsection"]['immunotherapy'] = "Immunotherapy";
$lang["patientsection"]['summary_plan'] = "Summary & Plan";
$lang["patientsection"]['referral'] = "Referral";
$lang["patientsection"]['sickleave'] = "Sick Leave";
$lang["patientsection"]['invoice'] = "Invoice";

//$lang['bhv_problem_field'] = "Behavioral problem";
$lang['bhv_problem_field'] = "Select";
$lang['desc_description_field'] = "Description goes here";

$lang['invoice_total'] = "Total";
$lang['insurance_pay'] = "Insurance pays";
$lang['customer_pay'] = "Customer pay";
$lang['invoice_knet'] = "Knet";
$lang['invoice_cash'] = "Cash";
$lang['invoice_debt'] = "Debt";

$lang['print_invoice'] = "Print invoice";
$lang['print_invoice_title'] = "Service invoice";
$lang['print_invoice_patient_copy'] = "Patient copy";
$lang['print_invoice_clinic_copy'] = "Clinic copy";

$lang['complaint_lbl'] = "Complaint";
$lang['dc_lbl'] = "DC";