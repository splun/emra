<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users_action']              = 'Action';
$lang['users_active']              = 'Active';
$lang['users_company']             = 'Company';
$lang['users_create_user']         = 'Create user';
$lang['users_created_on']          = 'Created on';
$lang['users_deactivate_question'] = 'Are you sure you want to deactivate the user %s';
$lang['users_edit_user']           = 'Edit user';
$lang['users_email']               = 'Email';
$lang['users_firstname']           = 'First name';
$lang['users_groups']              = 'Groups';
$lang['users_inactive']            = 'Inactive';
$lang['users_ip_address']          = 'IP address';
$lang['users_last_login']          = 'Last login';
$lang['users_lastname']            = 'Last name';
$lang['users_member_of_groups']    = 'Member of groups';
$lang['users_password']            = 'Password';
$lang['users_password_confirm']    = 'Password confirm';
$lang['users_phone']               = 'Phone';
$lang['users_status']              = 'Status';
$lang['users_username']            = 'User name / Pseudo';
$lang['users_position']            = 'Position / Title';
$lang['panel_color']               = 'Color';

$lang['tasks_title']               = 'Tasks';
$lang['tasks_title_label']         = 'Title';
$lang['tasks_start_label']     	   = 'Start';
$lang['tasks_end_label']     	   = 'End';
$lang['tasks_text_label']          = 'Description';
$lang['tasks_priority_label']      = 'Priority';
$lang['tasks_status_label']        = 'Status';
$lang['tasks_title_label']         = 'Title';

$lang['notes_title']               = 'Notes';
$lang['notes_patient']             = 'Patient';
$lang['notes_date']     	   	   = 'Date';
$lang['notes_text']     	  	   = 'Note';
$lang['notes_comment']             = 'Comment';
$lang['notes_allocated']           = 'Allocated users';
$lang['notes_status']      		   = 'Status';