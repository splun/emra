<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle'] = 'Appoinments';
$lang['pagetitle_calendar'] = 'Appoinments calendar';
$lang['pagetitle_edit'] = 'Edit apoinment';
$lang['pagetitle_add'] = 'New appoinment';

$lang['pagetitle_tooday'] = 'Today appoinments';
$lang['pagetitle_pending']= 'Pending cases';

$lang['time'] = "Time";
$lang['date'] = "Date";
$lang['patient_name'] = "Patient Name";
$lang['patient_telephone'] = "Patient telephone";
$lang['patient'] = "Patient";
$lang['select_patient'] = "Select patient";
$lang['reason'] = "Reason";
$lang['time_in'] ="Time In";
$lang['time_out'] ="Time Out";
$lang['doctor'] ="Doctor/Nurse";

$lang['appointment_type'] = "Appointment Type";
$lang['appointment_details'] = "Details";
$lang['start_date'] = "Date start";
$lang['date_appointment'] = "Date appointment";
$lang['time_period'] = "Time period";
$lang['patient_id'] = "File No.";
$lang['view_profile'] = "View profile";
$lang['or_add_patient'] ="Or add new patient";
$lang['all_doctors'] ="All";
$lang['rooms'] ="Room allocation";
