<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle_reports'] = 'Reports';
$lang['advanced_search'] = 'Advanced search reports';
$lang['search_result'] = 'Search results';
$lang['adv_search'] = 'Search';
$lang['back_adv_search'] = 'Back to search';

$lang['invoice_reports'] = 'Invoice reports';
$lang['sales_report'] = 'Sales report';
$lang['dov'] ="Date of visit";
$lang['adv_parents_name'] ="Parents name";
$lang['adv_from'] ="From";
$lang['adv_to'] ="To";
$lang['adv_age'] ="Age/year";
$lang['adv_month'] ="Age/month";
$lang['adv_skin_testing'] ="Skin testing";
$lang['adv_spirometr'] ="Spirometer";
$lang['adv_weight'] ="Weight";
$lang['adv_height'] ="Height";
$lang['adv_complaints'] ="Complaints";
$lang['adv_others'] ="Others";
$lang['adv_medications'] ="Medications";
$lang['adv_asthma'] ="Asthma";
$lang['adv_family_history'] ="Family History";
$lang['adv_environment'] ="Environment";
$lang['adv_bav'] ="Birth and Vaccination:";
$lang['adv_diet'] ="Diet";
$lang['adv_development'] ="Development";
$lang['adv_immunity'] ="Immunity";
$lang['adv_diagnosis'] ="Diagnosis";
$lang['adv_eczema'] ="Eczema";
$lang['adv_rhino_conjunctivitis'] ="Rhino-Conjunctivitis";
$lang['adv_food'] ="Food Allergy";
$lang['adv_questions'] ="Questions";
$lang['adv_immunoteraphy'] ="Immunotherapy";
$lang['adv_spirometer'] ="Spirometer";
$lang['adv_rhinoscopy'] ="Rhinoscopy";
$lang['adv_immunoCAP'] ="ImmunoCAP";
$lang['adv_physical_exam'] ="Physical Exam";
$lang['adv_investigations'] ="Investigations";
$lang['adv_prescription'] ="Prescription";
$lang['adv_drug_allergy'] ="Drug Allergy";


$lang['invoice_pending'] ="Pending payments";
$lang['invoice_item'] = "Invoice item";

$lang['inv_no'] = "No";
$lang['inv_date'] = "Date";
$lang['inv_quoted'] = "Quoted";
$lang['inv_deductable'] = "Deductible";
$lang['inv_net_amount'] = "NET amount";
$lang['inv_total'] = "Total";
$lang['inv_total_all'] = "Invoice Total";
$lang['inv_insurance_discount'] = "Insurance discount";
$lang['inv_insurance_pay'] = "Insurance pay";
$lang['inv_customer_pay'] = "Customer pay";
$lang['inv_total_deductable'] = "Total Deductible";
$lang['inv_total_net'] = "Total NET amount";
$lang['total_net_amount_with_debt'] = "Final Income";
$lang['inv_total_qty'] = "Total qty";
$lang['inv_knet'] = "KNet";
$lang['inv_cash'] = "Cash";
$lang['inv_debt'] = "Debt";
$lang['inv_invoice'] = "Invoice";
$lang['all_pay_info'] = "All Payments info";
$lang['doctor_name_label'] = "Doctor";

