<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['menu_online'] = 'Online';
$lang['menu_search'] = 'Search patients';

/* * */$lang['menu_access_website']             = 'Access to the Web site';

/* * */$lang['menu_main_navigation']            = 'Main';
/* ****** */$lang['menu_dashboard']             = 'Dashboard';

/* ****** */$lang['menu_appointments']          = 'Appointments';
/* ******** */$lang['menu_appointments_today'] = 'Today';
/* ******** */$lang['menu_appointments_pending']= 'Pending cases';
/* ******** */$lang['menu_appointments_add']	= 'Calendar';

/* ****** */$lang['menu_patients']            	= 'Patients';
/* ********** */$lang['menu_immuno_patients']   = 'Immunotherapy Patients';
/* ********** */$lang['menu_add_patients']   	= 'New patient';

/* * */$lang['menu_stock']       				= 'Stock';
/* **** */$lang['menu_stock_locations']    		= 'Locations';
/* **** */$lang['menu_stock_medications']      	= 'Stock items';
/* **** */$lang['menu_in_stock']      			= 'In Stock';

/* * */$lang['menu_administration']             = 'Administration';
/* ****** */$lang['menu_users']                 = 'Users';
/* *********** */$lang['menu_users_profile']    = 'Profile';
/* *********** */$lang['menu_users_deactivate'] = 'Deactivate';
/* *********** */$lang['menu_users_create']     = 'Create';
/* *********** */$lang['menu_users_edit']       = 'Edit';

/* ****** */$lang['menu_security_groups']       = 'Security groups';
/* *********** */$lang['menu_groups_create']    = 'Create';
/* *********** */$lang['menu_groups_edit']      = 'Edit';

/* ****** */$lang['menu_preferences']           = 'Settings';
/* *********** */$lang['menu_interfaces']       = 'Interfaces';
/* *********** */$lang['menu_patient_files']    = 'Patient Files';
/* *********** */$lang['menu_medications']      = 'Medications';
/* *********** */$lang['menu_settings_invoice'] = 'Invoice';
/* *********** */$lang['menu_settings_insurance'] = 'Insurance';
/* **************** */$lang['menu_int_admin']   = 'Administration';
/* **************** */$lang['menu_int_public']  = 'Public';

/* ****** */$lang['menu_pending_payments']      = 'Pending payments';

/* ****** */$lang['menu_reports']           	= 'Reports';
/* ********** */$lang['menu_adv_search_report'] = 'Advance Search Report';
/* ********** */$lang['menu_invoice_report']    = 'Invoice Report';
/* ********** */$lang['menu_sales_report']    	= 'Sales Report';

/* ****** */$lang['menu_files']                 = 'Files';
/* ****** */$lang['menu_database_utility']      = 'Database utility';


/* * */$lang['menu_webapp']                     = 'Web application';
/* ****** */$lang['menu_license']               = 'License';
/* ****** */$lang['menu_resources']             = 'Resources';
