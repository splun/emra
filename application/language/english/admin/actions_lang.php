<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['actions_title']         = 'Actions';
$lang['actions_cancel']         = 'Cancel';
$lang['actions_create']         = 'Create new';
$lang['actions_cat_create']     = 'Create category';
$lang['actions_cat_create_edit']     = 'Create/edit categories';
$lang['actions_default_values'] = 'Default values';
$lang['actions_edit']           = 'Edit';
$lang['actions_ok']             = 'Ok';
$lang['actions_no']             = 'No';
$lang['actions_reset']          = 'Reset';
$lang['actions_see']            = 'View';
$lang['actions_see_items']      = 'View items';
$lang['actions_submit']         = 'Submit';
$lang['actions_done']           = 'Done';
$lang['actions_start']          = 'Start';
$lang['actions_search']         = 'Search';
$lang['actions_save']           = 'Save';
$lang['actions_yes']            = 'Yes';
$lang['actions_delete']         = 'Delete';
$lang['actions_active']         = 'Active';
$lang['actions_inactive']       = 'Inactive';
$lang['actions_show']           = 'Show';
$lang['actions_hide']           = 'Hide';
$lang['actions_back']           = 'Back';
$lang['actions_close']           = 'Close';
$lang['actions_status']         = 'Status';
$lang['actions_upload']         = 'Upload';
$lang['actions_upload_files']   = 'Upload Files';
$lang['actions_filter']         = 'Filter';
$lang['actions_loading']         ='Loading...';
$lang['actions_security_error']     = 'Security error';
$lang['actions_file_install_exist'] = 'You must %s the installation files.';


/*******Alert Area Warning,Success,Info,Error**********/
$lang['actions_success'] =  
	'<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Success!</h4>
    </div>';
$lang['actions_upload_success'] =  
    '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>File uploaded successfully</h4>
    </div>';

$lang['actions_item_update'] =  
	'<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Item successful update!</h4>
    </div>';

$lang['actions_category_update'] =  
    '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Category successful update!</h4>
    </div>';

$lang['actions_item_delete'] =  
	'<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Item successful delete!</h4>
    </div>';

$lang['actions_category_delete'] =  
    '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Category successful delete!</h4>
    </div>';

$lang['actions_item_create'] =  
	'<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Item successful create!</h4>
    </div>';

$lang['actions_category_create'] =  
    '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-check"></i>Category successful create!</h4>
    </div>';

$lang['actions_cannot_delete']  = '
	<div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-warning"></i>This item can not deleted while it is active!</h4>
    </div>';

$lang['actions_cannot_delete_category']  = '
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-warning"></i>This category can not be deleted because it contains the active elements!</h4>
    </div>';

$lang['actions_cannot_delete_using_item']  = '
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-warning"></i>This item can not be deleted because it is used by other items!</h4>
    </div>';

$lang['actions_error']  = '
	<div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-ban"></i>Warning!</h4>
    </div>';

$lang['actions_upload_error']  = '
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-ban"></i>Upload Errors! Please Select file!</h4>
    </div>';

$lang['actions_info']  = '
	<div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5 style="margin:0px;"><i class="icon fa fa-info"></i>Info</h4>
    </div>';