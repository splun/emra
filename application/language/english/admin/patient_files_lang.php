<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['section_1'] = "Complaints";
$lang['section_ddl_1'] = "Complaints DDL";
$lang['section_2'] = "Diagnosis";
$lang['section_ddl_2'] = "Diagnosis DDL";
$lang['section_3'] = "Environment";
$lang['section_4'] = "Family History";
$lang['section_5'] = "Asthma";
$lang['section_6'] = "Eczema";
$lang['section_7'] = "Rhino-Conjunctivitis";
$lang['section_9'] = "Immunity";
$lang['section_10'] = "Development";
$lang['section_11'] = "Birth and Vaccination";
$lang['section_12'] = "Diet";
$lang['section_13'] = "Physical Exam";
$lang['section_15'] = "ImmunoCAP";
$lang['section_16'] = "Skin Testing";
$lang['section_17'] = "Rhinoscopy";
$lang['section_18'] = "Spirometer";
$lang['section_25'] = "Investigations";
$lang['section_26'] = "Immunotherapy";
$lang['section_ddl_27'] = "Referred Speciality DDL";
$lang['section_29'] = "Food Allergy Name";
$lang['section_ddl_29'] = "Food Allergy Question";
$lang['section_30'] = "Drug Allergy Name";
$lang['section_ddl_30'] = "Drug Allergy Question";

$lang['pagetitle_category'] = 'Patient files category';
$lang['pagetitle_category_edit'] = 'Edit patient files category';
$lang['pagetitle_category_add'] = 'Create patient files category';

$lang['pagetitle'] = 'Patient Files';
$lang['pagetitle_list'] = 'list';
$lang['pagetitle_edit'] = 'Edit';
$lang['pagetitle_add'] = 'Create';

$lang['patient_file_add_label'] = 'Add';
$lang['patient_file_category_label'] = 'Category';
$lang['item_add'] = 'Create item';

$lang['name_label'] = 'Name';