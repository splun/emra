<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['footer_copyright']           = 'Copyright';
$lang['footer_all_rights_reserved'] = 'All rights reserved';
$lang['footer_version']             = 'version';

$lang['upload_title']  = 'Upload Results';
$lang['upload_result_description'] = 'Description';
$lang['upload_result_date'] = 'Results date';
$lang['upload_result_select_section'] = 'Section';
$lang['upload_result_select_patient'] = 'Select patient';
$lang['upload_results_files'] = 'Files';
$lang['upload_empty_field'] = 'Please fill this field';

