<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['instock_pagetitle'] = 'Stock';
$lang['instock_pagetitle_edit'] = 'Edit stock item';
$lang['instock_pagetitle_add'] = 'Create stock item';

$lang['stockmedications_pagetitle'] = 'Stock items';
$lang['stockmedications_pagetitle_edit'] = 'Edit stock item';
$lang['stockmedications_pagetitle_add'] = 'Create stock item';
$lang['stockmedications_pagetitle_category'] = 'Stock items category';
$lang['stockmedications_pagetitle_category_edit'] = 'Edit stock items category';
$lang['stockmedications_pagetitle_category_add'] = 'Create stock items category';

$lang['locations_pagetitle'] = 'Stock locations';
$lang['locations_pagetitle_edit'] = 'Edit stock location';
$lang['locations_pagetitle_add'] = 'Create stock location';

$lang['name_code_label'] = 'Name/code';
$lang['item_category_label'] = 'Item category';
$lang['item_name_label'] = 'Item name';
$lang['type_label'] = 'Item type';
$lang['remaining_qty_label'] = 'Remaining qty/dose';
$lang['qty_label'] = 'Qty/dose';
$lang['price_label'] = 'Purchase price';
$lang['expire_date_label'] = 'Expiration date'; //Expire date
$lang['purchase_date_label'] = 'Admission date'; //Purchase date
$lang['start_qty_label'] = 'Start quantity/dose';
$lang['start_qty_warning'] = 'WARNING: This is the starting amount, change it only if there is an error when creating this record';

$lang['name_label'] = 'Name';
$lang['category_label'] = 'Category';
$lang['location_label'] = 'Location';
$lang['locations_notes_label'] = 'Notes';
$lang['active_stock_items'] = 'Items in stock';
$lang['sc_label'] = 'Subсutaneous';
$lang['sl_label'] = 'Sublingual';
$lang['sc_sl_label'] = 'SC/SL';
$lang['out_stock_label'] = 'Out of stock';
$lang['other_type_label'] = 'Other';
$lang['unit_label'] = 'Unit';
$lang['currency_label'] = 'Currency';
$lang['vile_label'] = 'Vile';