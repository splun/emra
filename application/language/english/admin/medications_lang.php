<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pagetitle_category'] = 'Medications category';
$lang['pagetitle_category_edit'] = 'Edit medications category';
$lang['pagetitle_category_add'] = 'Create medications category';

$lang['pagetitle'] = 'Medications';
$lang['pagetitle_list'] = 'Medications list';
$lang['pagetitle_edit'] = 'Edit medications';
$lang['pagetitle_add'] = 'Create medications';

$lang['create_medications'] = 'Create medication';
$lang['name_label'] = 'Name';
$lang['category_label'] = 'Category';