<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mini_calendar_title']  = 'Mini Calendar';
$lang['notes_title'] = 'Notes';
$lang['tasks_title']  = 'Tasks';
$lang['appointments_title']  = 'Appointments';
$lang['current_patients_title']  = 'Current Patient';
$lang['appointments_total_title']  = 'Total';
$lang['today_patients_title']  = 'Today patients';
$lang['stock_title']  = 'Stock';
$lang['finance_title']  = 'Finance';

$lang['notes_text_label']       = 'Note';
$lang['notes_patientid_label']  = 'File No.';

$lang['task_title_label']  = 'Title';
$lang['task_date_label']   = 'Date';
$lang['task_status']  	   = 'Status';
$lang['task_to_label']     = 'To';
$lang['task_low_label']    = 'Low';
$lang['task_med_label']    = 'Medium';
$lang['task_hight_label']  = 'High';
$lang['task_text_label']   = 'Description';
$lang['status_not_started']= 'Not Started';
$lang['status_started']	   = 'In Progress';
$lang['task_allusers_label'] = 'All users';


$lang['stock_expires_items'] = 'Items to expire';
$lang['stock_lowqty_items'] = 'Item quantity low';

$lang['finance_today_label'] = 'Today';
$lang['finance_week_label'] = 'This week';
$lang['finance_month_label'] = 'This month';
$lang['finance_total_label'] = 'Total payed';
$lang['finance_cash_label'] = 'Cash';
$lang['finance_knet_label'] = 'KNET';
$lang['finance_pending_label'] = 'Pending payments';


$lang['app_type'] = 'Type';
$lang['app_time'] = 'Time';
$lang['app_patient'] = 'Patient name';
$lang['information'] = "Information";
$lang['doctor'] ="Doctor/Nurse";
$lang['app_all_doctors'] = 'All';
$lang['app_time_in'] ="In";
$lang['app_time_out'] ="Out";

$lang['last_records_visits'] = 'Visits';
$lang['last_records_skin'] = 'Allergy test';
$lang['last_records_prescripts'] ="Prescription";
$lang['last_records_investigations'] ="Investigations";
$lang['last_records_invoices'] ="Invoices";
$lang['last_records_summary'] ="Summary";
$lang['last_records_results'] ="Results";
$lang['last_records_sickleaves'] ="Sick Leaves";
$lang['last_records_referals'] ="Referals";
$lang['last_exam_photo'] = "Exam/Photo";
$lang['last_spirometry'] = "Spirometry";