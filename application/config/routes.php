<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'admin/dashboard';
$route['admin/settings/interfaces/(:any)'] = 'admin/settings/prefs/interfaces/$1';
$route['admin/settings/patient_files/(:any)'] = 'admin/settings/patientFiles/index/$1';
$route['admin/settings/patient_files/ddl/(:any)'] = 'admin/settings/patientFiles/ddl/$1';
$route['admin/settings/patient_files/edit/(:any)'] = 'admin/settings/patientFiles/edit/$1';
$route['admin/settings/patient_files/edit_ddl/(:any)'] = 'admin/settings/patientFiles/edit_ddl/$1';
$route['admin/settings/patient_files/add/(:any)'] = 'admin/settings/patientFiles/add/$1';
$route['admin/settings/patient_files/add_ddl/(:any)'] = 'admin/settings/patientFiles/add_ddl/$1';
$route['admin/settings/patient_files/delete/(:any)'] = 'admin/settings/patientFiles/delete/$1';
$route['admin/settings/patient_files/delete_ddl/(:any)'] = 'admin/settings/patientFiles/delete_ddl/$1';
$route['admin/settings/patient_files/category_add/(:any)'] = 'admin/settings/patientFiles/category_add/$1';
$route['admin/settings/patient_files/category_edit/(:any)'] = 'admin/settings/patientFiles/category_edit/$1';
$route['admin/settings/patient_files/category_delete/(:any)'] = 'admin/settings/patientFiles/category_delete/$1';
$route['admin/settings/patient_files/ddl_category_add/(:any)'] = 'admin/settings/patientFiles/ddl_category_add/$1';
$route['admin/settings/patient_files/ddl_category_edit/(:any)'] = 'admin/settings/patientFiles/ddl_category_edit/$1';
$route['admin/settings/patient_files/ddl_category_delete/(:any)'] = 'admin/settings/patientFiles/ddl_category_delete/$1';
