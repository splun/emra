<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_file_install()
    {     
        if (file_exists('install.php'))
        {
            $val = '<div class="row">';
            $val.= '<div class="col-md-12">';
            $val.= '<div class="alert alert-danger">';
            $val.= '<h4><i class="icon fa fa-warning"></i>' . lang('actions_security_error') . '</h4>';
            $val.= '<p>' . sprintf(lang('actions_file_install_exist'), '<a href="#" class="btn btn-warning btn-flat btn-xs">' . strtolower(lang('actions_delete')) . '</a>') . '</p>';
            $val.= '</div>';
            $val.= '</div>';
            $val.= '</div>';

            return $val;
        }
    }

    /************Global**************/
    public function get_items($table,$filter=0,$order="",$orderby="",$limit=999999){
        if($filter !== 0){
            if(count($filter) == 1 && isset($filter["id"])){
                return $this->db->get_where($table, $filter)->row();
            }else{

                if(!empty($order) && !empty($orderby)){
                    return $this->db->order_by($order,$orderby)->get_where($table, $filter, $limit)->result();
                }else{
                    return $this->db->get_where($table, $filter,$limit)->result();
                }
            }
        }else{
            if(empty($order) && empty($orderby)){
                return $this->db->limit($limit)->get($table)->result();
            }else{
                return $this->db->limit($limit)->order_by($order,$orderby)->get($table)->result();
            }   
        }

    }

    public function get_item($table,$filter=0){
        if($filter !== 0){
          return $this->db->get_where($table, $filter)->row();
        }
    }

    public function get_count_items($table,$filter=0){
        $this->db->from($table);

        if($filter !== 0){
            $this->db->where($filter);
        }

        return $this->db->count_all_results();
    }

    public function insert_item($table,$fields)
    {    
        $this->db->insert($table, $fields);
        return $this->db->insert_id();
    }

    public function insert_all_items($table,$fields)
    {    
        return $this->db->insert_batch($table, $fields);
         
    }

    public function update_item($table,$fields,$where=0) 
    {
        if($where==0){
            return $this->db->insert($table,$fields);
        }else{
            $this->db->where($where);
            return $this->db->update($table,$fields);
        }        
    }

    public function delete_item($table,$id)
    {
        return $this->db->delete($table, array('id' => $id));
    }

    public function delete_items($table,$filter)
    {
        return $this->db->delete($table, $filter);
    }


}
