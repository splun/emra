<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppointmentsModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_appointments($date=0,$doctor_id = 0,$status="all",$limit=3000,$sorted=FALSE){

        $this->db->select("appointments.*,rooms.name as room_name,appointments_type.name as type_name,appointments_type.abbr as type_abbr,appointments_type.color as type_color,patients.`patient_name` as patient_name,patients.`id` as patient_id,patients.`telephone` as patient_phone,patients.`mobile` as patient_mobile,patients.`mobile2` as patient_mobile2,patients.`dob` as patient_dob,insurance.name as insurance_name,users.id as uid,users.first_name as doctor_name,users.last_name as doctor_lname,users.panel_color as ucolor")
            ->from('appointments')
            ->join('rooms', 'appointments.`room` = rooms.`id`')
            ->join('appointments_type', 'appointments.`type` = appointments_type.`id`')
            //->join('patients', 'patients.`id` = appointments.`event_name` OR patients.`telephone` = appointments.`event_name` OR patients.`mobile` = appointments.`event_name` ')
			->join('patients', 'patients.`id` = appointments.`event_name`')
            ->join('insurance', 'patients.`ddl_insurance` = insurance.`id`','left')
            ->join('users', 'users.`id` = appointments.`doctor_id`','left');

        if($date !== 0){
            if(is_array($date)){
                $start_date = date("Y-m-d",strtotime($date[0]));
                $end_date = date("Y-m-d",strtotime($date[1]));
                $this->db->where(array("DATE(appointments.start_date) >="=>$start_date,"DATE(appointments.start_date) <="=>$end_date));
            }else{
                $this->db->where(array("DATE(appointments.start_date)"=>$date));
            }
        }

        if($doctor_id !== 0){
            $this->db->where(array("appointments.doctor_id"=>$doctor_id));
        }


        if($status !== "all"){
        $this->db->where(array("appointments.done_flag"=>$status));
        }

        if($sorted){
            $this->db->order_by("appointments.out_come_flag ASC,appointments.in_come_time DESC,appointments.start_date DESC");
        }else{
            $this->db->order_by("appointments.start_date DESC");
        }

        
        if((int)$limit !== -1){
            $this->db->limit($limit);
        }

        return $this->db->get()->result();  
    }

    public function get_appointments_by_patient($filter=NULL,$date=0,$doctor_id=0){

        if(!$filter){
            return NULL;
        }

        $this->db->select("appointments.*,rooms.name as room_name,appointments_type.name as type_name,appointments_type.abbr as type_abbr,appointments_type.color as type_color,users.id as uid,users.first_name as doctor_name,users.last_name as doctor_lname,users.panel_color as ucolor")
            ->from('appointments')
            ->join('rooms', 'appointments.`room` = rooms.`id`')
            ->join('appointments_type', 'appointments.`type` = appointments_type.`id`')
            ->join('users', 'users.`id` = appointments.`doctor_id`','left');
        
        if($date !== 0){
            if(is_array($date)){
                $start_date = date("Y-m-d",strtotime($date[0]));
                $end_date = date("Y-m-d",strtotime($date[1]));
                $this->db->where(array("DATE(appointments.start_date) >="=>$start_date,"DATE(appointments.start_date) <="=>$end_date));
            }else{
                $this->db->where(array("DATE(appointments.start_date)"=>$date));
            }
        }

        if($doctor_id !== 0){
            $this->db->where(array("appointments.doctor_id"=>$doctor_id));
        }

        $patient_id = $filter["patient_id"];
        $patient_name = $filter["patient_name"];
        $patient_phone = $filter["patient_telephone"];

        $this->db->where("appointments.`event_name` = $patient_id OR appointments.`event_name` = '{$patient_name}' OR appointments.`event_name` = '{$patient_phone}' ");

        $this->db->order_by("appointments.out_come_flag ASC,appointments.start_date DESC");

        return $this->db->get()->result();

    }

    public function get_appointment($patient,$date){

        if(!$date && !$patient){
            return NULL;
        }

        $patient_id = $patient->id;
        $patient_name = $patient->patient_name;
        $patient_phone = $patient->telephone;

        $this->db->select("appointments.doctor_id")
            ->from('appointments')
            ->where("DATE(appointments.start_date) = '{$date}' AND (appointments.`event_name` = $patient_id OR appointments.`event_name` = '{$patient_name}' OR appointments.`event_name` = '{$patient_phone}')")
            ->limit(1);
        return $this->db->get()->row();
    }

    public function get_count_pending_cases($doctor_id=0){
        $this->db->from('appointments')
        ->join('patients', 'patients.`id` = appointments.`event_name`');

        if($doctor_id !== 0){
            $this->db->where(array("appointments.doctor_id"=>$doctor_id));
        }

        $this->db->where("appointments.done_flag = 0 AND appointments.doctor_id = 5");//Comment if all doctors pending cases

        return $this->db->count_all_results();
    }

    public function get_count_outcome($date, $doctor_id=0){
        $this->db->from('appointments')
        ->join('patients', 'patients.`id` = appointments.`event_name`');

        if($doctor_id !== 0){
            $this->db->where(array("appointments.doctor_id"=>$doctor_id));
            $this->db->where("appointments.out_come_flag = 1 AND DATE(appointments.start_date) = '{$date}' AND  appointments.doctor_id = {$doctor_id}");//Comment if all doctors pending cases
        }else{
            $this->db->where("appointments.out_come_flag = 1 AND DATE(appointments.start_date) = '{$date}'");//Comment if all doctors pending cases
        }

        
        
        return $this->db->count_all_results();
    }

    public function updateAppointmentInCome($date_appointment,$patient_id){

        $this->db->where(array("DATE(appointments.start_date)"=>date("Y-m-d",strtotime($date_appointment)),"event_name"=>$patient_id));

        return $this->db->update("appointments",array("in_come_flag"=>1,"in_come_time"=>date("Y-m-d H:i:s"),"done_flag" => 0));  
    }
    /************Global**************/
    public function get_items($table,$filter=0,$order="",$orderby=""){
        if($filter !== 0){
            if(count($filter) == 1 && isset($filter["id"])){
                return $this->db->get_where($table, $filter)->row();
            }else{
                if(isset($order) && isset($orderby)){
                    return $this->db->order_by($order,$orderby)->get_where($table, $filter)->result();
                }else{
                    return $this->db->get_where($table, $filter)->result();
                }
            }
        }else{
            if(isset($order) && isset($orderby)){
                return $this->db->get($table)->result();
            }else{
                return $this->db->order_by($order,$orderby)->get($table)->result();
            }   
        }

    }


    public function insert_item($table,$fields)
    {    
        $this->db->insert($table, $fields);
        return $this->db->insert_id();
    }

    public function insert_all_items($table,$fields)
    {    
        return $this->db->insert_batch($table, $fields);
         
    }

    public function update_item($table,$fields,$id) 
    {
        if($id==0){
            return $this->db->insert($table,$fields);
        }else{
            $this->db->where('event_id',$id);
            return $this->db->update($table,$fields);
        }        
    }


    public function delete_item($table,$id)
    {
        return $this->db->delete($table, array('event_id' => $id));
    }

    public function delete_items($table,$filter)
    {
        return $this->db->delete($table, $filter);
    }

}
