<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PatientsModel extends CI_Model {

     function __construct(){

        parent::__construct();
    }

     function get_patients($filter=0){

        $this->db->select("patients.id,patients.patient_name,patients.telephone,patients.mobile");
        $this->db->from('patients');

        if($filter !== 0){

            if(isset($filter["immuno"]) &&  $filter["immuno"] == 1){
                $this->db->join('immunotherapy_procedure_plan', 'immunotherapy_procedure_plan.patient_id = patients.id');
                unset($filter["immuno"]); 
            }

            $this->db->where($filter);

        }

        $this->db->group_by('patients.id'); 

        return $this->db->get()->result(); 
    }

     function search_patients($search=""){

        $this->db->select("patients.id,patients.patient_name,patients.telephone,patients.mobile");
        $this->db->from('patients');

        //if($search !== ""){
        $this->db->where('patients.id', $search) 
                ->or_where('patients.telephone', $search)
                ->or_where('patients.mobile', $search)
                ->or_like('patients.patient_name', $search)
                ->or_like('patients.patient_name_ar', $search);;
        //}


        return $this->db->get()->result(); 
    }

     function get_patient_report_files($patient){
        $this->db->select("patients.file03 as urls")
        ->from('patients')
        ->where("id = $patient");

        return $this->db->get()->row();
     } 

     function advanced_search_patients($filter=0,$join = 0){

        $this->db->select("patients.id,patients.patient_name,patients.telephone,patients.mobile");
        $this->db->from('patients');

        if($filter !== 0){

            if(isset($filter["immuno"]) &&  $filter["immuno"] == 1){
                $this->db->join('immunotherapy_procedure_plan', 'immunotherapy_procedure_plan.patient_id = patients.id');
                unset($filter["immuno"]); 
            }

            if($join !== 0 && is_array($join)){
                foreach($join as $t){
                  $this->db->join("{$t['table']}","{$t['on']}");  
                }
            }

            $this->db->where($filter);
            $this->db->group_by('patients.id');
        }

        return $this->db->get()->result(); 
    }

     function get_patient_prescriptions($patient_id,$date_appointment = 0){

        $this->db->select("prescription.*,
            patient_file.`name` as med_name,
            patient_file_categories.`name` as med_cat_name,
            prescription_forms.`name` as form_name,
            prescription_frequency.`name` as freq_name,
            prescription_route.`name` as route_name,
            prescription_use.`name` as use_name");
        $this->db->from('prescription');
        $this->db->join('patient_file','patient_file.`id` = prescription.`patient_file_id`','left');
        $this->db->join('patient_file_categories','patient_file_categories.id = prescription.`patient_file_category_id`','left');
        $this->db->join('prescription_forms','prescription_forms.`id` = prescription.`form`','left');
        $this->db->join('prescription_frequency','prescription_frequency.`id` = prescription.`freq`','left');
        $this->db->join('prescription_route','prescription_route.`id` = prescription.`route`','left');
        $this->db->join('prescription_use','prescription_use.`id` = prescription.`p_use`','left');

        if($date_appointment !== 0){
            $this->db->order_by("patient_file.`name`","DESC");
        }else{
            $this->db->order_by("prescription.`date_add`","DESC");
        }
        

        $fields = array(
            "prescription.`patient_id`"=>$patient_id,
        );

        if($date_appointment !== 0){
            $fields += array("prescription.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        if($date_appointment !== 0){
            return $this->db->get()->result_array(); 
        }else{
            return $this->db->get()->result();
        }
    }

     function get_patient_prescription_history($patient_id,$limit=0){
        $this->db->select("prescription.*")
            ->from('prescription')
            ->order_by("prescription.`date_add`","DESC")
            ->group_by("prescription.`date_add`");

        $this->db->where(array(
            "prescription.`patient_id`"=>$patient_id
        ));

        if($limit !== 0){
            $this->db->limit($limit);
        }
        return $this->db->get()->result();
    }

     function get_patient_drug_allergy($patient_id,$section,$date_appointment = 0){

        $this->db->select("allergy.*,patient_file.`name` as allergy_name");
        $this->db->from('allergy');
        $this->db->join('patient_file','patient_file.`id` = allergy.`patient_file_id`');
        if($date_appointment !== 0){
            $this->db->order_by("allergy.`id`","ASC");
        }else{
            $this->db->order_by("allergy.`date_add`","DESC");
        }

        $fields = array(
            "allergy.`patient_id`"=>$patient_id,
            "allergy.`section`"=>$section
        );

        if($date_appointment !== 0){
            $fields += array("allergy.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        if($date_appointment !== 0){
            return $this->db->get()->result_array();
        }else{
            return $this->db->get()->result();
        } 
    }


     function get_patient_diagnosis($patient_id,$section,$date_appointment = 0){

        $this->db->select("patient_checkboxpage.*,patient_file.`name` as name_diagnos");
        $this->db->from('patient_checkboxpage');
        $this->db->join('patient_file','patient_file.`id` = patient_checkboxpage.`patient_file_id`');

        if($date_appointment == 0){
            $this->db->group_by("patient_file.`id`");
        }

        $this->db->order_by("patient_checkboxpage.`date_add`","DESC");

        $fields = array(
                    "patient_checkboxpage.`patient_id`"=>$patient_id,
                    "patient_checkboxpage.`section`"=>$section
                );

        if($date_appointment !== 0){
            $fields += array("patient_checkboxpage.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        return $this->db->get()->result();

    }


    function get_patient_last_spirometry($patient_id,$limit=0){

        $this->db->select("*")
            ->from('patient_checkboxpage')
            ->group_by("patient_checkboxpage.`date_add`")
            ->order_by("patient_checkboxpage.`date_add`","DESC")
            ->where(array(
                "patient_checkboxpage.`patient_id`"=>$patient_id,
                "patient_checkboxpage.`section`"=>18
            ));

        if($limit !== 0){
            $this->db->limit($limit);
        }

        return $this->db->get()->result();

    }

     function get_patient_skintests_history($patient_id,$limit=0){
        $this->db->select("patient_checkboxpage.*")
            ->from('patient_checkboxpage')
            ->order_by("patient_checkboxpage.`date_add`","DESC")
            ->group_by("patient_checkboxpage.`date_add`");;

        $this->db->where(array(
            "patient_checkboxpage.`patient_id`"=>$patient_id,
            "patient_checkboxpage.`section`"=>16
        ));

        if($limit !== 0){
            $this->db->limit($limit);
        }
        return $this->db->get()->result();
    }

     function get_patient_results_history($patient_id,$limit = 0,$date_appointment = 0){

        $this->db->select("*");
        $this->db->from('share_field_section');
        $this->db->order_by("`date_test`","DESC");

        if($date_appointment == 0){
            $this->db->where(
                array(
                    "`patient_id`"=>$patient_id,
                    "desc !="=>""
                )
            );

            if($limit !== 0){
                $this->db->limit($limit);
            }
        
            return $this->db->get()->result();

        }else{

            $this->db->where(
                array(
                    "`patient_id`"=>$patient_id,
                    "date_test"=>$date_appointment
                )
            );

           return $this->db->get()->row();
        }

    }

     function get_patient_treatments($patient_id,$date_appointment = 0){

        $this->db->select("treatments.*,patient_file.`name` as name_treatment")
            ->from('treatments')
            ->join('patient_file','patient_file.`id` = treatments.`patient_file_id`')
            ->join('patient_file_categories','patient_file_categories.`id` = treatments.`patient_file_category_id`')
            ->order_by("treatments.`id`","ASC");

        $fields = array(
            "treatments.`patient_id`"=>$patient_id,
        );

        if($date_appointment !== 0){
            $fields += array("treatments.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        return $this->db->get()->result_array();
    }

     function get_patient_investigations($patient_id,$date_appointment = 0,$group=0){

        $this->db->select("investigation.*,patient_file.`name` as name_investigations,patient_file_categories.`name` as investigations_cat_name")
            ->from('investigation')
            ->join('patient_file','patient_file.`id` = investigation.`patient_file_id`')
            ->join('patient_file_categories','patient_file_categories.`id` = investigation.`patient_file_category_id`')
            ->order_by("investigation.`id`","ASC");

        $fields = array(
            "investigation.`patient_id`"=>$patient_id,
        );

        if($date_appointment !== 0){
            $fields += array("investigation.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        if($group !== 0){
            $this->db->where("patient_file_categories.`id` = $group");
        }

        return $this->db->get()->result_array();
    }

    function get_patient_investigation_category($patient_id,$date_appointment = 0){

        $this->db->select("patient_file_categories.`id` as category_id,patient_file_categories.`name` as cat_name")
            ->from('investigation')
            ->join('patient_file','patient_file.`id` = investigation.`patient_file_id`')
            ->join('patient_file_categories','patient_file_categories.`id` = investigation.`patient_file_category_id`')
            ->order_by("investigation.`id`","ASC");

        $fields = array(
            "investigation.`patient_id`"=>$patient_id,
        );

        if($date_appointment !== 0){
            $fields += array("investigation.`date_add`"=>$date_appointment);
        }

        $this->db->where($fields);

        $this->db->group_by("patient_file_categories.id");

        return $this->db->get()->result_array();
    }


     function get_patient_investigations_history($patient_id,$limit=0){

        $this->db->select("investigation.*")
            ->from('investigation')
            ->join('patient_file','patient_file.`id` = investigation.`patient_file_id`')
            ->join('patient_file_categories','patient_file_categories.`id` = investigation.`patient_file_category_id`')
            ->where(array("investigation.`patient_id`"=>$patient_id))
            ->order_by("investigation.`date_add`","DESC")
            ->group_by("investigation.`date_add`");

        if($limit !== 0){
            $this->db->limit($limit);
        }

        return $this->db->get()->result();
    }

     function get_patient_descriptions($patient_id,$section,$date_appointment = 0){

        $this->db->select("*");
        $this->db->from('share_field_section');
        $this->db->order_by("`date_test`","DESC");

        if($date_appointment == 0){
            $this->db->where(
                array(
                    "`patient_id`"=>$patient_id,
                    "`section`"=>$section,
                    "desc !="=>""
                )
            );

            return $this->db->get()->result();

        }else{

            $this->db->where(
                array(
                    "`patient_id`"=>$patient_id,
                    "`section`"=>$section,
                    "date_test"=>$date_appointment
                )
            );
           return $this->db->get()->row();
        }

    }

     function get_last_referrals($patient_id,$limit=0){

        $this->db->select("share_field_section.*,patient_file_ddl.`name` as refname")
                ->from('share_field_section')
                ->join('patient_file_ddl','patient_file_ddl.id = share_field_section.ddl_id')
                ->order_by("share_field_section.`date_test`","DESC")
                ->where(
                    array(
                        "share_field_section.`patient_id`"=>$patient_id,
                        "share_field_section.`section`"=>27
                    )
                );
        if($limit !== 0){
            $this->db->limit($limit);
        }     

        return $this->db->get()->result();

    }
     function get_immunoteraphy_plan($patient_id){
        return $this->db->get_where("immunotherapy_procedure_plan", array("patient_id"=>$patient_id))->row();
    }

     function get_visit_history($patient_id,$limit=0,$checkout=FALSE){

        $this->db->select("nurse.`date_visit`,appointments_type.name as type_name,appointments_type.abbr as type_abbr,appointments_type.color as type_color")
            ->from('patients')
            ->join('nurse','patients.`id` = nurse.`patient_id`')
            ->join('appointments','appointments.`event_name` = patients.`telephone` OR  appointments.`event_name` = patients.`mobile` OR appointments.`event_name`=patients.`email` OR appointments.`event_name`=patients.`id`')
            ->join('appointments_type', 'appointments_type.`id` = appointments.`type`', 'right');

            if($checkout){
                $this->db->where("patients.`id` = $patient_id AND DATE(appointments.`start_date`) = nurse.`date_visit` AND appointments.done_flag = 1");
            }else{
                $this->db->where("patients.`id` = $patient_id AND DATE(appointments.`start_date`) = nurse.`date_visit`");
            }

            $this->db->group_by("nurse.`date_visit`")
            ->order_by("nurse.`date_visit`","DESC");

            if($limit !== 0){
                $this->db->limit($limit);
            }
 
        return $this->db->get()->result();
    }

     function check_immunotherapy_procedure_history($vacId,$type,$plan_id){
        $this->db->select("`id`, `date_app`")
            ->from("`immunotherapy_procedure_history`")
            ->where(
                array(
                    "`plan_id`"=>$plan_id,
                    "`vaccine`" => $vacId,
                    "`type`" => $type,
                )
            )
            ->limit(1);

         return $this->db->get()->row();    
    }

     function get_immunotherapy_procedure_records($plan_id){
        $this->db->select("immunotherapy_procedure_records.*,stock_items.name as stname")
            ->from("immunotherapy_procedure_records")
            ->join("stock_items","immunotherapy_procedure_records.stock_vaccines = stock_items.id","left")
            ->where(
                array(
                    "immunotherapy_procedure_records.`plan_id`"=>$plan_id,
                )
            )
            ->order_by("immunotherapy_procedure_records.id","DESC");

         return $this->db->get()->result();    
    }

     function get_immunotherapy_procedure_last_record($plan_id,$type){
        $this->db->select("*")
            ->from("immunotherapy_procedure_records")
            ->where(
                array(
                    "`plan_id`"=>$plan_id,
                    "`type`"=>$type,
                )
            )
            ->order_by("id","DESC")
            ->limit(1);

         return $this->db->get()->row();
    }

     function get_immuno_procedure_last_record_by_patient($patient_id){
        $this->db->select("date_vacc,type")
            ->from("immunotherapy_procedure_records")
            ->where(
                array(
                    "`patient_id`"=>$patient_id
                )
            )
            ->order_by("id","DESC")
            ->limit(1);

         return $this->db->get()->row();
    }
    
     function get_stock_items_by_medications($medications,$type){
        $this->db->select("stock_items.*,stock_locations.name as location_name,units.name as unit_name")
        ->from("stock_items")
        ->join("stock_locations","stock_locations.id = stock_items.location_id","left")
        ->join("units","units.id = stock_items.unit","left")
        ->where(array('type'=>$type,'qty >'=>0,'expire_day >='=>date("Y-m-d")))
        ->where_in('medication_id', $medications);
        return $this->db->get()->result();
    }


     function get_files($patient_id,$date_appointment,$section){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`date_file`"=>$date_appointment,
                    "files_to_patient.`section`"=>$section
                )
            );

         return $this->db->get()->result();    
    }

    function get_spirometry_files($patient_id){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section`"=>18
                )
            );

         return $this->db->get()->result();    
    }

    function get_multiple_reports_files($patient_id){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid,files_to_patient.date_file as date_add")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section`"=>999
                )
            )
            ->order_by("files_to_patient.`date_file`","DESC");

         return $this->db->get()->result();    
    }


    function get_last_skintest_files($patient_id,$limit=1){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section`"=>105
                )
            )
            ->order_by("files_to_patient.`date_file`","DESC")
            ->limit($limit);

         return $this->db->get()->result();    
    }

    function get_examphoto_files($patient_id){
        $this->db->select("files_to_patient.date_file")
            ->from("files_to_patient")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section`"=>55
                )
            )
            ->order_by("files_to_patient.`date_file`","DESC")
            ->group_by("files_to_patient.`date_file`");

         return $this->db->get()->result();    
    }

    function get_last_examphoto_files($patient_id,$limit=1){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section`"=>55
                )
            )
            ->order_by("files_to_patient.`date_file`","DESC")
            ->limit($limit);

         return $this->db->get()->result();    
    }
    
    function get_results_files_dates($patient_id){
        $this->db->select("files_to_patient.*")
            ->from("files_to_patient")
            ->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`section` >="=>101,
                    "files_to_patient.`section` <="=>106
                )
            )
            ->group_by("files_to_patient.date_file");

        return $this->db->get()->result(); 

    }

    function get_results_files($patient_id,$date_appointment){
        $this->db->select("files_to_patient.*, files.name as fname,files.id as fid")
            ->from("files_to_patient")
            ->join("files","files_to_patient.file_id = files.id","left");

            $this->db->where(
                array(
                    "files_to_patient.`patient_id`"=>$patient_id,
                    "files_to_patient.`date_file`"=>$date_appointment,
                    "files_to_patient.`section` >="=>101,
                    "files_to_patient.`section` <="=>106
                )
            );

        return $this->db->get()->result();    
    }

    function get_patient_invoice($patient_id,$date_appointment){
        $this->db->select("patient_balance.*, invoice.name as item_name,invoice.category as invoice_category,invoice.price as invoice_price")
            ->from("patient_balance")
            ->join("invoice","patient_balance.invoice_item_id = invoice.id","left")
            ->order_by("patient_balance.id","ASC")
            ->where(
                array(
                    "patient_balance.`patient_id`"=>$patient_id,
                    "patient_balance.`invoice_date`"=>$date_appointment,
                )
            );

         return $this->db->get()->result();
    }

    function get_patient_invoice_by_date($patient_id,$date_appointment){
        $this->db->select("patient_balance.*, invoice.name as item_name,invoice.category as invoice_category,invoice.price as invoice_price")
            ->from("patient_balance")
            ->join("invoice","patient_balance.invoice_item_id = invoice.id","left")
            ->order_by("patient_balance.id","ASC")
            ->where(
                array(
                    "patient_balance.`patient_id`"=>$patient_id,
                    "DATE(patient_balance.`invoice_date`)"=>$date_appointment,
                )
            );

         return $this->db->get()->result();
    }

    function get_invoices_groupby_date($patient_id,$limit = 0){
        $this->db->select("patient_balance.*, invoice.name as item_name,invoice.category as invoice_category")
            ->from("patient_balance")
            ->join("invoice","patient_balance.invoice_item_id = invoice.id","left")
            ->order_by("patient_balance.invoice_date","DESC")
            ->group_by("patient_balance.invoice_date")
            ->where(
                array(
                    "patient_balance.`patient_id`"=>$patient_id,
                )
            );
            
            if($limit !== 0){
                $this->db->limit($limit);
            }

         return $this->db->get()->result();
    }

    function get_invoices($debt=0,$filter="",$doctor=0){

        if($debt !=0){
            $this->db->select("`patient_balance`.`ins_discount`,`patient_balance`.`qty` as qty, `patient_balance`.`discount_item`, `patient_balance`.`total`,`patient_balance`.`net_price`, `patients`.`id` as pid, `patients`.`patient_name`,`patients`.`telephone`, `patients`.`ddl_insurance`, `patient_pays`.`debt`, 
            `patient_pays`.`cash`,`patient_pays`.`knet`,`patient_pays`.`pay_day`,`patient_pays`.`invoice_doctor_id`,`patients`.`doctor`")

            ->from("patient_pays")
            ->join("patients","patient_pays.patient_id = patients.id")
            ->join("patient_balance","patients.id = patient_balance.patient_id");

        }else{

            $this->db->select("`patients`.`id` as pid, 
                `patients`.`patient_name`,
                `patients`.`ddl_insurance`,
                `patients`.`telephone`, 
                `patient_balance`.`id` as balid, 
                `patient_balance`.`invoice_date`, 
                `patient_balance`.`total`,
                `patient_balance`.`net_price`,
                `patient_balance`.`discount_item`,
                `patient_balance`.`discount_type`,
                `patient_balance`.`qty`,
                `patient_balance`.`ins_discount`,
                `patient_pays`.`invoice_doctor_id`
                ")

            ->from("patients")
            ->join("patient_balance","patient_balance.patient_id = patients.id")
            ->join("patient_pays","patients.id = patient_pays.patient_id AND patient_balance.invoice_date = patient_pays.pay_day");
        }


        if($debt != 0){

            $this->db->group_by("`patient_pays`.`id`")
            ->order_by("`patient_pays`.`pay_day`, `patients`.`id`");
        }else{

            $this->db->group_by("`patient_balance`.`patient_id`,`patient_balance`.`invoice_date`");
            $this->db->order_by("`patient_balance`.`invoice_date`,  `patients`.`id`");
        }

        if($doctor !==0){
            $this->db->where("patient_pays.`invoice_doctor_id` = ".$doctor);
        }

        if(trim($filter) != ""){
            $this->db->where($filter);
        }

        return $this->db->get()->result();
        
    }

    function check_doctor_appointment($patient,$doctor=0,$date){
        
        $this->db->select("doctor_id")->from("appointments");

        if($doctor){
            $this->db->where("DATE(`appointments`.`start_date`) = '{$date}' AND `appointments`.`event_name` = $patient AND `appointments`.`doctor_id` = ".$doctor."");
        }else{
            $this->db->where("DATE(`appointments`.`start_date`) = '{$date}' AND `appointments`.`event_name` = $patient");
        }

        $check = $this->db->get()->row();

        if($check){
            return $check;
        }else{
            return 0;
        }

    }

    function get_sales_per_item($filter="",$doctor=0){

        //if(empty($filter)) return;
        $this->db->select("`patient_balance`.*,`patients`.`id` as pid, `patients`.`patient_name`,`patients`.`telephone`, `invoice`.name as item_name,`users`.first_name as main_doctor_first_name,`users`.last_name as main_doctor_last_name, patient_pays.invoice_doctor_id as invoice_doctor_id")

            ->from("patient_balance")
            ->join("patients","patient_balance.patient_id = patients.id")
            ->join("users","patients.doctor = users.id")
            ->join("invoice","patient_balance.invoice_item_id = invoice.id","left")
            ->join("patient_pays","patients.id = patient_pays.patient_id AND patient_balance.invoice_date = patient_pays.pay_day","left");


        //$this->db->group_by("`patient_balance`.`patient_id`,invoice.id");
        
        $this->db->order_by("`patient_balance`.`invoice_date`,  `patients`.`id`");
        //var_dump($filter);
        if($doctor !== 0){
            $this->db->where("patient_pays.`invoice_doctor_id` = ".$doctor);
        }

        if(trim($filter) != ""){
            $this->db->where($filter);
        }

        return $this->db->get()->result();
    }

    function get_patient_pays($patient_id,$date_appointment){
        return $this->db->get_where("patient_pays", array("patient_id"=>$patient_id,"pay_day"=>$date_appointment))->row();
    }

    function get_count_pending_payments($doctor_id = 0){
        $this->db->select('patient_pays.id as payid, patients.id as pid')
        ->from('patient_pays')
        ->join('patients', 'patients.`id` = patient_pays.`patient_id`');

        if($doctor_id !== 0){
            $this->db->where(array("patients.doctor"=>$doctor_id));
        }

        $this->db->where(array("patient_pays.debt >"=>0));
        return $this->db->count_all_results();
    }

    function get_stock_item($invoice_item_id){
        $this->db->select("stock_items.id as stock_item_id,stock_items.qty as stock_item_qty")
            ->from("invoice")
            ->join("stock_items","invoice.selling_item_id = stock_items.id","left")
            ->where(
                array(
                    "invoice.`id`"=>$invoice_item_id,
                )
            );

         return $this->db->get()->row();
    }


    /************Global**************/
    function get_items($table,$filter=0,$order="",$orderby=""){
        if($filter !== 0){
            if(count($filter) == 1 && isset($filter["id"])){
                return $this->db->get_where($table, $filter)->row();
            }else{
                if(isset($order) && isset($orderby)){
                    return $this->db->order_by($order,$orderby)->get_where($table, $filter)->result();
                }else{
                    return $this->db->get_where($table, $filter)->result();
                }
            }
        }else{
            if(isset($order) && isset($orderby)){
                return $this->db->get($table)->result();
            }else{
                return $this->db->order_by($order,$orderby)->get($table)->result();
            }   
        }

    }


    function insert_item($table,$fields)
    {    
        $this->db->insert($table, $fields);
        return $this->db->insert_id();
    }

    function insert_all_items($table,$fields)
    {    
        return $this->db->insert_batch($table, $fields);
         
    }

    function update_item($table,$fields,$id) 
    {
        if($id==0){
            return $this->db->insert($table,$fields);
        }else{
            $this->db->where('id',$id);
            return $this->db->update($table,$fields);
        }        
    }


    function delete_item($table,$id)
    {
        return $this->db->delete($table, array('id' => $id));
    }

    function delete_items($table,$filter)
    {
        return $this->db->delete($table, $filter);
    }

}
