<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_stock_medications($id=0){

        if($id !== 0){
            return $this->db->get_where('stock_medications', array('id' => $id))->row();
        }else{
            $this->db->select('stock_medications.*,stock_categories.name as category_name');
            $this->db->from('stock_medications');
            $this->db->join('stock_categories', 'stock_categories.id = stock_medications.category',"left");

            return  $this->db->get()->result();   
        }

    }

    public function get_stock_medications_categories($id=0){

        if($id !== 0){
            return $this->db->get_where('stock_categories', array('id' => $id))->row();
        }else{
            return $this->db->get_where("stock_categories")->result();   
        }

    }

    public function get_stock_items($filter=0,$or_where=0,$id=0){

        if($id !== 0){
            return $this->db->get_where('stock_items', array('id' => $id))->row();
        }else{
            $this->db->select('stock_items.*,stock_locations.name as location,stock_medications.name as medication,stock_categories.name as category,units.name as item_unit,currencies.code as currency');
            $this->db->from('stock_items');
            $this->db->join('stock_locations', 'stock_locations.id = stock_items.location_id',"left");
            $this->db->join('stock_medications', 'stock_medications.id = stock_items.medication_id',"left");
            $this->db->join('stock_categories', 'stock_categories.id = stock_items.category_id',"left");
            $this->db->join('units', 'units.id = stock_items.unit',"left");
            $this->db->join('currencies', 'currencies.id = stock_items.currency',"left");
            //$this->db->where('patient_file.section', $section);

            if($filter !== 0) {
                $this->db->where($filter);
            }

            if($or_where !== 0) {
                $this->db->where($or_where);
            }

            return  $this->db->get()->result();   
        }

    }

    public function get_over_stock_items(){

        $this->db->select("qty,startQty,expire_day")
        ->from('stock_items')
        ->where(array("qty >" => 0,"startQty >" => 0,"expire_day >="=>date("Y-m-d h:i:s")));


        $count_over_items = 0;
        $count_overdue = 0;

        if($items = $this->db->get()->result()){

            foreach ($items as $item):
                $datetime1 = new DateTime(date('Y-m-d'));
                $datetime2 = new DateTime($item->expire_day);
                $interval = $datetime1->diff($datetime2);
                $overdue = $interval->days; 

                $qtyPercent = 0;

                //$qtyPercent = ($item->qty / $item->startQty) * 100;

                //if((int)$qtyPercent <= 30 OR $item->qty < 10){
                    //$count_over_items ++;
                //}else{
                if($overdue < 120){ 
                  $count_overdue ++;
                }
                //}

            endforeach;

        }

        //return $count_over_items + $count_overdue;
        return $count_overdue;
    }
    /************Global**************/

    public function get_items($table,$filter=0){
        if($filter !== 0){
            if(count($filter) < 2 && isset($filter["id"])){
                return $this->db->get_where($table, $filter)->row();
            }else{
                return $this->db->get_where($table, $filter)->result();
            }
        }else{
            return $this->db->get($table)->result();   
        }

    }

    public function insert_item($table,$fields)
    {    
        $this->db->insert($table, $fields);
        return $this->db->insert_id();
    }


    public function update_item($table,$fields,$id) 
    {
        if($id==0){
            return $this->db->insert($table,$fields);
        }else{
            $this->db->where('id',$id);
            return $this->db->update($table,$fields);
        }        
    }


    public function delete_item($table,$id)
    {
        return $this->db->delete($table, array('id' => $id));
    }

}
