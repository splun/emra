<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotesModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_notes($type = "all",$owner="",$limit = 25){

        $this->db->select("notes.*,patients.patient_name,patients.telephone")
            ->from("notes")
            ->join("patients","patients.`id` = notes.`patient_id`","left");
        
        $this->db->order_by("`done_flag` ASC,`id` DESC");


        if($type == "owner" && !empty($owner)){
            $this->db->where(array("notes.create_user"=>$owner));
            $this->db->or_where(array("notes.for_user REGEXP"=>$owner));
        }

        if($type == "pending"){
            $this->db->limit($limit);
            $this->db->where(array("done_flag"=>0));
        }

        if($type == "pending-owner" && !empty($owner)){
            $this->db->limit($limit);
            $this->db->where(array("done_flag"=>0));
            $this->db->group_start();
            $this->db->where(array("notes.create_user"=>$owner));
            $this->db->or_where(array("notes.for_user REGEXP"=>$owner));
            $this->db->group_end();
        }


        return $this->db->get()->result();
    }

    public function get_allocate_user_info($uid=0){
        if($uid != 0){
            return $this->db->select("first_name,last_name")->from("users")->where(array("id"=>$uid))->get()->row();
        }
    }



}