<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_interface($table)
    {
        $query = $this->db->get($table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }


    public function update_interfaces($table, $data)
    {
        $where = "id = 1";

        return $this->db->update($table, $data, $where);
    }

    public function get_patient_file_sections($section,$id=0){

        if($id !== 0){
            return $this->db->get_where('patient_file_categories', array('id' => $id))->row();
        }else{
            return $this->db->order_by('name', 'ASC')->get_where("patient_file_categories",array('section' => $section))->result();   
        }

    }

    public function get_patient_file($section,$id=0){

        if($id !== 0){
            return $this->db->get_where('patient_file', array('id' => $id))->row();
        }else{
            $this->db->select('patient_file.*,patient_file_categories.name as category_name');
            $this->db->from('patient_file');
            $this->db->join('patient_file_categories', 'patient_file_categories.id = patient_file.category',"left");
            $this->db->where('patient_file.section', $section);
            $this->db->order_by('patient_file.name',"ASC");
            return  $this->db->get()->result();   
        }

    }

    public function get_patient_file_ddl($section,$id=0){

        if($id !== 0){
            return $this->db->get_where('patient_file_ddl', array('id' => $id))->row();
        }else{
            return $this->db->order_by('name',"ASC")->get_where('patient_file_ddl', array('section' => $section))->result();
 
        }

    }


    public function get_invoice_items($id=0,$category=0){

        if($id !== 0){
            return $this->db->get_where('invoice', array('id' => $id))->row();
        }else{
            $this->db->select('invoice.*,invoice_categories.name as category_name, invoice_categories.parent as category_parent');
            $this->db->from('invoice');
            $this->db->join('invoice_categories', 'invoice_categories.id = invoice.category',"left");
            $this->db->order_by("invoice.flag_status", "desc");
            $this->db->order_by("invoice.name asc");
            
            if($category !== 0){
                $this->db->where("invoice.category",$category);
            }

           return  $this->db->get()->result();  
        }

    }

     public function get_invoice_selling_items($category = 0){

            $this->db->select('invoice.*,invoice_categories.name as category_name, invoice_categories.parent as category_parent,stock_items.qty as qty,units.name as unit,stock_items.expire_day as expire,stock_locations.name as location_name,stock_items.type as type');
            $this->db->from('invoice');
            $this->db->join('invoice_categories', 'invoice_categories.id = invoice.category',"left");
            $this->db->join('stock_items', 'stock_items.id = invoice.selling_item_id',"left");
            $this->db->join('units', 'units.id = stock_items.unit',"left");
            $this->db->join('stock_locations', 'stock_locations.id = stock_items.location_id',"left");
            $this->db->order_by("invoice.flag_status", "desc");
            $this->db->order_by("invoice.name asc");



            if($category !== 0){
                $this->db->where(array("invoice.category"=>$category,"invoice.flag_status >"=>0));
            }else{
                $this->db->where(array("invoice.flag_status >"=>0));   
            }


           return  $this->db->get()->result();  

    }

    public function get_invoice_items_per_category($id=0){

        if($id !== 0){
            return $this->db->get_where('invoice', array('id' => $id))->row();
        }else{
            $this->db->select('invoice.*,invoice_categories.name as category_name, invoice_categories.parent as category_parent');
            $this->db->from('invoice');
            $this->db->join('invoice_categories', 'invoice_categories.id = invoice.category',"left");
            $this->db->order_by("invoice.flag_status", "desc");
            $this->db->order_by("invoice.name asc");
            return  $this->db->get()->result();   
        }

    }

    public function get_invoice_child_categories($id=0){

        if($id !== 0){
            return $this->db->get_where("invoice_categories", array('parent' => $id))->result();
        }

    }

    /************Global**************/
    public function get_items($table,$filter=0){
        if($filter !== 0){
            if(count($filter) < 2 && isset($filter["id"])){
                return $this->db->get_where($table, $filter)->row();
            }else{
                return $this->db->get_where($table, $filter)->result();
            }
        }else{
            return $this->db->get($table)->result();   
        }

    }
    public function find_items($table,$id)
    {
        return $this->db->get_where($table, array('id' => $id))->row();
    }


    public function insert_item($table,$fields)
    {    
        return $this->db->insert($table, $fields);
    }


    public function update_item($table,$fields,$id) 
    {
        if($id==0){
            return $this->db->insert($table,$fields);
        }else{
            $this->db->where('id',$id);
            return $this->db->update($table,$fields);
        }        
    }


    public function delete_item($table,$id)
    {
        return $this->db->delete($table, array('id' => $id));
    }

}
