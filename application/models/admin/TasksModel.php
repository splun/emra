<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TasksModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_tasks($uid=0,$pending=false){
    	$this->db->select("tasks.*,task_users.task_status,users.first_name as owner_name,users.last_name as owner_lastname")
        ->from("task_users")
        ->join("tasks","task_users.`task_id` = tasks.`id`")
        ->join("users","tasks.`create_user` = users.`id`");

        if($uid !== 0){
        	$this->db->where(array("task_users.user_id" => $uid));
        }
        if($pending){
        	$this->db->where(array("task_users.task_status" => 0));
        }

        return $this->db->get()->result();
    }

    public function get_allocate_user_info($uid=0){
        if($uid != 0){
            return $this->db->select("first_name,last_name")->from("users")->where(array("id"=>$uid))->get()->row();
        }
    }

}