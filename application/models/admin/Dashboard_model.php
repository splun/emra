<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }

    public function get_finance_info($period = 0,$doctor=0){
        $this->db->select("SUM(patient_pays.knet) as totalKnet, 
                           SUM(patient_pays.cash) as totalCash,
                           SUM(patient_pays.debt) as totalDebt");
        $this->db->from("patient_pays")
        ->join('patients', 'patients.`id` = patient_pays.`patient_id`');

        if($period !== 0){
            $filter = 0;
            switch($period){
                case "day" : $filter = "patient_pays.`pay_day` = CURDATE()"; break;
                case "week" : $filter = "YEAR(patient_pays.`pay_day`) = YEAR(NOW()) AND WEEK(patient_pays.`pay_day`,1) = WEEK(NOW(),1)"; break;
                case "month" : $filter = "MONTH(patient_pays.`pay_day`) = MONTH(NOW()) AND YEAR(patient_pays.`pay_day`) = YEAR(NOW())"; break;
            }

            if($doctor !==0){
                $filter .=" AND patients.`doctor`=".$doctor;
            }

            $this->db->where($filter);
        }

        return $this->db->get()->row();
    }


}
