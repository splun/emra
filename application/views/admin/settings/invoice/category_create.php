<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('pagetitle_category_add'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create')); ?>
                            <div class="form-group">
                                <?php echo lang('name_label', 'name', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($name);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('category_label_parent', 'category', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="parent" class="form-control">
                                    <?php foreach($categories as $category){
                                        if($category->parent == 0){?>
                                            <option value="<?php echo $category->id; ?>">
                                            <?php echo $category->name; ?>
                                            </option>

                                            <?php $childs = $func->cild_categories($category->id);
                                                if($childs){ 
                                                    foreach($childs as $child){?>
                                                        <option value="<?php echo $child->id; ?>">
                                                        - <?php echo $child->name; ?>
                                                        </option>
                                                    <?}?>
                                                <?}?>
                                            <?}?>
                                        <?}?>
                                        <option value="0">
                                            Uncategorized
                                        </option>
                                    </select>
                                </div>
                            </div>                                    
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php echo anchor('admin/settings/invoice', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('pagetitle_category_edit'); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data">Name</th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($categories as $category):
                            if($category->parent == 0){?>
                                <tr>
                                    <form action="/admin/settings/invoice/category_edit/<?php echo $category->id;?>" method="POST">
                                        <td>
                                            <div class="form-group"><input class="form-control" type="text" name="name" value="<?php echo htmlspecialchars($category->name, ENT_QUOTES, 'UTF-8'); ?>"/>
                                            </div>
                                        </td>
                                        <td class="action-links">
                                            <button type="submit" class="btn btn-primary btn-flat"><?php echo lang('actions_save'); ?></button>
                                            <?php echo anchor('admin/settings/invoice/category_delete/'.$category->id, lang('actions_delete'),array('class' => 'delete-action btn btn-warning btn-flat')); ?>

                                        </td>
                                    </form>
                                </tr>

                                <?php $childs = $func->cild_categories($category->id);
                                if($childs) : ?>
                                <?php foreach($childs as $child){?>
                                <tr>
                                    <form action="/admin/settings/invoice/category_edit/<?php echo $child->id;?>" method="POST" class="child-category-form">
                                        <td style="padding-left:40px;">
                                            <div class="form-group"><input class="form-control" type="text" name="name" value="<?php echo htmlspecialchars($child->name, ENT_QUOTES, 'UTF-8'); ?>"/>
                                            </div>
                                        </td>
                                        <td class="action-links">
                                            <button type="submit" class="btn btn-primary btn-flat"><?php echo lang('actions_save'); ?></button>
                                            <?php echo anchor('admin/settings/invoice/category_delete/'.$child->id, lang('actions_delete'),array('class' => 'delete-action btn btn-warning btn-flat')); ?>

                                        </td>
                                    </form>
                                <?} endif;?>
                                </tr>
                            <?php $i++; }?>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
