<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('pagetitle_add'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create')); ?>
                            <div class="form-group">
                                <?php echo lang('name_label', 'name', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($name);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('price_label', 'price', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($price);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('net_price_label', 'net_price', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($net_price);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('category_label', 'category', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="category" class="form-control">
                                    <?php foreach($categories as $category){
                                        if($category->parent == 0){?>
                                            <option value="<?php echo $category->id; ?>">
                                            <?php echo $category->name; ?>
                                            </option>

                                            <?php $childs = $func->cild_categories($category->id);
                                                if($childs){ 
                                                    foreach($childs as $child){?>
                                                        <option value="<?php echo $child->id; ?>">
                                                        - <?php echo $child->name; ?>
                                                        </option>
                                                    <?}?>
                                                <?}?>
                                            <?}?>
                                        <?}?>
                                    </select>
                                </div>
                            </div>                                    
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php echo anchor('admin/settings/invoice/', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
