<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/settings/invoice/category_add/', '<i class="fa fa-plus"></i> '. lang('actions_cat_create_edit'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                    <h3 class="box-title">
                                    	<?php echo anchor('admin/settings/invoice/add/', '<i class="fa fa-plus"></i> '. lang('create_item'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>
                                    <table class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th class="sorting">#</th>
                                                <th class="sorting" aria-sort="ascending" ><?php echo lang('name_label');?></th>
                                                <th class="sorting" aria-sort="descending"><?php echo lang('price_label');?></th>
                                                <th class="sorting" aria-sort="descending"><?php echo lang('net_price_label');?></th>
                                                <th class="sorting" aria-sort="descending"><?php echo lang('category_label');?></th>
                                                <th class="sorting" aria-sort="descending"><?php echo lang('actions_status');?></th>
                                                <th><?php echo lang('actions_title');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1; foreach ($items as $item):?>
                                            <tr>
                                                <td><?php echo $i;?></td>

                                                <td><?php echo htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?>
                                                </td>
                                                <td><?php echo $item->price; ?></td>
                                                <td><?php echo $item->net_price; ?></td>
                                                <td>
                                                    <?php if(!empty($item->category_name)){

                                                        $parents = $func->parent_categories($item->category_parent);

                                                         if(!empty($parents)){
                                                            echo $parents->name.", ";
                                                         }

                                                        echo htmlspecialchars($item->category_name, ENT_QUOTES, 'UTF-8');
                                                        }else{ 
                                                            echo "---";
                                                        }

                                                    ?>
                                                </td>
                                                <td><?php echo ($item->flag_status) ? anchor('admin/settings/invoice/deactivate/'.$item->id, '<span class="label label-success">'.lang('actions_active').'</span>') : anchor('admin/settings/invoice/activate/'. $item->id, '<span class="label label-default">'.lang('actions_inactive').'</span>'); ?></td>
                                                <td class="action-links">
                                                    <?php echo anchor('admin/settings/invoice/edit/'.$item->id, lang('actions_edit')); ?>
                                                    <?php echo anchor('admin/settings/invoice/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>

                                                </td>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>