<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <?php if($type !== "ddl"){?>
                                    <h3 class="box-title"><?php echo anchor('admin/settings/patient_files/category_add/'.$section_id, '<i class="fa fa-plus"></i> '. lang('actions_cat_create_edit'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                    <h3 class="box-title">
                                    	<?php echo anchor('admin/settings/patient_files/add/'.$section_id, '<i class="fa fa-plus"></i> '. lang('item_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                    <?}else{?>
                                      <h3 class="box-title"><?php echo anchor('admin/settings/patient_files/ddl_category_add/'.$section_id, '<i class="fa fa-plus"></i> '. lang('actions_cat_create_edit'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                      <h3 class="box-title">
                                        <?php echo anchor('admin/settings/patient_files/add_ddl/'.$section_id, '<i class="fa fa-plus"></i> '. lang('item_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>  
                                    <?}?>    
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>
                                    <table class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th class="sorting" aria-controls="list-data">#</th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data">Name</th>
                                                <?php if($type !== "ddl"){?>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data">Category</th>
                                                <?}?>
                                                <th><?php echo lang('actions_title');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1; foreach ($items as $item):?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <?php if($type !== "ddl"){?>
                                                <td><?php echo (!empty($item->category_name)) ? htmlspecialchars($item->category_name, ENT_QUOTES, 'UTF-8') : "---"; ?></td>
                                                <?}?>
                                                <?php if($type !== "ddl"){?>
                                                <td class="action-links">
                                                    <?php echo anchor('admin/settings/patient_files/edit/'.$item->id, lang('actions_edit')); ?>
                                                    <?php echo anchor('admin/settings/patient_files/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                                </td>
                                                <?}else{?>
                                                <td class="action-links">
                                                    <?php echo anchor('admin/settings/patient_files/edit_ddl/'.$item->id, lang('actions_edit')); ?>
                                                    <?php echo anchor('admin/settings/patient_files/delete_ddl/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                                </td>
                                                <?}?>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>