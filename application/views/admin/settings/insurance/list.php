<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/settings/insurance/add/', '<i class="fa fa-plus"></i> '. lang('pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>



                    <div class="box-body">
                        <?php echo $message;?>
                        <table class="table table-bordered table-hover insuranceTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data">#</th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('name_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('discount_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('show_discount_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('status_label');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($items as $item):?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo $item->discount; ?>% </td>
                                    <td><?php echo ($item->showDiscount) ? anchor('admin/settings/insurance/showDiscountOff/'.$item->id, '<span class="label label-success">'.lang('actions_show').'</span>') : anchor('admin/settings/insurance/showDiscountOn/'. $item->id, '<span class="label label-default">'.lang('actions_hide').'</span>'); ?></td>
                                    <td><?php echo ($item->status) ? anchor('admin/settings/insurance/deactivate/'.$item->id, '<span class="label label-success">'.lang('actions_active').'</span>') : anchor('admin/settings/insurance/activate/'. $item->id, '<span class="label label-default">'.lang('actions_inactive').'</span>'); ?></td>
                                    <td class="action-links"><?php echo anchor('admin/settings/insurance/edit/'.$item->id, lang('actions_edit')); ?>
                                        <?php echo anchor('admin/settings/insurance/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
    $("document").ready(function(){
        var table = $('.insuranceTable').DataTable({
            "pageLength": 25,
            "aaSorting": [[ 1, "asc" ]]
        });
    });

</script>