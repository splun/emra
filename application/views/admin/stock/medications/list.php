<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/stock/stockmedications/category_add/', '<i class="fa fa-plus"></i> '. lang('actions_cat_create_edit'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                    <h3 class="box-title">
                                    	<?php echo anchor('admin/stock/stockmedications/add/', '<i class="fa fa-plus"></i> '. lang('stockmedications_pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>

                                <div class="box-body">
                                    <?php echo $message;?>

                                    <table class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th class="sorting" aria-controls="list-data">#</th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data">Name</th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('category_label');?></th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('sc_sl_label');?></th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('sc_label');?></th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('sl_label');?></strong></th>
                                                <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('active_stock_items');?></th>
                                                <th><?php echo lang('actions_title');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=1; foreach ($items as $item):?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo (!empty($item->category_name)) ? htmlspecialchars($item->category_name, ENT_QUOTES, 'UTF-8') : "---"; ?></td>
                                                <td>
                                                <?php 
                                                    if($item->sc == 1){
                                                        echo "SC";
                                                    }
                                                    if($item->sc == 1 AND $item->sl == 1){
                                                        echo "/";
                                                    }
                                                    if($item->sl == 1){
                                                        echo "SL";
                                                    }
                                                ?>
                                                </td>
                                                <td class="check-column"><?php echo ($item->sc == 1) ? "<i class='fa fa-check'></i><span style='visibility: hidden';>1</span>" : "<i class='fa fa-close'></i><span style='visibility: hidden';>0</span>"; ?></td>
                                                <td class="check-column"><?php echo ($item->sl == 1) ? "<i class='fa fa-check'></i><span style='visibility: hidden';>1</span>" : "<i class='fa fa-close'></i><span style='visibility: hidden';>0</span>"; ?></td>
                                                <td><a href="/admin/stock/instock/?filter=1&medication_id=<?php echo $item->id;?>&outofstock=1"><?php echo $func->get_active_stock_items($item->id); ?></a></td>
                                                
                                                <td class="action-links">
                                                    <?php echo anchor('admin/stock/stockmedications/edit/'.$item->id, lang('actions_edit')); ?>
                                                    <?php echo anchor('admin/stock/stockmedications/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>

                                                </td>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>