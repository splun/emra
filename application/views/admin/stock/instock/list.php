<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/stock/instock/add', '<i class="fa fa-plus"></i> '. lang('instock_pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <form id="filtered-stock" class="box-header with-border" action="/admin/stock/instock">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-2"><?php echo lang('name_code_label');?>:</div>
                                <div class="col-md-10 form-group">
                                    <?php echo form_input($name);?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-3"><?php echo lang('item_category_label');?>:</div>
                                <div class="col-md-9 form-group"> 
                                    <select data-index="2" name="category_id" class="form-control" onchange="itemBycategories(this)">
                                        <option value="0">--- Select ---</option>
                                        <?php foreach($categories as $category){?>
                                        <option value="<?php echo $category->id; ?>" <?php echo (isset($filter['category_id']) && $category->id == $filter['category_id']) ? 'selected="selected"' : "" ?>>
                                            <?php echo $category->name; ?>
                                        </option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-2">Items:</div>
                                <div class="col-md-10 form-group" id="dynamic-list">
                                    <select name="medication_id" data-index="3"  class="form-control">
                                    <option value="0">--- Select ---</option>
                                    <?php foreach($medications as $medication){?>
                                        <option value="<?php echo $medication->id; ?>" <?php echo (isset($filter['medication_id']) && $medication->id == $filter['medication_id']) ? 'selected="selected"' : "" ?>>
                                            <?php echo $medication->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-2"><?php echo lang('qty_label');?>:</div>
                                <div class="col-md-10 form-group">
                                    <?php echo form_input($qty);?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-3"><?php echo lang('price_label');?>:</div>
                                <div class="col-md-9 form-group">
                                    <?php echo form_input($price);?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-2"><?php echo lang('location_label');?>:</div>
                                <div class="col-md-10 form-group">
                                    <select name="location_id" data-index="9" class="form-control">
                                    <option value="0">--- Select ---</option>
                                    <?php foreach($locations as $location){?>
                                        <option value="<?php echo $location->id; ?>" <?php echo (isset($filter['location_id']) && $location->id == $filter['location_id']) ? 'selected="selected"' : "" ?>>
                                            <?php echo $location->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 date-fileds">
                            <div class="row">
                                <div class="col-md-3"><?php echo lang('expire_date_label');?>:</div>
                                <?php /* <div class="col-md-5 form-group">
                                    <!--From:-->
                                    <?php echo form_input($expire_day_from);?> 
                                </div> */?>
                                <div class="col-md-9 form-group">
                                    <!--To:-->
                                    <?php echo form_input($expire_day_to);?> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><?php echo lang('purchase_date_label');?>:</div>
                                <?php /*<div class="col-md-5 form-group">
                                    <!--From:-->
                                    <?php echo form_input($purchase_date_from);?> 
                                </div> */?>
                                <div class="col-md-9 form-group">
                                    <!--To:-->
                                    <?php echo form_input($purchase_date_to);?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 switch-controls slsc-type-box">
                            <div class="row">
                                <div class="col-md-5"><?php echo lang('sc_label');?>:</div>
                                <div class="col-md-7 form-group checkbox">
                                    <label class="switch">
                                        <input type="checkbox" name="sc" value="SC" data-index="4" />
                                        <span class="slider"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row margined">
                                <div class="col-md-5"><?php echo lang('sl_label');?>:</div>
                                <div class="col-md-7 form-group checkbox">
                                    <label class="switch">
                                        <input type="checkbox" data-index="4" name="sl" value="SL" />
                                        <span class="slider"></span>
                                    </label>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-md-2 switch-controls">
                            <div class="row">
                                <div class="col-md-5"><?php echo lang('out_stock_label');?>:</div>
                                <div class="col-md-7 form-group checkbox">
                                    <label class="switch">
                                        <input type="checkbox" name="outofstock" value="1" data-index="4" />
                                        <span class="slider"></span>
                                    </label>
                                </div>
                            </div>                   
                        </div>
                        <input type="hidden" name="filter" value="1">
                    </form>
                    <div class="box-header with-border text-center">
                        <div class="legend">
                          <span class="yell"></span> Item quantity low
                          <span class="rd"></span> 3 month to expire
                        </div>
                    </div>
                    <div class="box-body" id="stock-table-wrap">
                        <?php echo $message;?>
                        <table class="table table-bordered table-hover customTable" id="stock-items" role="grid">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data">#</th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data">
                                        <?php echo lang('name_code_label');?></th>
                                    <th class="sorting select-filter" aria-sort="descending" aria-controls="list-data"><?php echo lang('item_category_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('item_name_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('type_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('remaining_qty_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('qty_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('unit_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('price_label');?></th>
                                    <th class="sorting centered" aria-sort="descending" aria-controls="list-data"><?php echo lang('expire_date_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('location_label');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($items as $item):

                                $datetime1 = new DateTime(date('Y-m-d'));
                                $datetime2 = new DateTime($item->expire_day);

                                $interval = $datetime1->diff($datetime2);
                                $overdue = $interval->days; 


                                $alert_row = ''; 
                                if($curent_out_of_stock !== '1'){
                                    $qtyPercent = 0;

                                    if((float)$item->startQty > 0){
                                        $qtyPercent = ($item->qty / $item->startQty) * 100;

                                        if((int)$qtyPercent <= 30 OR $item->qty < 10){
                                              $alert_row = 'remaind';
                                        }
                                    }

                                    if($overdue < 120){ 
                                      $alert_row = 'overdue';
                                    }
                                }
                                ?>
                                <tr class="<?php echo $alert_row;?>">
                                    <td><?php echo $i;?></td>
                                    <td><?php echo htmlspecialchars($item->name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo $item->category; ?></td>
                                    <td><?php echo $item->medication; ?></td>
                                    <td class="centered"><?php echo $item->type; ?></td>
                                    <td class="qty-column centered"><?php echo $item->qty; ?></td>
                                    <td class="centered"><?php echo $item->startQty; ?></td>
                                    <td class="centered"><?php echo $item->item_unit; ?></td>
                                    <td class="centered"><?php echo $item->price; ?> <?php echo $item->currency; ?></td>
                                    <td class="expday-column centered"><?php echo $item->expire_day; ?>
                                    <td><?php echo $item->location; ?>
                                    </td>
                                    <td class="action-links"><?php echo anchor('admin/stock/instock/edit/'.$item->id, lang('actions_edit')); ?>
                                        <?php echo anchor('admin/stock/instock/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>

<script>

function itemBycategories(el){

    var cat = $(el).val();

    if(parseInt(cat) !== 1 && parseInt(cat) !== 0){
        $(".slsc-type-box input[type=checkbox]").removeAttr("checked");
        $(".slsc-type-box").hide();
    }else{
        $(".slsc-type-box").show();
    }

    if(parseInt(cat) !== 0){
        $.ajax({
          type: "POST",
          url: '/admin/stock/instock/medication_by_cat',
          data: 'category_id='+cat,
          success: function(response){
            $("#dynamic-list").html(response);
            $("#dynamic-list select").prepend('<option selected="selected" value="0">--- Select ---</option>');
          }

        });
    }

}
$("body,html").on("change","#filtered-stock input, #filtered-stock select", function(){

    if($(this).attr("name") == "category_id"){
        $("#dynamic-list select").val("0");
    }

    $.ajax({
      type: "POST",
      url: '/admin/stock/instock/filter',
      data: $("#filtered-stock").serialize(),
      dataType: "json",
      beforeSend: function() {
        $("#stock-items tbody").html('<tr class="od"><td valign="top" colspan="11" class="dataTables_empty">Loading...</td></tr>');
      },
      success: function(result){

        var html = '<table class="table table-bordered table-hover" id="stock-items" role="grid" >';
    
        html += '<thead>'+$("#stock-items thead").html()+'</thead>';
        html += '<tbody>';

        if(result){



            $(result).each(function(i){
                var qtyPercent = 0;
                var alert_row = ''; 
                var overdue = 120;
                var now = new Date();
                var expire_day = new Date(result[i].expire_day);

                if(expire_day){
                    var timeDiff = Math.abs(expire_day.getTime() - now.getTime());
                    var overdue = Math.ceil(timeDiff / (1000 * 3600 * 24));
                }

                qtyPercent = (result[i].qty / result[i].startQty) * 100;

                if(parseInt(qtyPercent) <= 30 || result[i].qty < 10){
                    alert_row = 'remaind';
                }

                if(overdue < 120){ 
                    alert_row = 'overdue';
                }


                html += '<tr class="odd '+alert_row+'" role="row"><td class="sorting_1">'+i+1+'</td><td>'+result[i].name+'</td><td>'+result[i].category+'</td><td>'+result[i].medication+'</td><td>'+result[i].type+'</td><td class="qty-column">'+result[i].qty+'</td><td>'+result[i].startQty+'</td><td>'+result[i].item_unit+'</td><td>'+result[i].price+'</td><td class="expday-column">'+result[i].expire_day+' </td><td> '+result[i].location+'</td><td class="action-links"><a href="/admin/stock/instock/edit/'+result[i].id+'">Edit</a><a href="/admin/stock/instock/delete/'+result[i].id+'" class="delete-action">Delete</a></td></tr>';
            });

        }

        html += '</tbody>';
        html += '</table>';

        $("#stock-table-wrap").html(html);
        var table = $('#stock-items').DataTable({
            "pageLength": 25
        });
        
      }

    });


});


$("document").ready(function(){

    if(window.location.href.indexOf("?") > -1) {
        window.history.pushState('page2', 'Title',location.origin + location.pathname);
    }

});
</script>