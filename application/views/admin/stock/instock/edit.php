<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('instock_pagetitle_edit'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>
                            <div class="form-group">
                                <?php echo lang('name_code_label', 'name', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($name);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('item_category_label', 'category', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="category_id" class="form-control" onchange="itemBycategories(this)">
                                    <?php foreach($categories as $category){?>
                                        <option value="<?php echo $category->id; ?>" <?php echo ($category->id == $item->category_id) ? 'selected="selected"' : "" ?>>
                                            <?php echo $category->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('item_name_label', 'category', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10" id="dynamic-list">
                                   <select name="medication_id" class="form-control">
                                    <?php foreach($medications as $medication){?>
                                        <option value="<?php echo $medication->id; ?>" <?php echo ($medication->id == $item->medication_id) ? 'selected="selected"' : "" ?>>
                                            <?php echo $medication->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('type_label', 'type', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="type" class="form-control">
                                       <option value="<?php echo $item->type; ?>" <?php echo ($item->type == 'Other') ? 'selected="selected"' : "" ?>>
                                            <?php echo lang('other_type_label'); ?>
                                        </option>
                                        <option value="<?php echo $item->type; ?>" <?php echo ($item->type == 'SC') ? 'selected="selected"' : "" ?>>
                                            <?php echo lang('sc_label'); ?>
                                        </option>
                                         <option value="<?php echo $item->type; ?>" <?php echo ($item->type == 'SL') ? 'selected="selected"' : "" ?>>
                                            <?php echo lang('sl_label'); ?>
                                        </option>
                                       
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('remaining_qty_label', 'qty', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($qty);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('purchase_date_label', 'purchase_date', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($purchase_date);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('price_label', 'price', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-9">
                                    <?php echo form_input($price);?>
                                </div>
                                <div class="col-sm-1">
                                   <select name="currency" class="form-control">
                                    <?php var_dump($currencies); foreach($currencies as $currency){?>
                                        <option value="<?php echo $currency->id; ?>" <?php echo ($currency->id == $item->currency) ? 'selected="selected"' : "" ?>>
                                            <?php echo $currency->code; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('expire_date_label', 'expire_day', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($expire_day);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('location_label', 'location_id', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="location_id" class="form-control">
                                    <?php foreach($locations as $location){?>
                                        <option value="<?php echo $location->id; ?>" <?php echo ($location->id == $item->location_id) ? 'selected="selected"' : "" ?>>
                                            <?php echo $location->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('start_qty_label', 'startQty', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($startQty);?><?php echo lang('start_qty_warning'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('unit_label', 'unit', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="unit" class="form-control">
                                    <?php foreach($units as $unit){?>
                                        <option value="<?php echo $unit->id; ?>" <?php echo ($unit->id == $item->unit) ? 'selected="selected"' : "" ?>>
                                            <?php echo $unit->name; ?>
                                        </option>
                                    <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('vile_label', 'vile', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                   <select name="vile" class="form-control"><option value="0">Select Vile</option><option <?php echo ($item->vile == "A") ? "selected='selected'" : "";?> value="A">VILE-A</option><option <?php echo ($item->vile == "B") ? "selected='selected'" : "";?> value="B">VILE-B</option><option <?php echo ($item->vile == "C") ? "selected='selected'" : "";?> value="C">VILE-C</option><option <?php echo ($item->vile == "D") ? "selected='selected'" : "";?> value="D">VILE-D</option></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('locations_notes_label', 'info', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_textarea($info);?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <?php echo form_hidden('id', $item->id);?>
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php echo anchor('admin/stock/instock', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
function itemBycategories(el){

    var cat = $(el).val();

      $.ajax({
      type: "POST",
      url: '/admin/stock/instock/medication_by_cat',
      data: 'category_id='+cat,
      success: function(response){
        console.log(response);
        $("#dynamic-list").html(response);
      }

  });

}
</script>