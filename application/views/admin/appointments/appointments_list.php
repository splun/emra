<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                    <div class="box-body">
                        <?php echo $message;?>
                        <div class="row">
                            <form id="appointments-search" method="POST" action="<?php echo current_url();?>">
                                <h3 class="col-xs-12">Search Appointments</h3>
                                <?php if($func->router->fetch_method() == 'tooday'){?>
                                <div class="form-group col-sm-5">
                                    <label>Date appointment:</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                     
                                      <input type="text" class="form-control pull-right" name="date_appointments" id="reservation" <?php echo (isset($date_appointments)) ? "value='".explode('-',$date_appointments)[0]."-".explode('-',$date_appointments)[1]."'" : "";?>>
                                    </div>
                                    <!-- /.input group -->
                                </div>

                                <div class="clearfix"></div>
                                <?php }?>
 
                                <div class="form-group col-sm-5">
                                    <?php echo lang('doctor', 'doctor_id'); ?>
                                    <select name="doctor_id" id="doctor_id" name="type" class="form-control">
                                        <option value="ALL">--- Select ---</option>
                                        <?php if($doctors): foreach($doctors as $doctor):?>
                                        <option value="<?php echo $doctor->id;?>" <?php echo (isset($doctor_id) && $doctor_id == $doctor->id) ? "selected='selected'" : "";?>><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name." ".$doctor->last_name; ?></option>
                                        <?php endforeach;endif; ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <hr />
                        <table id="appointments-list" class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                            <thead>

                                <tr>
                                    <?php if(isset($date_appointments) || $func->router->fetch_method() == 'pending'){?>
                                       <th class="sorting"  aria-sort="descending" aria-controls="list-data"><?php echo lang('date');?></th> 
                                    <?}?>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('time');?></th>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('doctor');?></th>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('patient');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($appointments as $appointment):?>
                                <tr>
                                    <?php if(isset($date_appointments) || $func->router->fetch_method() == 'pending'){?>
                                    <td><strong><?php echo date('d-m-Y',strtotime($appointment->start_date));?></strong></td>
                                    <td>
                                        <strong><?php echo date('H:i',strtotime($appointment->start_date));?></strong>
                                        
                                        <?php if($func->router->fetch_method() == 'pending'){?>
                                        <div class="red">
                                            
                                            <span style="color:#4A6C9B;"><?php echo lang('time_in');?>:</span>
                                            <?php echo date('H:i:s',strtotime($appointment->in_come_time));?>

                                            <span style="color:#4A6C9B;"><?php echo lang('time_out');?>:</span>
                                            <?php if($appointment->out_come_time){ 
                                                echo date('H:i:s',strtotime($appointment->out_come_time));
                                            }else{ echo "---";}?>
                                        </div>
                                        <?}?>

                                    </td>
                                    <?}else{?>
                                    <td>
                                        <strong class="red"><?php echo date('H:i',strtotime($appointment->start_date));?></strong>

                                        <?php if($func->router->fetch_method() == 'pending'){?>
                                        <div class="red">
                                            <span style="color:#4A6C9B;"><?php echo lang('time_in');?>:</span>
                                            <?php echo date('H:i:s',strtotime($appointment->in_come_time));?>
                                            <span style="color:#4A6C9B;"><?php echo lang('time_out');?>:</span>
                                            <?php echo date('H:i:s',strtotime($appointment->out_come_time));?>
                                        </div>
                                        <?}?>

                                    </td>
                                    <?}?>
                                    <td><?php echo $appointment->doctor_name." ".$appointment->doctor_lname; ?><br /><?php echo $appointment->room_name;?></td>
                                    <td><a href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>"><?php echo htmlspecialchars($appointment->patient_name, ENT_QUOTES, 'UTF-8'); ?> [<?php echo $appointment->patient_id;?>]</a>
                                        <div class="red">
                                           <span style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_name;?></span>  
                                           <?php echo $func->calculate_age($appointment->patient_dob);?>
                                        </div>
                                    </td>
                                    <td class="action-links">

                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>">
                                                <img src="/assets/images/edit.png" />
                                        </a>


                                        <?php if($func->patientsModel->get_patient_drug_allergy($appointment->patient_id,30)){?>
                                                <a target="_blank" class="red_flag" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('d-m-Y',strtotime($appointment->start_date));?>#prescription-block">
                                                    <img src="/assets/images/red_flag.png" />
                                                </a>
                                        <?}?>

                                        <?php if($func->patientsModel->get_immunoteraphy_plan($appointment->patient_id)){?>
                                                <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('d-m-Y',strtotime($appointment->start_date));?>#immunoteraphy-block">
                                                    <img src="/assets/images/immuno.png" />
                                                </a>
                                        <?}?>

                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#history-block">
                                                <img  src="/assets/images/history-icon.gif" />
                                        </a>

                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                <img  src="/assets/images/prescription_drugs.png" />
                                        </a>

                                        <?php if($func->PatientsModel->get_patient_invoice_by_date($appointment->patient_id, date('Y-m-d',strtotime($appointment->start_date)))){ ?>
                                            <?php /*<a target="_blank" title="View invoice" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id.'/'.date('d-m-Y',strtotime($appointment->start_date));?>/#invoice-block">*/?>
                                             <a target="_blank" title="View invoice" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#invoice-block">        
                                                    <img src="/assets/images/doctor-icon.png" />
                                            </a>
                                        <?}else{?>
                                            <?php /*<a target="_blank" class="skip_nurse_rec" title="Add invoice" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id.'/'.strtotime($appointment->start_date);?>/#invoice-block">*/?>
                                            <a target="_blank" class="skip_nurse_rec" title="Add invoice" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/#invoice-block">    
                                                <img src="/assets/images/doctor-icon.png" />
                                            </a>
                                        <?php }?>                                        
                                        <?php if($appointment->in_come_flag == 1){?>
                                            <a title="View nurse record" style="margin-left:5px;" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#nurse-block">
                                                <img src="/assets/images/nurse.png" />
                                            </a>
                                        <?}else{?>
                                            <a class="skip_nurse_rec" style="margin-left:5px;" title="Skip nurse record" href="/admin/patients/nurse_record_skip/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>?tooday=1">
                                                <img src="/assets/images/nurse.png" />
                                            </a>
                                        <?}?> 

                                        <?php if($func->appointmentsModel->get_items("nurse",
                                                array(
                                                "patient_id"=>$appointment->patient_id,
                                                "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                )
                                            ) && trim($appointment->out_come_time)==''){?>

                                            <a href=" /admin/appointments/out/<?php echo $appointment->event_id?>">
                                                <img src="/assets/images/out.png" />
                                            </a>

                                        <?}?>
                                        
                                        <?php if($func->appointmentsModel->get_items("nurse",
                                            array(
                                            "patient_id"=>$appointment->patient_id,
                                            "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                            )
                                        ) && trim($appointment->out_come_time)<>''){?>

                                        <a href=" /admin/appointments/done/<?php echo $appointment->event_id?>">
                                            <img src="/assets/images/done.png" />
                                        </a>

                                        <?}?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $("document").ready(function(){
        $("body,html").on("click",".applyBtn",function(){
            $("#appointments-search").submit();
        });
        $("body,html").on("change","#doctor_id",function(){
            $("#appointments-search").submit();
        });

        $("#appointments-search .input-group-addon").click(function(){
            $("#reservation").trigger("click");
        });
    });

</script>