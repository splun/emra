<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php //var_dump($appointments);?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-body">
                        <div class="submit-response"><?php echo $message;?></div>
                        <div id="doctor_calendar">
                            <?php if(isset($_GET["doctor"])){?>
                                <a style="border-color:#333;" onmouseover="this.style.backgroundColor='#333';" onmouseout="this.style.backgroundColor='#fff';" href="/admin/appointments/"><?php echo lang("all_doctors");?></a> 
                            <?}else{?>
                                <a style="border-color:#333;background-color:#333;" class="active" href="/admin/appointments/"><?php echo lang("all_doctors");?></a>
                            <?}?>
                            <?php if($doctors) : foreach($doctors as $doctor):?>
                                <?php if(isset($_GET["doctor"]) && $doctor->id == $_GET["doctor"]){?>
                                    <a style="border-color:<?php echo $doctor->panel_color;?>; background-color:<?php echo $doctor->panel_color;?>" class="active" href="/admin/appointments/?doctor=<?php echo $doctor->id;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; } echo $doctor->first_name;?></a>
                                <?}else{?>
                                    <a style="border-color:<?php echo $doctor->panel_color;?>;" onmouseover="this.style.backgroundColor='<?php echo $doctor->panel_color;?>';" onmouseout="this.style.backgroundColor='#fff';" href="/admin/appointments/?doctor=<?php echo $doctor->id;?>"><?php if($doctor->position !== "Nurse") {echo "Dr. ";} echo $doctor->first_name;?></a>
                                <?}?>
                            <?php endforeach; endif;?>
                        </div>
                        <div id="loading-text" class="text-center"><?php echo lang("actions_loading");?></div>
                        <div id="calendar" class="fc fc-unthemed fc-ltr <?php if($is_reception){?>reception-calendar<?}?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="editEvent" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="app-add-title"><?php echo lang("pagetitle_add");?> <span class="app_edit_period"></span></h4>
        <h4 class="modal-title" style="display:none;" id="app-edit-title"><?php echo lang("pagetitle_edit");?> <span class="app_edit_period"></span></h4>
      </div>
      <?php echo form_open(uri_string(), array('id' => 'form-edit')); ?>
      <div class="modal-body">
            <div class="form-group row">
                <?php echo lang('date_appointment', 'date_appointment', array('class' => 'col-xs-12 control-label')); ?>
                <div class="date col-sm-12">
                  <input type="date" id="date_appointment" class="form-control" name="date_appointment" value="<?php echo date('Y-m-d');?>"></input>
                </div>
            </div>
            <div class="form-group row">
                <?php echo lang('time_period', 'time_period', array('class' => 'col-xs-12 control-label')); ?>
                <div class="date col-sm-4">
                  <input type="time" class="form-control" name="start_time" id="start_time" value="<?php echo date('h:i:s');?>"></input>
                </div>
                <div style="float:left;display:inline-block">
                  -
                </div>
                <div class="date col-sm-4">
                  <input type="time" class="form-control" name="end_time" id="end_time" value="<?php echo date('h:i:s');?>"></input>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('doctor', 'doctor_id', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="doctor_id" id="doctor_id" class="form-control">
                        <?php if($doctors): foreach($doctors as $doctor):?>
                        <option <?php if(isset($_GET['doctor']) && $_GET['doctor'] == $doctor->id){?>selected="selected"<?php }?> value="<?php echo $doctor->id;?>" data-event-color="<?php echo $doctor->panel_color;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?><?php echo $doctor->first_name." ".$doctor->last_name; ?></option>
                        <?php endforeach;endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('appointment_type', 'appoitment_type', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="type" id="appoitment_type" name="type" class="form-control">   
                    <?php foreach($types as $type):?>
                        <option data-group="<?php echo $type->user_groups;?>" value="<?php echo $type->id;?>"><?php echo $type->name;?></option>
                    <?php endforeach;?> 
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('rooms', 'room_id', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="room_id" id="room_id" class="form-control">
                        <?php if($rooms): foreach($rooms as $room):?>
                        <option value="<?php echo $room->id;?>"><?php echo $room->name; ?></option>
                        <?php endforeach;endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('select_patient', 'event_name', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select class="form-control select2 select2-hidden-accessible" name="event_name" id="event_name" style="width: 100%;" tabindex="-1" aria-hidden="true">
                        <option value="0" selected="selected">--Select patient--</option>
                        <?php foreach($patients as $patient){?>
                            <option value="<?php echo $patient->id;?>"><?php echo $patient->patient_name;?> [<?php echo $patient->id;?>] (tel:<?php echo $patient->telephone;?>; <?php echo $patient->mobile;?>)</option>
                        <?}?>
                    </select>
                </div>
                <div class="col-xs-12" id="or-add-new-patient">
                    <a data-toggle="collapse" href="#collapseAddPatient" class="or-separate"><i class="fa fa-user-plus"></i><?php echo lang("or_add_patient");?></a>
                    <div class="form-group row collapse" id="collapseAddPatient">
                        <div class="col-md-6">
                            <?php echo lang('patient_name', 'new-patient-name', array('class' => 'control-label')); ?>
                            <input type="text" id="new-patient-name" name="new-patient-name" class="form-control" value="" />
                        </div>
                        <div class="col-md-6">
                            <?php echo lang('patient_telephone', 'new-patient-telephone', array('class' => 'control-label')); ?>
                            <input type="text" id="new-patient-telephone" name="new-patient-telephone" class="form-control" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="view-profile-link">
                    <a target="_blank" href=""><i class="fa fa-search"></i><?php echo lang('view_profile');?></a>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-12">
                    <?php echo lang('appointment_details', 'appointment_details', array('class' => 'control-label')); ?>
                    <input rows="4" type="text" id="appointment_details" name="appointment_details" class="form-control"></input>
                </div>
            </div>
            <input type="hidden" id="event_id" value="0" name="event_id" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang("actions_close");?></button>
        <button type="button" type="submit" id="update" class="btn btn-primary"><?php echo lang("actions_save");?></button>
        <button type="button" id="delete" class="btn btn-danger"><?php echo lang("actions_delete");?></button>
      </div>
      <?php echo form_close();?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>

  $("document").ready(function(){

    $("#doctor_id").change(function(){
        var doctor = $(this).val();
        if(parseInt(doctor) !== 8){
            $("#appoitment_type option[data-group=3]").attr('disabled', 'disabled');
            $("#appoitment_type option[data-group=2]").attr("disabled",false);
        }else{
            $("#appoitment_type option[data-group=2]").attr('disabled', 'disabled');
            $("#appoitment_type option[data-group=3]").attr("disabled",false);
        }
    });

    var date = new Date();
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear();

    $('#calendar').fullCalendar({
        header    : {
            left  : 'prev,next today',
            center: 'title',
            right : 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week : 'week',
            day  : 'day'
        },  
        slotDuration : '00:15:00',
        minTime: '09:00:00',
        allDaySlot: false,
        axisFormat: 'HH:mm',
        timeFormat: 'HH:mm',
        slotLabelFormat:"HH:mm",
        editable  : true,
        droppable : true,
        selectable: true,
        defaultTimedEventDuration: '00:15:00',
        forceEventDuration: true,
        eventAfterAllRender: function() {
            $('#loading-text').hide();
        },
        <?php if($appointments):?>
        events    : [
            <?php foreach($appointments as $appointment):?>
                {
                  event_id : <?php echo $appointment->event_id;?>,
                  patient_id : <?php echo $appointment->patient_id;?>,
                  <?php if($is_reception){?>
                  title : '<?php echo (!empty($appointment->patient_phone)) ? $appointment->patient_phone."; " : "";?>
                  <?php echo (!empty($appointment->patient_mobile)) ? $appointment->patient_mobile."; " : "";?>
                  <?php echo (!empty($appointment->patient_mobile2)) ? $appointment->patient_mobile2."; " : "";?>
                  <?php echo $appointment->patient_name;?> [<?php echo $appointment->patient_id;?>]; <?php echo htmlspecialchars($appointment->details, ENT_QUOTES);?>',
                  <?php }else{?>
                  title : '<?php echo $appointment->patient_name;?> [<?php echo $appointment->patient_id;?>]',
                  <?}?>
                  start          : '<?php echo $appointment->start_date;?>',
                  end            : '<?php echo $appointment->end_date;?>',
                  appointment_type : '<?php echo $appointment->type;?>',
                  appointment_details : '<?php echo trim(stripslashes(preg_replace("/\/\/.*?\n/", "\n", $appointment->details)));?>',
                  doctor_id : '<?php echo $appointment->doctor_id;?>',
                  room_id : '<?php echo $appointment->room;?>',
                  allDay         : false,
                  <?php
                  if($appointment->ucolor !== 0){?>
                    backgroundColor: '<?php echo $appointment->ucolor;?>',
                    borderColor    : '<?php echo $appointment->ucolor;?>',
                  <?}else{?>
                    backgroundColor: '#00c0ef',
                    borderColor    : '#00c0ef',
                  <?}?>
                  url            : ''
                },
            <?php endforeach;?>
        ],
        <?php endif;?>
        dayClick: function(date) {
            $('#calendar').fullCalendar('gotoDate', date);
            $(".fc-agendaDay-button").click();
            return;
        },
        select: function(start,end, jsEvent, view,) {
            newAppointment(start,end);
        },
        eventClick: function(calEvent, jsEvent, view) {
            editAppointment(calEvent);                
        },
    });
  });

    var newAppointment = function(app_start,app_end) {
        var start = moment(app_start);
        var end = moment(app_end);

        inpStart = "--:--";
        inpEnd = "--:--";

        if(start.format("HH") !== "00"){
            inpStart = start.format("HH:mm:ss");
            inpEnd = end.format("HH:mm:ss");
        }

        $("#delete,#view-profile-link,#app-edit-title").hide();
        $("#app-add-title,#or-add-new-patient").show();

        $('input#date_appointment').val(start.format("YYYY-MM-DD"));
        $('input#start_time').val(inpStart);
        $('input#end_time').val(inpEnd);

        $('input#event_id').val(0);
        $('select#room_id').val(1);
        $('#appoitment_details').val("");
        $('select#event_name').val(0);
        $('select#event_name').trigger("change");

        $('#editEvent').modal('show');
        $('#update').unbind();

        $('#update').on('click', function() {

            var required_date = $('input#date_appointment').val();
            var required_start_time = $('input#start_time').val();
            var patient_id = $('select#event_name').val();
            var new_patient_name = $('#new-patient-name').val();
            var new_patient_telephone = $('#new-patient-telephone').val();
            var event_name = $('select#event_name option:selected').text();
            var event_color = $('select#doctor_id option:selected').attr("data-event-color");

            var eventData;

            if (required_date && required_start_time && (parseInt(patient_id) !== 0 || (new_patient_name !== "" && new_patient_telephone !== ""))) {
              
                $.ajax({
                  url: '/admin/appointments/add_appointments',
                  type: "POST",
                  data: $("#form-edit").serialize(),
                  dataType: "JSON",
                  success: function(response){
                    $(".submit-response").html(response.msg);
                    if(parseInt(response.data) !== 0){
                        

                        if(new_patient_name !== "" && parseInt(response.new_patient_id) !== 0){
                            patient_id = response.new_patient_id;
                            var title = new_patient_name+"["+patient_id+"]";
                        }else{
                            patient_id = $('select#event_name').val();
                            var title = event_name.split("(")[0];
                        }


                        var eventData = {
                            title: title,
                            patient_id: patient_id,
                            start : $('#date_appointment').val()+" "+$('#start_time').val(),
                            end: $('#date_appointment').val()+" "+$('#end_time').val(),
                            appointment_type: $('select#appoitment_type').val(),
                            appointment_details : $('#appointment_details').val(),
                            room_id:$('select#room_id').val(),
                            doctor_id:$('select#doctor_id').val(),
                            backgroundColor: event_color,
                            borderColor: event_color,
                            event_id: response.data
                        };

                    }

                    $('#calendar').fullCalendar('renderEvent', eventData, true);
                    $('#newEvent').modal('hide');
                  }
                });

                $('#editEvent').modal('hide');
            }else{
                alert("Please fill in all fields!")
            }
        });
    };

    var editAppointment = function(calEvent) {
        var start = moment(calEvent.start._i);
        var end = moment(calEvent.end._i);

        $("#app-add-title,#or-add-new-patient").hide();
        $("#delete,#app-edit-title,#view-profile-link").show();
        $("#view-profile-link a").attr("href","/admin/patients/card/"+calEvent.patient_id+"");

        $('input#date_appointment').val(start.format("YYYY-MM-DD"));
        $('input#start_time').val(start.format("H:mm:ss"));
        $('input#end_time').val(end.format("H:mm:ss"));
        $('input#event_id').val(calEvent.event_id);
        $('select#appoitment_type').val(calEvent.appointment_type);
        $('select#room_id').val(calEvent.room_id);
        $('#appointment_details').val(calEvent.appointment_details);
        $('select#doctor_id').val(calEvent.doctor_id);
        $('select#event_name').val(calEvent.patient_id);
       
        $('select#event_name').trigger("change");

        $('#editEvent').modal('show');
        $('#update').unbind();
        $('#update').on('click', function() {

            var required_date = $('input#date_appointment').val();
            var required_start_time = $('input#start_time').val();
            var required_event_name = $('select#event_name option:selected').text();
            $('#editEvent').modal('hide');
            var eventData;

            if (required_date && required_start_time && required_event_name !== "--Select patient--") {
              
                $.ajax({
                  url: '/admin/appointments/add_appointments',
                  type: "POST",
                  data: $("#form-edit").serialize(),
                  dataType: "JSON",
                  success: function(response){
                    $(".submit-response").html(response.msg);
                    if(parseInt(response.data) !== 0){
                        calEvent.event_id = response.data;
                    }
                    calEvent.title = required_event_name.split("(")[0];
                    calEvent.patient_id = $('select#event_name').val();
                    calEvent.start = $('#date_appointment').val()+" "+$('#start_time').val();
                    calEvent.end = $('#date_appointment').val()+" "+$('#end_time').val();
                    calEvent.appointment_type = $('select#appoitment_type').val();
                    calEvent.room_id = $('select#room_id').val();
                    calEvent.appointment_details = $('#appointment_details').val();
                    calEvent.doctor_id = $('select#doctor_id').val();

                    $('#calendar').fullCalendar('updateEvent', calEvent);
                  }
                });

            }else{
                alert("Please fill in all fields!")
            }
        });

        $('#delete').on('click', function() {
            $('#delete').unbind();

            $.ajax({
                  url: '/admin/appointments/delete',
                  type: "POST",
                  data: "id="+calEvent.event_id+"",
                  success: function(response){
                    if(parseInt(response) == 1){
                        if (calEvent._id.includes("_fc")){
                          $('#calendar').fullCalendar('removeEvents', [calEvent._id]);
                        } else {
                          $('#calendar').fullCalendar('removeEvents', [calEvent._id]);
                        }
                        $('#editEvent').modal('hide');
                    }
                  }
            });

        });
    };

    var getCal1Id = function(cal2Id) {
      var num = cal2Id.replace('_fc', '') - 1;
      var id = "_fc" + num;
      return id;
    };

</script>
