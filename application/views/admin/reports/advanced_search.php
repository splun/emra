<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php echo $message;?>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('adv_search');?></h3>
                        <a href="" style="display:none;" id="back-to-search" class="btn btn-default btn-flat"><?php echo lang('back_adv_search');?></a>
                    </div>
                    <form id="advanced-filtered-patients" class="box-header with-border" method="POST" action="/admin/reports/filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_name_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($patient_name);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_parents_name');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($parents_name);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_civil_id_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($civil_id);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_file_no_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($file_no);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_phone_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($telephone);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2" style="padding-right:0px"><?php echo lang('patient_insurance_company_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="insurance" data-index="3"  class="form-control">
                                        <option value="0">--- Select ---</option>
                                        <?php foreach($insurances as $insurance){?>
                                            <option value="<?php echo $insurance->id; ?>" <?php echo (isset($filter['medication_id']) && $insurance->id == $filter['insurance']) ? 'selected="selected"' : "" ?>>
                                            <?php echo $insurance->name; ?>
                                            </option>
                                        <?}?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('dov');?>:</div>
                                    <div class="input-group col-md-10">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" name="date_appointments" id="adv-reservation" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_doctor_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="doctor" data-index="3"  class="form-control">
                                            <option value="0">--- Select ---</option>
                                            <?php  foreach($doctors as $doctor){?>
                                                <option value="<?php echo $doctor->id; ?>" <?php echo (isset($filter['doctor']) && $doctor->id == $filter['doctor']) ? 'selected="selected"' : "" ?>>
                                                    Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?>
                                                </option>
                                            <?}?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_age');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?></label>
                                        <?php echo form_input($age_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?></label>
                                        <?php echo form_input($age_to);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_month');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?></label>
                                        <?php echo form_input($month_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?></label>
                                        <?php echo form_input($month_to);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_skin_testing');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?></label>
                                        <?php echo form_input($skin_testing_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?></label>
                                        <?php echo form_input($skin_testing_to);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_spirometr');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?></label>
                                        <?php echo form_input($spirometr_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?></label>
                                        <?php echo form_input($spirometr_to);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_weight');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?>(kg)</label>
                                        <?php echo form_input($weight_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?>(kg)</label>
                                        <?php echo form_input($weight_to);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row from-to-block">
                                    <div class="col-md-2"><?php echo lang('adv_height');?>:</div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_from');?>(cm)</label>
                                        <?php echo form_input($height_from);?>
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label><?php echo lang('adv_to');?>(cm)</label>
                                        <?php echo form_input($height_to);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_complaints');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="complaints" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($complaints): foreach($complaints as $complaint):?>
                                                <option value="<?php echo $complaint->id;?>"><?php echo $complaint->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_others');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="complaints_other" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($complaints_other): foreach($complaints_other as $complaint_other):?>
                                                <option value="<?php echo $complaint_other->id;?>"><?php echo $complaint_other->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_medications');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="medications" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($medications): foreach($medications as $medication):?>
                                                <option value="<?php echo $medication->id;?>"><?php echo $medication->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_asthma');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="asthma" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($asthma): foreach($asthma as $ast):?>
                                                <option value="<?php echo $ast->id;?>"><?php echo $ast->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_family_history');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="family_history" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($family_history): foreach($family_history as $family):?>
                                                <option value="<?php echo $family->id;?>"><?php echo $family->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_environment');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="environment" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($environments): foreach($environments as $environment):?>
                                                <option value="<?php echo $environment->id;?>"><?php echo $environment->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_bav');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="birth_vaccination" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($birth_vaccination): foreach($birth_vaccination as $birth_vaccination):?>
                                                <option value="<?php echo $birth_vaccination->id;?>"><?php echo $birth_vaccination->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_diet');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="diet" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($diets): foreach($diets as $diet):?>
                                                <option value="<?php echo $diet->id;?>"><?php echo $diet->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_development');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="development" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($developments): foreach($developments as $development):?>
                                                <option value="<?php echo $development->id;?>"><?php echo $development->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_immunity');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="immunity" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($immunity): foreach($immunity as $immun):?>
                                                <option value="<?php echo $immun->id;?>"><?php echo $immun->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_diagnosis');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="diagnosis" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($diagnosis): foreach($diagnosis as $diagnos):?>
                                                <option value="<?php echo $diagnos->id;?>"><?php echo $diagnos->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_others');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="diagnosis_other" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($diagnosis_other): foreach($diagnosis_other as $dother):?>
                                                <option value="<?php echo $dother->id;?>"><?php echo $dother->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_eczema');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="eczema" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($eczema): foreach($eczema as $ecz):?>
                                                <option value="<?php echo $ecz->id;?>"><?php echo $ecz->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_rhino_conjunctivitis');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="rhino_conjunctivitis" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($rhino_conjunctivitis): foreach($rhino_conjunctivitis as $conjunctivitis):?>
                                                <option value="<?php echo $conjunctivitis->id;?>"><?php echo $conjunctivitis->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_food');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="food_allergy" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($food_allergy): foreach($food_allergy as $fallergy):?>
                                                <option value="<?php echo $fallergy->id;?>"><?php echo $fallergy->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_questions');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="food_questions" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($food_questions): foreach($food_questions as $fquestion):?>
                                                <option value="<?php echo $fquestion->id;?>"><?php echo $fquestion->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_immunoteraphy');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="immunoteraphy" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($immunoteraphy_items): foreach($immunoteraphy_items as $immuno_item):?>
                                                <option value="<?php echo $immuno_item->id;?>"><?php echo $immuno_item->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_skin_testing');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="skin_testing" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($skin_testing): foreach($skin_testing as $testing):?>
                                                <option value="<?php echo $testing->id;?>"><?php echo $testing->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_spirometer');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="spirometers" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($spirometers): foreach($spirometers as $spirometer):?>
                                                <option value="<?php echo $spirometer->id;?>"><?php echo $spirometer->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_rhinoscopy');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="rhinoscopy" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($rhinoscopy): foreach($rhinoscopy as $rhino):?>
                                                <option value="<?php echo $rhino->id;?>"><?php echo $rhino->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_immunoCAP');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="immunoCAP" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($immunoCAP): foreach($immunoCAP as $iCAP):?>
                                                <option value="<?php echo $iCAP->id;?>"><?php echo $iCAP->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_physical_exam');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="physical_exam" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($physicals): foreach($physicals as $physical):?>
                                                <option value="<?php echo $physical->id;?>"><?php echo $physical->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_investigations');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="investigations" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($investigations): foreach($investigations as $investigation):?>
                                                <option value="<?php echo $investigation->id;?>"><?php echo $investigation->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_prescription');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="prescriptions" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($prescriptions): foreach($prescriptions as $prescription):?>
                                                <option value="<?php echo $prescription->id;?>"><?php echo $prescription->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_drug_allergy');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="drug_allergy" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($drug_allergy): foreach($drug_allergy as $drug):?>
                                                <option value="<?php echo $drug->id;?>"><?php echo $drug->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('adv_questions');?>:</div>
                                        <div class="col-md-10 form-group">
                                        <select name="drug_questions" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($drug_questions): foreach($drug_questions as $dquestion):?>
                                                <option value="<?php echo $dquestion->id;?>"><?php echo $dquestion->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="filter" value="1">
                        <hr />
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary"><?php echo lang("actions_search");?></button>
                        </div>
                    </form>
                    <div class="box-body" style="display:none;" id="patients-table-wrap">
                        <table id="patients-list" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('patient_file_no_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('patient_name_label');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
         
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
$("body,html").on("click","#advanced-filtered-patients button[type=submit]", function(){

    $.ajax({
      type: "POST",
      url: '/admin/reports/filter',
      data: $("#advanced-filtered-patients").serialize(),
      beforeSend: function() {
        $("#advanced-filtered-patients").fadeOut();
        $('.box-header h3').text("loading...");
      },
      success: function(result){
        var html = '<table class="table table-bordered table-hover" id="patients-list" role="grid" >';
        html += '<thead>'+$("#patients-list thead").html()+'</thead>';
        html += '<tbody>'+result+'</tbody>';
        html += '</table>';

        $("#patients-table-wrap").html(html);

        var table = $('#patients-list').DataTable({
            "pageLength": 25
        });

        $('.box-header h3').text("<?php echo lang('search_result');?>").addClass('results-title');
        $("#back-to-search").fadeIn();
        $("#patients-table-wrap").fadeIn();
      }

    });

    return false;
});

$("body,html").on("click","#back-to-search", function(){
    $(this).hide();
    $("#patients-table-wrap").fadeOut();
    $("#advanced-filtered-patients").fadeIn();
    $('.box-header h3').text("<?php echo lang('adv_search');?>").removeClass('results-title');;
    return false;
});

$("document").ready(function(){
    $('#adv-reservation').daterangepicker({
        autoUpdateInput: false,
        locale: {
          cancelLabel: 'Clear'
        }
    });

      $('#adv-reservation').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      });

      $('#adv-reservation').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });

    if(window.location.href.indexOf("?") > -1) {
        window.history.pushState('page2', 'Title',location.origin + location.pathname);
    }

    $("#advanced-filtered-patients .input-group-addon").click(function(){
        $("#adv-reservation").trigger("click");
    });
});
</script>
