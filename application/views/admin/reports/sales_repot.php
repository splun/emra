<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <?php echo $message;?>
                 <div class="box">
                    <?php if(!$is_nurse && !$is_reception){?>
                    <form id="invoice-reports" class="box-header with-border" method="POST" action="/admin/patients/filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_doctor_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="doctor_id" id="doctor_id" name="type" class="form-control">

                                            <?php if(empty($current_doctor)){?>
                                            <option value="">--- Select ---</option>
                                            <?}?>

                                            <?php if($doctors): foreach($doctors as $doctor): 
                                                if(!empty($current_doctor) && $current_doctor != $doctor->id) continue;?>
                                                <option value="<?php echo $doctor->id;?>">Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?></option>
                                            <?php endforeach;endif; ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('invoice_item');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select multiple name="invoice_items[]" class="form-control">
                                            <option value="0">---Select---</option>
                                            <?php if($invoice_items): foreach($invoice_items as $item):?>
                                                <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                            <?php endforeach; endif;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('dov');?>:</div>
                                    <div class="input-group col-md-10">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" name="date_appointments" id="reservation" value="<?php date('Y-m-d');?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2 paddingr-5"><?php echo lang('invoice_pending');?>:</div>
                                    <div class="col-md-7 form-group checkbox">
                                        <label class="switch">
                                            <input type="checkbox" <?php if(isset($_GET['debt']) || isset($_POST['debt'])){echo "checked=checked";} ?> name="debt" value="1" data-index="4" />
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary"><?php echo lang("actions_search");?></button>
                        </div>
                    </form>
                    <?}?>
                    <div class="box-body" id="invoice-table-wrap">
                        <table id="invoice-list" class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info" cellpadding="5" cellspacing="0">
                            <thead>
                                <tr>
                                    <th><?php echo lang('inv_no');?></th>
                                    <th><?php echo lang('inv_date');?></th>
                                    <th><?php echo lang('patient_name_label');?></th>
                                    <th><?php echo lang('doctor_name_label');?></th>
                                    <th>Item</th>
                                    <th>Item price</th>
                                    <th>Qty</th>
                                    <th>Discount</th>
                                    <th><?php echo lang('inv_insurance_pay');?></th>
                                    <th><?php echo lang('inv_total');?></th>
                                    <th><?php echo lang('inv_total_deductable');?></th>
                                    <th><?php echo lang('inv_net_amount');?></th>
                                    <th><?php echo lang('inv_invoice');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($list !==""){ 
                                    echo $list; 
                                }?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
$("body,html").on("click","#invoice-reports button[type=submit]", function(){

    $.ajax({
      type: "POST",
      url: '/admin/reports/sales_report',
      data: $("#invoice-reports").serialize(),
      beforeSend: function() {
        $("#invoice-list tbody").html('<tr class="od"><td valign="top" colspan="11" class="dataTables_empty">Loading...</td></tr>');
      },
      success: function(result){

        var html = '<table class="table table-bordered table-hover" id="invoice-list" role="grid" >';
    
        html += '<thead>'+$("#invoice-list thead").html()+'</thead>';
        html += '<tbody>';
        html += result;
        html += '</tbody>';
        html += '</table>';

        $("#invoice-table-wrap").html(html);
        var table = $('#invoice-list').DataTable({
            "pageLength": 25
        });
        
      }

    });

    return false;
});


$("document").ready(function(){
    <?php if(!$is_nurse && !$is_reception){?>
    if(window.location.href.indexOf("?") > -1) {
        window.history.pushState('page2', 'Title',location.origin + location.pathname);
    }
    <?}?>

    $("#invoice-reports .input-group-addon").click(function(){
        $("#reservation").trigger("click");
    });

});
</script>