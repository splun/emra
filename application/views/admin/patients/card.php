<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <h1><?php echo $patient->patient_name;?> [<?php echo $patient->id;?>]</h1>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <?php /* <h3 class="box-title"><?php echo anchor('admin/patients/add/', '<i class="fa fa-plus"></i> '. lang('pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3> */?>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";}?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->mobile2){ echo lang('patient_mobile2_label').": ".$patient->mobile2.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>

                        <h3><?php echo anchor('admin/patients', lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li id="main-block" class="active"><a href="#main" data-toggle="tab" aria-expanded="true"><?php echo lang('patient_main_tab');?></a></li>
                              <li id="nurse-block"><a href="#nurse" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_nurse_tab');?></a></li>
                              <li id="prescription-block"><a href="#prescription" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_prescription_tab');?></a></li>
                              <li id="diagnosis-block"><a href="#diagnosis" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_diagnosis_tab');?></a></li>
                              <li id="immunoteraphy-block"><a href="#immunoteraphy" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_immunoteraphy_tab');?></a></li>
                              <li id="results-block"><a href="#results" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_results_tab');?></a></li>
                              <li id="allergy-block"><a href="#allergy" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_allergy_tab');?></a></li>

                              <li id="exam-photo-block"><a href="#exam-photo" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_examphoto_tab');?></a></li>
                              <li id="spirometry-block"><a href="#spirometry" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_spirometry_tab');?></a></li>
                              <li id="sickleaves-block"><a href="#sickleaves" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_sickleaves_tab');?></a></li>

                              <li id="history-block"><a href="#history" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_visit_history_tab');?></a></li>
                              <li id="invoice-block"><a href="#invoice" data-toggle="tab" aria-expanded="false"><?php echo lang('patient_invoice_tab');?></a></li>
                              <li id="patientsections-block" class="tab-purple"><a href="#patientsections" data-toggle="tab" aria-expanded="false"><i class="fa fa-plus"></i></a></li>
                            
                            </ul>
                            <div class="message-box" style="margin-top:10px;margin-bottom:0px;"><?php echo $message;?></div>
                            <div class="tab-content">
                              <div class="tab-pane active" id="main">
                                <h2><?php echo lang('patient_main_tab');?></h2>
                                <p><br /></p>
                                <div class="information-form">
                                    <?php echo form_open_multipart('/admin/patients/main_edit/'.$patient->id, array('class' => 'form-horizontal', 'id' => 'form-patient-edit')); ?>
                                        <div class="form-group">
                                            <?php echo lang('patient_civil_id_label', 'civil_id', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($civil_id);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_name_label', 'patient_name', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($patient_name);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_name_ar_label', 'patient_name_ar', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($patient_name_ar);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_dob_label', 'dob', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($dob);?>
                                                
                                                <?php if($patient_year){ echo $patient_year.";";}?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_b_weight_label', 'b_weight', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($b_weight);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_gender_label', 'gender', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                               <select name="gender" class="form-control" onchange="itemBycategories(this)">
                                                <?php foreach($gender as $key=>$val){?>
                                                    <option value="<?php echo $key; ?>" <?php echo($key == $patient->gender) ? "selected=selected" : "";?>>
                                                        <?php echo $val; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_doctor_label', 'doctor', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <select name="doctor" data-index="3"  class="form-control">
                                                    <option value="0">--- Select ---</option>
                                                    <?php  foreach($doctors as $doctor){?>
                                                        <option value="<?php echo $doctor->id; ?>" <?php echo ($doctor->id == $patient->doctor) ? 'selected="selected"' : "" ?>>
                                                            Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?>
                                                        </option>
                                                    <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_governance_label', 'governance', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <select name="governance" id="governance" class="form-control">
                                                <option value="">--- Select ---</option>
                                                <?php for($i=0; $i<count(lang('patient_governance')); $i++){?>
                                                    <option value="<?php echo lang('patient_governance')[$i]; ?>" <?php echo (lang('patient_governance')[$i] == $patient->governance) ? 'selected="selected"' : "" ?>>
                                                    <?php echo lang('patient_governance')[$i]; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_address_label', 'address', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_textarea($address);?>
                                            </div>
                                        </div>
                                        <?php /*
                                        <div class="form-group">
                                            <?php echo lang('patient_parents_name_label', 'parents_name', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($parents_name);?>
                                            </div>
                                        </div>
                                        */?>
                                        <!--<div class="form-group">
                                            <?php echo lang('patient_job_label', 'job', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-sm-11">
                                                <?php echo form_input($job);?>
                                            </div>
                                        </div>-->
                                        
                                        <div class="form-group">
                                            <?php echo lang('patient_phone_label', 'job', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($telephone);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_mobile_label', 'mobile', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($mobile);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_mobile2_label', 'mobile2', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($mobile2);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_email_label', 'job', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($email);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_insurance_company_label', 'insurance_company', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11 ">
                                                <select name="insurance_company" class="form-control">
                                                <option value="0">--- Select ---</option>
                                                <?php foreach($insurances as $insurance){?>
                                                    <option value="<?php echo $insurance->id; ?>" <?php echo ($insurance->id == $patient->ddl_insurance) ? 'selected="selected"' : "" ?>>
                                                    <?php echo $insurance->name; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_insurance_number_label', 'insurance_number', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_input($insurance_number);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <?php echo lang('patient_file01_label', 'file01', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-2">
                                                <?php echo form_input($file01);?>
                                            </div>
                                            <?php if($patient->file01) {?>
                                                <div class="col-md-4 file-view">
                                                    <a href="/upload/patients/<?php echo $patient->file01;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a> 
                                                    <div class="hidden-result-image">
                                                        <img src="/upload/patients/<?php echo $patient->file01;?>" width="300px">
                                                    </div>                 
                                                    - <input type="checkbox" class="patient-file-delete" id="dfile01" style="vertical-align:middle;" name="delete_file_1" value="<?php echo $patient->file01;?>" /><label for="dfile01" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                                </div>
                                            <?}?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_file02_label', 'file02', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-2">
                                                <?php echo form_input($file02);?>
                                            </div>
                                            <?php if($patient->file02) {?>
                                                <div class="col-md-4 file-view">
                                                    <a href="/upload/patients/<?php echo $patient->file02;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                    <div class="hidden-result-image">
                                                        <img src="/upload/patients/<?php echo $patient->file02;?>" width="300px">
                                                    </div>
                                                     - <input type="checkbox" class="patient-file-delete" id="dfile02" style="vertical-align:middle;" name="delete_file_2" value="<?php echo $patient->file02;?>" /><label for="dfile02" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                                </div>
                                            <?}?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_file03_label', 'file03', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-2">
                                                <?php echo form_input($file03);?>
                                            </div>
                                            <?php if($patient->file03) {?>
                                                <div class="col-md-4 file-view">
                                                    <a href="/upload/patients/<?php echo $patient->file03;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                    <div class="hidden-result-image">
                                                        <img src="/upload/patients/<?php echo $patient->file03;?>" width="300px">
                                                    </div>
                                                     - <input type="checkbox" class="patient-file-delete" id="dfile03" style="vertical-align:middle;" name="delete_file_3" value="<?php echo $patient->file03;?>" /><label for="dfile03" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>

                                                </div>
                                            <?}?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_othe_report_label', 'multiple_files', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-2">
                                                <?php echo form_input($multiple_files);?>
                                            </div>
                                            <?php if($multiple_report_files) {?>
                                                <div class="col-md-4 file-view">
                                                    <?php foreach($multiple_report_files as $m_file) : ?>
                                                    <a href="/upload/patients/<?php echo $m_file->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                    <div class="hidden-result-image">
                                                        <img src="/upload/patients/<?php echo $m_file->fname;?>" width="300px">
                                                    </div>
                                                     - <input type="checkbox" class="patient-file-delete" id="dmultiplefile" style="vertical-align:middle;" name="delete_multiple_files" value="<?php echo $m_file->id;?>" /><label for="dmultiplefile" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                                    <br />
                                                    <?php endforeach; ?>
                                                </div>
                                            <?}?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_referral_label', 'referral', array('class' => 'col-md-1 control-label')); ?>
                                            
                                            <div class="col-md-11">
                                                <?php //echo form_textarea($referral);?>
                                                <select name="referral" id="referral" class="form-control">
                                                <option value="">--- Select ---</option>
                                                <?php for($i=0; $i<count(lang('patient_referral')); $i++){?>
                                                    <option value="<?php echo lang('patient_referral')[$i]; ?>" <?php echo (lang('patient_referral')[$i] == $patient->hear) ? 'selected="selected"' : "" ?>>
                                                    <?php echo lang('patient_referral')[$i]; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_notes_label', 'info', array('class' => 'col-md-1 control-label')); ?>
                                            <div class="col-md-11">
                                                <?php echo form_textarea($notes);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-1 col-md-11">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="nurse">
                                <h2><?php echo lang('patient_nurse_tab');?></h2>
                                <div class="box-header">
                                <?php if($date_appointment !== 0){?>
                                    <h3 class="box-title"><?php echo anchor('admin/patients/nurse_record_add/'.$patient->id.'/'.$date_appointment, '<i class="fa fa-plus"></i> '. lang('nurse_record_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?>
                                    </h3>
                                <?}else{?>
                                    <h3 class="box-title"><?php echo anchor('admin/patients/nurse_record_add/'.$patient->id, '<i class="fa fa-plus"></i> '. lang('nurse_record_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?>
                                    </h3>
                                <?}?>
                                </div>
                                <table id="nurse-records" class="table table-bordered table-hover nurseTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_datev');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_weight');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_height');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_heart');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_blood');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_respiratory');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_temperature');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_circumference');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_bmi');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_sao');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('nurse_records_comments');?></th>
                                            <th><?php echo lang('actions_title');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($nurse_records) : foreach($nurse_records as $nurse_record):?>
                                    <tr>
                                        <td><?php echo $nurse_record->date_visit;?></td>
                                        <td><?php echo $nurse_record->weight;?></td>
                                        <td><?php echo $nurse_record->height;?></td>
                                        <td><?php echo $nurse_record->heart;?></td>
                                        <td><?php echo $nurse_record->blood;?></td>
                                        <td><?php echo $nurse_record->respiratory;?></td>
                                        <td><?php echo $nurse_record->temp;?></td>
                                        <td><?php echo $nurse_record->Circumference;?></td>
                                        <td><?php echo $nurse_record->bmi;?></td>
                                        <td><?php echo $nurse_record->sao;?></td>
                                        <td><?php echo $nurse_record->desc;?></td>
                                        <td class="action-links">
                                            <?php echo anchor('/admin/patients/nurse_record_edit/'.$nurse_record->id, lang('actions_edit')); ?>
                                            <?php echo anchor('/admin/patients/nurse_record_delete/'.$patient->id.'/'.$nurse_record->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; endif;?>
                                    </tbody>
                                </table>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="prescription">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2><?php echo lang('patient_prescription');?></h2>
                                
                                        <table id="prescription-records" class="table table-bordered" role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    <th class="sorting" aria-controls="list-data">#</th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_name');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_cat_name');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_qty');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_date');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1;  foreach($prescriptions as $prescription){?>
                                                <tr>
                                                    <td><?php echo $i;?></td>
                                                    <td><?php echo $prescription->med_name;?></td>
                                                    <td><?php echo $prescription->med_cat_name;?></td>
                                                    <td><?php echo $prescription->dose;?></td>
                                                    <td><a target="_blank" href="/admin/patientsections/main/prescription/<?php echo $patient->id;?>/<?php echo $prescription->date_add;?>"><?php echo $prescription->date_add;?></a></td>
                                                </tr>
                                                <?php $i++; }?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-5 col-md-offset-1">
                                        <h2><?php echo lang('patient_drugallrgy');?></h2>
                                        <table id="drugallergy-records" class="table table-bordered" role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    <th class="sorting" aria-controls="list-data">#</th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_name');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_date');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1;  foreach($drug_allergy as $allergy_record){?>
                                                <tr>
                                                    <td><?php echo $i;?></td>
                                                    <td><?php echo $allergy_record->allergy_name;?></td>
                                                    <td><a target="_blank" href="/admin/patientsections/main/drug_allergy/<?php echo $patient->id;?>/<?php echo $allergy_record->date_add;?>"><?php echo $allergy_record->date_add;?></a></td>
                                                </tr>
                                                <?php $i++; }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="diagnosis">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2><?php echo lang('patient_diagnosis_tab');?></h2>
                                        <table id="diagnosis-records" class="table table-bordered" role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_date');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_name');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($diagnosis): foreach($diagnosis as $diagnos):?>
                                                <tr>
                                                    <td><a target="_blank" href="/admin/patientsections/main/diagnosis/<?php echo $patient->id;?>/<?php echo $diagnos->date_add;?>"><?php echo $diagnos->date_add;?></a></td>
                                                    <td><?php echo $diagnos->name_diagnos;?></td>
                                                </tr>
                                                <?php  endforeach; endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h2><?php echo lang('diagnosis_description');?></h2>
                                        <table id="descriptions-records" class="table table-bordered" role="grid" aria-describedby="example2_info">
                                            <thead>
                                                <tr>
                                                    <th width="100px" class="sorting" aria-controls="list-data"><?php echo lang('prescription_records_date');?></th>
                                                    <th class="sorting" aria-controls="list-data"><?php echo lang('diagnosis_decription');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($descriptions): foreach($descriptions as $description):?>
                                                <tr>
                                                    <td><a target="_blank" href="/admin/patientsections/main/diagnosis/<?php echo $patient->id;?>/<?php echo $description->date_test;?>"><?php echo $description->date_test;?></a></td>
                                                    <td><?php echo $description->desc;?></td>
                                                </tr>
                                                <?php endforeach; endif;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                              </div>
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="immunoteraphy">
                                <h2><?php echo lang('immuno_shedule_title');?> <?php if($iplan) {?><a href="/admin/patients/immunoteraphy_print/<?php echo $patient->id;?>" class="btn btn-block btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a><?}?></h2> 
                                <?php echo form_open_multipart(($iplan) ? '/admin/patients/edit_immunoteraphy_plan/'.$iplan->id.'/'.$patient->id : '/admin/patients/add_immunoteraphy_plan/'.$patient->id, array('class' => 'form-horizontal', 'id' => 'shedule-form-create')); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('immuno_shedule_diagnos_label', 'diagnosis', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <select name="diagnosis" class="form-control">
                                                <option value="0">---Select Item---</option>
                                                <?php foreach($immuno_diagnosis as $diagnos) {?>
                                                    <option <?php echo ($iplan && $diagnos->id == $iplan->diagnosis) ? "selected=selected" : "";?> value="<?php echo $diagnos->id;?>"><?php echo $diagnos->name;?></option>
                                                <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('immuno_shedule_date_label', 'date_consented', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($date_consented);?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <a data-toggle="modal" class="modal-open" data-target="#scVacPopup">
                                                <?php echo lang('immuno_shedule_vacc_sc_label', 'vaccine_sc', array('class' => 'col-sm-2 control-label')); ?>
                                            </a>
                                            <div class="col-sm-10">
                                                <div class="modal modal-primary fade vaccine-popup" id="scVacPopup">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                            <select style="height:320px !important" multiple="multiple" name="vaccine_sc[]" class="form-control">
                                                            <option value="0">---Select Item---</option>
                                                            <?php foreach($stock_medications as $medication) { 

                                                                if($medication->sc == 0) continue;?>
                                                                <option <?php echo (isset($vaccines_sc) && $vaccines_sc !== 0 AND array_search($medication->id, $vaccines_sc) !== false) ? 'selected="selected"' : '';?>  value="<?php echo $medication->id;?>">
                                                                    <?php echo $medication->name;?>
                                                                </option>
                                                            <?php }?>
                                                            </select>
                                                            <button type="button" class="btn btn-block btn-primary btn-flat" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> <?php echo lang("actions_close");?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="medList">
                                                    <?php foreach($stock_medications as $medication) {
                                                        if($medication->sc == 0 || $vaccines_sc == 0) continue;
                                                        if(array_search($medication->id, $vaccines_sc) !== false AND $vaccines_sc !== 0){
                                                            $dateUpdateSC = $func->patientsModel->check_immunotherapy_procedure_history($medication->id,"SC",$iplan->id);
                                                            echo $medication->name.' - <input type="date" name="upVacDateSC['.$medication->id.']" value="'.((isset($dateUpdateSC)) ? $dateUpdateSC->date_app : date("Y-m-d")).'"><br />';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <a data-toggle="modal" class="modal-open" data-target="#slVacPopup">
                                                <?php echo lang('immuno_shedule_vacc_sl_label', 'vaccine_sl', array('class' => 'col-sm-2 control-label')); ?>
                                            </a>
                                            <div class="col-sm-10">
                                                <div class="modal modal-primary fade vaccine-popup" id="slVacPopup">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                            <select style="height:320px !important" multiple="multiple" name="vaccine_sl[]" class="form-control">
                                                            <option value="0">---Select Item---</option>
                                                            <?php foreach($stock_medications as $medication) { 
                                                                if($medication->sl == 0) continue;?>
                                                                <option <?php echo (isset($vaccines_sl) && $vaccines_sl !== 0 AND array_search($medication->id, $vaccines_sl) !== false) ? 'selected="selected"' : '';?>  value="<?php echo $medication->id;?>">
                                                                    <?php echo $medication->name;?>
                                                                </option>
                                                            <?php }?>
                                                            </select>
                                                            <button type="button" class="btn btn-block btn-primary btn-flat" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> <?php echo lang("actions_close");?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="medList">
                                                    <?php foreach($stock_medications as $medication) {
                                                        if($medication->sl == 0 || $vaccines_sl == 0) continue;
                                                        if(array_search($medication->id, $vaccines_sl) !== false AND $vaccines_sl !== 0){
                                                            $dateUpdateSL = $func->patientsModel->check_immunotherapy_procedure_history($medication->id,"SL",$iplan->id);
                                                            echo $medication->name.' - <input type="date" name="upVacDateSL['.$medication->id.']" value="'.((isset($dateUpdateSL)) ? $dateUpdateSL->date_app : date("Y-m-d")).'"><br />';
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('immuno_shedule_remark_label', 'txt_remark', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_textarea($txt_remark);?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('patient_notes_label', 'notes', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_textarea($notes);?>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('immuno_shedule_env_label', 'enviropment', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                               <?php 
                                               if($enviropments): foreach($enviropments as $enviropment) :
                                               echo $enviropment->name_diagnos."; ";
                                               endforeach;else: echo "---";endif;
                                               ?>
                                            </div>
                                        </div>
                                        <div class="form-group file-group">
                                            <?php echo lang('immuno_shedule_upload1_label', 'file01', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?php echo form_input($immuno_file01);?>
                                            </div>
                                            <?php if($iplan && $iplan->file1) {?>
                                            <div class="col-md-4 file-view">
                                                <a href="/upload/patients/<?php echo $iplan->file1;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a> 
                                                <div class="hidden-result-image">
                                                    <img src="/upload/patients/<?php echo $iplan->file1;?>" width="300px">
                                                 </div> 
                                                - <input type="checkbox" class="immuno-file-delete" id="dfile1" style="vertical-align:middle;" name="delete_file_1" value="<?php echo $iplan->file1;?>" /><label for="dfile1" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                            </div>
                                            <?}?>
                                        </div>
                                        <div class="form-group file-group">
                                            <?php echo lang('immuno_shedule_upload2_label', 'file02', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?php echo form_input($immuno_file02);?>
                                            </div>
                                            <?php if($iplan && $iplan->file2) {?>
                                            <div class="col-md-4 file-view">
                                                <a href="/upload/patients/<?php echo $iplan->file2;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a> 
                                                <div class="hidden-result-image">
                                                    <img src="/upload/patients/<?php echo $iplan->file2;?>" width="300px">
                                                </div> 
                                                - <input type="checkbox" class="immuno-file-delete" id="dfile2" style="vertical-align:middle;" name="delete_file_2" value="<?php echo $iplan->file2;?>" /><label for="dfile2" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                            </div>
                                            <?}?>
                                        </div> 
                                        <div class="form-group file-group">
                                            <?php echo lang('immuno_shedule_upload3_label', 'file03', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?php echo form_input($immuno_file03);?>
                                            </div>
                                            <?php if($iplan && $iplan->file3) {?>
                                            <div class="col-md-4 file-view">
                                                <a href="/upload/patients/<?php echo $iplan->file3;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                <div class="hidden-result-image">
                                                    <img src="/upload/patients/<?php echo $iplan->file3;?>" width="300px">
                                                </div>  
                                                - <input type="checkbox" class="immuno-file-delete" id="dfile3" style="vertical-align:middle;" name="delete_file_3" value="<?php echo $iplan->file3;?>" /><label for="dfile3" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
                                            </div>
                                            <?}?>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 form-group text-center immuno-plan-button">
                                        <div class="btn-group">
                                            <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                            <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                            <a href="/admin/stock/instock" class="btn btn-flat btn-success">In stock</a>
                                        </div>
                                    </div>
                                </div>

                                <?php echo form_close();?>

                                <hr />
                                <?php if($iplan && unserialize($iplan->vaccine_sc)) { ?>
                                <a href="/admin/patients/add_immunoteraphy_record_sc/<?php echo $patient->id;?>/<?php echo $iplan->id;?>/" class="btn btn-block btn-primary btn-flat add-immuno-record-button"><i class="fa fa-plus"></i><?php echo lang("nurse_record_add");?></a>
                                <?}?>
                                <h2 align="center"><?php echo lang('immuno_records_sc');?></h2>
                                <table width="100%" class="table table-bordered table-hover procedure-table"  id="immuno_table_sc" role="grid">
                                    <thead>
                                    <tr colspan="2">
                                        <th align="center"  rowspan="2"><?php echo lang("immuno_records_ville");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_schedule");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_dose");?></th>
                                        <th align="center" class="sorting" rowspan="2"><?php echo lang("immuno_records_date");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_vac");?></th>
                                        <th align="center" colspan="2"><?php echo lang("immuno_records_arm");?></th>
                                        <th align="center" colspan="2"><?php echo lang("immuno_records_time");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_icomments");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_lcomments");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_given");?></th>
                                        <th align="center" rowspan="2"><?php echo lang("immuno_records_paid");?> </th>
                                        <th align="center" rowspan="2"><?php echo lang('actions_title');?></th>
                                    </tr>
                                    <tr>
                                        <th align="center"><?php echo lang("immuno_records_handrt");?></th>
                                        <th align="center"><?php echo lang("immuno_records_handlt");?></th>
                                        <th align="center"><?php echo lang("immuno_records_timest");?></th>
                                        <th align="center"><?php echo lang("immuno_records_timeed");?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($procedure_records){
                                        foreach($procedure_records as $record_sc){ 
                                            if($record_sc->type !== "SC") continue;?>
                                            <tr>
                                                <td align="center" class="block-ville block-ville-<?php echo $record_sc->vile;?>">Vile - <?php echo $record_sc->vile;?></td>
                                                <td align="center" class="schedule schedule-<?php echo $record_sc->shedname;?>"><?php echo $record_sc->shedname;?></td>
                                                <td align="center"><?php echo $record_sc->dose;?></td>
                                                <td align="center"><?php echo date('Y/m/d', strtotime($record_sc->date_vacc));?></td>
                                                <td align="center"><?php echo ($record_sc->stname) ? $record_sc->stname : lang("immuno_records_nonstock");?></td>
                                                <td align="center" class="check-column"><?php if($record_sc->hand_rt == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sc->hand_rt;?></span></td>
                                                <td align="center" class="check-column"><?php if($record_sc->hand_lt == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sc->hand_lt;?></span></td>
                                                <td align="center"><?php echo substr($record_sc->stTime,0,-3);?></td>
                                                <td align="center"><?php echo substr($record_sc->edTime,0,-3);?></td>
                                                <td align="center"><?php echo $record_sc->iComments;?></td>
                                                <td align="center"><?php echo $record_sc->lComments;?></td>
                                                
                                                <td align="center" class="check-column"><?php if($record_sc->given == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sc->given;?></span></td>
                                                <td align="center" class="check-column"><?php if($record_sc->paid == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sc->paid;?></span></td>
                                                <td class="action-links">
                                                    <?php echo anchor('admin/patients/edit_immunoteraphy_record_sc/'.$record_sc->id, lang('actions_edit')); ?>
                                                    <?php echo anchor('/admin/patients/delete_immunoteraphy_record/'.$record_sc->id.'/'.$patient->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                                    <span class="authorName"><?php echo $user->first_name;?></span>
                                                </td>
                                            </tr>
                                        <?}
                                    }?>   
                                    </tbody>
                                </table>
                                <hr />
                                <?php if($iplan && unserialize($iplan->vaccine_sl)) { ?>
                                <a href="/admin/patients/add_immunoteraphy_record_sl/<?php echo $patient->id;?>/<?php echo $iplan->id;?>/" class="btn btn-block btn-primary btn-flat add-immuno-record-button"><i class="fa fa-plus"></i><?php echo lang("nurse_record_add");?></a>
                                <?}?>
                                <h2 align="center"><?php echo lang('immuno_records_sl');?></h2>
                                <table width="100%" class="table table-bordered table-hover procedure-table" id="immuno_table_sl" role="grid">
                                    <thead>
                                    <tr>
                                        <th align="center"><?php echo lang("immuno_records_date");?></th>
                                        <th align="center"><?php echo lang("immuno_records_vac");?></th>
                                        <th align="center"><?php echo lang("immuno_records_qty");?></th>
                                        <th align="center"><?php echo lang("immuno_records_comments");?></th>
                                        <th align="center"><?php echo lang("immuno_records_given");?></th>
                                        <th align="center"><?php echo lang("immuno_records_paid");?> </th>
                                        <th align="center"><?php echo lang('actions_title');?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($procedure_records){
                                        foreach($procedure_records as $record_sl){ 
                                            if($record_sl->type !== "SL") continue;?>
                                            <tr>
                                                <td align="center"><?php echo date('Y/m/d', strtotime($record_sl->date_vacc));?></td>
                                                <td align="center"><?php echo ($record_sl->stname) ? $record_sl->stname : lang("immuno_records_nonstock");?></td>
                                                <td align="center"><?php echo $record_sl->dose;?></td>
                                                <td align="center"><?php echo $record_sl->iComments;?></td>
                                                <td align="center" class="check-column"><?php if($record_sl->given == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sl->given;?></span></td>
                                                <td align="center" class="check-column"><?php if($record_sl->paid == 1){?><i class="fa fa-check"></i><?}?><span style="visibility: hidden" ;=""><?php echo $record_sl->paid;?></span></td>
                                                <td class="action-links">
                                                    <?php echo anchor('admin/patients/edit_immunoteraphy_record_sl/'.$record_sl->id, lang('actions_edit')); ?>
                                                    <?php if($is_admin){?>
                                                    <?php echo anchor('/admin/patients/delete_immunoteraphy_record/'.$record_sl->id.'/'.$patient->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                                    <?}?>
                                                    <span class="authorName"><?php echo $user->first_name;?></span>
                                                </td>
                                            </tr>
                                        <?}
                                    }?>   
                                    </tbody>
                                </table>
                                <script>
                                	$("document").ready(function(){
                                		scTable = jQuery('#immuno_table_sc').DataTable();
                                		scTable.order( [3,'desc'] ).draw();
                                		slTable = jQuery('#immuno_table_sl').DataTable();
                                		slTable.order( [0,'desc'] ).draw();
                                	});
                                	
                                </script>
                              </div>

                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="results">
                                <h2><?php echo lang('patient_results_tab');?></h2>
                                <table class="table table-bordered table-hover nurseTable dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Files</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php foreach($result_dates as $res_date){ ?>
                                        <tr>
                                            <td><a href="/admin/patientsections/results/<?php echo $patient->id?>/<?php echo $res_date->date_file;?>" target="_blank"><?php echo $res_date->date_file;?></a></td>
                                            <?php if(isset($result_sections)){?>
                                                <td>
                                                    <?php foreach($result_sections as $key=>$label){?>
                                                        <div class="file-view">
                                                        <?php $active_files = $func->get_active_files_by_section($patient->id,$res_date->date_file,$key);
                                                        if($active_files): ?>
                                                            <strong class="result-section-label"><?php echo $label;?>:</strong>
                                                            <?php $i=1; foreach($active_files as $files):?>
                                                                <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"><i class="fa fa-search"></i><?php echo lang("view_file_label");?><?php echo $i;?></a>
                                                                &nbsp;&nbsp; &nbsp;  
                                                                <div class="hidden-result-image"><img src="/upload/patients/<?php echo $files->fname;?>" width="300px" /></div>
                                                            <?php $i++; endforeach; ?>
                                                        <?php endif;?>
                                                        </div>
                                                    <?php }?>
                                                </td>
                                            <?}?>   
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                              </div>
                              <!-- /.tab-pane -->

                              <!-- tab-pane -->
                              <div class="tab-pane" id="allergy">
                                <h2><?php echo lang('patient_allergy_tab');?></h2>
                                <table class="table table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Files</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php foreach($result_dates as $res_date){ 
                                            $allergy_active_files = $func->get_active_files_by_section($patient->id,$res_date->date_file,105);
                                            if(!$allergy_active_files) continue; 
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="/admin/patientsections/results/<?php echo $patient->id?>/<?php echo $res_date->date_file;?>" target="_blank">
                                                    <?php echo $res_date->date_file;?>    
                                                </a>
                                            </td>
                                            <?php if(isset($result_sections)){?>
                                                <td>
                                                    <div class="file-view">
                                                        <strong class="result-section-label"><?php echo $result_sections[105];?>:</strong>
                                                        <?php $i=1; foreach($allergy_active_files as $files):?>
                                                            <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"><i class="fa fa-search"></i><?php echo lang("view_file_label");?><?php echo $i;?></a>
                                                            &nbsp;&nbsp; &nbsp;  
                                                            <div class="hidden-result-image"><img src="/upload/patients/<?php echo $files->fname;?>" width="300px" /></div>
                                                        <?php $i++; endforeach; ?>
                                                    </div>
                                                </td>
                                            <?}?>   
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                              </div>
                              <!-- /.tab-pane -->

                              <!-- tab-pane -->
                              <div class="tab-pane" id="exam-photo">
                                <h2><?php echo lang('patient_examphoto_tab');?></h2>
                                <table class="table table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Files</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    <?php if($examphotos) { ?>
                                        <?php foreach($examphotos as $exam_record){?>
                                        <tr>
                                            <td>
                                                <a target="_blank" href="/admin/patientsections/main/exam_photo/<?php echo $patient->id?>/<?php echo $exam_record["date_test"];?>">
                                                    <?php echo $exam_record["date_test"];?>
                                                </a>
                                            </td>
                                            <td><?php if(!empty($exam_record["files"])) {?>
                                                <div class="file-view">
                                                <?php $i=1; foreach($exam_record["files"] as $record_file){?>
                                                    <a href="/upload/patients/<?php echo $record_file;?>" target="_blank"><i class="fa fa-search"></i><?php echo lang("view_file_label");?><?php echo $i;?></a>
                                                            &nbsp;&nbsp; &nbsp;  
                                                    <div class="hidden-result-image"><img src="/upload/patients/<?php echo $record_file;?>" width="300px" /></div>
                                                <?php $i++;}?>
                                                </div>
                                            <?}?></td>

                                            <td><?php echo $exam_record["desc"];?></td>
                                        </tr>
                                        <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                              </div>
                              <!-- /.tab-pane -->

                              <!-- tab-pane -->
                              <div class="tab-pane" id="spirometry">
                                <h2><?php echo lang('patient_spirometry_tab');?></h2>
                                <table class="table table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Spirometers</th>
                                            <th>Files</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    <?php if($spirometry) { ?>
                                        <?php foreach($spirometry as $spirometry_record){?>
                                        <tr>
                                            <td>
                                                <a target="_blank" href="/admin/patientsections/main/spirometer/<?php echo $patient->id?>/<?php echo $spirometry_record["date_test"];?>">
                                                    <?php echo $spirometry_record["date_test"];?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php echo $spirometry_record["spirometers"];?>   
                                            </td>
                                            <td><?php if(!empty($spirometry_record["files"])) {?>
                                                <div class="file-view">
                                                <?php $i=1; foreach($spirometry_record["files"] as $sprecord_file){?>
                                                    <a href="/upload/patients/<?php echo $sprecord_file;?>" target="_blank"><i class="fa fa-search"></i><?php echo lang("view_file_label");?><?php echo $i;?></a>
                                                            &nbsp;&nbsp; &nbsp;  
                                                    <div class="hidden-result-image"><img src="/upload/patients/<?php echo $sprecord_file;?>" width="300px" /></div>
                                                <?php $i++;}?>
                                                </div>
                                            <?}?></td>

                                            <td><?php echo $spirometry_record["desc"];?></td>
                                        </tr>
                                        <?php }?>
                                    <?php }?>
                                    </tbody>
                                </table>
                              </div>
                              <!-- /.tab-pane -->

                              <!-- tab-pane -->
                              <div class="tab-pane" id="sickleaves">
                                <h2><?php echo lang('patient_sickleaves_tab');?></h2>
                                 <table class="table table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th><?php echo lang("date_visit");?></th>
                                            <th><?php echo lang("sickleave_leave_diag");?></th>
                                            <th><?php echo lang("sickleave_leave_days");?></th>
                                            <th><?php echo lang("sickleave_destination");?></th>
                                            <th><?php echo lang("sickleave_extra_notes");?></th>
                                            <th><?php echo lang("sickleave_signature");?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($sickleaves_records){?>
                                            <?php foreach($sickleaves_records as $sick_record){?>
                                            <tr>
                                                <td>
                                                    <a href="/admin/patientsections/sickleave/<?php echo $patient->id?>/<?php echo $sick_record->date_add;?>">
                                                        <?php echo $sick_record->date_add;?>
                                                    </a>
                                                </td>
                                                <td><?php echo $sick_record->diagnosis;?></td>
                                                <td><?php echo $sick_record->leave_days;?></td>
                                                <td><?php echo $sick_record->destination;?></td>
                                                <td><?php echo $sick_record->extra_notes;?></td>
                                                <td></td>
                                            </tr>
                                            <?}?>
                                        <?}?>
                                    </tbody>
                                 </table>
                              </div>
                              <!-- /.tab-pane -->

                              <div class="tab-pane" id="history">
                                <div class="col-md-4">
                                    <h2><?php echo lang('patient_visit_history_tab');?></h2>
                                
                                    <table class="table">
                                        <?php foreach($history as $history_record){?>                          
                                        <tr>
                                            <td>
                                                <span style='color:<?php echo $history_record->type_color;?> !important'><?php echo $history_record->type_name;?></span>
                                                &nbsp;&nbsp;<?php echo $func->calculate_age($patient->dob,$history_record->date_visit);?>
                                            </td>
                                            <td><?php echo $history_record->date_visit;?></td>
                                            <td>
                                            <a href="/admin/patientsections/summary/summary_plan/<?php echo $patient->id;?>/<?php echo $history_record->date_visit;?>"><?php echo lang("actions_see")." ".lang("patientsection")["summary_plan"];?> </a>
                                            </td>
                                        </tr>
                                        <?}?>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <h2><?php echo lang('patient_referral_tab');?></h2>
                                    <table class="table">
                                        <?php foreach($referrals_history as $referral_history){?>                          
                                        <tr>
                                            <td><?php echo $referral_history->date_test;?></td>
                                            <td><?php echo $func->calculate_age($patient->dob,$referral_history->date_test);?></td>
                                            <td><?php echo $referral_history->refname;?></td>
                                            <td>
                                            <a href="/admin/patientsections/summary/referral/<?php echo $patient->id;?>/<?php echo $referral_history->date_test;?>"><?php echo lang("actions_see")." ".lang("patientsection")["referral"];?> </a>
                                            </td>
                                        </tr>
                                        <?}?>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <h2><?php echo lang('patient_reports_tab');?></h2>
                                    
                                    <?php if($patient->file03) {?>
                                        <p><strong>Main File</strong></p>
                                        <p>
                                            <a href="/upload/patients/<?php echo $patient->file03;?>" target="_blank"> <i class="fa fa-search"></i><?php echo $patient->file03;?> </a>
                                        </p>
                                    <?}?>
                                    <p>&nbsp;</p>
                                    <?php if($multiple_report_files) {?>
                                        <p><strong>Multiple Files</strong></p>
                                        <?php foreach($multiple_report_files as $m_file) : ?>
                                        <p>
                                            <span><?php echo $m_file->date_add;?>-</span>
                                            <a href="/upload/patients/<?php echo $m_file->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo $m_file->fname;?></a>
                                        </p>
                                        <?php endforeach; ?>
                                    <?}?>
                                    <table class="table">
                                        
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                              </div>
                              <!-- /.tab-pane -->

                              <div class="tab-pane" id="invoice">
                                <h2><?php echo lang('patient_invoice_tab');?></h2>
                                <table class="table">
                                    <tr>
                                        <th>Date</th>
                                        <th>Doctor</th>
                                        <th>Customer pay</th>
                                        <th>Knet</th>
                                        <th>Cash</th>
                                        <th>Debt</th>
                                        <th><?php echo lang("actions_title");?></th>
                                    </tr>
                                    <?php if(isset($invoices)): 
                                        foreach($invoices as $invoice):
                                        $customer_pay = $func->get_customer_pay_indate($patient->id,$invoice->invoice_date);
                                        if(empty($customer_pay)){continue;}
                                        if($customer_pay->invoice_doctor_id != 0){ 
                                            $invoice_doctor = $func->get_invoice_doctor($customer_pay->invoice_doctor_id); 
                                        }else{
                                            $invoice_doctor = $func->get_invoice_doctor($patient->doctor); 
                                        }
                                    ?>
                                    <tr>
                                        <td><a href="/admin/patientsections/invoice/<?php echo $patient->id;?>/<?php echo $invoice->invoice_date;?>/#invoice-block"><?php echo $invoice->invoice_date;?></a>
                                        </td>
                                        <td><?php echo $invoice_doctor;?></td>
                                        <td><?php echo $customer_pay->knet+$customer_pay->cash+$customer_pay->debt;?></td>
                                        <td><?php echo $customer_pay->knet ;?></td>
                                        <td><?php echo $customer_pay->cash;?></td>
                                        <td><?php echo $customer_pay->debt;?></td>
                                        <td class="action-links"><a href="/admin/patientsections/invoice/<?php echo $patient->id;?>/<?php echo $invoice->invoice_date;?>/#invoice-block"><?php echo lang("actions_see");?></a>
                                            <a href="/admin/patientsections/print_invoice/<?php echo $patient->id;?>/<?php echo $invoice->invoice_date;?>/patient">Print <?php echo lang("print_invoice_patient_copy");?></a>
                                            <a href="/admin/patientsections/print_invoice/<?php echo $patient->id;?>/<?php echo $invoice->invoice_date;?>/clinic"> Print <?php echo lang("print_invoice_clinic_copy");?></a></td>
                                    </tr>
                                    <?php endforeach;endif;?>
                                </table>
                              </div>
                              <!-- /.tab-pane -->
                              <!-- /.tab-pane -->
                              <div class="tab-pane" id="patientsections">
                                <h2><?php echo lang('doctor_record_edit&add');?></h2>
                                <table  class="table table-bordered">
                                    <?php foreach(lang("patientsection") as $key=>$section){?>
                                        <tr>
                                            <?php if($date_appointment !== 0 && $key != "invoice"){?>
                                                <td><a href="/admin/patientsections/main/<?php echo $key;?>/<?php echo $patient->id;?>/<?php echo $date_appointment;?>"><?php echo $section;?></a></td>   
                                            <?}else{?>
                                                <td><a href="/admin/patientsections/main/<?php echo $key;?>/<?php echo $patient->id;?>"><?php echo $section;?></a></td>
                                            <?}?>
                                        </tr>
                                    <?}?>
                                    
                                </table>
                              </div>
                              <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                          </div>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
$("document").ready(function(){
    var table = $('.nurseTable').DataTable({
        "pageLength": 25,
        "aaSorting": [[ 0, "desc" ]]
    });

    if(window.location.href.indexOf("?") > -1) {
        window.history.pushState('page2', 'Title',location.origin + location.pathname);
    }

    if(window.location.hash !== ""){
        $(".nav-tabs li#"+window.location.hash.replace("#","",window.location.hash)+" a").trigger("click");
    }
    $(".nav-tabs li a").click(function(){
        window.location.hash = "";
        $(".message-box").remove();
    });
    
    $(".medList").click(function(){
        if($(this).html() == "" ){
            $(this).parent().parent().find(".modal-open").trigger("click");
        }
    });

    $(".immuno-file-delete").click(function(){
        $("#shedule-form-create").submit();
    });
    $(".patient-file-delete").click(function(){
        $("#form-patient-edit").submit();
    });
});
</script>