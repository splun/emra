<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->dob){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";}?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>

                            <div class="form-group">
                                
                                <?php echo lang('immuno_records_qty', 'date_visit', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <strong>Notice: When editing this field, subtraction from the storage of this vaccine will not occur. If the dose was entered incorrectly, recalculate this stock item quantity, manually on the stock page!</strong>
                                    <?php echo form_input($dose);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('immuno_records_date', 'date_vacc', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($date_vacc);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php //var_dump($stock_items);
                                echo lang('immuno_records_vac', 'stock_vaccines', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <select name="stock_vaccines" class="form-control">
                                    <option value="0">---Select Item---</option>
                                    <option value="-1"><?php echo lang("immuno_records_nonstock");?></option>
                                    <?php foreach($stock_items as $stock_item) {?>
                                    <option <?php if($record->stock_vaccines==$stock_item->id){echo "selected=selected";}?> value="<?php echo $stock_item->id;?>"><?php echo $stock_item->name."(Location: ".$stock_item->location_name."; Qty: ".$stock_item->qty." ".$stock_item->unit_name."; Expire day: ".$stock_item->expire_day.")";?></option>
                                    <?php }?>
                                    </select>
                          
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('immuno_records_comments', 'iComments', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_textarea($iComments);?>
                                </div>
                            </div>
                            <div class="form-group">    
                                <?php echo lang('immuno_records_given', 'given', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">  
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="given" <?php echo ($record->given == 1) ? "checked=checked" : "";?> value="1"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">    
                                <?php echo lang('immuno_records_paid', 'paid', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">  
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="paid" <?php echo ($record->paid == 1) ? "checked=checked" : "";?> value="1"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php echo anchor('admin/patients/card/'.$patient->id."#immunoteraphy-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>