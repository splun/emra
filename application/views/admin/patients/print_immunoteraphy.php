<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><a href="/admin/patients/card/<?php echo $patient->id;?>/#immunoteraphy-block" class="btn btn-default btn-flat btn-back"><?php echo lang('actions_back');?></a><a onclick="printContent('print-table-wrap');" class="btn btn-block btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a></h3>
                    </div>
                    <div class="box-body" id="print-table-wrap">
                        <?php echo $message;?>
                        <h2 class="text-center"><?php echo lang('immuno_shedule_title');?></h2> 
                        <div class="row print-table-head">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_name_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo $patient->patient_name;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_age_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php if($patient_year){ echo $patient_year.";";}?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_file_no_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo $patient->id;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_diagnos_label'); ?></div>
                                    <div class="col-sm-8">
                                        <?php echo $diagnos->name;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_vacc_sc_label'); ?></div>
                                    <div class="col-sm-8">
                                            <?php foreach($stock_medications as $medication) {
                                                if($medication->sc == 0 || $vaccines_sc == 0) continue;
                                                if(array_search($medication->id, $vaccines_sc) !== false AND $vaccines_sc !== 0){
                                                    $dateUpdateSC = $func->patientsModel->check_immunotherapy_procedure_history($medication->id,"SC",$iplan->id);
                                                    echo $medication->name.' - '.((isset($dateUpdateSC)) ? $dateUpdateSC->date_app : date("Y-m-d"))."; ";
                                                }
                                            }
                                            ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_vacc_sl_label'); ?></div>
                                    <div class="col-sm-8">
                                            <?php foreach($stock_medications as $medication) {
                                                if($medication->sl == 0 || $vaccines_sl == 0) continue;
                                                if(array_search($medication->id, $vaccines_sl) !== false AND $vaccines_sl !== 0){
                                                    $dateUpdateSL = $func->patientsModel->check_immunotherapy_procedure_history($medication->id,"SL",$iplan->id);
                                                    echo $medication->name.' - '.((isset($dateUpdateSL)) ? $dateUpdateSL->date_app : date("Y-m-d"))."; ";
                                                }
                                            }
                                            ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_remark_label'); ?></div>
                                    <div class="col-sm-8">
                                        <?php echo $iplan->txt_remark;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_notes_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo $patient->notes;?>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_env_label'); ?></div>
                                    <div class="col-sm-8">
                                       <?php 
                                       if($enviropments): foreach($enviropments as $enviropment) :
                                       echo $enviropment->name_diagnos."; ";
                                       endforeach;else: echo "---";endif;
                                       ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_sex_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo ($patient->gender == 1) ? "M" : "F";?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_phone_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo $patient->telephone;?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_insurance_company_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo ($insurance) ? $insurance->name : "None";?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('patient_treatingDR_label'); ?>:</div>
                                    <div class="col-sm-8">
                                        <?php echo ($doctor) ? "Dr. ".$doctor->first_name." ".$doctor->last_name : "";?>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-4 control-label"><?php echo lang('immuno_shedule_date_label'); ?></div>
                                    <div class="col-sm-8">
                                        <?php echo $iplan->date_consented;?>
                                    </div>
                                </div>
                                 
                            </div>

                        </div>

                        <hr />
                        <h2 align="center"><?php echo lang('immuno_records_sc');?></h2>
                        <table width="100%" class="table table-bordered table-hover" role="grid">
                            <thead>
                            <tr colspan="2">
                                <th align="center"  rowspan="2"><?php echo lang("immuno_records_ville");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_schedule");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_dose");?></th>
                                <th align="center" class="sorting" rowspan="2"><?php echo lang("immuno_records_date");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_vac");?></th>
                                <th align="center" colspan="2"><?php echo lang("immuno_records_arm");?></th>
                                <th align="center" colspan="2"><?php echo lang("immuno_records_time");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_icomments");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_lcomments");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_given");?></th>
                                <th align="center" rowspan="2"><?php echo lang("immuno_records_paid");?> </th>
                            </tr>
                            <tr>
                                <th align="center"><?php echo lang("immuno_records_handrt");?></th>
                                <th align="center"><?php echo lang("immuno_records_handlt");?></th>
                                <th align="center"><?php echo lang("immuno_records_timest");?></th>
                                <th align="center"><?php echo lang("immuno_records_timeed");?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($procedure_records){
                                foreach($procedure_records as $record_sc){ 
                                    if($record_sc->type !== "SC") continue;?>
                                    <tr>
                                        <td align="center">Vile - <?php echo $record_sc->vile;?></td>
                                        <td align="center"><?php echo $record_sc->shedname;?></td>
                                        <td align="center"><?php echo $record_sc->dose;?></td>
                                        <td align="center"><?php echo date('d/m/Y', strtotime($record_sc->date_vacc));?></td>
                                        <td align="center"><?php echo ($record_sc->stname) ? $record_sc->stname : lang("immuno_records_nonstock");?></td>
                                        <td align="center" class="check-column"><?php if($record_sc->hand_rt == 1){?>+<?}?></td>
                                        <td align="center" class="check-column"><?php if($record_sc->hand_lt == 1){?>+<?}?></td>
                                        <td align="center"><?php echo substr($record_sc->stTime,0,-3);?></td>
                                        <td align="center"><?php echo substr($record_sc->edTime,0,-3);?></td>
                                        <td align="center"><?php echo $record_sc->iComments;?></td>
                                        <td align="center"><?php echo $record_sc->lComments;?></td>
                                        
                                        <td align="center" class="check-column"><?php if($record_sc->given == 1){?>+<?}?></td>
                                        <td align="center" class="check-column"><?php if($record_sc->paid == 1){?>+<?}?></td>
                                        
                                    </tr>
                                <?}
                            }?>   
                            </tbody>
                        </table>
                        <hr />
                        <h2 align="center"><?php echo lang('immuno_records_sl');?></h2>
                        <table width="100%" class="table table-bordered table-hover" role="grid">
                            <thead>
                            <tr>
                                <th align="center"><?php echo lang("immuno_records_date");?></th>
                                <th align="center"><?php echo lang("immuno_records_vac");?></th>
                                <th align="center"><?php echo lang("immuno_records_qty");?></th>
                                <th align="center"><?php echo lang("immuno_records_comments");?></th>
                                <th align="center"><?php echo lang("immuno_records_given");?></th>
                                <th align="center"><?php echo lang("immuno_records_paid");?> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($procedure_records){
                                foreach($procedure_records as $record_sl){ 
                                    if($record_sl->type !== "SL") continue;?>
                                    <tr>
                                        <td align="center"><?php echo date('d/m/Y', strtotime($record_sl->date_vacc));?></td>
                                        <td align="center"><?php echo ($record_sl->stname) ? $record_sl->stname : lang("immuno_records_nonstock");?></td>
                                        <td align="center"><?php echo $record_sl->dose;?></td>
                                        <td align="center"><?php echo $record_sl->iComments;?></td>
                                        <td align="center" class="check-column"><?php if($record_sl->given == 1){?>+<?}?></td>
                                        <td align="center" class="check-column"><?php if($record_sl->paid == 1){?>+<?}?></td>
                                    </tr>
                                <?}
                            }?>   
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
  <script type="text/javascript">
    printContent('print-table-wrap');
  </script> 