<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        <h3>

                            <?php if($date_appointment !== 0){?>
                            <?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?> 
                            <?}else{?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?>
                            <?}?>

                            &nbsp;&nbsp;<a target="_blank" href="?dublicate=1" class="btn btn-warning btn-flat btn-copy"><i class="fa fa-copy"></i> <?php echo lang('prescription_dublicate');?></a>
                            &nbsp;&nbsp;<a onclick="printContent('summary-printable-area');" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a>
                        </h3> 
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>
                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>
                        <?php if(isset($referral)){?>
                        <table class="table referral-select-area" style="margin-bottom: 10px;">
                                <tr>
                                    <td style="border:0px;width: 20%;font-weight: 600;"><?php echo lang("refer_to");?>:</td>
                                    <td style="border:0px;">
                                    <select name="ddl_other" class="form-control">
                                        <option value="0">---Select---</option>
                                        <?php  foreach($referral as $ref){ ?>
                                        <option <?php echo (isset($active_ddl_option) && $active_ddl_option == $ref->id) ? "selected=selected" : "" ?> value="<?php echo $ref->id;?>"><?php echo $ref->name;?></option>
                                        <?}?>
                                    </select>
                                    </td>
                                </tr>
                        </table>
                        <?}?>
                        <div id="summary-printable-area">
                            <?php if(isset($referral)){?> 
                                <h2 class="summary-title hidden" style="text-align:center; margin-bottom: 40px;">Referal&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">تحويل </span></h2>
                            <?php }else{?>
                                <h2 class="summary-title hidden" style="text-align:center; margin-bottom: 40px;">Summary&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">ملخص
                                </span></h2>
                                
                            <?}?>
                            <table class="table" id="summary-head-table">
                                <?php if(isset($referral)){?>  
                                <tr>
                                    <td class="odd"><?php echo lang("refer_to");?>:</td>
                                    <td><?php echo $active_ddl_option_name;?></td>
                                </tr>
                                <?}?>    
                                <tr>
                                    <td class="odd"><?php echo lang("date_visit");?>:</td>
                                    <td><?php echo $date_visit;?></td>
                                </tr>
                                <tr>
                                    <td class="odd"><?php echo lang("patient_name_label");?>:</td>
                                    <td><?php echo $patient->patient_name;?></td>
                                </tr>
                                <tr>
                                    <td class="odd"><?php echo lang("patient_age_label");?>:</td>
                                    <td><?php echo $patient_year;?></td>
                                </tr>
                                <tr>
                                    <td class="odd"><?php echo lang("patient_file_no_label");?>:</td>
                                    <td><?php echo $patient->id;?></td>
                                </tr>
                                <tr>
                                    <td class="odd"><?php echo lang('patient_civil_id_label');?>:</td>
                                    <td><?php echo $patient->civil_id;?></td>
                                </tr>
                                <?php if($doctor){?>
                                <tr>
                                    <td class="odd"><?php echo lang("sickleave_signature");?>:</td>
                                    <td><?php echo "Dr. ".$doctor->first_name." ".$doctor->last_name;?></td>
                                </tr>
                                <?php } ?>

                            </table>

                            <table class="table" id="summary-list-table">
                                <?php if($diagnosis){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patient_diagnosis_tab");?>:</td>
                                    <td class="diagnosis-items-list">
                                        <?php foreach($diagnosis as $diagnos){
                                            echo $diagnos->name_diagnos."; ";
                                        }?>
                                        <div class="small" style="padding-top:10px;">
                                        <?php if($diagnosis_desc){
                                            echo $diagnosis_desc->desc;
                                        }?>
                                        </div>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($medications){?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")["medications"];?>:</td>
                                    <td>
                                        <?php foreach($medications as $medication){
                                            echo $medication["name_treatment"]."; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($complaints){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['complaints'];?>:</td>
                                    <td>
                                        <?php foreach($complaints as $complaint){
                                            echo $complaint->name_diagnos."(".$complaint->patient_file_description."); ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($family_history){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['family_social_history'];?>:</td>
                                    <td>
                                        <?php foreach($family_history as $history){
                                            echo $history->name_diagnos."; ";
                                        }
                                        if($family_history_desc){
                                            echo "<br />".$family_history_desc->desc;
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($environments){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['environment'];?>:</td>
                                    <td>
                                        <?php foreach($environments as $environment){
                                            echo $environment->name_diagnos."; ";
                                        }
                                        if($environments_desc){
                                            echo "<br />".$environments_desc->desc;
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($birth_vaccination){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['birth_vaccination'];?>:</td>
                                    <td>
                                        <?php foreach($birth_vaccination as $vaccination){
                                            echo $vaccination->name_diagnos."; ";
                                        }
                                        if($birth_vaccination_desc){
                                            echo "<br />".$birth_vaccination_desc->desc;
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($developments || $developments_active_files){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['development'];?>:</td>
                                    <td>
                                        <?php if($developments): foreach($developments as $development):
                                            echo $development->name_diagnos."; ";
                                        endforeach; endif;?>

                                        <?php if($developments_active_files): ?>

                                            <?php foreach($developments_active_files as $files):?>
                                                <div class="file-view">
                                                    <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif;?>

                                        <?php if($developments_desc){
                                            echo "<br />".$developments_desc->desc;
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($immunity){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['immunity'];?>:</td>
                                    <td>
                                        <?php foreach($immunity as $imm){
                                            echo $imm->name_diagnos."; ";
                                        }
                                        if($immunity_desc){
                                            echo "<br />".$immunity_desc->desc;
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($asthma){?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['asthma'];?>:</td>
                                    <td>
                                        <?php foreach($asthma as $as){
                                            echo $as->name_diagnos."; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($eczema){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['eczema'];?>:</td>
                                    <td>
                                        <?php foreach($eczema as $ecz){
                                            echo $ecz->name_diagnos."; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($rhino_conjunctivitis){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['rhino_conjunctivitis'];?>:</td>
                                    <td>
                                        <?php foreach($rhino_conjunctivitis as $rhino_conjunctiviti){
                                            echo $rhino_conjunctiviti->name_diagnos."; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($food_allergy){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['food_allergy'];?>:</td>
                                    <td>
                                        <table border="1"  cellspacing="3" cellpadding="3" class="table table-bordered table-hover">
                                        <tr><th>Name</th><th>Questions</th></tr>   
                                        <?php foreach($food_allergy as $food):?>
                                        <tr>
                                            <td><?php echo $food["allergy_name"];?></td>
                                            <td><?php if(!empty($food["ddl_id"])){
                                                foreach(explode(",",$food["ddl_id"]) as $key=>$ddl){
                                                    $question = $func->get_allergy_questions($food["section"],$ddl);

                                                    echo $question->name."; ";
                                                }
                                            }?>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        </table>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($drug_allergy){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['drug_allergy'];?>:</td>
                                    <td>
                                        <table border="1"  cellspacing="3" cellpadding="3" class="table table-bordered table-hover">
                                        <tr><th>Name</th><th>Questions</th></tr>   
                                        <?php foreach($drug_allergy as $drug):?>
                                        <tr>
                                            <td><?php echo $drug["allergy_name"];?></td>
                                            <td><?php if(!empty($drug["ddl_id"])){
                                                foreach(explode(",",$drug["ddl_id"]) as $key=>$ddl){
                                                    $question = $func->get_allergy_questions($drug["section"],$ddl);

                                                    echo $question->name."; ";
                                                }
                                            }?>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        </table>
                                        <?php echo ($drug_allergy_desc) ? " (".$drug_allergy_desc->desc.")" : ""; ?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($diets){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['diet'];?>:</td>
                                    <td>
                                        <?php foreach($diets as $diet){
                                            echo $diet->name_diagnos."; ";
                                        }?>
                                        <?php echo ($diets_desc) ? " (".$diets_desc->desc.")" : ""; ?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($nurse_records){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patient_nurse_tab");?>:</td>
                                    <td>
                                        <?php  foreach($nurse_records as $record):?>
                                            <?php if($record->weight){?>
                                            Wt: <span class="red"><?php echo $record->weight;?> (kg)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->height){?>
                                            Ht: <span class="red"><?php echo $record->height;?> (cm)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->heart){?>
                                            Hr: <span class="red"><?php echo $record->heart;?> (b/m)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->blood){?>
                                            BP: <span class="red"><?php echo $record->blood;?> (mmh)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->respiratory){?>
                                            RR: <span class="red"><?php echo $record->respiratory;?> (b/m)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->temp){?>
                                            Temp: <span class="red"><?php echo $record->temp;?> (c)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->Circumference){?>
                                            HC: <span class="red"><?php echo $record->Circumference;?> (cm.)</span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->bmi){?>
                                            Bmi: <span class="red"><?php echo round($record->bmi,2);?></span>&nbsp;&nbsp;
                                            <?}?>
                                            <?php if($record->sao){?>
                                            SaO2: <span class="red"><?php echo $record->sao;?></span>&nbsp;&nbsp;
                                            <?}?>
                                        <?php endforeach;?>
                                    </td>
                                </tr>
                                <?}?>

                                <?php if($physicals){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['physical'];?>:</td>
                                    <td>
                                        <?php foreach($physicals as $physical){
                                            echo $physical->name_diagnos;
                                            echo ($physical->patient_file_description) ? "(".$physical->patient_file_description.");" : "; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($skin_testing){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['skin_testing'];?>:</td>
                                    <td>
                                        <?php foreach($skin_testing as $skin){
                                            echo $skin->name_diagnos."; ";
                                        }?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($spirometers || $spirometers_active_files){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['spirometer'];?>:</td>
                                    <td>
                                        <?php if($spirometers): foreach($spirometers as $spirometer):
                                            echo $spirometer->name_diagnos."; ";
                                        endforeach; endif;?>

                                        <?php if($spirometers_active_files): ?>

                                            <?php foreach($spirometers_active_files as $files):?>
                                                <div class="file-view">
                                                    <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($rhinoscopy || $rhinoscopy_active_files){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['rhinoscopy'];?>:</td>
                                    <td>
                                        <?php if($rhinoscopy): foreach($rhinoscopy as $rh):
                                            echo $rh->name_diagnos."; ";
                                        endforeach; endif;?>

                                        <?php if($rhinoscopy_active_files): ?>

                                            <?php foreach($rhinoscopy_active_files as $files):?>
                                                <div class="file-view">
                                                    <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($results){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['results'];?>:</td>
                                    <td>
                                        <?php if($results): ?>

                                            <?php foreach($results as $result):?>
                                                <div style='content:" <" attr(href) "' class="file-view">
                                                    <a href="/upload/patients/<?php echo $result->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?}?>
                                <?php if($immunoCAP || $immunoCAP_active_files){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['immunoCAP'];?>:</td>
                                    <td>
                                        <?php if($immunoCAP): foreach($immunoCAP as $cap):
                                            echo $cap->name_diagnos."; ";
                                        endforeach; endif;?>

                                        <?php if($immunoCAP_active_files): ?>

                                            <?php foreach($immunoCAP_active_files as $files):?>
                                                <div class="file-view">
                                                    <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                <?}?>

                                <?php if($investigations){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['investigations'];?>:</td>
                                    <td>
                                        <?php  foreach($investigations as $investigation):?>
                                        <?php echo $investigation["name_investigations"];?>;
                                        <?php endforeach;?>
                                        <?php /*
                                        <table border="1"  cellspacing="3" cellpadding="3"  class="table table-bordered table-hover">
                                        <tr><th>Category</th><th>Item</th></tr>   
                                        <?php  foreach($investigations as $investigation):?>
                                        <tr>
                                            <td><?php echo $investigation["investigations_cat_name"];?></td>

                                            <td><?php echo $investigation["name_investigations"];?>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        </table> */?>

                                        <?php if($investigations_desc) :?>
                                            <div class="small"><?php echo $investigations_desc->desc;?></div>
                                        <?php endif;?>
                                    </td>
                                </tr>
                                    
                                <?}?>

                                <?php if($prescriptions){ ?>
                                <tr>
                                    <td class="odd"><?php echo lang("patientsection")['prescription'];?>:</td>
                                    <td>

                                        <?php  foreach($prescriptions as $prescription): echo $prescription["med_name"].";"; endforeach; ?> 

                                        <?php /* 
                                        <table border="1"  cellspacing="3" cellpadding="3" class="table table-bordered table-hover">
                                        <tr>
                                            <th>Category</th>
                                            <th>Item</th>
                                            <th>Dose</th>
                                            <th>Form</th>
                                            <th>Frequency</th>
                                            <th>Route</th>
                                            <th>Length/Days</th>
                                        </tr>   
                                        <?php  foreach($prescriptions as $prescription):?>
                                        <tr>
                                            <td><?php echo $prescription["med_cat_name"];?></td>

                                            <td><?php echo $prescription["med_name"];?>
                                            </td>
                                            <td><?php echo $prescription["dose"];?>
                                            </td>
                                            <td><?php echo $prescription["form_name"];?>
                                            </td>
                                            <td><?php echo $prescription["freq_name"];?>
                                            </td>
                                            <td><?php echo $prescription["route_name"];?>
                                            </td>
                                            <td><?php echo $prescription["length"];?>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        </table>
                                        */?>
                                    </td>
                                </tr>
                                <?}?>

                                <?php if(!empty($advice)){ ?>
                                <tr>
                                    <td class="odd">Advice</td>
                                </tr>
                                <tr>
                                    <td><?php echo $advice?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>

                        <?php if(isset($desc)): ?>
                        <div class="desc-area">
	                        <h3><?php echo lang("advice");?></h3>
	                        <table class="table">
	                        	<tr class="text-center">
	                        		<td>
	                        			 <?php echo form_textarea($desc);?>
	                        		</td>
	                        	</tr>
	                        </table> 
                        </div>
                        <?php endif;?> 

                        <div class="form-group col-sm-12 text-center bottom-button-area">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <input type="hidden" name="dublicate" value="<?php echo (isset($_GET["dublicate"])) ? $_GET["dublicate"] : ''; ?>" />
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
	$(".patient-file-delete").click(function(){
        $("#form-edit button[type=submit]").trigger("click");
    });

    $(".checkbox").each(function(){
    	if($(this).find("input[type='checkbox']").attr("checked") == "checked"){
    		$(this).parent().parent().parent().parent().removeClass("collapsed").addClass("collapse").addClass("in").css("height","auto");
    	}
    });
    
</script>

