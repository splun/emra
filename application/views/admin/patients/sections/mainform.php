<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        
                        <?php if($date_appointment !== 0){?>
                            <h3><?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                        <?}else{?>
                            <h3><?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                        <?}?>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>

                          <?php if(isset($group_checkboxes) && !empty($group_checkboxes)):?>
                          	
                          	<h3>Select <?php echo strtolower(strip_tags($pagetitle));?></h3>
                    		 	
                    		<div class="box-group" id="accordion"> 	
                          	<?php $i=1; foreach($group_checkboxes as $key=>$group):?>
                          		<div class="panel box box-primary">
	                    			<div class="box-header with-border">
					                    <h4 class="box-title">
					                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>" aria-expanded="false" class="collapsed">
					                        <?php echo $key;?>
					                      </a>
					                    </h4>
				                  	</div>
				                  	<div class="panel-collapse collapse" aria-expanded="false" id="collapse<?php echo $i;?>">
					                  	<table class="table">
			                    			<?php foreach($group as $chk_id=>$chk_name):  ?>
				                            <tr>
				                                <td style="width:60%"><?php echo $chk_name; ?>
				                            	</td>
                                                <?php if($section == 16){?>
				                            	<td>
                                                    <input type="number" min="0" step="1" name="chk_description[<?php echo $chk_id;?>]" value="<?php echo(array_search($chk_id, $active_checkboxes) !== false) ? $active_chk_description[$chk_id] : "";?>"> / mm              
                                                </td>
                                                <?}?>
				                                <td class="checkbox">  
				                                    <label class="switch"><input type="checkbox" name="checkbox[]" <?php echo(array_search($chk_id, $active_checkboxes) !== false) ? "checked=checked" : "";?> value="<?php echo $chk_id;?>" ><span class="slider"></span></label>
				                                </td>
				                            </tr>
				                        	<?php endforeach;?>
			                        	</table>
		                        	</div>
	                        	</div>
                            <?php $i++; endforeach; ?>
                            </div>
                          <?php endif;?>

                          <table class="table">
                          	<?php if(isset($checkboxes) && !empty($checkboxes)):?>
                          	<h3>Select <?php echo strtolower(strip_tags($pagetitle));?></h3>
                    		 
                    			<?php foreach($checkboxes as $checkbox):?>

	                            <tr>
	                                <td align="left" style="width:20%"><?php echo $checkbox->name; ?>
	                            	</td>
	                            	
	                                <td align="left" class="checkbox">  
	                                    <label class="switch"><input type="checkbox" name="checkbox[<?php echo $checkbox->id;?>]" <?php echo(array_search($checkbox->id, $active_checkboxes) !== false) ? "checked=checked" : "";?> value="<?php echo $checkbox->id;?>" ><span class="slider"></span></label>
	                                </td>
                                    <td align="left" style="width:30%">
                                            <textarea name="chk_description[<?php echo $checkbox->id;?>]" rows="2" class="form-control" placeholder="Description goes here"><?php echo(array_search($checkbox->id, $active_checkboxes) !== false) ? $active_chk_description[$checkbox->id] : "";?></textarea>
                                    </td>
	                            </tr>
                            <?php endforeach; endif;?>

                            <?php if(!empty($other_select)):?>
	                            <tr>
	                                <td style="width:60%"><?php echo $other_select["label"]; ?>
	                            	</td>
	                                <td class="form-group">  
	                                	<select name="<?php echo $other_select["name"];?>" class="form-control">
                                                <option value="0">
                                                   ---Select---     
                                                </option>
                                                <?php foreach($other_select["options"] as $option){?>
                                                    <option <?php echo($option->id == $other_select["active"]) ? "selected=selected" : ""; ?> value="<?php echo $option->id; ?>">
                                                        <?php echo $option->name; ?>
                                                    </option>
                                                <?}?>
                                        </select>
	                                </td>
	                            </tr>
	                        <?php endif;?>
	                        <?php if(!empty($pmh)):?>
	                        	<tr>
	                        		<td><strong><?php echo $pmh["label"]?></strong></td>
	                        		<td><?php echo $pmh["value"];?> </td>
	                        	</tr>
	                        <?php endif;?>
	                    </table> 

	                    <?php if(isset($file)) {?>
	                    	<h3><?php echo lang('actions_upload_files'); ?></h3>	
                          	<div class="form-group">
	                            <?php echo form_input($file);?>
	                      	</div>
	                      	
	                        <?php if($active_files): ?>

	                        	<?php foreach($active_files as $files):?>
	                            <div class="file-view">
	                                <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a> - <input type="checkbox" class="patient-file-delete" id="dfile" style="vertical-align:middle;" name="delete_file" value="<?php echo $files->fid;?>" /><label for="dfile" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
	                            </div>
	                        	<?php endforeach; ?>
	                        	</table>
	                    	<?php endif;?>

	                    <?}?>

                        <?php if(isset($desc)): ?>
	                        <h3>Description</h3>
	                        <table class="table">
	                        	<tr class="text-center">
	                        		<td>
	                        			 <?php echo form_textarea($desc);?>
	                        		</td>
	                        	</tr>
	                        </table> 
                        <?php endif;?> 

                        <div class="form-group col-sm-12 text-center">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
	$(".patient-file-delete").click(function(){
        $("#form-edit button[type=submit]").trigger("click");
    });

    $(".checkbox").each(function(){
    	if($(this).find("input[type='checkbox']").attr("checked") == "checked"){
    		$(this).parent().parent().parent().parent().removeClass("collapsed").addClass("collapse").addClass("in").css("height","auto");
    	}
    });
    
</script>