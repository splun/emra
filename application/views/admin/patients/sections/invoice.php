<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper" id="custom-form-page">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?> <?php echo ($patient_insurance) ? "<strong class='red'>(".$patient_insurance->name." - ".$patient_insurance->discount."%)</strong>" : "";?></h3>
                        <p><?php if($patient->dob){ echo $gender[$patient->gender].";";}?> 
                        <?php if($patient->dob){ echo $patient->dob.";";}
                         echo " ".$patient_year.";";
                        ?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        <h3>
                            <?php if($date_appointment !== 0){?>
                        	   <?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?>
                            <?}else{?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?>
                            <?}?>
                            &nbsp;&nbsp;<a target="_blank" href="?dublicate=1" class="btn btn-warning btn-flat btn-copy"><i class="fa fa-copy"></i> <?php echo lang('prescription_dublicate');?></a>  
                        	&nbsp;&nbsp;<?php echo anchor("/admin/patientsections/print_invoice/".$patient->id."/".$date_appointment."/patient", lang('print_invoice'), array('class' => 'btn btn-primary btn-flat btn-print')); ?>
                       </h3> 
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>
                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>
                        <table class="table list-table" id="invoice-form" align="center">
                        	<thead>
                        		<tr>
                        			<th>Category</th>
                        			<th>Item</th>
                        			<th>Insurance</th>
                        			<th>Discount</th>
                        			<th>Qty</th>
                        			<th>Price</th>
									<th class="ded-price-th">Deductible price</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		<h3>Select <?php echo strtolower(strip_tags($pagetitle));?> items</h3>
                        		<?php if(isset($active_items)): $i=0; foreach($active_items as $active_item):?>
                        			<tr class="form-field-row">
                            		<td>
                            			<select class="form-control  category-select" name="row[<?=$i+1;?>][categories]" onchange="invoiceItemBycategories(this)">
                            				<option value="0">---Select category--</option>
                            				<?php foreach($categories as $category){
		                                        if($category->parent == 0){?>
		                                            <option <?php echo ($category->id == $active_item->invoice_category) ? "selected=selected" : "";?> value="<?php echo $category->id; ?>">
		                                            <?php echo $category->name; ?>
		                                            </option>

		                                            <?php $childs = $func->invoice_child_categories($category->id);
		                                                if($childs){ 
		                                                    foreach($childs as $child){?>
		                                                        <option <?php echo ($child->id == $active_item->invoice_category) ? "selected=selected" : "";?> value="<?php echo $child->id; ?>">
		                                                        - <?php echo $child->name; ?>
		                                                        </option>
		                                                    <?}?>
		                                            <?}?>
		                                        <?}?>
                                        	<?}?>
                            			</select>
                            		</td>
                            		<td>
                     					<select name="row[<?=$i+1;?>][items]" class=" form-control select2 category-item">
                            				<option value="0">---Select item---</option>
                            				<?php foreach($invoice_items as $item){?>
                            					<?php if((int)$item->selling_item_id !== 0) { ?>
                            						<option <?php echo ($item->id == $active_item->invoice_item_id) ? "selected=selected" : "";?> data-stock-id="<?php echo $item->selling_item_id;?>" data-price="<?php echo $item->price;?>" data-netprice="<?php echo $item->net_price;?>" value="<?php echo $item->id;?>"><?php echo $item->name."(Location: ".$item->location_name."; Qty: ".$item->qty." ".$item->unit."; Expire day: ".$item->expire."Type: ".$item->type.")";?></option>
                            					<?}else{?>
                            						<option <?php echo ($item->id == $active_item->invoice_item_id) ? "selected=selected" : "";?> data-price="<?php echo $item->price;?>" data-netprice="<?php echo $item->net_price;?>" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                            					<?}?>
                            				<?php }?>
                            			</select>
                            		</td>
                            		<td width="7%" style="vertical-align:middle">
                            			<?php $insurace_pay = 0; if(!empty($patient_insurance) && $active_item->ins_flag !== "0") {
                            				$with_ins = $active_item->total*($active_item->ins_discount/100); 
                              				$insurace_pay = $active_item->total - $active_item->total*($active_item->ins_discount/100);
  											}
                            			?>
                            			<input class="insurance_check" <?php echo(empty($patient_insurance)) ? "disabled=disabled" : "";?> type="checkbox" name="row[<?=$i+1;?>][insurance]" <?php echo ($active_item->ins_flag == 1) ? "checked=checked" : "";?> value="1" /><?php echo (isset($insurace_pay) && $active_item->ins_flag == 1) ? "<span class='insurance-pay'><br />".$insurace_pay." KD</span>" : "<span class='insurance-pay'></span>";?> 
                            			<input type="hidden" class="insdiscount" name="row[<?=$i+1;?>][ins_discount]" value="<?php echo $active_item->ins_discount;?>" />
                            		</td>
                            		<td>
                            			<input type="text" class="form-control invoice-discount-fileds"  name="row[<?=$i+1;?>][discount_item]" value="<?php echo $active_item->discount_item;?>" />
                            			<select name="row[<?=$i+1;?>][discount_type]" class="form-control invoice-discount-type">
                            				<option <?php echo ($active_item->discount_type == 0) ? "selected=selected" : "";?> value="0">%</option>
                            				<option <?php echo ($active_item->discount_type == 1) ? "selected=selected" : "";?> value="1">KD</option>
                            			</select>
                            			<input type="hidden" class="discountvalue" name="row[<?=$i+1;?>][discount]" value="<?php echo $active_item->discount;?>" />
                            		</td>
                            		<td>
                            			<input type="number" step="1" min="1" name="row[<?=$i+1;?>][qty]" class="form-control invoice-qty-input" value="<?php echo $active_item->qty;?>" />
                            		</td>
                            		<td>
                            			<input readonly name="row[<?=$i+1;?>][total]" type="text"  class="form-control invoice-item-totalprice" value="<?php echo $active_item->total;?> KD" />
                            		</td>
									
									<td>
                            			<input name="row[<?=$i+1;?>][net_price]" type="text"  class="form-control invoice-item-net_price" value="<?php echo $active_item->net_price;?> KD" />
                            		</td>
                            		<input type="hidden" name="row[<?=$i+1;?>][active_item_id]" value="<?php echo $active_item->invoice_item_id;?>" />
                            		<input type="hidden" name="row[<?=$i+1;?>][old_qty]" value="<?php echo $active_item->qty;?>" />
                            	</tr>
                        		<?php $i++; endforeach; endif;?>

                        		<tr class="form-field-row dynamic">
                            		<td>
                            			<select class="form-control  category-select" name="row[0][categories]" onchange="invoiceItemBycategories(this)">
                            				<option value="0">---Select category--</option>
                            				<?php foreach($categories as $category){
		                                        if($category->parent == 0){?>
		                                            <option value="<?php echo $category->id; ?>">
		                                            <?php echo $category->name; ?>
		                                            </option>

		                                            <?php $childs = $func->invoice_child_categories($category->id);
		                                                if($childs){ 
		                                                    foreach($childs as $child){?>
		                                                        <option value="<?php echo $child->id; ?>">
		                                                        - <?php echo $child->name; ?>
		                                                        </option>
		                                                    <?}?>
		                                            <?}?>
		                                        <?}?>
                                        	<?}?>
                            			</select>
                            		</td>
                            		<td>
                     					<select name="row[0][items]" class=" form-control select2 category-item">
                            				<option value="0">---Select item---</option>
                            				<?php foreach($invoice_items as $item){?>
                            					<?php if((int)$item->selling_item_id !== 0) { ?>
                            						<option data-stock-id="<?php echo $item->selling_item_id;?>" data-netprice="<?php echo $item->net_price;?>" data-price="<?php echo $item->price;?>" value="<?php echo $item->id;?>"><?php echo $item->name."(Location:".$item->location_name."; Qty:".$item->qty."".$item->unit."; Expire day:".$item->expire."Type:".$item->type.")";?></option>
                            					<?}else{?>
                            						<option data-price="<?php echo $item->price;?>" data-netprice="<?php echo $item->net_price;?>" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                            					<?}?>
                            				<?php }?>
                            			</select>
                            		</td>
                            		<td width="7%" style="vertical-align:middle">
                            			<input class="insurance_check" <?php echo(empty($patient_insurance)) ? "disabled=disabled" : "";?> type="checkbox" checked="checked" name="row[0][insurance]" value="1" /><span class='insurance-pay'></span>
                            			<input type="hidden" class="insdiscount" name="row[0][ins_discount]" value="<?php echo ($patient_insurance) ? $patient_insurance->discount : 0;?>" />
                            		</td>
                            		<td>
                            			<input type="text" class="form-control invoice-discount-fileds"  name="row[0][discount_item]" value="0" />
                            			<select name="row[0][discount_type]" class="form-control invoice-discount-type">
                            				<option value="0">%</option>
                            				<option value="1">KD</option>
                            			</select>
                            			<input type="hidden" class="discountvalue" name="row[0][discount]" value="0" />
                            		</td>
                            		<td>
                            			<input type="number" step="1" min="1" name="row[0][qty]" class="form-control invoice-qty-input" value="1" />
                            		</td>
                            		<td>
                            			<input readonly name="row[0][total]" type="text" class="form-control invoice-item-totalprice" value="" />
                            		</td>
									<td>
                            			<input name="row[0][net_price]" type="text" class="form-control invoice-item-net_price" value="" />
                            		</td>
                            	</tr>
                            	<tr class="add-new-row"><td><a style="margin-left:0px;" id="add-block" class="btn btn-block btn-primary btn-flat"><i class="fa fa-plus"></i></a>
                        </td></tr>
                            </tbody>
                        </table>

                        <table class="table">
                        	<tr>
                        		<td width="25%"><strong><?php echo lang("invoice_total");?>:</strong></td>
                        		<td><span class="invoice_total">0</span> KD</td>
                        	</tr>
                        	<?php if(isset($patient_insurance)){?>
                        	<tr>	
                        		<td width="25%"><strong><?php echo lang("insurance_pay");?>:</strong></td>
                        		<td><span class="total_insurance_pay">0</span> KD</td>
                        	</tr>
                        	<?}?>
                        	<tr>	
                        		<td width="25%"><strong><?php echo lang("customer_pay");?>:</strong></td>
                        		<td><span class="customer_pay">0</span> KD</td>
                        	</tr>
                        	<tr>	
                        		<td width="25%"><strong><?php echo lang("invoice_knet");?>:</strong></td>
                        		<td><input type="number" step="0.1" min="0" class="form-control" id="knet-method" name="knet" value="<?php echo(!empty($patient_pay)) ? $patient_pay->knet : 0;?>" /> &nbsp;KD</td>
                        	</tr>	
                        	<tr>
                        		<td width="25%"><strong><?php echo lang("invoice_cash");?>:</strong></td>
                        		<td><input type="number" step="0.1" min="0" class="form-control" id="cash-method" name="cash" value="<?php echo(!empty($patient_pay)) ? $patient_pay->cash : 0;?>" /> &nbsp;KD</td>
                        	<tr>	
                        		<td width="25%"><strong><?php echo lang("invoice_debt");?>:</strong></td>
                        		<td><span class="invoice_debt">0</span> KD</td>
                        		<input type="hidden" class="form-control" id="debt" name="debt" value="<?php echo(!empty($patient_pay)) ? $patient_pay->debt : 0;?>" />
							</tr>
							<tr>	
                        		<td width="25%"><strong>Doctor<?php //echo lang("invoice_debt");?>:</strong></td>
                        		<td>
									<select style="max-width:320px;" class="form-control" name="doctor">
										<?php foreach($doctors as $doc){?>
											<option <?php if($doctor && $doctor->id == $doc->id){ echo "selected='selected'";}?> value="<?php echo $doc->id;?>"><?php echo "Dr. ".$doc->first_name." ".$doc->last_name;?></option>
										<?php }?>
									</select>
								</td>

                        	</tr>
                        </table>
                        <div class="form-group col-sm-12 text-center">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <input type="hidden" name="dublicate" value="<?php echo (isset($_GET["dublicate"])) ? $_GET["dublicate"] : 0; ?>" />
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
$("document").ready(function(){

	$(".patient-file-delete").click(function(){
        $("#form-edit button[type=submit]").trigger("click");
    });

    var row = $("tr.dynamic").html();

	$("body").on("click","#add-block",function(){
		count_rows = $(".list-table tbody tr.form-field-row").length;
    	new_row = row.replace(/row\[0]/g,"row["+count_rows+"]");
		$("tr.add-new-row").before("<tr class='form-field-row'>"+new_row+"</tr>");
	});

  	$("#cash-method,#knet-method").change(function(){
    
	    var sumval = parseFloat($("#cash-method").val()) + parseFloat($("#knet-method").val());
	    var maxpay = parseFloat($(".customer_pay").text());
	    var limitpay = 0;

	    if(sumval > maxpay){

	      if($(this).attr("id") == "knet-method"){

	        limitpay =  maxpay - parseFloat($("#cash-method").val());

	      }else{

	        limitpay =  maxpay - parseFloat($("#knet-method").val());

	      }

	      $(this).val(limitpay);

	    }

	    invoice_totals();

  	});

	invoice_totals();
 	
 	$("body").on("change",".category-item,.invoice-discount-fileds,.invoice-discount-type,.invoice-qty-input",function(){
 		var itemPrice = parseFloat($(this).parents(".form-field-row").find(".category-item option:selected").attr("data-price"));
 		var itemNetPrice = parseFloat($(this).parents(".form-field-row").find(".category-item option:selected").attr("data-netprice"));
		var itemDiscount = parseInt($(this).parents(".form-field-row").find(".invoice-discount-fileds").val());
 		var discountType = $(this).parents(".form-field-row").find(".invoice-discount-type").val();
 		var qty = parseInt($(this).parents(".form-field-row").find(".invoice-qty-input").val());
 		var discount = parseFloat($(this).parents(".form-field-row").find(".discountvalue").val());
 		var insdicount = parseInt($(this).parents(".form-field-row").find(".insdiscount").val());

 		if($(this).parents(".form-field-row").find(".category-item").val() !== "0"){
 			total(itemPrice,itemNetPrice,itemDiscount,discountType,qty,discount,insdicount,$(this))
 		}
		
 		
 	});

 	$("body").on("change",".insurance_check", function(){

 		var itemPrice = parseFloat($(this).parents(".form-field-row").find(".category-item option:selected").attr("data-price"));
 		var itemDiscount = parseInt($(this).parents(".form-field-row").find(".invoice-discount-fileds").val());
 		var discountType = $(this).parents(".form-field-row").find(".invoice-discount-type").val();
 		var qty = parseInt($(this).parents(".form-field-row").find(".invoice-qty-input").val());
 		var discount = parseFloat($(this).parents(".form-field-row").find(".discountvalue").val());
 		var insdicount = parseInt($(this).parents(".form-field-row").find(".insdiscount").val());

 		if($(this).parents(".form-field-row").find(".category-item").val() !== "0"){
            total(itemPrice,itemDiscount,discountType,qty,discount,insdicount,$(this));
 		}else{
 			$(this).parents(".form-field-row").find(".insurance-pay").html("");
 		}
		 

 	});

});

function total(itemPrice,itemNetPrice,itemDiscount,discountType,qty,discount,insdicount,el)  {
 		var final_price = 0;
		var final_price = 0;
 		var total_invoice_price = 0;
 		var with_ins = 0;
 		var insurace_pay = 0;
 		var total_insurance_pay = 0;

 		$(el).parents(".form-field-row").find(".invoice-item-totalprice").val(itemPrice+" KD");
		$(el).parents(".form-field-row").find(".invoice-item-net_price").val(itemNetPrice*qty+" KD");

 		if(itemDiscount>0){
	 		if(discountType != 1){
	 			final_price=itemPrice-(itemPrice*(itemDiscount/100));
	 			final_price=final_price*qty;
	  			$(el).parents(".form-field-row").find(".invoice-item-totalprice").val(final_price.toFixed(2)+" KD");

	 		}else{
	 			final_price = itemPrice*qty - itemDiscount;
	 			$(el).parents(".form-field-row").find(".invoice-item-totalprice").val(final_price.toFixed(2)+" KD");

	 		}
	 	}else{
	 		final_price = (qty > 0) ? itemPrice * qty : itemPrice;
	 		$(el).parents(".form-field-row").find(".invoice-item-totalprice").val(final_price.toFixed(2)+" KD");
	 	}

	 	if($(el).parents(".form-field-row").find(".insurance_check").prop("checked")){

		 	if(insdicount > 0){
			 		
		 		with_ins =  final_price*(insdicount/100); 
	            insurace_pay = final_price - final_price*(insdicount/100);
	  			$(el).parents(".form-field-row").find(".insurance-pay").html("<br />"+insurace_pay.toFixed(2)+" KD");
		 										
		 	}else{

	            insurace_pay = final_price;
	  			$(el).parents(".form-field-row").find(".insurance-pay").html("<br />"+insurace_pay.toFixed(2)+" KD");
		 		
		 	}
	 	}else{
	 		$(el).parents(".form-field-row").find(".insurance-pay").html("");
	 	}

	 	invoice_totals();
}

function invoice_totals(){

	var total_invoice_price = 0;
  	var total_invoice_price_with_insurance = 0;
    var total_insurance_pay = 0;

 	$(".insurance-pay").each(function(el){
 		if($(this).html() == "") {return true};

 		var cur_insurance_pay = $(this).html().replace(" KD","").replace("<br>","");
 		total_insurance_pay = total_insurance_pay + parseFloat(cur_insurance_pay);
 	
	});

	$(".invoice-item-totalprice").each(function(el){
		var cur_price = $(this).val().replace(" KD","");
		if(cur_price !== ""){
 			total_invoice_price = total_invoice_price + parseFloat(cur_price);
 			if(!$(this).parents(".form-field-row").find(".insurance_check").prop("checked")){
 				total_invoice_price_with_insurance = total_invoice_price_with_insurance+parseFloat(cur_price);
 			}else{
 				total_invoice_price_with_insurance = total_invoice_price_with_insurance+0;
 			}
 		}
 	});



 	$(".total_insurance_pay").text(total_insurance_pay.toFixed(2));
 	$(".invoice_total").text( (parseFloat(total_invoice_price)).toFixed(2));
 	$(".customer_pay").text(parseFloat(total_invoice_price_with_insurance).toFixed(2));
 	$(".invoice_debt").text( (parseFloat(total_invoice_price_with_insurance) - (parseFloat($("#knet-method").val()) + parseFloat($("#cash-method").val()))).toFixed(2));
 	$("#debt").val( (parseFloat(total_invoice_price_with_insurance) - (parseFloat($("#knet-method").val()) + parseFloat($("#cash-method").val()))).toFixed(2));
}

function invoiceItemBycategories(el){
    var cat = $(el).val();
    var select = $(el).parent().parent().find(".category-item");
    $(el).parent().parent().find(".invoice-item-totalprice").val("");
    $.ajax({
      type: "POST",
      url: '/admin/patientsections/invoice_items_by_cat',
      data: 'category_id='+cat,
      dataType:"json",
      success: function(data){
      	var option = "<option value='0'>---Select item---</option>";
      	$(data).each(function(i){
      		var content = "";
      		if(data[i].selling_item_id !== "0"){
      			option += "<option data-price="+data[i].price+" data-netprice="+data[i].net_price+"  data-stock-id="+data[i].selling_item_id+" value='"+data[i].id+"'>"+data[i].name+"( Location: "+data[i].location_name+"; Qty: "+data[i].qty+""+data[i].unit+"; Expire day: "+data[i].expire+", Type: "+data[i].type+" )</option>";
      		}else{
      			option += "<option data-price="+data[i].price+" data-netprice="+data[i].net_price+"  value='"+data[i].id+"'>"+data[i].name+"</option>";
      	    }

      	});
      	$(select).html(option);
      }
    });
}

  //$("#append_total_info").append($("#total-item").html());
</script>

<?php if(!$is_admin){?>
	<script>
		$(".invoice-item-net_price,.ded-price-th").hide();
	</script>
<?}?>