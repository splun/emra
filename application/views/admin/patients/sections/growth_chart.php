<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";"; ?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        <h3>

                            <?php if($date_appointment !== 0){?>
                            <?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?> 
                            <?}else{?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?>
                            <?}?>
                        </h3> 
                    </div>
                    <div class="box-body" id="accordion">
                        <?php echo $message;?>
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#headcircumference" aria-expanded="false" class="collapsed"><?php echo lang("headcircumference");?></a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="headcircumference">
                                <?php if($headcircumference){?>
                                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td align="center">
                                        <img src="/charts/chart.php?style=head-age&sex=<?php echo $patient->gender; ?>&maxage=36&xvals=<?php echo $headcircumference['xvals']; ?>&yvals=<?php echo $headcircumference['yvals']; ?>" />
                                        </td>
                                    </tr>
                                </table>
                                <?}?>
                            </div>
                        </div>
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#weight" aria-expanded="false" class="collapsed"><?php echo lang("growth_weight");?></a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="weight">
                                <?php if($weight){?>
                                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td align="center">
                                        <img src="/charts/chart.php?style=weight-age&sex=<?php echo $patient->gender; ?>&maxage=36&xvals=<?php echo $weight['xvals']; ?>&yvals=<?php echo $weight['yvals']; ?>" />
                                        </td>
                                    </tr>
                                </table>
                                <?}?>
                            </div>
                        </div>
                        <div class="panel box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#height" aria-expanded="false" class="collapsed"><?php echo lang("growth_height");?></a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="height">
                                <?php if($height){?>
                                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td align="center">
                                            <img src="/charts/chart.php?style=length-age&sex=<?php echo $patient->gender; ?>&maxage=36&xvals=<?php echo $height['xvals']; ?>&yvals=<?php echo $height['yvals']; ?>" />
                                        </td>
                                    </tr>
                                </table>
                                <?}?>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>