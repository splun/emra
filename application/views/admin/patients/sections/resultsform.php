<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";}
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        
                        <?php if($date_appointment !== 0){?>
                            <h3><?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                        <?}else{?>
                            <h3><?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                        <?}?> 
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>


	                    <?php if(isset($labels)) {?>
                            <table class="table" id="results-table">
                                <tbody>	
                                <?php foreach($labels as $key=>$label){?>
                                    <tr>
                                        <td style="vertical-align:middle"><h3 style="margin:0px;"><?php echo $label;?></h3></td>
                                        <td style="vertical-align:middle;width:25%">
                                          	<div class="form-group">
                                                <?php echo lang('actions_upload_files'); ?>
                	                            <input type="file" multiple name="file[<?php echo $key;?>][]">
                	                      	</div>
	                      	            </td>
                                        <td>
                	                        <?php $active_files = $func->get_active_files_by_section($patient->id,$date_appointment,$key);
                                            if($active_files): ?>

                	                        	<?php $i=1; foreach($active_files as $files):?>
                	                            <div class="file-view">
                	                                <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?><?php echo $i;?> </a> - 
                                                    <div class="hidden-result-image"><img src="/upload/patients/<?php echo $files->fname;?>" width="300px" /></div>       
                                                    <input type="checkbox" class="patient-file-delete" id="dfile" style="vertical-align:middle;" name="delete_file" value="<?php echo $files->fid;?>" />
                                                    <a class="delete-label" href="/admin/patientsections/results/<?php echo $patient->id;?>/<?php echo $date_appointment;?>/<?php echo $files->fid;?>"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></a>
                                                </div>
                	                        	<?php $i++; endforeach; ?>
                	                    	<?php endif;?>
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                            </table>
        	            <?}?>

                        <?php if(isset($desc)): ?>
	                        <h3>Description</h3>
	                        <table class="table">
	                        	<tr class="text-center">
	                        		<td>
	                        			 <?php echo form_textarea($desc);?>
	                        		</td>
	                        	</tr>
	                        </table> 
                        <?php endif;?> 

                        <div class="form-group col-sm-12 text-center">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
	$(".patient-file-delete").click(function(){
        $("#form-edit button[type=submit]").trigger("click");
    });

    $(".checkbox").each(function(){
    	if($(this).find("input[type='checkbox']").attr("checked") == "checked"){
    		$(this).parent().parent().parent().parent().removeClass("collapsed").addClass("collapse").addClass("in").css("height","auto");
    	}
    });
    
</script>