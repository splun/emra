<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><a href="/admin/patientsections/invoice/<?php echo $patient->id;?>/<?php echo $date_appointment;?>" class="btn btn-default btn-flat btn-back"><?php echo lang('actions_back');?></a><a onclick="printContent('print-table-wrap');" class="btn btn-block btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('print_invoice');?></a>
                        <?php if($copy == "patient"){?>
                        	<a href="/admin/patientsections/print_invoice/<?php echo $patient->id;?>/<?php echo $date_appointment;?>/clinic" class="btn btn-warning btn-flat"><?php echo lang('print_invoice_clinic_copy');?></a>
                        <?}else{?>
                        	<a href="/admin/patientsections/print_invoice/<?php echo $patient->id;?>/<?php echo $date_appointment;?>/patient" class="btn btn-warning btn-flat"><?php echo lang('print_invoice_patient_copy');?></a>
                        <?}?>
                    </h3>
                    </div>
                    <div class="box-body" id="print-table-wrap">
                        <?php echo $message;?>
                        <?php if($copy == "patient"){?>
	                        <table width="100%" id="prtTable" class="patient_copy_table" border="0" cellspacing="0" cellpadding="0">

					          <tbody>
					          <tr>

					            <td align="center"><h2 class="text-center"><?php echo lang('print_invoice_title');?>&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">فاتورة</span></h2> </td>

					          </tr>
					          <tr>

					            <td>&nbsp;</td>

					          </tr>

					          <tr>

					            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

					              <tbody><tr>

					                <td width="50%" align="left" valign="top">

					                <table width="100%" border="0" cellspacing="2" cellpadding="2">

					                  <tbody><tr>

					                    <th width="40%" align="left">File No.:</th>

					                    <td><?php echo $patient->id;?></td>

					                  </tr>

					                  <tr>

					                    <th align="left">Patient Name:</th>

					                    <td style="font-size:12px !important"><?php echo $patient->patient_name;?></td>

					                  </tr>
					                  
					                  <tr>
                                    	<th align="left"><?php echo lang('patient_civil_id_label');?>:</th>
                                    	<td><?php echo $patient->civil_id;?></td>
                                	 </tr>

					                  <tr>

					                    <th align="left">Date:</th>

					                    <td><?php echo date("l M d, Y",strtotime($date_appointment));?></td>

					                  </tr>

					                  <tr>

					                    <td>&nbsp;</td>

					                    <td>&nbsp;</td>

					                  </tr>

					                </tbody></table>

					                </td>

					                <td width="50%" align="left" valign="top">

					                <table width="100%" border="0" cellspacing="2" cellpadding="2">

					                  <tbody><tr>

					                    <th width="40%" align="left">Doctor Name:</th>

					                    <td><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></td>

					                  </tr>

					                  <tr>

					                    <th align="left">Cashier Name:</th>

					                    <td>admin</td>

					                  </tr>

					                  <tr>

					                    <th align="left">Invoice No.:</th>

					                    <td><?php echo date("Y",strtotime($date_appointment)).date("m",strtotime($date_appointment)).date("d",strtotime($date_appointment)).$patient->id;?></td>

					                  </tr>

					                  <tr>

					                    <td>&nbsp;</td>

					                    <td>&nbsp;</td>

					                  </tr>

					                </tbody></table>

					                </td>

					              </tr>

					            </tbody></table></td>

					          </tr>

					          

					          <tr><td>&nbsp;</td></tr>

					          

					          <tr>

					            <td valign="top" align="left">
					            <?php 
					            $insFlag = false;
							    $customer_pay = 0;
								$discountFlag=false;
								$qtyFlag=false;

							    $total_ins = 0;

					            if($patient_invoice){ 
					            	foreach($patient_invoice as $invoice){
					            		$discount_item =  ($invoice->discount == 0) ? $invoice->discount_item : $invoice->discount;
										$qty_item = $invoice->qty;

										if($discount_item>0) $discountFlag=true;

										if($qty_item>1) $qtyFlag=true;
					            	}

					            }

					            ?>

					            <table class="invoice_item_table" width="92%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid #666; border-bottom:0px; border-left:0px;border-spacing:0px">

					              <tbody><tr style="border-left: 1px solid;">

					                <th>S.No.</th>

					                <th>Description</th>

					                <th>Item price</th>
					                <?php if($discountFlag){?>
					                <th>Discount</th>
					                <?}?>
					                <?php if($qtyFlag){?>
					                <th>Qty</th>
					                <?}?>
					                <!--<th>Insurance Discount</th>-->

					                <th>Pay</th>


					              	</tr>

									
									<?php if($patient_invoice): $i=1;$final_total=0;$item_price = 0; foreach($patient_invoice as $invoice):

										$insFlag = FALSE;
						                $item_ins = 0;
						                $prc = 0;
						                $insur_field = $invoice->ins_flag;
						                if($insur_field>0) $insFlag=true;
						                $total = $invoice->total;
						                $discount = $invoice->discount;
						                $discount_item =  ($invoice->discount == 0) ? $invoice->discount_item : $invoice->discount;
						                $discount_type = $invoice->discount_type;
						                
						                if($discount_item<0) $discount_item = 0;
						                
						                if($discount_item > 0){
						                	if($discount_type !== "0"){
						                		$item_price = $total + $discount_item;
						                	}else{
						                		$item_price = $total + ($total * $discount_item / (100-$discount_item));
						                	}
						                	
						                }else{
						                	$item_price = $total;
						                }

						                if($qtyFlag){
						                	$item_price = $item_price / $invoice->qty;
						                }

						                $insDis=$invoice->ins_discount;

						                ?>
					                <tr style="border-left: 1px solid;">

					                  <td width="2%" align="center" valign="middle"><?php echo $i; ?></td>

					                  <td width="40%"><?php echo $invoice->item_name;?></td>

					                  
					                  <td width="10%" align="center"><?php echo $item_price;?> KD</td>

					                  <?php if($discountFlag){?>
					                  <td align="center" width="10%"><?php echo  $discount_item;?> <?php echo ($discount_type !== "0") ? "KD" : "%";?></td>
					                  <?}?>

					                  <?php if($qtyFlag){?>
					                  <td align="center" width="10%"><?php echo $invoice->qty;?></td>
					                  <?}?>

					                  <?php if($insFlag == true AND $insDis > 0){ //$item_ins = $prc-$prc*($insDis/100); ?>

					                    <td width="10%"> 
					                      <?php echo $total." KD"; $total_ins +=  $total; 
					                      ?>
					                    </td>

					                  <?php $final_total += $total;
					                  
					                  }else{?>

					                    <!--<td width="8%" align="center" >0</td>-->

					                    <td width="10%"> 

					                    	<?php echo $total." KD"; $total_ins +=  0; ?>

					                    </td>

					                    <?php $total_ins +=  0; $final_total += $total; $customer_pay += $total;

					                  }?>

					                </tr>
					                <?php $i++; endforeach;endif;?>
							      

							      <tr style="border:0px;">
							      	<?php 

								        $cols=5;

								        if($discountFlag==false) $cols=$cols-1; 

								        if($qtyFlag==false)      $cols=$cols-1;

								    ?>

					                <th style="border: 0px;border-top: 1px solid #000;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Total:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $final_total;?> KD</td>

					              </tr>

					                            <tr style="border:0px;">

					                <th style="border: 0px; border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Insurance Pays:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $total_ins;?> KD
					                </td>

					              </tr>              

					              
					              <tr style="border:0px;border-bottom:0px">

					                <th style="border: 0px;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Customer pays:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $customer_pay;?> KD</td>

					              </tr>


					            </tbody></table>

					            </td>

					          </tr>



					          <tr><td height="10px">&nbsp;</td></tr>

							  


									  
					                  <tr><td>&nbsp;</td></tr>

					                  <tr>

					                    <td>

					                     <table width="92%" border="0" cellspacing="2" cellpadding="2">

					                        <tbody><tr>

					                            <th width="20%" align="left"><?php echo ucfirst($copy);?> Copy&nbsp;</th>

					                            <th width="20%">&nbsp;Insurance:</th>

					                            <td width="20%"><?php echo ($patient_insurance) ? $patient_insurance->name : "";?></td>

					                            <th width="20%">&nbsp;Policy No.</th>

					                            <td width="20%" style="font-size:12px !important"><?php echo $patient->insurance_number;?></td>

					                        </tr>

					                    </tbody></table>

					                   </td>

					                  </tr>

					                  <tr><td>&nbsp;</td></tr>
							</tbody></table>
						<?php }else{?>
							<table width="100%" id="prtTable" class="clinic_copy_table" border="0" cellspacing="0" cellpadding="0">

					          <tbody>
					          <tr>

					            <td align="center"><h2 class="text-center"><?php echo lang('print_invoice_title');?>&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">فاتورة</span></h2></td>

					          </tr>
					          <tr>

					            <td>&nbsp;</td>

					          </tr>

					          <tr>

					            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

					              <tbody><tr>

					                <td width="50%" align="left" valign="top">

					                <table width="100%" border="0" cellspacing="2" cellpadding="2">

					                  <tbody><tr>

					                    <th width="40%" align="left">File No.:</th>

					                    <td><?php echo $patient->id;?></td>

					                  </tr>

					                  <tr>

					                    <th align="left">Patient Name:</th>

					                    <td style="font-size:12px !important"><?php echo $patient->patient_name;?></td>

					                  </tr>

					                  <tr>

					                    <th align="left">Date:</th>

					                    <td><?php echo date("l M d, Y",strtotime($date_appointment));?></td>

					                  </tr>

					                  <tr>

					                    <td>&nbsp;</td>

					                    <td>&nbsp;</td>

					                  </tr>

					                </tbody></table>

					                </td>

					                <td width="50%" align="left" valign="top">

					                <table width="100%" border="0" cellspacing="2" cellpadding="2">

					                  <tbody><tr>

					                    <th width="40%" align="left">Doctor Name:</th>

					                    <td><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></td>

					                  </tr>

					                  <tr>

					                    <th align="left">Cashier Name:</th>

					                    <td>admin</td>

					                  </tr>

					                  <tr>

					                    <th align="left">Invoice No.:</th>

					                    <td><?php echo date("Y",strtotime($date_appointment)).date("m",strtotime($date_appointment)).date("d",strtotime($date_appointment)).$patient->id;?></td>

					                  </tr>

					                  <tr>

					                    <td>&nbsp;</td>

					                    <td>&nbsp;</td>

					                  </tr>

					                </tbody></table>

					                </td>

					              </tr>

					            </tbody></table></td>

					          </tr>

					          

					          <tr><td>&nbsp;</td></tr>

					          

					          <tr>

					            <td valign="top" align="left">
					            <?php 
					            $insFlag = false;
							    $customer_pay = 0;
								$discountFlag=false;
								$qtyFlag=false;

							    $total_ins = 0;

					            if($patient_invoice){ 
					            	foreach($patient_invoice as $invoice){
					            		$discount_item =  ($invoice->discount == 0) ? $invoice->discount_item : $invoice->discount;
										$qty_item = $invoice->qty;

										if($discount_item>0) $discountFlag=true;

										if($qty_item>1) $qtyFlag=true;
					            	}

					            }

					            ?>

					            <table class="invoice_item_table" width="95%" border="1" cellspacing="2" cellpadding="2" style="border:1px solid #666; border-bottom:0px; border-left:0px;border-spacing:0px">

					              <tbody><tr style="border-left: 1px solid;">

					                <th>S.No.</th>

					                <th>Description</th>

					                <th>Item price</th>
					                <?php if($discountFlag){?>
					                <th>Discount</th>
					                <?}?>
					                <?php if($qtyFlag){?>
					                <th>Qty</th>
					                <?}?>

					                <th>Insurance Discount</th>

					                <th>Pay</th>


					              	</tr>

									
									<?php if($patient_invoice): $i=1;$final_total=0;$item_price=0; foreach($patient_invoice as $invoice):

										$insFlag = FALSE;
						                $item_ins = 0;

						                $insur_field = $invoice->ins_flag;
						                if($insur_field>0) $insFlag=true;
						                $total = $invoice->total;
						                $discount = $invoice->discount;
						                $discount_item =  ($invoice->discount == 0) ? $invoice->discount_item : $invoice->discount;
						                $discount_type = $invoice->discount_type;
						                if($discount_item<0) $discount_item = 0;

						                if($discount_item > 0){
						                	if($discount_type !== "0"){
						                		$item_price = $total + $discount_item;
						                	}else{
						                		$item_price = $total + ($total * $discount_item / (100-$discount_item));
						                	}
						                	
						                }else{
						                	$item_price = $total;
						                }

						                if($qtyFlag){
						                	$item_price = $item_price / $invoice->qty;
						                }

						                $insDis=$invoice->ins_discount;

						                ?>
					                <tr style="border-left: 1px solid;">

					                  <td width="2%" align="center" valign="middle"><?php echo $i; ?></td>

					                  <td width="40%"><?php echo $invoice->item_name;?></td>

					                  
					                  <td width="10%" align="center"><?php echo $item_price;?> KD</td>

					                  <?php if($discountFlag){?>
					                  <td align="center" width="10%"><?php echo  $discount_item;?> <?php echo ($discount_type !== "0") ? "KD" : "%";?></td>
					                  <?}?>

					                  <?php if($qtyFlag){?>
					                  <td align="center" width="10%"><?php echo $invoice->qty;?></td>
					                  <?}?>

					                  <?php if($insFlag == true AND $insDis > 0){ $item_ins = $invoice->total-$invoice->total*($insDis/100); ?>

					                    <td width="8%" align="center" ><?php echo $invoice->total - $item_ins;?></td>

					                    <td width="10%"> 
					                      &nbsp;<?php echo $item_ins." KD"; $total_ins +=  $item_ins; ?>
					                    </td>

					                  <?php $final_total += $item_ins;
					                  
					                  }else{?>

					                    <td width="8%" align="center" >0</td>

					                    <td width="10%"> 
					                    &nbsp;<?php echo ($invoice->total) ? $invoice->total : 0;?> KD <?php $total_ins +=  0; ?>

					                    </td>

					                    <?php $total_ins +=  0; $final_total += $invoice->total; $customer_pay += $invoice->total;

					                  }?>

					                </tr>

					                <?php $i++; endforeach;endif;?>
							      

							      <tr style="border:0px;">
							      	<?php 

								        $cols=6;

								        if($discountFlag==false) $cols=$cols-1; 

								        if($qtyFlag==false)      $cols=$cols-1;

								    ?>

					                <th style="border: 0px;border-top: 1px solid #000;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Total:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $final_total;?> KD</td>

					              </tr>

					               <tr style="border:0px;">

					                <th style="border: 0px; border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Insurance Pays:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $total_ins;?> KD
					                </td>

					              </tr>              

					              
					              <tr style="border:0px;border-bottom:0px">

					                <th style="border: 0px;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right">Customer pays:</th>

					                <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $customer_pay;?> KD</td>

					              </tr>

					              <?php if(isset($patient_pay->knet) AND (float)$patient_pay->knet > 0){?>

					                <tr>
					                  <th style="border: 0px;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right" style="font-size:16px;">Knet:</th>

					                  <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $patient_pay->knet;?> KD</td>
					                
					                </tr>

					              <?php }?>
					              
					              <?php  if(isset($patient_pay->cash) AND (float)$patient_pay->cash > 0){?>
					                <tr>
					                  <th style="border: 0px;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right" style="font-size:16px;">Cash:</th>

					                  <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $patient_pay->cash;?> KD</td>
					                
					                </tr>
					              <?}?>

					              	<tr>

					                  <th style="border: 0px;border-right: 1px solid grey;text-align:right;padding-right:10px;" colspan="<?=$cols;?>" align="right" style="font-size:16px;">Debt:</th>

					                  <td style="border: 1px solid #000;border-right: 1px solid grey;"><?php echo $patient_pay->debt;?> KD</td>
					                
					                </tr>

					            </tbody></table>

					            </td>

					          </tr>

					          <tr><td height="10px">&nbsp;</td></tr>

							  


									  
					                  <tr><td>&nbsp;</td></tr>

					                  <tr>

					                    <td>

					                     <table width="92%" border="0" cellspacing="2" cellpadding="2">

					                        <tbody><tr>

					                            <th width="20%" align="left"><?php echo ucfirst($copy);?> Copy&nbsp;</th>

					                            <th width="20%">&nbsp;Insurance:</th>

					                            <td width="20%"><?php echo ($patient_insurance) ? $patient_insurance->name : "";?></td>

					                            <th width="20%">&nbsp;Policy No.</th>

					                            <td width="20%" style="font-size:12px !important"><?php echo $patient->insurance_number;?></td>

					                        </tr>

					                    </tbody></table>

					                   </td>

					                  </tr>

					                  <tr><td>&nbsp;</td></tr>
							</tbody></table>
						<?}?>

                    </div>
                </div>
             </div>
        </div>
    </section>
</div>