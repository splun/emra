<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper" id="custom-form-page">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->dob){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";} 
                        	echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        <h3>
                        <?php if($date_appointment !== 0){?>
                        	<?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?> 
                        <?}else{?>
                        	<?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?> 
                        <?}?>
                        <?php if(isset($clone_flag)){?>

	                        <a style="margin-left:15px;" target="_blank" href="?dublicate=1" class="btn btn-warning btn-flat btn-copy"><i class="fa fa-copy"></i> <?php echo lang('prescription_dublicate');?></a>
	                    <?}?>
	                    <?php if(isset($print_page)){?>
	                        <a style="margin-left:15px;" target="_blank" href="/admin/patientsections/<?php echo $print_page;?>/<?php echo $patient->id;?>/<?php echo $date_appointment;?>" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a>
                        <?}?>
                    	</h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>
                          
                          <table class="table list-table" align="center">
                          	
                          	<h3>Select <?php echo strtolower(strip_tags($pagetitle));?></h3>
                    		<thead>
	                    		<tr>

	                    		<?php if(isset($selects) && !empty($selects)): foreach($selects as $select): ?>
		                            <th><?php echo $select['label']; ?></th>
	                            <?php endforeach; endif;?>

	                            <?php if(isset($dose) && !empty($dose)):?>
		                            <th><?php echo $dose['label']; ?></th>
	                            <?php endif;?>

	                            <?php if(isset($custom_selects) && !empty($custom_selects)): foreach($custom_selects as $custom): ?>
		                            <th><?php echo $custom['label']; ?></th>
	                            <?php endforeach; endif;?>

		                    	<?php if(isset($length) && !empty($length)):?>
		                            <th><?php echo $length['label']; ?></th>
	                            <?php endif;?>

	                            <?php if(isset($checkboxes) && !empty($checkboxes)): foreach($checkboxes as $checkbox): ?>
		                        	<th><?php echo $checkbox['label']; ?></th>
		                    	<?php endforeach; endif;?>

		                    	<?php if(isset($chk_descriptions) && !empty($chk_descriptions)):  ?>
		                        	<th><?php echo $chk_descriptions['label']; ?></th>
		                    	<?php endif;?>

	                            </tr> 
                        	</thead>
                        	<tbody>
                            <?php if(!empty($active_rows)): $i=0;  
                            	foreach($active_rows as $row): $active = array_values($row); ?>
                            	<tr>
	                            	<?php if(isset($selects) && !empty($selects)): 
	                            	foreach($selects as $select): ?>
		                            <td> 	
		                            	<select <?php echo (isset($select['type'])) ? $select['type'] : ""?> <?php echo (isset($select['class'])) ? 'class="'.$select['class'].'"' : "";?> name="row[<?=$i+1;?>][<?php echo $select['name'];?>]<?php echo (isset($select['type']) && $select['type'] == 'multiple') ? '[]' : '';?>" <?php echo (isset($select['onchange'])) ? 'onchange="'.$select['onchange'].'"' : "";?> >

	                                        <option value="0">--- Select ---</option>
	                                    	<?php if(is_array($select["options"])) { 
	                                    		foreach($select["options"] as $option){ ?>
		                            				<option <?php echo ($option->id == $active[$select["active-db-index"]]) ? "selected=selected" : "";?>
		                            				<?php echo (isset($select["type"]) && $select["type"] == "multiple" && array_search($option->id, explode(",",$active[$select["active-db-index"]])) !== FALSE) ? "selected=selected" : "";?> value="<?php echo $option->id; ?>">
		                            					<?php echo $option->name; ?>
		                            				</option>
		                            			<?} 
		                            		}else{
		                            			
		                            			if(!empty($select["active_options"])){
		                            				foreach($select["active_options"][$i] as $active_option){?>
				                            			<option <?php echo ($active_option->id == $active[$select["active-db-index"]]) ? "selected=selected" : "";?> value="<?php echo $active_option->id ?>">
				                            				<?php echo $active_option->name;?>
				                            			</option>
				                            		<?}?>
		                            			<?}?>
		                            		<?}?>
		                            	</select>
		                            </td>
		                        <?php endforeach; endif?>

		                        <?php if(isset($dose) && !empty($dose)){?>
	                        	<td>
	                        		<input class="<?php echo $dose['class'];?>" type="<?php echo $dose['type'];?>" name="row[<?=$i+1;?>][<?php echo $dose['name'];?>]" <?php echo ($dose['step']) ? "step=".$dose['step']."" : "";?> value="<?php echo $active[$dose['active-db-index']]?>" />
	                        	</td>
	                        	<?}?>


		                        <?php if(isset($custom_selects) && !empty($custom_selects)): 
	                            	foreach($custom_selects as $custom_select): ?>
		                            <td> 	
		                            	<select <?php echo (isset($custom_select['type'])) ? $custom_select['type'] : ""?> <?php echo (isset($custom_select['class'])) ? 'class="'.$custom_select['class'].'"' : "";?> name="row[<?=$i+1;?>][<?php echo $custom_select['name'];?>]<?php echo (isset($custom_select['type']) && $custom_select['type'] == 'multiple') ? '[]' : '';?>">

	                                        <option value="0">--- Select ---</option>
	                                    	<?php if(is_array($custom_select["options"])) { 
	                                    		foreach($custom_select["options"] as $custom_option){ ?>
		                            				<option <?php echo ($custom_option->id == $active[$custom_select["active-db-index"]]) ? "selected=selected" : "";?>
		                            				<?php echo (isset($custom_select["type"]) && $custom_select["type"] == "multiple" && array_search($custom_option->id, explode(",",$active[$custom_select["active-db-index"]])) !== FALSE) ? "selected=selected" : "";?> value="<?php echo $custom_option->id; ?>">
		                            					<?php echo $custom_option->name; ?>
		                            				</option>
		                            			<?} 
		                            		}else{
		                            			if(!empty($custom_select["active_options"])){
		                            				foreach($custom_select["active_options"][$i] as $active_custom_option){?>
				                            			<option <?php echo ($active_custom_option->id == $active[$custom_select["active-db-index"]]) ? "selected=selected" : "";?> value="<?php echo $active_custom_option->id ?>">
				                            				<?php echo $active_custom_option->name;?>
				                            			</option>
				                            		<?}?>
		                            			<?}?>
		                            		<?}?>
		                            	</select>
		                            </td>
		                        <?php endforeach; endif?>

		                        <?php if(isset($length) && !empty($length)){?>
	                        	<td>
	                        		<input class="<?php echo $length['class'];?>" type="<?php echo $length['type'];?>" name="row[<?=$i+1;?>][<?php echo $length['name'];?>]" <?php echo ($length['step']) ? "step=".$length['step']."" : "";?> value="<?php echo $active[$length['active-db-index']]?>" />
	                        	</td>
	                        	<?}?>
	                        	
		                        <?php if(isset($checkboxes) && !empty($checkboxes)): foreach($checkboxes as $checkbox): 
		                        	$checked = ($active[$checkbox["active-db-index"]] == 1) ? "checked=checked" : "";?>
		                        	<td width="10%">
		                        		<div class="checkbox">
			                        		<label class="switch">
		                                        <input <?php echo $checked;?> type="checkbox" name="row[<?=$i+1;?>][<?php echo $checkbox['name'];?>]" value="1" />
		                                        <span class="slider"></span>
		                                    </label>
	                                	</div>
		                        	</td>
		                    	<?php endforeach; endif;?>

		                    	<?php if(isset($chk_descriptions) && !empty($chk_descriptions)): ?>
		                        	<td width="20%" class="form-group">
		                                <textarea class="form-control" name="row[<?=$i+1;?>][<?php echo $chk_descriptions['name'];?>]"><?php echo $active[$chk_descriptions['active-db-index']]?></textarea>
		                        	</td>
	                    		<?php endif;?>
	                            </tr>
                            <?php $i++; endforeach; endif;?>
                            <tr class="dynamic">
                            	<?php if(isset($selects) && !empty($selects)):foreach($selects as $select): ?>
	                            <td> 	
	                            	<select <?php echo (isset($select['type'])) ? $select['type'] : "";?> <?php echo (isset($select['class'])) ? 'class="'.$select['class'].'"' : "";?> name="row[0][<?php echo $select['name'];?>]<?php echo (isset($select['type']) && $select['type'] == 'multiple') ? '[]' : '';?>" <?php echo (isset($select['onchange'])) ? 'onchange="'.$select['onchange'].'"' : "";?> >

                                        <option value="0">--- Select ---</option>
                                    	<?php if(is_array($select["options"])) {
                                    		foreach($select["options"] as $option){?>
	                            				<option value="<?php echo $option->id; ?>">
	                            					<?php echo $option->name; ?>
	                            				</option>
	                            			<?} 
	                            		}?>
	                            	</select>
	                            </td>
	                        	<?php endforeach; endif?>
	                        	<?php if(isset($dose) && !empty($dose)){?>
	                        	<td>
	                        		<input class="<?php echo $dose['class'];?>" type="<?php echo $dose['type'];?>" name="row[0][<?php echo $dose['name'];?>]" <?php echo ($dose['step']) ? "step=".$dose['step']."" : "";?> value="0" />
	                        	</td>
	                        	<?}?>
	                        	<?php if(isset($custom_selects) && !empty($custom_selects)):foreach($custom_selects as $custom_select): ?>
	                            <td> 	
	                            	<select <?php echo (isset($custom_select['type'])) ? $custom_select['type'] : "";?> <?php echo (isset($custom_select['class'])) ? 'class="'.$custom_select['class'].'"' : "";?> name="row[0][<?php echo $custom_select['name'];?>]<?php echo (isset($custom_select['type']) && $custom_select['type'] == 'multiple') ? '[]' : '';?>">

                                        <option value="0">--- Select ---</option>
                                    	<?php if(is_array($custom_select["options"])) {
                                    		foreach($custom_select["options"] as $custom_option){?>
	                            				<option value="<?php echo $custom_option->id; ?>">
	                            					<?php echo $custom_option->name; ?>
	                            				</option>
	                            			<?} 
	                            		}?>
	                            	</select>
		                        </td>
		                        <?php endforeach; endif?>

		                        <?php if(isset($length) && !empty($length)){?>
	                        	<td>
	                        		<input class="<?php echo $length['class'];?>" type="<?php echo $length['type'];?>" name="row[0][<?php echo $length['name'];?>]" <?php echo ($length['step']) ? "step=".$length['step']."" : "";?> value="0" />
	                        	</td>
	                        	<?}?>

	                        	<?php if(isset($checkboxes) && !empty($checkboxes)): foreach($checkboxes as $checkbox): ?>
	                        	<td width="10%">
	                        		<div class="checkbox">
		                        		<label class="switch">
	                                        <input type="checkbox" name="row[0][<?php echo $checkbox['name'];?>]" value="1" />
	                                        <span class="slider"></span>
	                                    </label>
                                	</div>
	                        	</td>
	                    		<?php endforeach; endif;?>

	                    		<?php if(isset($chk_descriptions) && !empty($chk_descriptions)): ?>
	                        	<td width="20%" class="form-group">
	                                <textarea style="" class="form-control" name="row[0][<?php echo $chk_descriptions['name'];?>]" ></textarea>
	                        	</td>
	                    		<?php endif;?>
                            </tr>
                            </tbody>
	                    </table> 

		                    <a id="add-block" class="btn btn-block btn-primary btn-flat"><i class="fa fa-plus"></i></a>
	                    

	                    <?php if(isset($file)) {?>
	                    	<h3><?php echo lang('actions_upload'); ?></h3>	
                          	<div class="form-group">
	                            <?php echo form_input($file);?>
	                      	</div>
	                      	
	                        <?php if($active_files): ?>

	                        	<?php foreach($active_files as $files):?>
	                            <div class="file-view">
	                                <a href="/upload/patients/<?php echo $files->fname;?>" target="_blank"> <i class="fa fa-search"></i><?php echo lang("view_file_label");?> </a> - <input type="checkbox" class="patient-file-delete" id="dfile" style="vertical-align:middle;" name="delete_file" value="<?php echo $files->fid;?>" /><label for="dfile" class="delete-label"><i class="fa fa-close"></i> <?php echo lang("actions_delete");?></label>
	                            </div>
	                        	<?php endforeach; ?>
	                        	</table>
	                    	<?php endif;?>

	                    <?}?>

                        <?php if(isset($desc)): ?>
	                        <h3>Description</h3>
	                        <table class="table">
	                        	<tr class="text-center">
	                        		<td>
	                        			 <?php echo form_textarea($desc);?>
	                        		</td>
	                        	</tr>
	                        </table> 
                        <?php endif;?> 
						
						<?php /* if(isset($doctors)){?>
							<h3>Doctor</h3>
							<table class="table">
							<tr>	
								<td>
								<select style="max-width:320px;" class="form-control" name="doctor">
										<?php foreach($doctors as $doc){?>
											<option <?php if($doctor && $doctor->id == $doc->id){ echo "selected='selected'";}?> value="<?php echo $doc->id;?>"><?php echo "Dr. ".$doc->first_name." ".$doc->last_name;?></option>
										<?php }?>
									</select>
								</td>

							</tr>
							</table> 
						<?php } */?>

                        <div class="form-group col-sm-12 text-center">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <input type="hidden" name="dublicate" value="<?php echo (isset($_GET["dublicate"])) ? $_GET["dublicate"] : 0; ?>" />
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
	$("document").ready(function(){

		$(".patient-file-delete").click(function(){
	        $("#form-edit button[type=submit]").trigger("click");
	    });

		$("#add-block").click(function(){
			var row = $("tr.dynamic").html();
			count_rows = $(".list-table tbody tr").length;
	    	row = row.replace(/row\[0]/g,"row["+count_rows+"]");
			$("tr.dynamic").parent().append("<tr>"+row+"</tr>");
		});
	});

    function itemBycategories(el){

	    var cat = $(el).val();
	    var select = $(el).parent().parent().find(".category-item");
	    if(cat == 0){
	    	$(select).val(0);
	    }
        $.ajax({
          type: "POST",
          url: '/admin/patientsections/items_by_cat',
          data: 'category_id='+cat,
          dataType:"json",
          success: function(data){
          	var option = "<option value='0'>---Select---</option>";
          	$(data).each(function(i){
          		option += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
          	});
          	$(select).html(option);
          }
        });
	}
</script>