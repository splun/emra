<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                       <?php /*  <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> 
                        <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p> */?>
                        <h3>
                        <a onclick="printContent('printable-area'); //window.print();" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a>
                        </h3> 
                    </div>
                    <div class="box-body">

                        
                        <div id="printable-area">

                            <?php if($categories) : $i=1; foreach($categories as $group) : ?>

                            <div class="group-section" id="<?php echo $i;?>">

                                <div style="page-break-before:always">

                                    <table id="printable-area" width="100%" class="main-info-table" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td>
                                                    <h2 style="text-align:center; margin-bottom: 40px;"><input type="checkbox" checked="checked" value="0" class="check-print-test">&nbsp;Investigations&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">فحوصات</span></h2>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table style="margin-bottom:15px;" class="head-table" width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                            <tr>
                                                                <td width="10%" style="text-align:left ; ">Visit Date:</td>
                                                                <td width="50%" align="left" style="text-align:left ; "><?php echo date("l M d, Y",strtotime($date_appointment));?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left ; ">Name:</td>
                                                                <td align="left" style="text-align:left ; "><?php echo $patient->patient_name;?></td>
                                                            </tr>

                                                            <tr>
                                                                <td style="text-align:left ; ">Age:</td>
                                                                <td align="left" style="text-align:left ; ">
                                                                    <?php echo $patient_year;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left ; ">File No.:</td>
                                                                <td align="left" style="text-align:left ; "><?php echo $patient->id;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left ; "><?php echo lang('patient_civil_id_label');?>:</td>
                                                                <td><?php echo $patient->civil_id;?></td>
                                                            </tr>
                                                            <?php if($doctor){?>
                                                            <tr>
                                                                <td style="text-align:left ; "><?php echo lang("sickleave_signature");?>:</td>
                                                                <td><?php echo "Dr. ".$doctor->first_name." ".$doctor->last_name;?></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                    <?php if(isset($diagnosis) || $diagnosis_desc) : ?>
                                                        <table style="margin:15px 0;" width="100%" class="items-table table" border="0" cellspacing="0" cellpadding="0">
                                                            <thead>
                                                              <tr>
                                                                <th align="left"><?php echo lang("patientsection")['diagnosis'];?></th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="diagnosis-items-list">
                                                                <td>
                                                                    <?php if($diagnosis) : ?>
                                                                    <?php foreach($diagnosis as $diagnos){?>
                                                                        <?php echo $diagnos->name_diagnos;?>;
                                                                    <?php } ?>
                                                                    <?php endif;?>

                                                                    <?php if($diagnosis_desc) : ?>
                                                                       
                                                                    <small style="padding-top:10px;">
                                                                     <?php echo $diagnosis_desc->desc;?><small>
                                                                    <?php endif;?>

                                                                </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    <?php endif;?>

                                                    <table style="margin:15px 0;" width="100%" class="items-table table" border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                          <tr>
                                                            <th align="left"><?php echo $group["cat_name"];?> </th>
                                                            <?php /*<th align="left">Name</th> */?>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                            $investigations = $method->patientsModel->get_patient_investigations($patient->id,$date_appointment,$group["category_id"]);

                                                            if($investigations) : foreach($investigations as $investigation):?>

                                                            <tr>
                                                                <td align="left"><?php echo $investigation["name_investigations"];?></td>                              
                                                                <?php /* <td align="left"><?php echo $date_appointment;?></td> */?>
                                                            </tr>
                                                            
                                                            <?php endforeach; endif; ?>
                                                     
                                                        </tbody>
                                                    </table>

                                                    <?php if(!empty($investigations_desc)) : ?>

                                                        <table width="100%" class="items-table table" border="1" cellspacing="5" cellpadding="5">
                                                            <thead>
                                                              <tr>
                                                                <th align="left"><?php echo lang("diagnosis_description");?></th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left"> <small><?php echo $investigations_desc;?></small></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    <?php endif; ?>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                            <?php $i++; endforeach; endif;?>

                        </div> 

                        
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>

<style>
    .main-footer{
        display:none;
    }
</style>
<script>
    $(".check-print-test").change(function(){
        if($(this).prop("checked") != true){
            $(this).parents(".group-section").remove();
        }
    });
</script>