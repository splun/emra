<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <?php /*<h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> 
                        <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>*/?>
                        <h3>
                        <a onclick="location.reload();" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a>
                        </h3> 
                    </div>
                    <div class="box-body">
                        <div id="printable-area">
                            <table width="100%" class="main-info-table" align="center" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th valign="top">
                                            <h2 style="text-align:center; margin-bottom: 40px;">Prescription &nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">وصفه طبيه</span></h2>
                                        </th>

                                        <?php /*
                                        <td width="2%" valign="top">&nbsp;</td>
                                        <th width="49%" valign="top"><h2 align="center">Drug Allergy</h2></th>
                                        */?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="100%" valign="top" align="center">
                                            <table width="100%" align="left" border="0" class="head-table" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr style="">
                                                        <td width="13%" style="text-align:left ; ">Visit Date:</td>
                                                        <td width="50%" align="left" style="text-align:left ; "><?php echo date("l M d, Y",strtotime($date_appointment));?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:left ; ">Patient Name:</td>
                                                        <td align="left" style="text-align:left ; "><?php echo $patient->patient_name;?></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align:left ; ">Patient Age:</td>
                                                        <td align="left" style="text-align:left ; ">
                                                            <?php echo $patient_year;?></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align:left ; ">File No.:</td>
                                                        <td align="left" style="text-align:left ; "><?php echo $patient->id;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="odd"><?php echo lang('patient_civil_id_label');?>:</td>
                                                        <td><?php echo $patient->civil_id;?></td>
                                                    </tr>
                                                    <?php if($doctor){?>
                                                    <tr>
                                                        <td class="odd"><?php echo lang("sickleave_signature");?>:</td>
                                                        <td><?php echo "Dr. ".$doctor->first_name." ".$doctor->last_name;?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr><td colspan="2" height="10px"></td></tr>
                                                </tbody>
                                            </table>
                                            <?php if($diagnosis || $diagnosis_desc) : ?>
                                            <table width="100%" class="items-table table" border="1" cellspacing="5" cellpadding="5">
                                                <thead>
                                                  <tr>
                                                    <th align="left"><?php echo lang("patientsection")['diagnosis'];?></th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="diagnosis-items-list">
                                                    <td>
                                                        <?php if($diagnosis) : ?>
                                                        <?php foreach($diagnosis as $diagnos){?>
                                                            <?php echo $diagnos->name_diagnos;?>;
                                                        <?php } ?>
                                                        <?php endif;?>

                                                        <?php if($diagnosis_desc) : ?>
                                                        <small style="padding-top:10px;">
                                                         <?php echo $diagnosis_desc->desc;?><small>
                                                        <?php endif;?>

                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <?php endif;?>
                                            
                                            <table width="100%" class="items-table table" border="1" cellspacing="5" cellpadding="5">
                                                <thead>
                                                  <tr>
                                                    <?php /*<th align="left"><?php echo lang("prescription_records_date");?></th> */?>
                                                    <th align="left">medication name</th>
                                                    <th align="left">dose</th>
                                                    <th align="left">form</th>
                                                    <th align="left">frequency</th>
                                                    <th align="left">route</th>
                                                    <th align="left">use</th>
                                                    <th align="left">length/days</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if($prescriptions) : foreach($prescriptions as $prescription):?>
                                                    <tr>
                                                        <?php /*<td align="left"><?php echo $date_appointment;?></td> */?>
                                                        <td align="left"><?php echo $prescription["med_name"];?></td>
                                                        <td align="left"><?php echo $prescription["dose"];?></td>
                                                        <td align="left"><?php echo $prescription["form_name"];?></td>
                                                        <td align="left"><?php echo $prescription["freq_name"];?></td>
                                                        <td align="left"><?php echo $prescription["route_name"];?></td>
                                                        <td align="left"><?php echo $prescription["use_name"];?></td>
                                                        <td align="left"><?php echo $prescription["length"];?></td>
                                                    </tr>
                                                    <?php endforeach; endif; ?>
                                                </tbody>
                                            </table>

                                            <?php if(!empty($prescription_desc)) : ?>

                                            <table width="100%" class="items-table table" border="1" cellspacing="5" cellpadding="5">
                                                <thead>
                                                  <tr>
                                                    <th align="left"><?php echo lang("diagnosis_description");?></th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td align="left"> <small><?php echo $prescription_desc;?></small></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <?php endif; ?>
                                        </td>
                                        <?php /*<td width="2%" valign="top"></td>
                                        
                                        <td width="49%" valign="top" align="center">
                                            <table width="90%" class="table bordered" border="1" cellspacing="5" cellpadding="5">
                                                <thead>
                                                  <tr>
                                                    <th align="left"><?php echo lang("prescription_records_name");?></th>
                                                    <th align="left"><?php echo lang("prescription_records_date");?></th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if($allergy) : foreach($allergy as $allerg):?>
                                                    <tr>
                                                        <td align="left"><?php echo $allerg["allergy_name"];?></td>                              
                                                        <td align="left"><?php echo $date_appointment;?></td>
                                                    </tr>
                                                    <?php endforeach; endif; ?>                             
                                                </tbody>
                                            </table>                    
                                        </td>   */?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
    $("document").ready(function(){
        window.print();
    });
</script>
<style>
    .main-footer{
        display:none;
    }
</style>
