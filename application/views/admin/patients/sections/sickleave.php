<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name."[".$patient->id."]";?></h3>
                        <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> 
                        <?php if($patient->dob){ echo $patient->dob.";";} 
                            echo " ".$patient_year.";";?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                        <h3>

                            <?php if($date_appointment !== 0){?>
                            <?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?> 
                            <?}else{?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?>
                            <?}?>

                            &nbsp;&nbsp;<a target="_blank" href="?dublicate=1" class="btn btn-warning btn-flat btn-copy"><i class="fa fa-copy"></i> <?php echo lang('prescription_dublicate');?></a>  
                       
                            &nbsp;&nbsp;<a onclick="printContent('printable-area');" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i> <?php echo lang('immunoteraphy_print');?></a>
                        </h3> 
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>
                        <?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>

                        <div class="hidden" id="printable-area">

                            <table width="100%" class="main-info-table" style="margin:0px auto;" align="center" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th valign="top">
                                            <h2>Sick Leave&nbsp;&nbsp;&nbsp;&nbsp;<span style="direction:rtl;">اجازه مرضيه</span></h2>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" class="table head-table" style="margin-bottom:50px;">
                                                <tr>
                                                    <td width="30%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label>Patient <?php echo lang("sickleave_patient_name");?>:</label> </td>

                                                    <td align="left" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><?php echo $patient->patient_name;?></td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label><?php echo lang("patient_age_label");?>: </label></td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"> <?php echo $patient_year;?>(DOB: <?php echo date("M d Y",strtotime($patient->dob));?>)</td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label><?php echo lang("sickleave_civil_id_label");?>:</label> </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><?php echo $patient->civil_id;?></td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label><?php echo lang("sickleave_file_no_label");?>:</label> </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><?php echo $patient->id;?></td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label><?php echo lang("date_visit");?>: </label></td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><?php echo date("M d Y",strtotime($date_appointment));?></td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <label><?php echo lang("patient_diagnosis_tab");?>:</label>
                                                    </td> 

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <?php echo (isset($check_sickl_record) && !empty($check_sickl_record->diagnosis)) ? $check_sickl_record->diagnosis : "";?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <label><?php echo lang("sickleave_leave_days");?>:</label> 
                                                    </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <?php echo (isset($check_sickl_record) && !empty($check_sickl_record->leave_days)) ? $check_sickl_record->leave_days : "";?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <label><?php echo lang("sickleave_destination");?>:</label>
                                                    </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <?php echo (isset($check_sickl_record) && !empty($check_sickl_record->destination)) ? $check_sickl_record->destination : "";?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                        <label><?php echo lang("patient_notes_label");?>:</label>
                                                    </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">  
                                                        <?php echo (isset($check_sickl_record) && !empty($check_sickl_record->extra_notes)) ? $check_sickl_record->extra_notes : "";?> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;"><label><?php echo lang("sickleave_signature");?>: </label>
                                                    </td>

                                                    <td style="font-size:20px;padding-bottom:10px;font-style:italic;font-weight:400;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
                                                    <?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>    

                        <table class="table" id="sikleave-head-table">
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_patient_name");?>:</td>
                                <td><?php echo $patient->patient_name;?></td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("patient_age_label");?>:</td>
                                <td><?php echo $patient_year;?>(DOB: <?php echo date("M d Y",strtotime($patient->dob));?>)</td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_civil_id_label");?>:</td>
                                <td><?php echo $patient->civil_id;?></td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_file_no_label");?>:</td>
                                <td><?php echo $patient->id;?></td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("date_visit");?>:</td>
                                <td><?php echo date("M d Y",strtotime($date_appointment));?></td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("patient_diagnosis_tab");?>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="diagnosis" rows="3" class="no-bordered form-control"><?php echo (isset($check_sickl_record) && !empty($check_sickl_record->diagnosis)) ? $check_sickl_record->diagnosis : "";?></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_leave_days");?>:</td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" class="no-bordered form-control" name="leave_days" value="<?php echo (isset($check_sickl_record) && !empty($check_sickl_record->leave_days)) ? $check_sickl_record->leave_days : "";?>" />
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_destination");?>:</td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" class="no-bordered form-control" name="destination" value="<?php echo (isset($check_sickl_record) && !empty($check_sickl_record->destination)) ? $check_sickl_record->destination : "";?>" />
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_extra_notes");?>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="extra_notes" rows="3" class="no-bordered form-control"><?php echo (isset($check_sickl_record) && !empty($check_sickl_record->extra_notes)) ? $check_sickl_record->extra_notes : "";?></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="odd"><?php echo lang("sickleave_signature");?>:</td>
                                <td><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></td>
                            </tr>
                        </table>


                        <div class="form-group col-sm-12 text-center">
                            <div class="btn-group">
                                <?php echo form_button(array('type' => 'submit', 'name'=>'save', 'value'=>'1', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                <?php echo anchor('admin/patients/card/'.$patient->id."#patientsections-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                            </div>
                        </div>
                        <input type="hidden" name="dublicate" value="<?php echo (isset($_GET["dublicate"])) ? $_GET["dublicate"] : ''; ?>" />
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
    $("document").ready(function(){
        if(window.location.hash == "#print"){
            window.location.hash = "";
            printContent('printable-area');
        }
    });
</script>