<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('pagetitle_add'); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>

                                    <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create')); ?>
                                        <div class="form-group">
                                            <?php echo lang('patient_civil_id_label', 'civil_id', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($civil_id);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_name_label', 'patient_name', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($patient_name);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_name_ar_label', 'patient_name_ar', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($patient_name_ar);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_dob_label', 'dob', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($dob);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_b_weight_label', 'b_weight', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($b_weight);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_gender_label', 'gender', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                               <select name="gender" class="form-control" onchange="itemBycategories(this)">
                                                <?php foreach($gender as $key=>$val){?>
                                                    <option value="<?php echo $key; ?>">
                                                        <?php echo $val; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_doctor_label', 'doctor', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <select name="doctor" data-index="3"  class="form-control">
                                                    <option value="0">--- Select ---</option>
                                                    <?php  foreach($doctors as $doctor){?>
                                                        <option value="<?php echo $doctor->id; ?>" <?php echo (isset($filter['doctor']) && $doctor->id == $filter['doctor']) ? 'selected="selected"' : "" ?>>
                                                            Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?>
                                                        </option>
                                                    <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_governance_label', 'governance', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <select name="governance" id="governance" class="form-control">
                                                <option value="">--- Select ---</option>
                                                <?php for($i=0; $i<count(lang('patient_governance')); $i++){?>
                                                    <option value="<?php echo lang('patient_governance')[$i]; ?>">
                                                    <?php echo lang('patient_governance')[$i]; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_address_label', 'address', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_textarea($address);?>
                                            </div>
                                        </div>
                                        <?php /* <div class="form-group">
                                            <?php echo lang('patient_parents_name_label', 'parents_name', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($parents_name);?>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <?php echo lang('patient_job_label', 'job', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($job);?>
                                            </div>
                                        </div>
                                        */ ?>
                                        <div class="form-group">
                                            <?php echo lang('patient_phone_label', 'job', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($telephone);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_mobile_label', 'job', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($mobile);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_mobile2_label', 'patient_mobile2_label', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($mobile2);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_email_label', 'job', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($email);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_insurance_company_label', 'insurance_company', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10 ">
                                                <select name="insurance_company" class="form-control">
                                                <option value="0">--- Select ---</option>
                                                <?php foreach($insurances as $insurance){?>
                                                    <option value="<?php echo $insurance->id; ?>">
                                                    <?php echo $insurance->name; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_insurance_number_label', 'insurance_number', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($insurance_number);?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <?php echo lang('patient_file01_label', 'file01', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?php echo form_input($file01);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_file02_label', 'file02', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($file02);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_file03_label', 'file03', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_input($file03);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_referral_label', 'referral', array('class' => 'col-sm-2 control-label')); ?>
                                            
                                            <div class="col-md-10">
                                                <?php //echo form_textarea($referral);?>
                                                <select name="referral" id="referral" class="form-control">
                                                <option value="">--- Select ---</option>
                                                <?php for($i=0; $i<count(lang('patient_referral')); $i++){?>
                                                    <option value="<?php echo lang('patient_referral')[$i]; ?>">
                                                    <?php echo lang('patient_referral')[$i]; ?>
                                                    </option>
                                                <?}?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('patient_notes_label', 'info', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?php echo form_textarea($notes);?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="btn-group">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                                    <?php echo anchor('admin/patients', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
<script>
function itemBycategories(el){

    var cat = $(el).val();

    $.ajax({
      type: "POST",
      url: '/admin/stock/instock/medication_by_cat',
      data: 'category_id='+cat,
      success: function(response){
        console.log(response);
        $("#dynamic-list").html(response);
      }

    });

}
</script>