<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box box-primary">
                    <div class="box-body">
                        <?php echo $message;?>
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-sm-6">
                                <?php /* <h3 class="box-title"><?php echo anchor('admin/patients/add/', '<i class="fa fa-plus"></i> '. lang('pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3> */?>
                                <p><?php if($patient->gender){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";}?></p>
                                <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                                    <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                                    <?php if($patient->mobile2){ echo lang('patient_mobile2_label').": ".$patient->mobile2.";";}?>
                                    <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                                </p>
                                <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                                <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>

                                <h3><?php echo anchor('admin/patients', lang('actions_back'), array('class' => 'btn btn-default btn-flat')); ?></h3> 
                                </div>
                                <div class="col-xs-6">
                                    <div class="row">
                                        <form id="appointments-search" method="POST" action="/admin/patients/appointmets_list/">
                                            
                                            <?php /*?>
                                            <div class="form-group col-sm-5">
                                                <label>Date appointment:</label>
                                                <div class="input-group">
                                                  <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                  </div>
                                                 
                                                  <input type="text" class="form-control pull-right" name="date_appointments" id="reservation" <?php echo (isset($date_appointments)) ? "value='".explode('-',$date_appointments)[0]."-".explode('-',$date_appointments)[1]."'" : "";?>>
                                                </div>
                                                <!-- /.input group -->
                                            </div>

                                            <div class="clearfix"></div>
                                            <?php */?>

                                            <div class="form-group col-xs-12">
                                            <label>File No.:</label>
                                                <input class="form-control" type="text" name="patient_id" value="<?php echo(isset($patient)) ? $patient->id : '';?>">
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="form-group col-xs-12">

                                                <label>Doctor/Nurse:</label>
                                                <select name="doctor_id" id="doctor_id" name="type" class="form-control">
                                                    <option value="ALL">--- Select ---</option>
                                                    <?php if($doctors): foreach($doctors as $doctor):?>
                                                    <option value="<?php echo $doctor->id;?>" <?php echo (isset($doctor_id) && $doctor_id == $doctor->id) ? "selected='selected'" : "";?>><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name." ".$doctor->last_name; ?></option>
                                                    <?php endforeach;endif; ?>
                                                </select>
                                            </div>

                                            <div class="form-group col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                            </div> 

                                            <?php /*
                                            <div class="form-group col-sm-5">
                                            <label>Patient Name:</label>
                                                <input class="form-control" type="text" name="patient_name" value="">
                                            </div>

                                            <div class="form-group col-sm-5">
                                            <label>Patient Phone:</label>
                                                <input class="form-control" type="text" name="patient_phone" value="">
                                            </div>

                                            <div class="clearfix"></div>

                                            */?>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top:15px;"></div>
                        <table id="appointments-list" class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                            <thead>

                                <tr>
                                    
                                    <th class="sorting"  aria-sort="descending" aria-controls="list-data">Date</th> 
                                    <th class="sorting" aria-controls="list-data">Time</th>
                                    <th class="sorting" aria-controls="list-data">Doctor/Room</th>
                                    <th class="sorting" aria-controls="list-data">Type</th>
                                    <th class="sorting" aria-controls="list-data">Invoice</th>
                                    <th class="sorting" aria-controls="list-data">Notes</th>
                                    <th class="sorting" aria-controls="list-data">Results</th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($appointments as $appointment): ?>
                                <tr>
                                    <td>
                                        <?php echo date('Y-m-d',strtotime($appointment->start_date));?>
                                    </td>

                                    <td>
                                        <?php echo date('H:i',strtotime($appointment->start_date));?>
                                    </td>

                                    <td><?php echo $appointment->doctor_name." ".$appointment->doctor_lname; ?><br /><?php echo $appointment->room_name;?></td>
                                    <td>
                                        <div class="red">
                                           <span style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_name;?></span>  
                                            <?php /* echo $func->calculate_age($appointment->patient_dob);*/?>
                                        </div>
                                    </td>
                                    <td class="action-links">
                                        <?php if($func->PatientsModel->get_patient_invoice($patient->id, date('Y-m-d',strtotime($appointment->start_date)))){ ?>

                                            <a target="_blank" title="View invoice" href="/admin/patientsections/invoice/<?php echo $patient->id.'/'.date('d-m-Y',strtotime($appointment->start_date));?>/#invoice-block">
                                                    <img src="/assets/images/doctor-icon.png" />
                                            </a>
                                        <?}else{?>
                                            <a target="_blank" class="skip_nurse_rec" title="Add invoice" href=" /admin/patientsections/invoice/<?php echo $patient->id.'/'.strtotime($appointment->start_date);?>/#invoice-block">
                                                <img src="/assets/images/doctor-icon.png" />
                                            </a>
                                        <?php }?>  

                                    </td>
                                    <td><?php echo $appointment->details;?></td>
                                    <td>
                                        <?php echo anchor('admin/patientsections/results/'.$patient->id."/", "Show/Upload"); ?>
                                    </td>
                                    <td class="action-links">
                                        <?php if(!empty($appointment->done_flag_time) && !empty($appointment->out_come_time)) : ?>
                                            ----
                                        <?php else:?>
                                            <a class="delete-action" href="/admin/patients/delete_appointment/<?php echo $appointment->event_id.'/'.$patient->id;?>">Delete</a>
                                            
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $("document").ready(function(){
        $("body,html").on("click",".applyBtn",function(){
            $("#appointments-search").submit();
        });
        $("body,html").on("change","#doctor_id",function(){
            $("#appointments-search").submit();
        });

        $("#appointments-search .input-group-addon").click(function(){
            $("#reservation").trigger("click");
        });
    });

</script>