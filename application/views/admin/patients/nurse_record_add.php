<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">
                    <div class="box-header with-border">
                        <h3><?php echo $patient->patient_name;?></h3>
                        <p><?php if($patient->dob){ echo $gender[$patient->gender].";";}?> <?php if($patient->dob){ echo $patient->dob.";";}?></p>
                        <p><?php if($patient->telephone){ echo lang('patient_phone_label').": ".$patient->telephone.";";}?>
                            <?php if($patient->mobile){ echo lang('patient_mobile_label').": ".$patient->mobile.";";}?>
                            <?php if($patient->address){ echo lang('patient_address_label').": ".$patient->address.";";}?>
                        </p>
                        <p><?php if($patient->civil_id){ echo lang('patient_civil_id_label').": ".$patient->civil_id.";";}?></p>
                        <p><?php if($doctor){ echo "Dr. ".$doctor->first_name." ".$doctor->last_name;}?></p>
                    </div>
                    <div class="box-body">
                        <?php echo $message;?>

                        <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit')); ?>
                            <div class="form-group">
                                <?php echo lang('nurse_records_datev', 'date_visit', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($date_visit);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_weight', 'weight', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($weight);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_height', 'height', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($height);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_heart', 'heart', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($heart);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_blood', 'blood', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($blood);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_respiratory', 'respiratory', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($respiratory);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_temperature', 'temp', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($temp);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_circumference', 'circumference', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($circumference);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_bmi', 'bmi', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($bmi);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('nurse_records_sao', 'sao', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($sao);?>
                                </div>
                            </div>
                             <div class="form-group">
                                <?php echo lang('nurse_records_comments', 'comments', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_textarea($comments);?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <?php echo form_hidden('patient_id', $patient->id);?>
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php if($date_appointment !== 0){?>
                                            <?php echo anchor('admin/patients/card/'.$patient->id."/".$date_appointment."#nurse-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                        <?}else{?>
                                            <?php echo anchor('admin/patients/card/'.$patient->id."#nurse-block", lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                        <?}?>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
function itemBycategories(el){

    var cat = $(el).val();

      $.ajax({
      type: "POST",
      url: '/admin/stock/instock/medication_by_cat',
      data: 'category_id='+cat,
      success: function(response){
        console.log(response);
        $("#dynamic-list").html(response);
      }

  });

}
</script>