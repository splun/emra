<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <?php echo $message;?>
                 <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/patients/add/', '<i class="fa fa-plus"></i> '. lang('pagetitle_add'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <form id="filtered-patients" class="box-header with-border" method="POST" action="/admin/patients/filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_name_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($patient_name);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_civil_id_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($civil_id);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_file_no_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($file_no);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_doctor_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="doctor" data-index="3"  class="form-control">
                                            <option value="0">--- Select ---</option>
                                            <?php  foreach($doctors as $doctor){?>
                                                <option value="<?php echo $doctor->id; ?>" <?php echo (isset($filter['doctor']) && $doctor->id == $filter['doctor']) ? 'selected="selected"' : "" ?>>
                                                    Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?>
                                                </option>
                                            <?}?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_phone_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_input($telephone);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2" style="padding-right:0px"><?php echo lang('patient_insurance_company_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <select name="insurance" data-index="3"  class="form-control">
                                        <option value="0">--- Select ---</option>
                                        <?php foreach($insurances as $insurance){?>
                                            <option value="<?php echo $insurance->id; ?>" <?php echo (isset($filter['medication_id']) && $insurance->id == $filter['insurance']) ? 'selected="selected"' : "" ?>>
                                            <?php echo $insurance->name; ?>
                                            </option>
                                        <?}?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_referral_label');?>:</div>
                                    <div class="col-md-10 form-group">
                                        <?php echo form_textarea($referral);?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2"><?php echo lang('patient_immuno_label');?>:</div>
                                    <div class="col-md-7 form-group checkbox">
                                        <label class="switch">
                                            <input type="checkbox" <?php if(isset($_GET['immuno']) || isset($_POST['immuno'])){echo "checked=checked";} ?> name="immuno" class="immuno_check" value="1" data-index="4" />
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row immuno_hidden_check" <?php if(isset($_GET['immuno']) || isset($_POST['immuno'])){echo "style='display:block'";} ?> >
                                    <div class="col-md-2"><?php echo lang('immuno_sc');?>:</div>
                                    <div class="col-md-7 form-group checkbox">
                                        <label class="switch">
                                            <input type="checkbox"  name="immuno_sc" value="1" data-index="4" />
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row immuno_hidden_check" <?php if(isset($_GET['immuno']) || isset($_POST['immuno'])){echo "style='display:block'";} ?> >
                                    <div class="col-md-2"><?php echo lang('immuno_sl');?>:</div>
                                    <div class="col-md-7 form-group checkbox">
                                        <label class="switch">
                                            <input type="checkbox"  name="immuno_sl" value="1" data-index="4" />
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="filter" value="1">
                    </form>
                    <div class="box-body" id="patients-table-wrap">

                        <table id="patients-list" class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('patient_file_no_label');?></th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('patient_name_label');?></th>
                                    <?php /* <th class="hide_not_immuno_check sorting" <?php if(isset($_GET['immuno']) || isset($_POST['immuno'])){echo "style='display:block'";} ?> aria-sort="descending" aria-controls="list-data"><?php echo lang('patient_immuno_last_visit');?></th> */?>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($items as $item):?>
                                <tr>
                                    <td><?php echo $item->id;?></td>
                                    <td><?php echo htmlspecialchars($item->patient_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <?php /*if(isset($_GET['immuno']) || isset($_POST['immuno'])){ ?>
                                    <td>
                                    <?php echo $func->get_patient_last_immuno_visit($item->id);?>   
                                    </td>
                                    <?} */?>
                                    <td class="action-links"><?php echo anchor('admin/patients/card/'.$item->id, lang('view_patient_card')); ?>
                                        <?php echo anchor('admin/patientsections/results/'.$item->id."/", "Upload Results"); ?>
                                        <a target="_blank" href="/admin/patients/appointmets_list/<?php echo $item->id;?>" >Appointments list</a>
                                        &nbsp;&nbsp;&nbsp;
                                        <?php echo anchor('admin/patients/delete/'.$item->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>
<script>
$("body,html").on("change","#filtered-patients input,#filtered-patients textarea, #filtered-patients select", function(){

    $.ajax({
      type: "POST",
      url: '/admin/patients/filter',
      data: $("#filtered-patients").serialize(),
      dataType: "json",
      beforeSend: function() {
        $("#patients-list tbody").html('<tr class="od"><td valign="top" colspan="11" class="dataTables_empty">Loading...</td></tr>');
      },
      success: function(result){

        var html = '<table class="table table-bordered table-hover" id="patients-list" role="grid" >';
    
        html += '<thead>'+$("#patients-list thead").html()+'</thead>';
        html += '<tbody>';

        if(result){

            $(result).each(function(i){
                html += '<tr class="odd" role="row"><td class="sorting_1">'+result[i].id+'</td><td>'+result[i].patient_name+'</td>';
                html += '<td class="action-links"><a href="/admin/patients/card/'+result[i].id+'">View card</a><a href="admin/patientsections/results/'+result[i].id+'">Upload Results</a><a target="_blank" href="/admin/patients/appointmets_list/'+result[i].id+'" >Appointments list</a>&nbsp;&nbsp;&nbsp;<a href="/admin/patients/delete/'+result[i].id+'" class="delete-action">Delete</a></td></tr>';
                                        
            });

        }

        html += '</tbody>';
        html += '</table>';

        $("#patients-table-wrap").html(html);
        var table = $('#patients-list').DataTable({
            "pageLength": 25
        });
        
      }

    });

});

$("body,html").on("change",".immuno_check",function(){
    if($(this).prop("checked")){
        $(".immuno_hidden_check").show();
    }else{
        $(".immuno_hidden_check input").prop("checked",false);
        $(".immuno_hidden_check").hide();
    }
});

$("document").ready(function(){

    if(window.location.href.indexOf("?") > -1) {
        window.history.pushState('page2', 'Title',location.origin + location.pathname);
    }

});
</script>