<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content" id="dashboard">
        <?php echo $dashboard_alert_file_install; ?>
        <?php echo $message;?>
        <div class="row">
            <div class="col-lg-2 col-md-3">
            	<div class="box collapsed-box">
                	<div class="box-header with-border">
					    <h3 class="box-title"><?php echo lang("mini_calendar_title");?></h3>
					    <div class="box-tools pull-right">
					      <!-- Collapse Button -->
					      <button type="button" class="btn btn-box-tool" data-widget="collapse">
					        <i class="fa fa-minus"></i>
					      </button>
					    </div>
					    <!-- /.box-tools -->
					</div>    
				    <div class="box-body" id="mini-calendar" style="width:100%;">

				    </div>
			  	</div>
                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("notes_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_notes", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($notes_patient_id);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <label>For</label>
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="notes_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12"><?php echo form_textarea($notes_text);?></div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>

                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("tasks_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_task", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_title);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_date);?></div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-6">
                                     <label><input checked name="task_status" type="radio" value="2"></input><?php echo lang("status_started");?></label>
                                </div>
                                <div class="radio col-xs-6">
                                     <label><input name="task_status" type="radio" value="1"></input><?php echo lang("status_not_started");?></label>
                                </div>  
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="task_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-4">
                                     <label><input checked name="task_priority" type="radio" value="1"></input><?php echo lang("task_low_label");?></label>
                                </div>
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="2"></input><?php echo lang("task_med_label");?></label>
                                </div>  
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="3"></input><?php echo lang("task_hight_label");?></label>
                                </div> 
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo form_textarea($task_text);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9" id="today-appoitnments-block">
            	<div class="box">
                	<div class="box-header with-border">
					    <h3 class="box-title"><?php echo lang("today_patients_title");?>&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo lang("appointments_title");?></h3>
                        
                        <ul id="doctors-period-chooser" class="pull-right" style="margin-right:5%;">
                            <li <?php echo (!isset($current_doctor_id) || $current_doctor_id == 0) ? "class='active'" : "";?>>
                            <a href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>"><?php echo lang("app_all_doctors");?></a></li>
                            <?php if($doctors): foreach($doctors as $doctor):?>
                            <li <?php echo (isset($current_doctor_id) && $current_doctor_id == $doctor->id) ? "class='active'" : "";?>> 
                                <a href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>&doctor=<?php echo $doctor->id;?>">Dr. <?php echo $doctor->first_name." ".$doctor->last_name; ?></a></li>
                            <?php endforeach;endif; ?>
                        </ul>

					    <div class="box-tools pull-right">
					      <!-- Collapse Button -->
					      <button type="button" class="btn btn-box-tool" data-widget="collapse">
					        <i class="fa fa-minus"></i>
					      </button>
					    </div>
					    <!-- /.box-tools -->
					</div>    
				    <div class="box-body">
                		<div class="row">
                			<div class="col-xs-12" >
                                <h4><strong><?php echo lang("appointments_total_title");?> <?php echo count($appointments);?></strong></h4>
                                <table id="dashboard-appointments-list" class="table table-bordered table-hover" role="grid">
                                    <thead>
                                        <tr>
                                            <th class="sorting"  aria-sort="descending" aria-controls="list-data">#</th> 
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_type');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_time');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_patient');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('doctor');?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($appointments as $appointment):?>
                                            <tr <?php if($i==1){?>class="activetr"<?}?> data-patient="<?php echo $appointment->patient_id;?>">
                                                <td><?php echo $i;?></td>
                                                <td>
                                                <?php if($appointment->type == 1){?>
                                                    <i class="fa fa-lightbulb-o"></i>&nbsp;
                                                <?php }?>
                                                    <span style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_abbr;?></span>  
                                                </td>
                                                <td>
                                                    <?php echo date('H:i',strtotime($appointment->start_date));?>
                                                    <br />
                                                    <?php echo lang('app_time_in');?>:
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->in_come_time));?></span> - 
                                                    <?php echo lang('app_time_out');?>:
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->out_come_time));?></span>
                                                </td>
                                                <td>
                                                    <?php echo htmlspecialchars($appointment->patient_name, ENT_QUOTES, 'UTF-8'); ?> [<?php echo $appointment->patient_id;?>]
                                                    <br />
                                                    <span class="text-blue"><?php echo $func->calculate_age($appointment->patient_dob);?></span>     
                                                </td>
                                                <td><?php echo $appointment->doctor_name." ".$appointment->doctor_lname; ?>,<?php echo $appointment->room_name;?>
                                                </td>
                                                <td class="action-icons">

                                                    <a title="View patient card" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <img src="/assets/images/edit.png" />
                                                    </a>

                                                    <a title="Results" style="margin-left:5px;margin-right:8px;" target="_blank" href=" /admin/patientsections/results/<?php echo $appointment->patient_id.'/'.$current_date;?>/">
                                                            <i class="fa fa-upload"></i>
                                                    </a>

                                                    <?php if($appointment->in_come_flag == 1){?>
                                                        <a title="View nurse record" style="margin-left:5px;" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#nurse-block">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?}else{?>         
                                                        <a class="skip_nurse_rec" title="Add nurse record" style="margin-left:5px;" target="_blank" href="/admin/patients/nurse_record_add/<?php echo $appointment->patient_id.'/'.$current_date;?>">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?php }?>

                                                    <a title="Prescriptions" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                        <img  src="/assets/images/prescription_drugs.png" />
                                                    </a>
                                                    
                                                    <?php if($func->PatientsModel->get_patient_invoice_by_date($appointment->patient_id,$current_date)){?>
                                                    
                                                    <?php /*<a title="View invoice" target="_blank" href=" /admin/patientsections/invoice/<?php echo $appointment->patient_id.'/'.$current_date;?>/#invoice-block">*/?>
                                                    
                                                    <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.$current_date;?>/#invoice-block">
                                                            <img src="/assets/images/doctor-icon.png" />
                                                    </a>

                                                    <?}?>
                                                    <?php if($func->PatientsModel->get_patient_drug_allergy($appointment->patient_id,30)){?>
                                                       
                                                        <a target="_blank" class="red_flag" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                            <img src="/assets/images/red_flag.png" />
                                                        </a>

                                                    <?}?>

                                                    <?php if($func->PatientsModel->get_immunoteraphy_plan($appointment->patient_id)){?>
                                                        
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#immunoteraphy-block">
                                                            <img src="/assets/images/immuno.png" />
                                                        </a>

                                                    <?}?>

                                                    <span class="visit-actions">
                                                    <?php if($func->appointmentsModel->get_items("nurse",
                                                        array(
                                                        "patient_id"=>$appointment->patient_id,
                                                        "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                        )
                                                    ) && trim($appointment->out_come_time)==''){?>

                                                    <a data-type="outcome" data-eventid="<?php echo $appointment->event_id?>" class="outcome" href="/admin/appointments/out/<?php echo $appointment->event_id?>">
                                                        <img src="/assets/images/out.png" />
                                                    </a>

                                                    <?}?>
                                                    
                                                    <?php if($func->core_model->get_items("nurse",
                                                        array(
                                                        "patient_id"=>$appointment->patient_id,
                                                        "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                        )
                                                    ) && (trim($appointment->out_come_time)<>'')){?>

                                                        <?php if($appointment->done_flag == 1 && $appointment->out_come_flag == 1) {?>
                                                            <span> <i class="fa fa-check"><?php //echo lang("actions_done");?></span>
                                                        
                                                        <?php }else{?>

                                                            <a data-type="done" data-eventid="<?php echo $appointment->event_id?>" class="done" href="/admin/appointments/done/<?php echo $appointment->event_id?>">
                                                            <img src="/assets/images/done.png" />
                                                        </a>

                                                        <?php } ?>
                                                        
                                                    <?}?>
                                                    </span>

                                                    <?php /*if($appointment->done_flag == 1 && $appointment->out_come_flag == 1) {?>
                                                        <i class="fa fa-check">
                                                    <?} */?>  

                                                </td>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                    </tbody>
                                </table>
                			</div>
                		</div>
				    </div>
			  	</div>
            </div>
        </div>
        <hr/>
    </section>
</div>

<link rel="stylesheet" href="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<script src="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
  $(document).ready(function() {
    curDate = 0;
    var eventDate = '<?php echo date("d-m-Y",strtotime($current_date));?>';

    var curDate = window.location.search.replace( '?date_appointments=', ''); 
    $( "#mini-calendar" ).datepicker({
    	format: 'dd-mm-yyyy',
    	todayHighlight:true,
        showOnFocus: true,
        defaultViewDate: eventDate,
        beforeShowDay: function(date) {
            var d = moment(date).format('DD-MM-YYYY');
            if(d == eventDate){
             return {enabled:true,classes: 'active', tooltip: eventDate};
            }

        },
    }).on("changeDate", function(e) {
        var chDate = $('#mini-calendar').datepicker('getFormattedDate');
        document.location.href = "/admin/dashboard/?date_appointments="+chDate+"&doctor=<?php echo $current_doctor_id;?>";
    });

    var table = $("#dashboard-appointments-list").DataTable({
        "pageLength": 25,
        "paging" : false,
        "searching" : false
    });

    $("#dashboard-appointments-list tbody tr").click(function(){
        var patient = $(this).attr("data-patient");
        $(".dash-current-patient").removeClass("active");
        $(".dash-current-patient#event-"+patient+"").addClass("active");
        $("#dashboard-appointments-list tbody > tr").removeClass("activetr");
        $(this).addClass("activetr");
    });

    $(".add-patient-record-form button[type=submit]").on("click",function(){
        var form = $(this).parent().parent();
        var data = form.serialize();

        var url = $(form).attr("action");
        $.ajax({
            url:url,
            type:"POST",
            dataType:"html",
            data: data,
            success: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message).fadeIn();
                if(result.status == "success"){
                    $(form).parent().find("sup").css("display","inline-block");
                    setTimeout(function(){
                        $(".action-message").fadeOut();
                    },1000);
                    $(form).reset();
                }
            },
            error: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message);
            }
        });

        return false;
    });

    $("body,html").on("click",".visit-actions a", function(){
        if (confirm("Are you sure?")) {

            var eventid = $(this).data("eventid");
            var action = $(this).attr("href");
            var type = $(this).data("type");

            var el = $(this);

            $.ajax({
                url:action,
                type:"GET",
                success: function(response) {
                    $(el).parent().find("a").fadeOut();
                    setTimeout(function(){
                        if(type == "outcome"){
                            $(el).parent().html("<a data-type='done' data-eventid='"+eventid+"' class='done' href='/admin/appointments/done/"+eventid+"'><img src='/assets/images/done.png' /></a>");
                        }
                        if(type == "done"){
                            $(el).parent().html("<i class='fa fa-check'></i>");
                        }
                    },300);
                }
            });
            return false;

        } else {
            return false;
        }
    });

  });

</script>