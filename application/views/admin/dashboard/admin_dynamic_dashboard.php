<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content" id="dashboard">
        <?php echo $dashboard_alert_file_install; ?>
        <?php echo $message;?>

        <div class="row">
            <div class="col-xs-12" id="today-appoitnments-block">
            	<div class="box">
                	<div class="box-header with-border">
					    <h3 class="box-title"><?php echo lang("today_patients_title");?>&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo lang("appointments_title");?></h3>
                        
                        <?php if($is_admin) : //Comment this check if view variations on doctor dashboard ?>

                        <ul id="doctors-period-chooser" class="pull-right" style="margin-right:5%;">

                            <?php if(!$is_doctor || $is_admin){?>
                            <li <?php echo (!isset($current_doctor_id) || $current_doctor_id == 0) ? "class='active'" : "";?>>
                            <a href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>"><?php echo lang("app_all_doctors");?></a></li>
                            <?}?>

                            <?php if($doctors): foreach($doctors as $doctor):?>
                            <li <?php echo (isset($current_doctor_id) && $current_doctor_id == $doctor->id) ? "class='active'" : "";?>> 
                                <a <?php if(!$is_doctor || $is_admin){?>href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>&doctor=<?php echo $doctor->id;?>"<?}?>><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name." ".$doctor->last_name; ?></a></li>
                            <?php endforeach;endif; ?>
                        </ul>

                        <?php endif;?>

					    <div class="box-tools pull-right">
					      <!-- Collapse Button -->
					      <button type="button" class="btn btn-box-tool" data-widget="collapse">
					        <i class="fa fa-minus"></i>
					      </button>
					    </div>
					    <!-- /.box-tools -->
					</div>    
				    <div class="box-body">
                		<div class="row">
                			<div class="col-md-6 left-app" >
                                <h4><strong><?php echo lang("appointments_total_title");?> <?php echo count($appointments);?></strong></h4>
                                <table id="dashboard-appointments-list" class="table table-bordered table-hover" role="grid">
                                    <thead>
                                        <tr>
                                            <th class="sorting"  aria-sort="descending" aria-controls="list-data">#</th> 
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_type');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_time');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('information');//lang('app_patient');?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($appointments as $appointment):?>
                                            <tr <?php if($i==1){?>class="activetr"<?}?> data-patient="<?php echo $appointment->patient_id;?>">
                                                <td><?php echo $i;?></td>
                                                <td>
                                                <?php if($appointment->type == 1){?>
                                                    <i class="fa fa-lightbulb-o"></i>&nbsp;
                                                <?php }?>   
                                                <span style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_abbr;?></span>  
                                                </td>
                                                <td><?php echo date('H:i',strtotime($appointment->start_date));?><br />
                                                <span class="small">
                                                <?php echo lang('app_time_in');?>:
                                                <?php if($appointment->in_come_time){?>
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->in_come_time));?></span> <?}else{ echo "--";}?> 
                                                    <?php echo lang('app_time_out');?>:
                                                    <?php if($appointment->out_come_time){?>
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->out_come_time));?><?}else{ echo "--";}?></span>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?php echo htmlspecialchars($appointment->patient_name, ENT_QUOTES, 'UTF-8'); ?> [<?php echo $appointment->patient_id;?>]
                                                    <br />
                                                    <span class="text-blue"><?php echo $func->calculate_age($appointment->patient_dob);?></span> <br />
                                                    <?php echo $appointment->room_name;?>, <?php echo $appointment->doctor_name." ".$appointment->doctor_lname; ?>
                                                </td>
                                                <td class="action-icons">

                                                    <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <img src="/assets/images/edit.png" />
                                                    </a>

                                                    <?php if($func->PatientsModel->get_patient_invoice($appointment->patient_id,$current_date)){?>
                                                    
                                                    <a target="_blank" href=" /admin/patientsections/invoice/<?php echo $appointment->patient_id.'/'.$current_date;?>/#invoice-block">
                                                            <img src="/assets/images/doctor-icon.png" />
                                                    </a>

                                                    <?}?>

                                                    <?php if($appointment->in_come_flag == 1){?>
                                                        <a title="View nurse records" style="margin-left:5px;" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#nurse-block">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?}else{?>
                                                        <a class="skip_nurse_rec" style="margin-left:5px;" title="Skip nurse record" href="/admin/patients/nurse_record_skip/<?php echo $appointment->patient_id.'/'.$current_date;?>">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?}?>
                                                    <?php if($func->PatientsModel->get_patient_drug_allergy($appointment->patient_id,30)){?>
                                                       
                                                        <a target="_blank" class="red_flag" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                            <img src="/assets/images/red_flag.png" />
                                                        </a>

                                                    <?}?>

                                                    <?php if($func->PatientsModel->get_immunoteraphy_plan($appointment->patient_id)){?>
                                                        
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#immunoteraphy-block">
                                                            <img src="/assets/images/immuno.png" />
                                                        </a>

                                                    <?}?>

                                                    <?php if($appointment->done_flag == 1 && $appointment->out_come_flag == 1) {?>
                                                        <i class="fa fa-check"></span>
                                                    <?}?>  

                                                </td>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                    </tbody>
                                </table>
                			</div>
 
                			<div class="col-md-6 right-app">
                				<h4><strong><?php echo lang("current_patients_title");?></strong></h4>
                                <?php if($appointments): $i=1; foreach ($appointments as $appointment):?>
                                <div class="dash-current-patient <?php if($i==1){?>active<?}?>" id="event-<?php echo $appointment->patient_id;?>">
                                    <div class="row current-patient-info">
                                        <div class="col-sm-1" style="text-align:center;">
                                        #<?php echo $appointment->patient_id;?>
                                        </div>
                                        <div class="col-sm-7">
                                        <span class="text-yellow"><?php echo date('H:i',strtotime($appointment->start_date));?></span>&nbsp;&nbsp;
                                        <a style="color:#333;" target="blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/<?php echo $current_date;?>/"><strong><?php echo $appointment->patient_name;?></strong></a>
                                        </div>
                                        <div class="col-sm-4 action-icons visit-actions">
                                            <?php if($func->appointmentsModel->get_items("nurse",
                                                    array(
                                                    "patient_id"=>$appointment->patient_id,
                                                    "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                    )
                                                ) && trim($appointment->out_come_time)==''){?>

                                                <a data-type="outcome" data-eventid="<?php echo $appointment->event_id?>" class="outcome" href="/admin/appointments/out/<?php echo $appointment->event_id?>">
                                                    <img src="/assets/images/out.png" />
                                                </a>

                                            <?}?>
                      
                                            <?php if($func->core_model->get_items("nurse",
                                                array(
                                                "patient_id"=>$appointment->patient_id,
                                                "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                )
                                            ) && (trim($appointment->out_come_time)<>'')){?>

                                                <?php if($appointment->done_flag == 1 && $appointment->out_come_flag == 1) {?>
                                                    <span><?php echo lang("actions_done");?></span>
                                                
                                                <?php }else{?>

                                                    <a data-type="done" data-eventid="<?php echo $appointment->event_id?>" class="done" href="/admin/appointments/done/<?php echo $appointment->event_id?>">
                                                    <img src="/assets/images/done.png" />
                                                </a>

                                                <?php } ?>
                                                
                                            <?}?>   
                                        </div>
                                    </div>
                                    <div class="row current-appointment-info">
                                        <div class="col-sm-1"></div>
                                        <div class="col-lg-5 col-sm-6">
                                        <span>
                                        <strong style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_name;?></strong>  
                                        &nbsp;&nbsp;&nbsp;
                                        <span class="text-blue"><?php echo $func->calculate_age($appointment->patient_dob);?></span>
                                        <br />
                                         
                                        <?php echo lang('app_time_in');?>:
                                            <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->in_come_time));?></span> - 
                                            <?php echo lang('app_time_out');?>:
                                            <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->out_come_time));?></span>
                                        </div>
                                        <div class="col-lg-5 col-sm-5">
                                            <strong style="color:red"><?php echo ($appointment->insurance_name) ? $appointment->insurance_name.";&nbsp;" : "";?></strong>
                                            <?php echo ($appointment->patient_phone) ? "T:".$appointment->patient_phone.";&nbsp;" : "";?>
                                            <?php echo ($appointment->patient_mobile) ? "M:".$appointment->patient_mobile.";&nbsp;" : "";?>
                                            <?php echo ($appointment->patient_mobile2) ? $appointment->patient_mobile2.";&nbsp;" : "";?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="patient-records-tabs">
                                        <div class="row">
                                            <div class="nav-tabs-custom col-md-9">
                                                <ul class="nav nav-tabs col-xs-2 col-lg-1">
                                                   <li id="comp-link<?php echo $i;?>" class="active"><a href="#comp-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">COMP</a></li>
                                                   <li id="diag-link<?php echo $i;?>"> <a href="#diag-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">DIAG</a></li>
                                                   <li id="envr-link<?php echo $i;?>"> <a href="#envr-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">ENVR</a></li> 
                                                   <li id="famhistory-link<?php echo $i;?>"> <a href="#famhistory-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">FH</a></li> 
                                                   <li id="birth-link<?php echo $i;?>"> <a href="#birth-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">PMH&V</a></li> 
                                                   <li id="dev-link<?php echo $i;?>"> <a href="#dev-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">DEV</a></li>
                                                   <li id="imty-link<?php echo $i;?>"> <a href="#imty-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">IMTY</a></li>
                                                   <li id="phys-link<?php echo $i;?>"> <a href="#phys-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">PHYS</a></li>
                                                   
                                                   <?php /*
                                                   <li id="sickl-link<?php echo $i;?>"> <a href="#sickl-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">SICK L</a></li>
                                                   <li id="refer-link<?php echo $i;?>"> <a href="#refer-block<?php echo $i;?>" data-toggle="tab" aria-expanded="true">REFER</a></li>
                                                   
                                                   <li id="addrecords-link<?php echo $i;?>"><a class="text-blue more-actions" data-toggle="tab" aria-expanded="true" href="#addrecords-block<?php echo $i;?>"><i class="fa fa-plus"></i></a></li>*/?>

                                                   <li> <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#immunoteraphy">IMYN</a></li>

                                                   <li> <a target="_blank" href="/admin/patientsections/growth_chart/<?php echo $appointment->patient_id;?>/<?php echo $current_date;?>/">CHART</a></li>
                                                   
                                                </ul>
                                                <div class="tab-content col-xs-10 col-lg-11">
                                                    <div class="tab-pane active" id="comp-block<?php echo $i;?>">
                                                        <?php $check_compl_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>1,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["complaints"]?></strong>
                                                            <sup <?php if(isset($check_compl_record) && !empty($check_compl_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_compl_record) > 0){
                                                                $chk_start_compl = count($check_compl_record) + 1;
                                                            }else{
                                                                $chk_start_compl = 1;
                                                            }
                                                            if($check_compl_record):

                                                                $y = 1; foreach($check_compl_record as $compl_record):?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $copmpl = $func->settings->get_patient_file(1); ?>
                                                                        <option class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($copmpl): foreach($copmpl as $cmp):?>
                                                                                <option class="form-control" <?php echo ($compl_record->patient_file_id == $cmp->id) ? "selected=selected" : "";?> value="<?php echo $cmp->id?>">
                                                                                    <?php echo $cmp->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <textarea name="description[<?php echo $y;?>]" rows="2" class="form-control" placeholder="<?php echo lang("desc_description_field");?>">
                                                                        <?php echo $compl_record->patient_file_description;?>  
                                                                    </textarea>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_compl;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $copmpl = $func->settings->get_patient_file(1); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($copmpl): foreach($copmpl as $cmp):?>
                                                                                <option class="form-control" value="<?php echo $cmp->id?>">
                                                                                    <?php echo $cmp->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <textarea name="description[<?php echo $y;?>]" rows="2" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"></textarea>
                                                                </div>
                                                            <?}?>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="1">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="diag-block<?php echo $i;?>">
                                                        <?php $check_diag_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>2,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["diagnosis"]?></strong>
                                                            <sup <?php if(isset($check_diag_record) && !empty($check_diag_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_diag_record) > 0){
                                                                $chk_start_diag = count($check_diag_record) + 1;
                                                            }else{
                                                                $chk_start_diag = 1;
                                                            }

                                                            if($check_diag_record):
                                                                $y = 1; foreach($check_diag_record as $diag_record):?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $diagnosis = $func->settings->get_patient_file(2); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($diagnosis): foreach($diagnosis as $diag):?>
                                                                                <option class="form-control" value="<?php echo $diag->id?>" <?php echo ($diag_record->patient_file_id == $diag->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $diag->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_diag;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $diagnosis = $func->settings->get_patient_file(2); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($diagnosis): foreach($diagnosis as $diag):?>
                                                                                <option class="form-control" value="<?php echo $diag->id?>">
                                                                                    <?php echo $diag->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,2,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="2">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="famhistory-block<?php echo $i;?>">
                                                        <?php $check_fh_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>4,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["family_social_history"]?></strong>
                                                            <sup <?php if(isset($check_fh_record) && !empty($check_fh_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_fh_record) > 0){
                                                                $chk_start_fh = count($check_fh_record) + 1;
                                                            }else{
                                                                $chk_start_fh = 1;
                                                            }

                                                            if($check_fh_record):
                                                                $y = 1; foreach($check_fh_record as $fh_record):?>
                                                                
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $family_social_history = $func->settings->get_patient_file(4); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($family_social_history): foreach($family_social_history as $fsh):?>
                                                                                <option class="form-control" value="<?php echo $fsh->id?>" <?php echo ($fh_record->patient_file_id == $fsh->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $fsh->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>

                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_fh;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $family_social_history = $func->settings->get_patient_file(4); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($family_social_history): foreach($family_social_history as $fsh):?>
                                                                                <option class="form-control" value="<?php echo $fsh->id?>">
                                                                                    <?php echo $fsh->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,4,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="4">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="envr-block<?php echo $i;?>">
                                                        <?php $check_envr_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>3,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["environment"]?></strong>
                                                            <sup <?php if(isset($check_envr_record) && !empty($check_envr_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_envr_record) > 0){
                                                                $chk_start_envr = count($check_envr_record) + 1;
                                                            }else{
                                                                $chk_start_envr = 1;
                                                            }

                                                            if($check_envr_record):
                                                                $y = 1; foreach($check_envr_record as $envr_record):?>
                                                                
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $envr = $func->settings->get_patient_file(3); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($envr): foreach($envr as $env):?>
                                                                                <option class="form-control" value="<?php echo $env->id?>" <?php echo ($envr_record->patient_file_id == $env->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $env->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_envr;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $envr = $func->settings->get_patient_file(3); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($envr): foreach($envr as $env):?>
                                                                                <option class="form-control" value="<?php echo $env->id?>">
                                                                                    <?php echo $env->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,3,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>

                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="3">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="birth-block<?php echo $i;?>">
                                                        <?php $check_birth_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>11,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["birth_vaccination"]?></strong>
                                                            <sup <?php if(isset($check_birth_record) && !empty($check_birth_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_birth_record) > 0){
                                                                $chk_start_birth = count($check_birth_record) + 1;
                                                            }else{
                                                                $chk_start_birth = 1;
                                                            }

                                                            if($check_birth_record):
                                                                $y = 1; foreach($check_birth_record as $birth_record):?>
                                                                
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $birth = $func->settings->get_patient_file(11); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($birth): foreach($birth as $bth):?>
                                                                                <option class="form-control" value="<?php echo $bth->id?>" <?php echo ($birth_record->patient_file_id == $bth->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $bth->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_birth;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $birth = $func->settings->get_patient_file(11); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($birth): foreach($birth as $bth):?>
                                                                                <option class="form-control" value="<?php echo $bth->id?>">
                                                                                    <?php echo $bth->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                                
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,11,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>

                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="11">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="dev-block<?php echo $i;?>">
                                                        <?php $check_dev_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>10,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["development"]?></strong>
                                                            <sup <?php if(isset($check_dev_record) && !empty($check_dev_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_dev_record) > 0){
                                                                $chk_start_dev = count($check_dev_record) + 1;
                                                            }else{
                                                                $chk_start_dev = 1;
                                                            }

                                                            if($check_dev_record):
                                                                $y = 1; foreach($check_dev_record as $dev_record):?>
                                                                
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $dev = $func->settings->get_patient_file(10); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($dev): foreach($dev as $d):?>
                                                                                <option class="form-control" value="<?php echo $d->id?>" <?php echo ($dev_record->patient_file_id == $d->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $d->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_dev;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $dev = $func->settings->get_patient_file(10); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($dev): foreach($dev as $d):?>
                                                                                <option class="form-control" value="<?php echo $d->id?>">
                                                                                    <?php echo $d->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,10,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>

                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="10">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="imty-block<?php echo $i;?>">
                                                        <?php $check_imty_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>9,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["immunity"]?></strong>
                                                            <sup <?php if(isset($check_imty_record) && !empty($check_imty_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                            <?php 
                                                            if(count($check_imty_record) > 0){
                                                                $chk_start_imty = count($check_imty_record) + 1;
                                                            }else{
                                                                $chk_start_imty = 1;
                                                            }

                                                            if($check_imty_record):
                                                                $y = 1; foreach($check_imty_record as $imty_record):?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $imty = $func->settings->get_patient_file(9); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($imty): foreach($imty as $imt):?>
                                                                                <option class="form-control" value="<?php echo $imt->id?>" <?php echo ($imty_record->patient_file_id == $imt->id) ? "selected=selected" : "";?>>
                                                                                    <?php echo $imt->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_imty;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $imty = $func->settings->get_patient_file(9); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($imty): foreach($imty as $imt):?>
                                                                                <option class="form-control" value="<?php echo $imt->id?>">
                                                                                    <?php echo $imt->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                            <?}?>

                                                            <?php $share_field_section = $func->PatientsModel->get_patient_descriptions($appointment->patient_id,9,date('Y-m-d',strtotime($appointment->start_date)));?>
                                                            <div class="form-group">
                                                                <textarea name="desc" rows="6" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"><?php echo($share_field_section && !empty($share_field_section->desc)) ? $share_field_section->desc : "";?></textarea>
                                                            </div>
                                                            <?php if($share_field_section && !empty($share_field_section->desc)){?>
                                                            <input type="hidden" name="share_field_section" value="<?php echo $share_field_section->id;?>">
                                                            <?}?>

                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="9">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="phys-block<?php echo $i;?>">
                                                        <?php $check_phys_record = $func->core_model->get_items("patient_checkboxpage",array("section"=>13,"patient_id"=>$appointment->patient_id,"date_add"=>$current_date),"id","ASC");?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["physical"]?></strong>
                                                            <sup <?php if(isset($check_phys_record) && !empty($check_phys_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_checkboxpage_records"  class="add-patient-record-form" multiple="multiple">
                                                                                                                        <?php 
                                                            if(count($check_phys_record) > 0){
                                                                $chk_start_phys = count($check_phys_record) + 1;
                                                            }else{
                                                                $chk_start_phys = 1;
                                                            }
                                                            if($check_phys_record):

                                                                $y = 1; foreach($check_phys_record as $phys_record):?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $phys = $func->settings->get_patient_file(13); ?>
                                                                        <option class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($phys): foreach($phys as $phs):?>
                                                                                <option class="form-control" <?php echo ($phys_record->patient_file_id == $phs->id) ? "selected=selected" : "";?> value="<?php echo $phs->id?>">
                                                                                    <?php echo $phs->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <textarea name="description[<?php echo $y;?>]" rows="2" class="form-control" placeholder="<?php echo lang("desc_description_field");?>">
                                                                        <?php echo $phys_record->patient_file_description;?>  
                                                                    </textarea>
                                                                </div>
                                                            <?php $y++; endforeach; 
                                                            endif;

                                                            for($y=$chk_start_phys;$y<=5;$y++){?>
                                                                <div class="form-group">
                                                                    <select name="item[<?php echo $y;?>]"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                        <?php $phys = $func->settings->get_patient_file(13); ?>
                                                                        <option selected class="select-placeholder"><?php echo lang("bhv_problem_field");?></option>
                                                                        <?php if($phys): foreach($phys as $phs):?>
                                                                                <option class="form-control" value="<?php echo $phs->id?>">
                                                                                    <?php echo $phs->name;?>      
                                                                                </option>
                                                                        <?php endforeach; endif;?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <textarea name="description[<?php echo $y;?>]" rows="2" class="form-control" placeholder="<?php echo lang("desc_description_field");?>"></textarea>
                                                                </div>
                                                            <?}?>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="13">
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    
                                                    <?php /*
                                                    <div class="tab-pane" id="refer-block<?php echo $i;?>">
                                                        <?php $check_refer_record = $func->core_model->get_item("share_field_section",array("section"=>27,"patient_id"=>$appointment->patient_id,"date_test"=>$current_date));?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["referral"]?></strong>
                                                            <sup <?php if(isset($check_refer_record) && !empty($check_refer_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_sharefields_records"  class="add-patient-record-form" multiple="multiple">
                                                            <div class="form-group">
                                                                <select name="item"  class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                                    <?php $referral = $func->settings->get_patient_file_ddl(27); ?>
                                                                    <option disabled selected class="select-placeholder"><?php echo lang("refer_to");?></option>
                                                                    <?php if($referral): foreach($referral as $ref):?>
                                                                            <option class="form-control" <?php echo ($check_refer_record && $check_refer_record->ddl_id == $ref->id) ? "selected=selected" : "";?> value="<?php echo $ref->id;?>">
                                                                                <?php echo $ref->name;?>      
                                                                            </option>
                                                                    <?php endforeach; endif;?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="description" rows="3" class="form-control" placeholder="<?php echo lang("advice");?>"><?php echo ($check_refer_record && $check_refer_record->desc) ? $check_refer_record->desc : "";?></textarea>
                                                            </div>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <input type="hidden" name="section" value="27">
                                                            <?php if(!empty($check_refer_record)){?>
                                                                <input type="hidden" name="record_id" value="<?php echo $check_refer_record->id?>" />
                                                            <?}?>
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="tab-pane" id="sickl-block<?php echo $i;?>">
                                                        <?php $check_sickl_record = $func->core_model->get_item("patients_sick_leave",array("patient_id"=>$appointment->patient_id,"date_add"=>$current_date));?>
                                                        <h5>
                                                            <strong><?php echo lang("patientsection")["sickleave"]?></strong>
                                                            <sup <?php if(isset($check_sickl_record) && !empty($check_sickl_record)){ echo 'style="display:inline-block"';}?>>done</sup>
                                                        </h5> 
                                                        <div class="action-message"></div>
                                                        <form method="POST" action="/admin/dashboard/add_sickl_records"  class="add-patient-record-form" multiple="multiple">
                                                            <div class="form-group">
                                                                <textarea name="diagnosis" rows="3" class="form-control" placeholder="<?php echo lang("patient_diagnosis_tab");?>"><?php echo ($check_sickl_record && !empty($check_sickl_record->diagnosis)) ? $check_sickl_record->diagnosis : "";?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="leave_days" placeholder="<?php echo lang("sickleave_leave_days");?>" value="<?php echo ($check_sickl_record && !empty($check_sickl_record->leave_days)) ? $check_sickl_record->leave_days : "";?>" />
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="destination" placeholder="<?php echo lang("sickleave_destination");?>" value="<?php echo ($check_sickl_record && !empty($check_sickl_record->destination)) ? $check_sickl_record->destination : "";?>" />
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea class="form-control" rows="3" name="extra_notes" placeholder="<?php echo lang("sickleave_extra_notes");?>"><?php echo ($check_sickl_record && !empty($check_sickl_record->extra_notes)) ? $check_sickl_record->extra_notes : "";?></textarea>
                                                            </div>
                                                            <input type="hidden" name="patient_id" value="<?php echo $appointment->patient_id;?>">
                                                            <input type="hidden" name="date_appointment" value="<?php echo date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <?php if(!empty($check_sickl_record)){?>
                                                                <input type="hidden" name="record_id" value="<?php echo $check_sickl_record->id?>" />
                                                            <?}?>
                                                            <div class="text-right">
                                                                <button type="submit" class="btn btn-primary btn-flat"><?php echo lang("actions_submit");?></button>
                                                                <button type="reset" class="btn btn-warning btn-flat"><?php echo lang("actions_reset");?></button>
                                                                <a target="_blank" href="/admin/patientsections/sickleave/<?php echo $appointment->patient_id;?>/<?php echo date('Y-m-d',strtotime($appointment->start_date));?>/#print" type="reset" class="btn btn-primary btn-flat btn-print"><i class="fa fa-print"></i>&nbsp;<?php echo lang('immunoteraphy_print');?></a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    

                                                    <div class="tab-pane" id="addrecords-block<?php echo $i;?>">
                                                        <h5><strong><?php echo lang('doctor_record_edit&add');?></strong></h5>
                                                        <table  class="table table-bordered">
                                                            <?php foreach(lang("patientsection") as $key=>$section){?>
                                                                <tr>
                                                                    <td><a target="_blank" href="/admin/patientsections/main/<?php echo $key;?>/<?php echo $appointment->patient_id;?>/<?php echo date('Y-m-d',strtotime($appointment->start_date));?>"><?php echo $section;?></a></td>     
                                                                </tr>
                                                            <?}?>
                                                            
                                                        </table>
                                                    </div>
                                                    */?>
                                                </div>
                                            </div>
                                            <?php /* Old last Records 22122018
                                            <div class="patient-records-date col-md-3">
                                                <div>
                                                    <h5><?php echo lang("last_records_visits");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $last_visits = $func->PatientsModel->get_visit_history($appointment->patient_id,5);?>
                                                        <?php if($last_visits) : foreach($last_visits as $visit):?>
                                                        <li><a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/<?php echo $visit->date_visit;?>/#history-block"><?php echo $visit->date_visit;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                
                                                <div>
                                                    <h5><a class="text-blue" target="_blank" href="/admin/patientsections/main/skin_testing/<?php echo $appointment->patient_id;?>/<?php echo $current_date;?>"><i class="fa fa-plus"></i></a><?php echo lang("last_records_skin");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $skintests_history = $func->PatientsModel->get_patient_skintests_history($appointment->patient_id,5);?>
                                                        <?php if($skintests_history) : foreach($skintests_history as $skintests):?>
                                                        <li><a target="_blank" href="/admin/patientsections/main/skin_testing/<?php echo $appointment->patient_id;?>/<?php echo $skintests->date_add;?>"><?php echo $skintests->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><a class="text-blue" target="_blank" href="/admin/patientsections/main/prescription/<?php echo $appointment->patient_id;?>/<?php echo $current_date;?>"><i class="fa fa-plus"></i></a> <?php echo lang("last_records_prescripts");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $prescriptions_history = $func->PatientsModel->get_patient_prescription_history($appointment->patient_id,5);?>
                                                        <?php if($prescriptions_history) : foreach($prescriptions_history as $prescript):?>
                                                        <li><a target="_blank" href="/admin/patientsections/main/prescription/<?php echo $appointment->patient_id;?>/<?php echo $prescript->date_add;?>"><?php echo $prescript->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><a class="text-blue" target="_blank" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/<?php echo $current_date;?>"><i class="fa fa-plus"></i></a> <?php echo lang("last_records_invoices");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $last_invoices = $func->PatientsModel->get_invoices_groupby_date($appointment->patient_id,5);?>
                                                        <?php if($last_invoices) : foreach($last_invoices as $invoice):?>
                                                        <li><a target="_blank" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/<?php echo $invoice->invoice_date;?>/#invoice-block"><?php echo $invoice->invoice_date;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><?php echo lang("last_records_summary");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $last_visits = $func->PatientsModel->get_visit_history($appointment->patient_id,5);?>
                                                        <?php if($last_visits) : foreach($last_visits as $visit):?>
                                                        <li><a target="_blank" href="/admin/patientsections/summary/summary_plan/<?php echo $appointment->patient_id;?>/<?php echo $visit->date_visit;?>"><?php echo $visit->date_visit;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><?php echo lang("last_records_results");?></h5>
                                                    <ul class="unstyled">
                                                        <?php $last_results = $func->PatientsModel->get_patient_results_history($appointment->patient_id,5);?>
                                                        <?php if($last_results) : foreach($last_results as $results):?>
                                                        <li><a target="_blank" href="/admin/patientsections/results/<?php echo $appointment->patient_id;?>/<?php echo $results->date_test;?>"><?php echo $results->date_test;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                            </div>
                                            */?>
                                            <div class="patient-records-date col-md-3">
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/results/<?php echo $appointment->patient_id;?>/"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#allergy-tests"><?php echo lang("last_records_skin");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_upload_files = $func->PatientsModel->get_last_skintest_files($appointment->patient_id,1);?>
                                                        <?php if($last_upload_files) : foreach($last_upload_files as $last_upload_file):?>
                                                        <li><a href="/upload/patients/<?php echo $last_upload_file->fname;?>" target="_blank"><?php echo $last_upload_file->date_file;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/prescription/<?php echo $appointment->patient_id;?>/"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#prescription-block"><?php echo lang("last_records_prescripts");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $prescriptions_history = $func->PatientsModel->get_patient_prescription_history($appointment->patient_id,1);?>
                                                        <?php if($prescriptions_history) : foreach($prescriptions_history as $prescript):?>
                                                        <li><a target="_blank" href="/admin/patientsections/prescription/<?php echo $appointment->patient_id;?>/<?php echo $prescript->date_add;?>?dublicate=1"><?php echo $prescript->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/investigations/<?php echo $appointment->patient_id;?>/"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#results-block"><?php echo lang("last_records_investigations");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $investigations_history = $func->PatientsModel->get_patient_investigations_history($appointment->patient_id,1);?>
                                                        <?php if($investigations_history) : foreach($investigations_history as $investig):?>
                                                        <li><a target="_blank" href="/admin/patientsections/investigations/<?php echo $appointment->patient_id;?>/<?php echo $investig->date_add;?>?dublicate=1"><?php echo $investig->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><a class="text-blue" target="_blank" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/<?php echo date("Y-m-d");?>"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#invoice-block"><?php echo lang("last_records_invoices");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_invoices = $func->PatientsModel->get_invoices_groupby_date($appointment->patient_id,1);?>
                                                        <?php if($last_invoices) : foreach($last_invoices as $invoice):?>
                                                        <li><a target="_blank" href="/admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/<?php echo $invoice->invoice_date;?>/?dublicate=1"><?php echo $invoice->invoice_date;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5><a class="text-blue" target="_blank" href="/admin/patientsections/sickleave/<?php echo $appointment->patient_id;?>/<?php echo date("Y-m-d");?>"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#sickleaves-block"><?php echo lang("last_records_sickleaves");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_sick_leaves = $func->core_model->get_items("patients_sick_leave",array("patient_id"=>$appointment->patient_id), "date_add","DESC",1);?>
                                                        <?php if($last_sick_leaves) : foreach($last_sick_leaves as $last_sick_leave):?>
                                                        <li><a target="_blank" href="/admin/patientsections/sickleave/<?php echo $appointment->patient_id;?>/<?php echo $last_sick_leave->date_add;?>/?dublicate=1"><?php echo $last_sick_leave->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/summary/referral/<?php echo $appointment->patient_id;?>/<?php echo date("Y-m-d");?>"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#history-block"><?php echo lang("last_records_referals");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_referals = $func->PatientsModel->get_last_referrals($appointment->patient_id,1); ?>
                                                        <?php if($last_referals) : foreach($last_referals as $last_referal):?>
                                                        <li><a target="_blank" href="/admin/patientsections/summary/referral/<?php echo $appointment->patient_id;?>/<?php echo $last_referal->date_test;?>/?dublicate=1"><?php echo $last_referal->date_test?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/summary/summary_plan/<?php echo $appointment->patient_id;?>/<?php echo date("Y-m-d");?>"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>#history-block"><?php echo lang("last_records_summary");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_visits = $func->PatientsModel->get_visit_history($appointment->patient_id,1);?>
                                                        <?php if($last_visits) : foreach($last_visits as $visit):?>
                                                        <li><a target="_blank" href="/admin/patientsections/summary/summary_plan/<?php echo $appointment->patient_id;?>/<?php echo $visit->date_visit;?>"><?php echo $visit->date_visit;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/main/exam_photo/<?php echo $appointment->patient_id;?>/"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#exam-photo-block"><?php echo lang("last_exam_photo");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_examphoto_upload_files = $func->PatientsModel->get_last_examphoto_files($appointment->patient_id,1);?>
                                                        <?php if($last_examphoto_upload_files) : foreach($last_examphoto_upload_files as $examphoto_upload_file):?>
                                                        <li><a href="/upload/patients/<?php echo $examphoto_upload_file->fname;?>" target="_blank"><?php echo $examphoto_upload_file->date_file;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h5>
                                                        <a class="text-blue" target="_blank" href="/admin/patientsections/main/spirometer/<?php echo $appointment->patient_id;?>/"><i class="fa fa-plus"></i></a> 
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#spirometry-block"><?php echo lang("last_spirometry");?></a>
                                                    </h5>
                                                    <ul class="unstyled">
                                                        <?php $last_spirometry = $func->PatientsModel->get_patient_last_spirometry($appointment->patient_id,1);?>
                                                        <?php if($last_spirometry) : foreach($last_spirometry as $last_spir):?>
                                                        <li><a href="/admin/patientsections/main/spirometer/<?php echo $appointment->patient_id;?>/<?php echo $last_spir->date_add;?>" target="_blank"><?php echo $last_spir->date_add;?></a></li>
                                                        <?php endforeach;endif;?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; endforeach; endif;?>
                			</div>

                		</div>
				    </div>
			  	</div>
            </div>
        </div>
        <hr/>

        <div class="row">
            <div class="col-md-2">
                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("mini_calendar_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body" id="mini-calendar" style="width:100%;">

                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("notes_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_notes", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($notes_patient_id);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <label>For</label>
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="notes_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12"><?php echo form_textarea($notes_text);?></div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
            <div class="col-md-5">    
                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("tasks_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_task", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_title);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_date);?></div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-6">
                                     <label><input checked name="task_status" type="radio" value="2"></input><?php echo lang("status_started");?></label>
                                </div>
                                <div class="radio col-xs-6">
                                     <label><input name="task_status" type="radio" value="1"></input><?php echo lang("status_not_started");?></label>
                                </div>  
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <label>For</label>
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="task_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-4">
                                     <label><input checked name="task_priority" type="radio" value="1"></input><?php echo lang("task_low_label");?></label>
                                </div>
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="2"></input><?php echo lang("task_med_label");?></label>
                                </div>  
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="3"></input><?php echo lang("task_hight_label");?></label>
                                </div> 
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo form_textarea($task_text);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        
        <?php /*
        <div class="row">
            <div class="col-md-3">
                <div class="box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("stock_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <div class="box-body">
                        <h5><?php echo lang("stock_expires_items");?></h5>
                        <?php //var_dump($stock_items_toexprire);?>
                        <?php if(!empty($stock_items_toexprire)){?>
                            <?php foreach($stock_items_toexprire as $toexprire){?>
                            <p><strong><?php echo $toexprire->name;?> - <?php echo $toexprire->expire_day;?></strong></p>
                            <?}?>
                        <?}?>
                        <hr />
                        <h5><?php echo lang("stock_lowqty_items");?></h5>
                        <?php if(!empty($stock_items_qtylow)){?>
                            <?php foreach($stock_items_qtylow as $qtylow){?>
                            <p><strong><?php echo $qtylow->name;?> - <?php echo $qtylow->qty;?> <?php echo $stock_items_units[$qtylow->unit-1]->name;?></strong></p>
                            <?}?>
                        <?}?>
                    </div>    
                </div>
            </div>
            <div class="col-md-9">
                <div class="box collapsed-box">
                    <div class="box-header finance-box-header with-border">
                        <h3 class="box-title"><?php echo lang("finance_title");?>
                        </h3>
                        <ul id="finance-period-chooser">
                          <li class="active"><a href="#today_tab" data-toggle="tab" aria-expanded="true"><?php echo lang("finance_today_label");?></a></li>
                          <li class=""><a href="#week_tab" data-toggle="tab" aria-expanded="false"><?php echo lang("finance_week_label");?></a></li>
                          <li><a href="#month_tab" data-toggle="tab"><?php echo lang("finance_month_label");?></a></li>
                        </ul>

                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>  
                    <div class="box-body" id="finance-boxes">
                        <div class="tab-content">
                            <div class="tab-pane active" id="today_tab">
                                <div class="col-md-2 total-box color-palette">
                                    <h4><?php echo lang("finance_total_label");?></h4>
                                    <strong><?php echo (!empty($today_finance_info)) ? $today_finance_info->totalCash + $today_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 cash-box color-palette">
                                    <h4><?php echo lang("finance_cash_label");?></h4>
                                    <strong><?php echo (!empty($today_finance_info->totalCash)) ? $today_finance_info->totalCash : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 knet-box color-palette">
                                    <h4><?php echo lang("finance_knet_label");?></h4>
                                    <strong><?php echo (!empty($today_finance_info->totalKnet)) ? $today_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-4 pending-box color-palette">
                                    <h4><?php echo lang("finance_pending_label");?></h4>
                                    <strong><?php echo (!empty($today_finance_info->totalDebt)) ? $today_finance_info->totalDebt : 0;?> KD</strong>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="week_tab">
                                <div class="col-md-2 total-box color-palette">
                                    <h4><?php echo lang("finance_total_label");?></h4>
                                    <strong><?php echo (!empty($week_finance_info)) ? $week_finance_info->totalCash + $week_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 cash-box color-palette">
                                    <h4><?php echo lang("finance_cash_label");?></h4>
                                    <strong><?php echo (!empty($week_finance_info->totalCash)) ? $week_finance_info->totalCash : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 knet-box color-palette">
                                    <h4><?php echo lang("finance_knet_label");?></h4>
                                    <strong><?php echo (!empty($week_finance_info->totalKnet)) ? $week_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-4 pending-box color-palette">
                                    <h4><?php echo lang("finance_pending_label");?></h4>
                                    <strong><?php echo (!empty($week_finance_info->totalDebt)) ? $week_finance_info->totalDebt : 0;?> KD</strong>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="month_tab">
                                <div class="col-md-2 total-box color-palette">
                                    <h4><?php echo lang("finance_total_label");?></h4>
                                    <strong><?php echo (!empty($month_finance_info)) ? $month_finance_info->totalCash + $month_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 cash-box color-palette">
                                    <h4><?php echo lang("finance_cash_label");?></h4>
                                    <strong><?php echo (!empty($month_finance_info->totalCash)) ? $month_finance_info->totalCash : 0;?> KD</strong>
                                </div>
                                <div class="col-md-2 knet-box color-palette">
                                    <h4><?php echo lang("finance_knet_label");?></h4>
                                    <strong><?php echo (!empty($month_finance_info->totalKnet)) ? $month_finance_info->totalKnet : 0;?> KD</strong>
                                </div>
                                <div class="col-md-4 pending-box color-palette">
                                    <h4><?php echo lang("finance_pending_label");?></h4>
                                    <strong><?php echo (!empty($month_finance_info->totalDebt)) ? $month_finance_info->totalDebt : 0;?> KD</strong>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        */?>
    </section>
</div>

<link rel="stylesheet" href="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<script src="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
  $(document).ready(function() {
    curDate = 0;
    var eventDate = '<?php echo date("d-m-Y",strtotime($current_date));?>';

    var curDate = window.location.search.replace( '?date_appointments=', ''); 
    $( "#mini-calendar" ).datepicker({
    	format: 'dd-mm-yyyy',
    	todayHighlight:true,
        showOnFocus: true,
        defaultViewDate: eventDate,
        beforeShowDay: function(date) {
            var d = moment(date).format('DD-MM-YYYY');
            if(d == eventDate){
             return {enabled:true,classes: 'active', tooltip: eventDate};
            }

        },
    }).on("changeDate", function(e) {
        var chDate = $('#mini-calendar').datepicker('getFormattedDate');
        document.location.href = "/admin/dashboard/?date_appointments="+chDate+"&doctor=<?php echo $current_doctor_id;?>";
    });

    var table = $("#dashboard-appointments-list").DataTable({
        "pageLength": 25,
        "paging" : false,
        "searching" : false
    });

    $("#dashboard-appointments-list tbody tr").click(function(){
        var patient = $(this).attr("data-patient");
        $(".dash-current-patient").removeClass("active");
        $(".dash-current-patient#event-"+patient+"").addClass("active");
        $("#dashboard-appointments-list tbody > tr").removeClass("activetr");
        $(this).addClass("activetr");
    });

    $(".add-patient-record-form button[type=submit]").on("click",function(){
        var form = $(this).parent().parent();
        var data = form.serialize();

        var url = $(form).attr("action");
        $.ajax({
            url:url,
            type:"POST",
            dataType:"html",
            data: data,
            success: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message).fadeIn();
                if(result.status == "success"){
                    $(form).parent().find("sup").css("display","inline-block");
                    setTimeout(function(){
                        $(".action-message").fadeOut();
                    },1000);
                    $(form).reset();
                }
            },
            error: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message);
            }
        });

        return false;
    });

    $("body,html").on("click",".current-patient-info .visit-actions a", function(){

            var eventid = $(this).data("eventid");
            var action = $(this).attr("href");
            var type = $(this).data("type");
            
            $.ajax({
                url:action,
                type:"GET",
                success: function(response) {
                    $(".visit-actions a").fadeOut();
                    setTimeout(function(){
                        if(type == "outcome"){
                            $(".visit-actions").html("<a data-type='done' data-eventid='"+eventid+"' class='done' href='/admin/appointments/done/"+eventid+"'><img src='/assets/images/done.png' /></a>");
                        }
                        if(type == "done"){
                            $(".visit-actions").html("<span><?php echo lang("actions_done");?></span>");
                        }
                    },300);
                }
            });

            return false;


    });

  });

</script>