<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content" id="dashboard">
        <?php echo $dashboard_alert_file_install; ?>
        <?php echo $message;?>
        <div class="row">
            <div class="col-lg-2 col-md-3">
            	<div class="box">
                	<div class="box-header with-border">
					    <h3 class="box-title"><?php echo lang("mini_calendar_title");?></h3>
					    <div class="box-tools pull-right">
					      <!-- Collapse Button -->
					      <button type="button" class="btn btn-box-tool" data-widget="collapse">
					        <i class="fa fa-minus"></i>
					      </button>
					    </div>
					    <!-- /.box-tools -->
					</div>    
				    <div class="box-body" id="mini-calendar" style="width:100%;">

				    </div>
			  	</div>
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("notes_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_notes", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($notes_patient_id);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <label>For</label>
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="notes_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12"><?php echo form_textarea($notes_text);?></div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang("tasks_title");?></h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>    
                    <div class="box-body">
                        <?php echo form_open_multipart("admin/dashboard/add_task", array('class' => 'form-horizontal', 'id' => 'form-create')); ?>   
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_title);?></div>
                            </div> 
                            <div class="form-group ">
                                <div class="col-xs-12"><?php echo form_input($task_date);?></div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-6">
                                     <label><input checked name="task_status" type="radio" value="2"></input><?php echo lang("status_started");?></label>
                                </div>
                                <div class="radio col-xs-6">
                                     <label><input name="task_status" type="radio" value="1"></input><?php echo lang("status_not_started");?></label>
                                </div>  
                            </div>
                            <div class="form-group ">
                                <div class="col-xs-12 ">
                                    <select data-placeholder="<?php echo lang("task_to_label");?>" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="task_users[]" multiple="multiple" >
                                        <?php if(!empty($task_users)) {
                                            foreach($task_users as $task_user) { ?>
                                             <option value="<?php echo $task_user->id;?>"><?php echo $task_user->first_name;?> <?php echo $task_user->last_name;?></option>   
                                        <?php }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="radio col-xs-4">
                                     <label><input checked name="task_priority" type="radio" value="1"></input><?php echo lang("task_low_label");?></label>
                                </div>
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="2"></input><?php echo lang("task_med_label");?></label>
                                </div>  
                                <div class="radio col-xs-4">
                                     <label><input name="task_priority" type="radio" value="3"></input><?php echo lang("task_hight_label");?></label>
                                </div> 
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <?php echo form_textarea($task_text);?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-right"><?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9" id="today-appoitnments-block">
                <div class="box" id="reception-dashboard-calendar">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Appointments</h3>
                        <div class="box-tools pull-right">
                          <!-- Collapse Button -->
                          <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                          </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="doctor_calendar">
                                    <?php if(isset($_GET["doctor"])){?>
                                        <a style="border-color:#333;" onmouseover="this.style.backgroundColor='#333';" onmouseout="this.style.backgroundColor='#fff';" href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>"><?php echo lang("all_doctors");?></a> 
                                    <?}else{?>
                                        <a style="border-color:#333;background-color:#333;" class="active" href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>"><?php echo lang("all_doctors");?></a>
                                    <?}?>
                                    <?php if($doctors) : foreach($doctors as $doctor):?>
                                        <?php if(isset($_GET["doctor"]) && $doctor->id == $_GET["doctor"]){?>
                                            <a style="border-color:<?php echo $doctor->panel_color;?>; background-color:<?php echo $doctor->panel_color;?>" class="active" href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>&doctor=<?php echo $doctor->id;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name;?></a>
                                        <?}else{?>
                                            <a style="border-color:<?php echo $doctor->panel_color;?>;" onmouseover="this.style.backgroundColor='<?php echo $doctor->panel_color;?>';" onmouseout="this.style.backgroundColor='#fff';" href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>&doctor=<?php echo $doctor->id;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name;?></a>
                                        <?}?>
                                    <?php endforeach; endif;?>
                                </div>
                                <div id="loading-text" class="text-center"><?php echo lang("actions_loading");?></div>
                                <div id="calendar" class="fc fc-unthemed fc-ltr"></div>
                            </div>
                        </div>
                    </div>
                </div>
            	<div class="box">
                	<div class="box-header with-border">
					    <h3 class="box-title"><?php echo lang("today_patients_title");?>&nbsp;&nbsp;&nbsp;&nbsp;<?php //echo lang("appointments_title");?></h3>
                        
                        <ul id="doctors-period-chooser" class="pull-right" style="margin-right:5%;">
                            <li <?php echo (!isset($current_doctor_id) || $current_doctor_id == 0) ? "class='active'" : "";?>>
                            <a href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>"><?php echo lang("app_all_doctors");?></a></li>
                            <?php if($doctors): foreach($doctors as $doctor):?>
                            <li <?php echo (isset($current_doctor_id) && $current_doctor_id == $doctor->id) ? "class='active'" : "";?>> 
                                <a href="/admin/dashboard/?date_appointments=<?php echo $current_date;?>&doctor=<?php echo $doctor->id;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?> <?php echo $doctor->first_name." ".$doctor->last_name; ?></a></li>
                            <?php endforeach;endif; ?>
                        </ul>

					    <div class="box-tools pull-right">
					      <!-- Collapse Button -->
					      <button type="button" class="btn btn-box-tool" data-widget="collapse">
					        <i class="fa fa-minus"></i>
					      </button>
					    </div>
					    <!-- /.box-tools -->
					</div>    
				    <div class="box-body">
                		<div class="row">
                			<div class="col-xs-12" >
                                <h4><strong><?php echo lang("appointments_total_title");?> <?php echo count($appointments);?></strong></h4>
                                <table id="dashboard-appointments-list" class="table table-bordered table-hover" role="grid">
                                    <thead>
                                        <tr>
                                            <th class="sorting"  aria-sort="descending" aria-controls="list-data">#</th> 
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_type');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_time');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('app_patient');?></th>
                                            <th class="sorting" aria-controls="list-data"><?php echo lang('doctor');?></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($appointments as $appointment):?>
                                            <tr <?php if($i==1){?>class="activetr"<?}?> data-patient="<?php echo $appointment->patient_id;?>">
                                                <td><?php echo $i;?></td>
                                                <td>
                                                <?php if($appointment->type == 1){?>
                                                    <i class="fa fa-lightbulb-o"></i>&nbsp;
                                                <?php }?>  
                                                    <span style='color:<?php echo $appointment->type_color;?> !important'><?php echo $appointment->type_abbr;?></span>  
                                                </td>
                                                <td>
                                                    <?php echo date('H:i',strtotime($appointment->start_date));?>
                                                    <?php echo lang('app_time_in');?>:
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->in_come_time));?></span> - 
                                                    <?php echo lang('app_time_out');?>:
                                                    <span class="text-blue"><?php echo date('h:i A',strtotime($appointment->out_come_time));?>
                                                </td>
                                                <td>
                                                    <?php echo htmlspecialchars($appointment->patient_name, ENT_QUOTES, 'UTF-8'); ?> [<?php echo $appointment->patient_id;?>]
                                                    <br />
                                                    <span class="text-blue"><?php echo $func->calculate_age($appointment->patient_dob);?></span> 
                                                </td>
                                                <td><?php echo $appointment->doctor_name." ".$appointment->doctor_lname; ?>,<?php echo $appointment->room_name;?>
                                                </td>
                                                <td class="action-icons">

                                                    <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>">
                                                            <img src="/assets/images/edit.png" />
                                                    </a>

                                                    <a title="Results" style="margin-left:5px;margin-right:8px;" target="_blank" href=" /admin/patientsections/results/<?php echo $appointment->patient_id.'/'.$current_date;?>/">
                                                            <i class="fa fa-upload"></i>
                                                    </a>

                                                    <?php if($func->PatientsModel->get_patient_invoice_by_date($appointment->patient_id,$current_date)){?>
                                                    
                                                    <?php /* <a target="_blank" href=" /admin/patientsections/invoice/<?php echo $appointment->patient_id.'/'.$current_date;?>/#invoice-block"> */?>
                                                    <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.$current_date;?>/#invoice-block">
                                                            <img src="/assets/images/doctor-icon.png" />
                                                    </a>

                                                    <?}else{?>
                                                        <a target="_blank" class="skip_nurse_rec" title="Add invoice" href=" /admin/patientsections/invoice/<?php echo $appointment->patient_id;?>/">
                                                            <img src="/assets/images/doctor-icon.png" />
                                                        </a>
                                                    <?}?>


                                                    <?php if($appointment->in_come_flag == 1){?>
                                                        <a style="margin-left:5px;" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id;?>/#nurse-block">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?}else{?>
                                                        <a class="skip_nurse_rec" style="margin-left:5px;" title="Skip nurse record" href="/admin/patients/nurse_record_skip/<?php echo $appointment->patient_id.'/'.$current_date;?>">
                                                            <img src="/assets/images/nurse.png" />
                                                        </a>
                                                    <?}?>
                                                    
                                                    <a title="Prescriptions" target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                        <img  src="/assets/images/prescription_drugs.png" />
                                                    </a>

                                                    <?php if($func->PatientsModel->get_patient_drug_allergy($appointment->patient_id,30)){?>
                                                       
                                                        <a target="_blank" class="red_flag" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#prescription-block">
                                                            <img src="/assets/images/red_flag.png" />
                                                        </a>

                                                    <?}?>

                                                    <?php if($func->PatientsModel->get_immunoteraphy_plan($appointment->patient_id)){?>
                                                        
                                                        <a target="_blank" href="/admin/patients/card/<?php echo $appointment->patient_id.'/'.date('Y-m-d',strtotime($appointment->start_date));?>#immunoteraphy-block">
                                                            <img src="/assets/images/immuno.png" />
                                                        </a>

                                                    <?}?>
                                                    <span class="visit-actions">
                                                    <?php if($func->appointmentsModel->get_items("nurse",
                                                        array(
                                                        "patient_id"=>$appointment->patient_id,
                                                        "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                        )
                                                    ) && trim($appointment->out_come_time)==''){?>

                                                    <a data-type="outcome" data-eventid="<?php echo $appointment->event_id?>" class="outcome" href="/admin/appointments/out/<?php echo $appointment->event_id?>">
                                                        <img src="/assets/images/out.png" />
                                                    </a>

                                                    <?}?>
                                                    
                                                    <?php if($func->core_model->get_items("nurse",
                                                        array(
                                                        "patient_id"=>$appointment->patient_id,
                                                        "date_visit"=>date('Y-m-d',strtotime($appointment->start_date))
                                                        )
                                                    ) && (trim($appointment->out_come_time)<>'')){?>

                                                        <?php if($appointment->done_flag == 1 && $appointment->out_come_flag == 1) {?>
                                                            <span> <i class="fa fa-check"></i></span>
                                                        
                                                        <?php }else{?>

                                                            <a data-type="done" data-eventid="<?php echo $appointment->event_id?>" class="done" href="/admin/appointments/done/<?php echo $appointment->event_id?>">
                                                            <img src="/assets/images/done.png" />
                                                        </a>

                                                        <?php } ?>
                                                        
                                                    <?}?>
                                                    </span>
                                                     

                                                </td>
                                            </tr>
                                        <?php $i++; endforeach;?>
                                    </tbody>
                                </table>
                			</div>
                		</div>
				    </div>
			  	</div>
            </div>
        </div>
        <hr/>
    </section>
</div>

<div class="modal fade" id="editEvent" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="app-add-title"><?php echo lang("pagetitle_add");?> <span class="app_edit_period"></span></h4>
        <h4 class="modal-title" style="display:none;" id="app-edit-title"><?php echo lang("pagetitle_edit");?> <span class="app_edit_period"></span></h4>
      </div>
      <?php echo form_open(uri_string(), array('id' => 'form-edit')); ?>
      <div class="modal-body">
            <div class="form-group row">
                <?php echo lang('date_appointment', 'date_appointment', array('class' => 'col-xs-12 control-label')); ?>
                <div class="date col-sm-12">
                  <input type="date" id="date_appointment" class="form-control" name="date_appointment" value="<?php echo date('Y-m-d');?>"></input>
                </div>
            </div>
            <div class="form-group row">
                <?php echo lang('time_period', 'time_period', array('class' => 'col-xs-12 control-label')); ?>
                <div class="date col-sm-4">
                  <input type="time" class="form-control" name="start_time" id="start_time" value="<?php echo date('h:i:s');?>"></input>
                </div>
                <div style="float:left;display:inline-block">
                  -
                </div>
                <div class="date col-sm-4">
                  <input type="time" class="form-control" name="end_time" id="end_time" value="<?php echo date('h:i:s');?>"></input>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('doctor', 'doctor_id', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="doctor_id" id="doctor_id" class="form-control">
                        <?php if($doctors): foreach($doctors as $doctor):?>
                        <option <?php if(isset($_GET['doctor']) && $_GET['doctor'] == $doctor->id){?>selected="selected"<?php }?> value="<?php echo $doctor->id;?>" data-event-color="<?php echo $doctor->panel_color;?>"><?php if($doctor->position !== "Nurse") { echo "Dr. "; }?><?php echo $doctor->first_name." ".$doctor->last_name; ?></option>
                        <?php endforeach;endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('appointment_type', 'appoitment_type', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="type" id="appoitment_type" name="type" class="form-control">
                        <?php foreach($types as $type):?>
                        <option data-group="<?php echo $type->user_groups;?>" value="<?php echo $type->id;?>"><?php echo $type->name;?></option>
                    <?php endforeach;?> 
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('rooms', 'room_id', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select name="room_id" id="room_id" class="form-control">
                        <?php if($rooms): foreach($rooms as $room):?>
                        <option value="<?php echo $room->id;?>"><?php echo $room->name; ?></option>
                        <?php endforeach;endif; ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <?php echo lang('select_patient', 'event_name', array('class' => 'col-xs-12 control-label')); ?>
                <div class="col-xs-12">
                    <select class="form-control select2 select2-hidden-accessible" name="event_name" id="event_name" style="width: 100%;" tabindex="-1" aria-hidden="true">
                        <option value="0" selected="selected">--Select patient--</option>
                        <?php foreach($patients as $patient){?>
                            <option value="<?php echo $patient->id;?>"><?php echo $patient->patient_name;?> [<?php echo $patient->id;?>] (tel:<?php echo $patient->telephone;?>; <?php echo $patient->mobile;?>)</option>
                        <?}?>
                    </select>
                </div>
                <div class="col-xs-12" id="or-add-new-patient">
                    <a data-toggle="collapse" href="#collapseAddPatient" class="or-separate"><i class="fa fa-user-plus"></i><?php echo lang("or_add_patient");?></a>
                    <div class="form-group row collapse" id="collapseAddPatient">
                        <div class="col-md-6">
                            <?php echo lang('patient_name', 'new-patient-name', array('class' => 'control-label')); ?>
                            <input type="text" id="new-patient-name" name="new-patient-name" class="form-control" value="" />
                        </div>
                        <div class="col-md-6">
                            <?php echo lang('patient_telephone', 'new-patient-telephone', array('class' => 'control-label')); ?>
                            <input type="text" id="new-patient-telephone" name="new-patient-telephone" class="form-control" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="view-profile-link">
                    <a target="_blank" href=""><i class="fa fa-search"></i><?php echo lang('view_profile');?></a>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-12">
                    <?php echo lang('appointment_details', 'appointment_details', array('class' => 'control-label')); ?>
                    <input  type="text" id="appointment_details" name="appointment_details" class="form-control"></input>
                </div>
            </div>
            <input type="hidden" id="event_id" value="0" name="event_id" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang("actions_close");?></button>
        <button type="button" type="submit" id="update" class="btn btn-primary"><?php echo lang("actions_save");?></button>
        <button type="button" id="delete" class="btn btn-danger"><?php echo lang("actions_delete");?></button>
      </div>
      <?php echo form_close();?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<link rel="stylesheet" href="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<script src="/assets/frameworks/bootstrap/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>

$(document).ready(function(){

    var eventDate = '<?php echo date("d-m-Y",strtotime($current_date));?>';


    $( "#mini-calendar" ).datepicker({
    	format: 'dd-mm-yyyy',
    	todayHighlight:true,
        showOnFocus: true,
        defaultViewDate: eventDate,
        beforeShowDay: function(date) {
            var d = moment(date).format('DD-MM-YYYY');
            if(d == eventDate){
             return {enabled:true,classes: 'active', tooltip: eventDate};
            }

        },
    }).on("changeDate", function(e) {
        var chDate = $('#mini-calendar').datepicker('getFormattedDate');
        document.location.href = "/admin/dashboard/?date_appointments="+chDate+"&doctor=<?php echo $current_doctor_id;?>";
    });

    var table = $("#dashboard-appointments-list").DataTable({
        "pageLength": 25,
        "paging" : false,
        "searching" : false
    });

    $("#dashboard-appointments-list tbody tr").click(function(){
        var patient = $(this).attr("data-patient");
        $(".dash-current-patient").removeClass("active");
        $(".dash-current-patient#event-"+patient+"").addClass("active");
        $("#dashboard-appointments-list tbody > tr").removeClass("activetr");
        $(this).addClass("activetr");
    });

    $(".add-patient-record-form button[type=submit]").on("click",function(){
        var form = $(this).parent().parent();
        var data = form.serialize();

        var url = $(form).attr("action");
        $.ajax({
            url:url,
            type:"POST",
            dataType:"html",
            data: data,
            success: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message).fadeIn();
                if(result.status == "success"){
                    $(form).parent().find("sup").css("display","inline-block");
                    setTimeout(function(){
                        $(".action-message").fadeOut();
                    },1000);
                    $(form).reset();
                }
            },
            error: function(response) {
                result = $.parseJSON(response);
                $(".action-message").html(result.message);
            }
        });

        return false;
    });

    $("body,html").on("click",".visit-actions a", function(){
        if (confirm("Are you sure?")) {

            var eventid = $(this).data("eventid");
            var action = $(this).attr("href");
            var type = $(this).data("type");

            var el = $(this);

            $.ajax({
                url:action,
                type:"GET",
                success: function(response) {
                    $(el).parent().find("a").fadeOut();
                    setTimeout(function(){
                        if(type == "outcome"){
                            $(el).parent().html("<a data-type='done' data-eventid='"+eventid+"' class='done' href='/admin/appointments/done/"+eventid+"'><img src='/assets/images/done.png' /></a>");
                        }
                        if(type == "done"){
                            $(el).parent().html("<i class='fa fa-check'></i>");
                        }
                    },300);
                }
            });
            return false;

        } else {
            return false;
        }
    });

    var curDate = "<?php echo date('M d Y',strtotime($current_date));?>";    

    var date = new Date(curDate);
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear();

    $('#calendar').fullCalendar({
        header    : {
            left  : 'prev,next today',
            center: 'title',
            right : 'agendaDay'
        },
        buttonText: {
            today: 'today',
            day  : 'day'
        },  
        slotDuration : '00:15:00',
        minTime: '09:00:00',
        allDaySlot: false,
        axisFormat: 'HH:mm',
        timeFormat: 'HH:mm',
        slotLabelFormat:"HH:mm",
        editable  : true,
        droppable : true,
        selectable: true,
        defaultTimedEventDuration: '00:15:00',
        forceEventDuration: true,
        eventAfterAllRender: function() {
            $('#loading-text').hide();
        },
        <?php if($appointments):?>
        events    : [
            <?php foreach($appointments as $appointment):?>
                {
                  event_id : <?php echo $appointment->event_id;?>,
                  patient_id : <?php echo $appointment->patient_id;?>,
                  title : '<?php if($appointment->type == 2){ echo "NEW! ";}?><?php echo (!empty($appointment->patient_phone)) ? $appointment->patient_phone."; " : "";?><?php echo (!empty($appointment->patient_mobile)) ? $appointment->patient_mobile."; " : "";?><?php echo (!empty($appointment->patient_mobile2)) ? $appointment->patient_mobile2."; " : "";?><?php echo $appointment->patient_name;?> [<?php echo $appointment->patient_id;?>]; <?php echo htmlspecialchars($appointment->details, ENT_QUOTES);?>',
                  start          : '<?php echo $appointment->start_date;?>',
                  end            : '<?php echo $appointment->end_date;?>',
                  appointment_type : '<?php echo $appointment->type;?>',
                  appointment_details : '<?php echo trim(stripslashes(stripcslashes($appointment->details)));?>',
                  doctor_id : '<?php echo $appointment->doctor_id;?>',
                  room_id : '<?php echo $appointment->room;?>',
                  allDay         : false,
                  <?php
                  if($appointment->ucolor !== 0){?>
                    backgroundColor: '<?php echo $appointment->ucolor;?>',
                    borderColor    : '<?php echo $appointment->ucolor;?>',
                  <?}else{?>
                    backgroundColor: '#00c0ef',
                    borderColor    : '#00c0ef',
                  <?}?>
                  url            : ''
                },
            <?php endforeach;?>
        ],
        <?php endif;?>
        dayClick: function(date) {
            $(".fc-agendaDay-button").click();
            return;
        },
        select: function(start,end, jsEvent, view,) {
            newAppointment(start,end);
        },
        eventClick: function(calEvent, jsEvent, view) {
            editAppointment(calEvent);                
        },
    });

    $("#calendar .fc-agendaDay-button").trigger("click");
    $('#calendar').fullCalendar('gotoDate', date);
});

var newAppointment = function(app_start,app_end) {
        var start = moment(app_start);
        var end = moment(app_end);

        inpStart = "--:--";
        inpEnd = "--:--";

        if(start.format("HH") !== "00"){
            inpStart = start.format("HH:mm:ss");
            inpEnd = end.format("HH:mm:ss");
        }

        $("#delete,#view-profile-link,#app-edit-title").hide();
        $("#app-add-title,#or-add-new-patient").show();

        $('input#date_appointment').val(start.format("YYYY-MM-DD"));
        $('input#start_time').val(inpStart);
        $('input#end_time').val(inpEnd);

        $('input#event_id').val(0);
        $('select#appoitment_type').val(1);
        $('select#room_id').val(1);
        $('#appoitment_details').val("");
        $('select#event_name').val(0);
        $('select#event_name').trigger("change");

        $('#editEvent').modal('show');
        $('#update').unbind();

        $('#update').on('click', function() {

            var required_date = $('input#date_appointment').val();
            var required_start_time = $('input#start_time').val();
            var patient_id = $('select#event_name').val();
            var new_patient_name = $('#new-patient-name').val();
            var new_patient_telephone = $('#new-patient-telephone').val();
            var event_name = $('select#event_name option:selected').text();
            var event_color = $('select#doctor_id option:selected').attr("data-event-color");

            var eventData;

            if (required_date && required_start_time && (parseInt(patient_id) !== 0 || (new_patient_name !== "" && new_patient_telephone !== ""))) {
              
                $.ajax({
                  url: '/admin/appointments/add_appointments',
                  type: "POST",
                  data: $("#form-edit").serialize(),
                  dataType: "JSON",
                  success: function(response){
                    $(".submit-response").html(response.msg);
                    if(parseInt(response.data) !== 0){
                        

                        if(new_patient_name !== "" && parseInt(response.new_patient_id) !== 0){
                            patient_id = response.new_patient_id;
                            var title = new_patient_telephone + "; " +new_patient_name+"["+patient_id+"]";
                        }else{
                            patient_id = $('select#event_name').val();
                            var title = event_name;
                        }


                        var eventData = {
                            title: title,
                            patient_id: patient_id,
                            start : $('#date_appointment').val()+" "+$('#start_time').val(),
                            end: $('#date_appointment').val()+" "+$('#end_time').val(),
                            appointment_type: $('select#appoitment_type').val(),
                            appointment_details : $('#appointment_details').val(),
                            room_id:$('select#room_id').val(),
                            doctor_id:$('select#doctor_id').val(),
                            backgroundColor: event_color,
                            borderColor: event_color,
                            event_id: response.data
                        };

                    }

                    $('#calendar').fullCalendar('renderEvent', eventData, true);
                    $('#newEvent').modal('hide');
                  }
                });

                $('#editEvent').modal('hide');
            }else{
                alert("Please fill in all fields!")
            }
        });
    };

    var editAppointment = function(calEvent) {
        var start = moment(calEvent.start._i);
        var end = moment(calEvent.end._i);

        $("#app-add-title,#or-add-new-patient").hide();
        $("#delete,#app-edit-title,#view-profile-link").show();
        $("#view-profile-link a").attr("href","/admin/patients/card/"+calEvent.patient_id+"");

        $('input#date_appointment').val(start.format("YYYY-MM-DD"));
        $('input#start_time').val(start.format("H:mm:ss"));
        $('input#end_time').val(end.format("H:mm:ss"));
        $('input#event_id').val(calEvent.event_id);
        $('select#appoitment_type').val(calEvent.appointment_type);
        $('select#room_id').val(calEvent.room_id);
        $('#appointment_details').val(calEvent.appointment_details);
        $('select#doctor_id').val(calEvent.doctor_id);
        $('select#event_name').val(calEvent.patient_id);
       
        $('select#event_name').trigger("change");

        $('#editEvent').modal('show');
        $('#update').unbind();
        $('#update').on('click', function() {

            var required_date = $('input#date_appointment').val();
            var required_start_time = $('input#start_time').val();
            var required_event_name = $('select#event_name option:selected').text();
            $('#editEvent').modal('hide');
            var eventData;

            if (required_date && required_start_time && required_event_name !== "--Select patient--") {
              
                $.ajax({
                  url: '/admin/appointments/add_appointments',
                  type: "POST",
                  data: $("#form-edit").serialize(),
                  dataType: "JSON",
                  success: function(response){
                    $(".submit-response").html(response.msg);
                    if(parseInt(response.data) !== 0){
                        calEvent.event_id = response.data;
                    }
                    calEvent.title = required_event_name;
                    calEvent.patient_id = $('select#event_name').val();
                    calEvent.start = $('#date_appointment').val()+" "+$('#start_time').val();
                    calEvent.end = $('#date_appointment').val()+" "+$('#end_time').val();
                    calEvent.appointment_type = $('select#appoitment_type').val();
                    calEvent.room_id = $('select#room_id').val();
                    calEvent.appointment_details = $('#appointment_details').val();
                    calEvent.doctor_id = $('select#doctor_id').val();

                    $('#calendar').fullCalendar('updateEvent', calEvent);
                  }
                });

            }else{
                alert("Please fill in all fields!")
            }
        });

        $('#delete').on('click', function() {
            $('#delete').unbind();

            $.ajax({
                  url: '/admin/appointments/delete',
                  type: "POST",
                  data: "id="+calEvent.event_id+"",
                  success: function(response){
                    if(parseInt(response) == 1){
                        if (calEvent._id.includes("_fc")){
                          $('#calendar').fullCalendar('removeEvents', [calEvent._id]);
                        } else {
                          $('#calendar').fullCalendar('removeEvents', [calEvent._id]);
                        }
                        $('#editEvent').modal('hide');
                    }
                  }
            });

        });
    };

    var getCal1Id = function(cal2Id) {
      var num = cal2Id.replace('_fc', '') - 1;
      var id = "_fc" + num;
      return id;
    };

</script>
