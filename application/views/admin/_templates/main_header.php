﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
            <header class="main-header">
                <a href="<?php echo site_url('admin/dashboard'); ?>" class="logo">
                    <span class="logo-mini"><?php echo $title_mini; ?></span>
                    <span class="logo-lg"><?php echo $title_lg; ?></span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <span class="cur-date"><?php echo (isset($current_date)) ? date("l, d M Y", strtotime($current_date)) : date("l, d M Y");?></span>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
<?php if ($admin_prefs['main_menu'] == TRUE): ?>
                            <!-- Messages -->
                            <li class="main-header-list"><a href="/admin"><?php echo lang('header_mainmenu_dashboard'); ?></a></li>
                            <li class="main-header-list"><a href="/admin/appointments"><?php echo lang('header_mainmenu_calendar'); ?></a></li>
                            <?php if(!$is_nurse){?>   
                                <li class="main-header-list"><a href="/admin/patients"><?php echo lang('header_mainmenu_patients'); ?></a></li>
                                <li class="main-header-list"><a href="/admin/patients?immuno=1"><?php echo lang('header_mainmenu_immunotherapy'); ?></a></li>
                            <?}?>
                            <?php if(!$is_reception){?>
                            <li class="main-header-list"><a href="/admin/appointments/pending"><?php echo lang('header_mainmenu_pendingcases'); ?>
                                <span class="label label-warning"><?php echo $count_pending_cases;?></span>
                            </a></li>
                            <?}?>

                            
                            <?php if(!$is_nurse){?>
                                <li class="main-header-list"><a href="/admin/stock/instock"><?php echo lang('header_mainmenu_stock'); ?><span class="label label-danger"><?php echo $over_stock;?></span></a></li>

                                <?php if(!$is_reception){?>   
                                    <li class="main-header-list"><a href="/admin/reports/invoice_report"><?php echo lang('header_mainmenu_invoice'); ?></a></li>
                                <?php } ?>

                                <li class="main-header-list"><a href="/admin/reports/invoice_report/?debt=1"><?php echo lang('header_mainmenu_pendingpayments'); ?><span class="label label-danger"><?php echo $count_pending_payments;?></span></a></li>

                            <?}?>

                            <?php if($is_reception){?> 
                            <li class="main-header-list add-appointment-button"><a href="#" data-toggle="modal" data-target="#quick-upload-results"><i class="fa fa-plus"></i><br /><?php echo lang('header_mainmenu_upload'); ?></a></li>   
                            <?}?>

                            <li class="main-header-list add-appointment-button"><a href="/admin/appointments"><i class="fa fa-plus"></i><br /><?php echo lang('header_mainmenu_addappointment'); ?></a></li>
<?php endif; ?>

<?php if ($admin_prefs['messages_menu'] == TRUE): ?>    
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success">0</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?php echo lang('header_you_have'); ?> 0 <?php echo lang('header_message'); ?></li>
                                    <li>
                                        <ul class="menu">
                                            <li><!-- start message -->
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="<?php echo base_url($avatar_dir . '/'.$avatar_img_2); ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>Support Team<small><i class="fa fa-clock-o"></i> 5 mins</small></h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li><!-- end message -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#"><?php echo lang('header_view_all'); ?></a></li>
                                </ul>
                            </li>

<?php endif; ?>

<?php echo $header_alert_file_install; ?>

<?php if ($admin_prefs['notifications_menu'] == TRUE): ?>
                            <!-- Notifications -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning"><?php echo $count_pending_notes;?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?php echo lang('header_you_have'); ?> <?php echo $count_pending_notes;?> <?php echo lang('header_pending_notes'); ?></li>
                                    <li>
                                        <ul class="menu">
                                            <?php foreach($pending_notes as $pending_note) {?>
                                            <li><!-- start notification -->
                                                <a href="#" data-noteid="<?php echo $pending_note->id;?>" class="notes-modal-link">
                                                    <span class="blue note-date"><?php echo date("d-m-Y",strtotime($pending_note->originate_date));?></span>&nbsp;&nbsp;<?php echo substr($pending_note->notes,0,60);?>...

                                                <span class="hidden notes_patient_id"><?php echo $pending_note->patient_name." ".$pending_note->telephone;?></span>
                                                <span class="hidden notes_allocate">

                                                <?php if(!empty($pending_note->for_user)){
                                                        $allocate = unserialize($pending_note->for_user);
                                                        foreach($allocate as $fuid){

                                                            $for_user_info = $glob->TasksModel->get_allocate_user_info($fuid);

                                                            echo $for_user_info->first_name." ".$for_user_info->last_name."; ";
                                                        }
                                                    }
                                                ?>
                                                    
                                                </span>
                                                <span class="hidden notes_full_text" ><?php echo $pending_note->notes;?></span>
                                                <span class="hidden notes_comment_text"><?php echo $pending_note->txt_notesReplay;?></span>
                                                </a>
                                            </li><!-- end notification -->
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="/admin/notes"><?php echo lang('header_view_all'); ?></a></li>
                                </ul>
                            </li>
            <script>
        $("document").ready(function(){
            $("a.notes-modal-link").click(function(){

                var title = $(this).find(".notes-title").text();
                var note_date = $(this).find(".note-date").text();
                var notes_allocate = $(this).find(".notes_allocate").text();
                var patient_id = $(this).find(".notes_patient_id").text();
                var note = $(this).find(".notes_full_text").text();
                var comment = $(this).find(".notes_comment_text").text();
                var note_id = $(this).attr('data-noteid');

                $('#viewNotesModal h4').html(title);
                $('#viewNotesModal .modal-body .note-info').html("<div class='note-modal-patient'><label>Patient:</label> "+patient_id+"</div><div class='note-modal-allocate'><label>Allocated users:</label> "+notes_allocate+"</div><div class='note-modal-date'><label>Date:</label> "+note_date+"</div><div class='note-modal-text'><label>Note:</label><br />"+note+"</div>");

                $("#viewNotesModal .modal_notes_comment_text").val(comment);
                $("#viewNotesModal .modal_note_id").val(note_id);

                $("a#modal-done-note-id").attr("href","/admin/notes/deactivate/"+note_id+"");

                $('#viewNotesModal').modal('show');
            });

        });
    </script>  
<?php endif; ?>
<?php if ($admin_prefs['tasks_menu'] == TRUE): ?>
                            <!-- Tasks -->
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger"><?php echo count($pending_tasks);?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><?php echo lang('header_you_have'); ?> <?php echo count($pending_tasks);?> <?php echo lang('header_task'); ?></li>
                                    <li>
                                        <ul class="menu">
                                            <li><!-- start task -->
                                                <?php if (count($pending_tasks) > 0){?>

                                                    <?php foreach($pending_tasks as $task){?>                   <a href="#" class="task-modal-link" data-taskid="<?php echo $task->id;?>">
                                                        <h3><span class="task-title"><?php echo $task->title;?></span><small class="pull-right"><?php echo $task->taskDate;?> - <?php echo $task->taskendDate;?></small></h3>
                                                        <?php /*<div class="progress xs">
                                                            <div class="progress-bar progress-bar-aqua" style="width: 0%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                                <span class="sr-only">0% <?php echo lang('header_complete'); ?></span>
                                                            </div>
                                                        </div>*/?>
                                                        <div class="advanced-status-info">
                                                            <small><?php echo ($task->rdo_Status == 1) ? "<span class='progress-bar-green'>Not Started</span>" : "";?>
                                                                <?php echo ($task->rdo_Status == 2) ? "<span class='progress-bar-yellow'>In Progress</span>" : "";?>
                                                            </small>
                                                            <small><?php echo ($task->rdo_Priority == 1) ? "<span class='progress-bar-green'>Low</span>" : "";?>
                                                                <?php echo ($task->rdo_Priority == 2) ? "<span class='progress-bar-yellow'>Medium</span>" : "";?>
                                                                <?php echo ($task->rdo_Priority == 3) ? "<span class='progress-bar-red'>High</span>" : "";?>
                                                            </small>

                                                            <?php /*<small class="task-owner">Owner:<?php echo $task->owner_name." ".$task->owner_lastname;?></small> */?>
                                                        </div>
                                                        <div class="hidden task-description">
                                                         <?php echo $task->txt_description;?>
                                                        </div>
                                                    </a>
                                                    <?}?>
                                                <?}?>
                                            </li><!-- end task -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="/admin/tasks"><?php echo lang('header_view_all'); ?></a></li>
                                </ul>
                            </li>

    <script>
        $("document").ready(function(){
            $("a.task-modal-link").click(function(){

                var title = $(this).find(".task-title").text();
                var date = $(this).find("h3 small").text().split(" - ");

                var date_start = date[0]; 
                var date_end = date[1];
                var status = $(this).find(".advanced-status-info").html();
                var description = $(this).find(".task-description").html();
                var task_id = $(this).attr('data-taskid');

                $('#viewTasksModal h4').html(title);
                $('#viewTasksModal .modal-body').html(
                    "<h4>Start: " + date_start + " - End: " + date_end + "</h4><div class='advanced-status-info'>" +
                    status + "</div><div class='task-description'><label>Description:</label><br />" +
                    description + "</div>"
                );
                $("a#modal-done-task-id").attr("href","/admin/tasks/complete/"+task_id+"");
                $('#viewTasksModal').modal('show');
            });

        });
    </script>                        

<?php endif; ?>
<?php if ($admin_prefs['user_menu'] == TRUE): ?>
                            <!-- User Account -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url($avatar_dir . '/'.$avatar_img); ?>" class="user-image" alt="User Image">
                                    <span class="hidden"><?php echo $user_login['username']; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="<?php echo base_url($avatar_dir . '/'.$avatar_img); ?>" class="img-circle" alt="User Image">
                                        <p><?php echo $user_login['firstname'].$user_login['lastname']; ?><small><?php echo lang('header_member_since'); ?> <?php echo date('d-m-Y', $user_login['created_on']); ?></small></p>
                                    </li>
                                    <?php /*<li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#"><?php echo lang('header_followers'); ?></a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#"><?php echo lang('header_sales'); ?></a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#"><?php echo lang('header_friends'); ?></a>
                                            </div>
                                        </div>
                                    </li>*/?>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('admin/users/profile/'.$user_login['id']); ?>" class="btn btn-default btn-flat"><?php echo lang('header_profile'); ?></a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('auth/logout/admin'); ?>" class="btn btn-default btn-flat"><?php echo lang('header_sign_out'); ?></a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

<?php endif; ?>
<?php if ($admin_prefs['ctrl_sidebar'] == TRUE): ?>
                            <!-- Control Sidebar Toggle Button -->
                            <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
<?php endif; ?>
                        </ul>
                    </div>
                </nav>
            </header>
