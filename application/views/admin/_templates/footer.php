<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

    <footer class="main-footer">
        <div class="hidden-xs">
            <b><?php echo lang('footer_version'); ?></b> 1.0
        </div>

    </footer>
</div>

<div class="modal fade" id="viewTasksModal" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary pull-left delete-action" id="modal-done-task-id" href="#"><i class="icon fa fa-check"></i><?php echo lang("actions_done");?></a>
        <button type="button" class="btn btn-submit pull-right" data-dismiss="modal"><?php echo lang("actions_close");?></button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="viewNotesModal" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">View Note</h4>
      </div>
      <form method="POST" action="/admin/notes/save">
          <div class="modal-body">
            <div class="note-info"></div>
            <div class="form-group">
                <label>Comment:</label>
                <textarea class="form-control modal_notes_comment_text" name="txt_notesReplay"></textarea>
                <input type="hidden" name="note_id" class="modal_note_id" value="" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary pull-left" id="modal-note-save"><i class="icon fa fa-save"></i>&nbsp;<?php echo lang("actions_save");?></button>
            <a class="btn btn-success pull-left" id="modal-done-note-id" href="#"><i class="icon fa fa-check"></i>&nbsp;<?php echo lang("actions_done");?></a>
            <button type="button" class="btn btn-submit pull-right" data-dismiss="modal"><?php echo lang("actions_close");?></button>
          </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<?php if($is_reception){?>
<div class="modal fade" id="quick-upload-results" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="app-add-title"><?php echo lang("upload_title");?></h4>
      </div>
      <form action="/admin/patientsections/quick_add_results" enctype="multipart/form-data" method="POST">
      <div class="modal-body">
        <div class="form-group row">
            <?php echo lang('upload_result_date', 'results_date', array('class' => 'col-xs-12 control-label')); ?>
            <div class="date col-sm-12">
              <input type="date" id="results_date" class="form-control required-field" name="results_date" value="<?php echo date('Y-m-d');?>"></input>
            </div>
        </div>

        <div class="form-group row">
            <?php echo lang('upload_result_select_patient', 'results_patient', array('class' => 'col-xs-12 control-label')); ?>
            <div class="col-xs-12">
                <select class="form-control select2 select2-hidden-accessible required-field" name="results_patient" id="results_patient" style="width: 100%;" tabindex="-1" aria-hidden="true">
                    <option value="0" selected="selected">--Select patient--</option>
                    <?php foreach($results_patients as $patient){?>
                        <option value="<?php echo $patient->id;?>"><?php echo $patient->patient_name;?> [<?php echo $patient->id;?>] (tel:<?php echo $patient->telephone;?>; <?php echo $patient->mobile;?>)</option>
                    <?}?>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <?php echo lang('upload_result_select_section', 'upload_section', array('class' => 'col-xs-12 control-label')); ?>
            <div class="col-xs-12">
                <select name="results_section" class="form-control required-field">
                <?php foreach($upload_sections as $key=>$label){?>
                    <option value="<?php echo $key;?>"><?php echo $label;?></option>
                <?php }?>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-xs-12">
                <?php echo lang('upload_results_files', 'results_files', array('class' => 'control-label')); ?>
                <input name="results_files[]" id="results_files" type="file" multiple class="form-control required-field"></input>
            </div>
        </div>

        <?php /*
        <div class="form-group row">
            <div class="col-xs-12">
                <?php echo lang('results_desc', 'results_desc', array('class' => 'control-label')); ?>
                <textarea name="results_desc" rows="4" id="results_desc" type="text" class="form-control"></textarea>
                <input type="hidden" name="desc_section" value="14">
            </div>
        </div>
        */?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang("actions_close");?></button>
        <button type="button" type="submit" class="btn btn-primary save_results"><?php echo lang("actions_save");?></button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
  $("document").ready(function(){
    
    $("#quick-upload-results .save_results").click(function(){
        var form = $("#quick-upload-results form");
        var validate = true;

        $(form).find("div.red").remove();
        $(form).find(".required-field").each(function(){ 
          
          if($(this).val() == "" || $(this).val() == 0){
            validate = false;
            $(this).parent().append("<div class='red'><?php echo lang('upload_empty_field')?></div>");
          }

        });

        if (validate){
          $(form).submit().reset();
        }else{
          return false;
        }

    });
  });
</script>
<?}?>

<script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="<?php echo base_url($frameworks_dir . '/bootstrap/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<script src="/assets/plugins/fullcalendar/3.9.0/fullcalendar.min.js"></script>

<script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>
<?php if ($mobile == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' || $this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>


<script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
<script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
<script type="text/javascript" src="/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/main.js"></script>
<script src="/assets/plugins/select2/select2.min.js"></script>

</body>
</html>