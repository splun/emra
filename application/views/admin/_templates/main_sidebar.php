<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <aside class="main-sidebar">
                <section class="sidebar">
<?php if ($admin_prefs['user_panel'] == TRUE): ?>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">  
                            <img src="<?php echo base_url($avatar_dir . '/' . $avatar_img); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $user_login['firstname'].$user_login['lastname']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                        </div>
                    </div>

<?php endif; ?>
<?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
                    <!-- Search form -->
                    <form action="/admin/patients/" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" value="<?php echo (isset($_GET['search'])) ? $_GET['search'] : '';?>" placeholder="<?php echo lang('menu_search'); ?>...">
                            <span class="input-group-btn">
                                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <ul class="sidebar-menu">
                        <li ><a href="/admin/patients/add/"><i class="fa fa-plus"></i> <span><?php echo lang('menu_add_patients'); ?></span></a></li>
                    </ul>
<?php endif; ?>
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu">

                        <li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
                        <li class="<?=active_link_controller('dashboard')?>">
                            <a href="<?php echo site_url('admin/dashboard'); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                            </a>
                        </li>

                        <li class="treeview <?=active_link_controller('appointments')?>">
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span><?php echo lang('menu_appointments'); ?></span><i class="fa fa-angle-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('tooday')?>">
                                    <a href="<?php echo site_url('admin/appointments/tooday'); ?>">
                                        <span><?php echo lang('menu_appointments_today'); ?></span>
                                    </a>
                                </li>
                                <li class="<?=active_link_function('pending')?>">
                                    <a href="<?php echo site_url('admin/appointments/pending'); ?>">
                                        <span><?php echo lang('menu_appointments_pending'); ?></span>
                                    </a>
                                </li>
                                <li class="<?=active_link_function('index')?>">
                                    <a href="<?php echo site_url('admin/appointments'); ?>">
                                        <span><?php echo lang('menu_appointments_add'); ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="treeview <?=active_link_controller('patients')?>">
                            <a href="#">
                                <i class="fa fa-users"></i> <span><?php echo lang('menu_patients'); ?></span><i class="fa fa-angle-down pull-right"></i>
                            </a>
                             <ul class="treeview-menu">
                                <li class="<?=active_link_controller('patients')?>">
                                    <a href="<?php echo site_url('admin/patients'); ?>">
                                        <?php echo lang('menu_patients');?>
                                        
                                    </a>
                                </li>
                                <li class="<?=(isset($_GET['immuno']) && $_GET['immuno'] == 1) ? 'active' : '';?>">
                                    <a href="<?php echo site_url('admin/patients?immuno=1'); ?>">
                                        <?php echo lang('menu_immuno_patients');?>
                                    </a>
                                </li>
                             </ul>
                        </li>

                        <li class="treeview <?=active_link_controller('stock');?>">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span><?php echo lang('menu_stock'); ?></span>
                                <i class="fa fa-angle-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_controller('instock');?>"><a href="<?php echo site_url('admin/stock/instock'); ?>"><?php echo lang('menu_in_stock'); ?></a></li>
                                <li class="<?=active_link_controller('stockmedications');?>"><a href="<?php echo site_url('admin/stock/stockmedications'); ?>"><?php echo lang('menu_stock_medications'); ?></a></li>
                                <li class="<?=active_link_controller('locations');?>"><a href="<?php echo site_url('admin/stock/locations'); ?>"><?php echo lang('menu_stock_locations'); ?></a></li>
                                </li>
                            </ul>
                        </li>

                        <?php if(!$is_nurse){?>
                        <li class="<?=(isset($_GET['debt']) && $_GET['debt'] == 1) ? 'active' : '';?>">
                            <a href="<?php echo site_url('/admin/reports/invoice_report/?debt=1'); ?>">
                                <i class="fa fa-money"></i> <span><?php echo lang('menu_pending_payments'); ?></span>
                            </a>
                        </li>
                        <?}?>

                        <li class="treeview <?=active_link_controller('reports')?>">
                            <a href="#">
                                <i class="fa fa-book"></i> <span><?php echo lang('menu_reports'); ?></span><i class="fa fa-angle-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_controller('advanced_user_search');?>"><a href="<?php echo site_url('admin/reports/advanced_user_search'); ?>"><?php echo lang('menu_adv_search_report'); ?></a></li>
                                <?php if(!$is_nurse){?>
                                <li class="<?=active_link_controller('invoice_report');?>"><a href="<?php echo site_url('admin/reports/invoice_report'); ?>"><?php echo lang('menu_invoice_report'); ?></a></li>
                                <li class="<?=active_link_controller('sales_report');?>"><a href="<?php echo site_url('admin/reports/sales_report'); ?>"><?php echo lang('menu_sales_report'); ?></a></li>
                                <?}?>
                            </ul>
                        </li>

                    <?php if($this->ion_auth->is_admin()){?>
                        <li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
                        <li class="<?=active_link_controller('users')?>">
                            <a href="<?php echo site_url('admin/users'); ?>">
                                <i class="fa fa-user"></i> <span><?php echo lang('menu_users'); ?></span>
                            </a>
                        </li>
                        <li class="<?=active_link_controller('groups')?>">
                            <a href="<?php echo site_url('admin/groups'); ?>">
                                <i class="fa fa-shield"></i> <span><?php echo lang('menu_security_groups'); ?></span>
                            </a>
                        </li>

                        <li class="treeview <?=active_link_controller('settings');?>">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span><?php echo lang('menu_preferences'); ?></span>
                                <i class="fa fa-angle-down pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('admin/settings/interfaces/admin'); ?>"><?php echo lang('menu_interfaces'); ?></a></li>
                                <li class="treeview <?=active_link_controller('patient_files')?>">
                                    <a href="#">
                                        <span><?php echo lang('menu_patient_files'); ?></span>
                                        <i style="margin-right:10px;" class="fa fa-angle-down pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu sub-2">
                                        <li class="<?=active_link_function('1')?>"><a href="/admin/settings/patient_files/1">Complaints</a></li>

                                        <li class="<?=active_link_subfunction('1','ddl')?>"><a href="/admin/settings/patient_files/ddl/1">Complaints DDL</a></li>

                                        <li class="<?=active_link_function('2')?>"><a href="/admin/settings/patient_files/2">Diagnosis</a></li>

                                        <li class="<?=active_link_subfunction('2','ddl')?>"><a href="/admin/settings/patient_files/ddl/2">Diagnosis DDL</a></li>

                                        <li class="<?=active_link_function('3')?>"><a href="/admin/settings/patient_files/3">Environment</a></li>

                                        <li class="<?=active_link_function('11')?>"><a href="/admin/settings/patient_files/11">Birth and Vaccination</a></li>

                                        <li class="<?=active_link_function('4')?>"><a href="/admin/settings/patient_files/4">Family History</a></li>

                                        <li class="<?=active_link_function('5')?>"><a href="/admin/settings/patient_files/5">Asthma</a></li>

                                        <li class="<?=active_link_function('6')?>"><a href="/admin/settings/patient_files/6">Eczema</a></li>

                                        <li class="<?=active_link_function('7')?>"><a href="/admin/settings/patient_files/7">Rhino-Conjunctivitis</a></li>

                                        <li class="<?=active_link_function('29')?>"><a href="/admin/settings/patient_files/29">Food Allergy Name</a></li>

                                        <li class="<?=active_link_subfunction('29','ddl')?>"><a href="/admin/settings/patient_files/ddl/29">Food Allergy Question</a></li>

                                        <li class="<?=active_link_function('30')?>"><a href="/admin/settings/patient_files/30">Drug Allergy Name</a></li>

                                        <li class="<?=active_link_subfunction('30','ddl')?>"><a href="/admin/settings/patient_files/ddl/30">Drug Allergy Question</a></li>
                                        <li class="<?=active_link_function('9')?>"><a href="/admin/settings/patient_files/9">Immunity</a></li>

                                        <li class="<?=active_link_function('12')?>"><a href="/admin/settings/patient_files/12">Diet</a></li>

                                        <li class="<?=active_link_function('10')?>"><a href="/admin/settings/patient_files/10">Development</a></li>

                                        <li class="<?=active_link_function('13')?>"><a href="/admin/settings/patient_files/13">Physical Exam</a></li>

                                        <li class="<?=active_link_function('15')?>"><a href="/admin/settings/patient_files/15">ImmunoCAP</a></li>

                                        <li class="<?=active_link_function('16')?>"><a href="/admin/settings/patient_files/16">Skin Testing</a></li>

                                        <li class="<?=active_link_function('17')?>"><a href="/admin/settings/patient_files/17">Rhinoscopy</a></li>

                                        <li class="<?=active_link_function('18')?>"><a href="/admin/settings/patient_files/18">Spirometer</a></li>

                                        <li class="<?=active_link_function('25')?>"><a href="/admin/settings/patient_files/25">Investigations</a></li>

                                        <li class="<?=active_link_function('26')?>"><a href="/admin/settings/patient_files/26">Immunotherapy</a></li>

                                        <li class="<?=active_link_subfunction('27','ddl')?>"><a href="/admin/settings/patient_files/ddl/27">Referred Speciality DDL</a></li>
                                    </ul>
                                </li>
                                <li class="<?=active_link_controller('medications')?>"><a href="<?php echo site_url('admin/settings/medications'); ?>"><?php echo lang('menu_medications'); ?></a></li>
                                <li class="<?=active_link_controller('invoice')?>"><a href="<?php echo site_url('admin/settings/invoice'); ?>"><?php echo lang('menu_settings_invoice'); ?></a></li>
                                <li class="<?=active_link_controller('insurance')?>"><a href="<?php echo site_url('admin/settings/insurance'); ?>"><?php echo lang('menu_settings_insurance'); ?></a></li>
                            </ul>
                        </li>


                        <li class="<?=active_link_controller('database')?>">
                            <a href="<?php echo site_url('admin/database'); ?>">
                                <i class="fa fa-database"></i> <span><?php echo lang('menu_database_utility'); ?></span>
                            </a>
                        </li>
                        <?php /*
                        <li class="<?=active_link_controller('resources')?>">
                            <a href="<?php echo site_url('admin/resources'); ?>">
                                <i class="fa fa-cubes"></i> <span><?php echo lang('menu_resources'); ?></span>
                            </a>
                        </li>
                        */?>
                    <?}?>
                    </ul>
                </section>
            </aside>
