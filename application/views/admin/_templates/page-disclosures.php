<?php
/**
 * Page (page.php)
 * @package WordPress
 * @subpackage Special Template
 */

get_header(); ?> 

	<main>
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="content" style="background:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)no-repeat;background-size:cover;">
			<div class="null1"></div>
			<div class="sub-navigation">
				<div class="container">
					<?php $args = array( 
						'theme_location' => 'sub',
						'container'=> false,
				  		'menu_id' => '',
				  		'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'menu_class' => ''	  		
							);
						wp_nav_menu($args);
					?>
				<?php/* <ul>
					<li><a href="/about-us/">ABOUT US</a></li>
					<li><a href="/about-us/history">OUR HISTORY</a></li>
					<li><a href="/about-us/vision">Vision, Mission, Values</a></li>
					<li><a href="/about-us/governance">Corporate Governance</a></li>
					<li><a href="/about-us/management">Executive Management</a></li>
				</ul>*/?>
				</div>
			</div>
			<div class="wrapper">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h2 class="slash"><?php the_title();?></h2>
							<?php the_content();?>
						</div>
					</div>
				</div>
			</div>

		</div>
		<?php endwhile; ?>
	</main>

<?php get_footer(); ?>