﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">

                    <div class="box-body">
                        <?php echo $message;?>
                        <table class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data">#</th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('notes_patient');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('notes_allocated');?></th>
                                    <th class="sorting"aria-controls="list-data"><?php echo lang('notes_date');?></th>
                                    <th style="width:50%" class="sorting" aria-controls="list-data"><?php echo lang('notes_text');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('notes_comment');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('notes_status');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($notes as $note):?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo $note->patient_name." ".$note->telephone; ?></td>
                                    <td>
                                        
                                        <?php if(!empty($note->for_user)){
                                                $allocate = unserialize($note->for_user);
                                                foreach($allocate as $fuid){

                                                    $for_user_info = $glob->NotesModel->get_allocate_user_info($fuid);

                                                    echo $for_user_info->first_name." ".$for_user_info->last_name."; ";
                                                }
                                            }
                                        ?>

                                    </td>
                                    <td><?php echo date("Y-m-d",strtotime($note->originate_date)); ?></td>
                                    <td><?php echo $note->notes;?></td>
                                    <td>
                                    	<form method="POST" action="/admin/notes/save">
                                    	<div class="form-group">
                                    		<textarea class="form-control" name="txt_notesReplay"><?php echo $note->txt_notesReplay; ?></textarea>
                                    		<input type="hidden" name="note_id" value="<?php echo $note->id; ?>" />
                                    		<input type="submit" class="hidden" />
                                    	</div>
                                    	</form>
                                    </td>
                                    <td><?php echo ($note->done_flag) ? anchor('admin/notes/deactivate/'.$note->id, '<span class="label label-default">'.lang('actions_done').'</span>') : anchor('admin/notes/activate/'. $note->id, '<span class="label label-success">Pending</span>'); ?></td>

                                    <td class="action-links">
                                    	<a class="" href="#" onclick="$(this).parent().parent().find('form').submit(); return false;"><i class="fa fa-save"></i>&nbsp;Save</a>
                                    	<?php echo anchor('admin/notes/delete/'.$note->id, lang('actions_delete'),array('class' => 'delete-action')); ?>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>