<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                 <div class="box">

                    <div class="box-body">
                        <?php echo $message;?>
                        <table class="table table-bordered table-hover customTable" role="grid" aria-describedby="example2_info">
                            <thead>
                                <tr>
                                    <th class="sorting" aria-controls="list-data">#</th>
                                    <th class="sorting" aria-sort="descending" aria-controls="list-data"><?php echo lang('tasks_title_label');?></th>
                                    <th class="sorting"aria-controls="list-data"><?php echo lang('tasks_start_label');?></th>
                                    <th class="sorting" aria-controls="list-data"><?php echo lang('tasks_end_label');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('tasks_text_label');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('tasks_priority_label');?></th>
                                    <th class="sorting"  aria-controls="list-data"><?php echo lang('tasks_status_label');?></th>
                                    <th><?php echo lang('actions_title');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach ($tasks as $task):?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td><?php echo htmlspecialchars($task->title, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo $task->taskDate; ?></td>
                                    <td><?php echo $task->taskendDate; ?></td>
                                    <td><?php echo $task->txt_description; ?></td>
                                    <td>
                                    	<div class="advanced-status-info">
                                            <small><?php echo ($task->rdo_Priority == 1) ? "<span class='progress-bar-green'>Low</span>" : "";?>
                                                <?php echo ($task->rdo_Priority == 2) ? "<span class='progress-bar-yellow'>Medium</span>" : "";?>
                                                <?php echo ($task->rdo_Priority == 3) ? "<span class='progress-bar-red'>High</span>" : "";?>
                                            </small>
                                        </div>
                                    </td>
                                    <td>
                                    	<div class="advanced-status-info">
                                    		<?php if($task->task_status == 1 && $task->rdo_Status == 2){?>
                                    			<small><span class='progress-bar-aqua'>Done</span>
                                            	</small>
                                            <?php } else {?>
                                            	<small><?php echo ($task->rdo_Status == 1) ? "<span class='progress-bar-green'>Not Started</span>" : "";?>
                                                <?php echo ($task->rdo_Status == 2) ? "<span class='progress-bar-yellow'>In Progress</span>" : "";?>
                                            	</small>
                                            <?}?>
                                    	</div>
                                    </td>
                                    <td class="action-links">
                                    <?php if($task->task_status != 1 || $task->rdo_Status != 2){?>
                                    	<?php if($task->rdo_Status == 2){

                                    	 echo anchor('admin/tasks/complete/'.$task->id, lang('actions_done'),array('class' => 'delete-action'));

                                    	}else{

                                        echo anchor('admin/tasks/start/'.$task->id, lang('actions_start'),array('class' => 'delete-action')); 

                                    	}?>
                                    <?}?>
                                    </td>
                                </tr>
                            <?php $i++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
    </section>
</div>