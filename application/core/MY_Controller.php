<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

        /* COMMON :: ADMIN & PUBLIC */
        /* Load */
        $this->load->database();
        $this->load->config('common/dp_config');
        $this->load->config('common/dp_language');
        $this->load->library(array('form_validation', 'ion_auth', 'template', 'common/mobile_detect'));
        $this->load->helper(array('array', 'language', 'url'));
        $this->load->model('common/prefs_model');

        /* Data */
        $this->data['lang']           = element($this->config->item('language'), $this->config->item('language_abbr'));
        $this->data['charset']        = $this->config->item('charset');
        $this->data['frameworks_dir'] = $this->config->item('frameworks_dir');
        $this->data['plugins_dir']    = $this->config->item('plugins_dir');
        $this->data['avatar_dir']     = $this->config->item('avatar_dir');

        /* Any mobile device (phones or tablets) */
        if ($this->mobile_detect->isMobile())
        {
            $this->data['mobile'] = TRUE;

            if ($this->mobile_detect->isiOS()){
                $this->data['ios']     = TRUE;
                $this->data['android'] = FALSE;
            }
            else if ($this->mobile_detect->isAndroidOS())
            {
                $this->data['ios']     = FALSE;
                $this->data['android'] = TRUE;
            }
            else
            {
                $this->data['ios']     = FALSE;
                $this->data['android'] = FALSE;
            }

            if ($this->mobile_detect->getBrowsers('IE')){
                $this->data['mobile_ie'] = TRUE;
            }
            else
            {
                $this->data['mobile_ie'] = FALSE;
            }
        }
        else
        {
            $this->data['mobile']    = FALSE;
            $this->data['ios']       = FALSE;
            $this->data['android']   = FALSE;
            $this->data['mobile_ie'] = FALSE;
        }
	}
}


class Admin_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

        if ( ! $this->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Load */
            $this->load->config('admin/dp_config');
            $this->load->library('admin/page_title');
            $this->load->library('admin/breadcrumbs');

            $this->load->model('admin/core_model');
            $this->load->model('admin/appointmentsModel');
            $this->load->model('admin/Stock');
            $this->load->model('admin/PatientsModel');
            $this->load->model('admin/NotesModel');
            $this->load->model('admin/TasksModel');

            $this->load->helper('menu');
            $this->lang->load(array('admin/main_header', 'admin/main_sidebar', 'admin/footer', 'admin/actions'));

            /* Load library function  */
            $this->breadcrumbs->unshift(0, $this->lang->line('menu_dashboard'), 'admin/dashboard');

            /* Data */
            $this->data['title']       = $this->config->item('title');
            $this->data['title_lg']    = $this->config->item('title_lg');
            $this->data['title_mini']  = $this->config->item('title_mini');
            $this->data['admin_prefs'] = $this->prefs_model->admin_prefs();
            $this->data['user_login']  = $this->prefs_model->user_info_login($this->ion_auth->user()->row()->id);
            
            $this->data['task_message_popup'] = (!empty($this->session->flashdata('task_message_popup')) ? $this->session->flashdata('task_message_popup') : "");


            /***User Groups***/
            $this->data['is_admin'] = $this->ion_auth->is_admin();

            $this->data['is_doctor'] = $this->ion_auth->in_group("Doctors");

            $this->data['is_nurse'] = ($this->ion_auth->in_group("Nurses"));

            $this->data['is_reception'] = ($this->ion_auth->in_group("Reception"));

            $this->data['avatar_img'] = ($this->ion_auth->in_group("Nurses") || $this->ion_auth->in_group("Reception")) ? "f_001.png" : "m_001.png";
            $this->data['avatar_img_2'] = ($this->ion_auth->in_group("Nurses") || $this->ion_auth->in_group("Reception")) ? "f_002.png" : "m_002.png";

            $this->data['count_pending_cases'] = $this->appointmentsModel->get_count_pending_cases();
            $this->data['over_stock'] = $this->Stock->get_over_stock_items();

            if($this->ion_auth->is_admin() || $this->ion_auth->in_group("Reception")){
                $this->data['count_pending_payments'] = $this->PatientsModel->get_count_pending_payments();
            }else{
                $this->data['count_pending_payments'] = $this->PatientsModel->get_count_pending_payments($this->ion_auth->user()->row()->id);
            }
        
            if($this->ion_auth->is_admin() || $this->ion_auth->user()->row()->id == 5){
                $this->data['count_pending_notes'] = $this->core_model->get_count_items("notes",array("done_flag"=>0));

                $this->data['pending_notes'] = $this->NotesModel->get_notes("pending","",25);
            }else{
                $this->data['count_pending_notes'] = $this->core_model->get_count_items("notes",array("done_flag"=>0,"for_user REGEXP"=>$this->ion_auth->user()->row()->id));

                $this->data['pending_notes'] = $this->NotesModel->get_notes("pending-owner",$this->ion_auth->user()->row()->id,25);
                //$this->data['pending_notes'] = $this->core_model->get_items("notes",array("done_flag"=>0,"create_user"=>$this->ion_auth->user()->row()->id),"id","DESC",25);
            }

            $this->data['pending_tasks'] = $this->TasksModel->get_tasks($this->ion_auth->user()->row()->id,true);
        
            if ($this->router->fetch_class() == 'dashboard')
            {
                $this->data['dashboard_alert_file_install'] = $this->core_model->get_file_install();
                $this->data['header_alert_file_install']    = NULL;
            }
            else
            {
                $this->data['dashboard_alert_file_install'] = NULL;
                $this->data['header_alert_file_install']    = NULL; /* << A MODIFIER !!! */
            }

        }

        $this->data["results_patients"] = $this->PatientsModel->get_patients(); 
        $this->data['upload_sections']= array(
            101 => "Bloods",
            102 => "Radiology",
            103 => "Culture",
            104 => "Immune",
            105 => "Allergy",
            106 => "Urine"
        );

        $this->data["glob"] = $this;
    }

    public function calculate_age($birthday) {
        $dob_day = date("d",strtotime($birthday));
        $dob_month = date("m",strtotime($birthday));
        $dob_year = date("Y",strtotime($birthday));

        $day = date("d") - $dob_day;
        $month = date("m") - $dob_month;
        $year = date("Y") - $dob_year;

        if($day < 0)
        {
            $day = 30 + $day;
            $month = $month - 1;    
        }

        if($month < 0)
        {
            $month = 12 + $month;
            $year = $year - 1;  
        }

      return $year." year ".$month." month ".$day." days ";
    }
}


class Public_Controller extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

        if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
        {
            $this->data['admin_link'] = TRUE;
        }
        else
        {
            $this->data['admin_link'] = FALSE;
        }

        if ($this->ion_auth->logged_in())
        {
            $this->data['logout_link'] = TRUE;
        }
        else
        {
            $this->data['logout_link'] = FALSE;
        }
	}
}
