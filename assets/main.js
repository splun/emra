$(document).ready(function () {
	
    if(location.hash == "#close"){
        window.close();
    }

    var table = $('.customTable').DataTable({
        "pageLength": 25
    });
    
   /*$('#filtered-stock tfoot th').each( function (i) {
        var title = $('#filtered-stock thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+i+'" />' );
    } ); 

    $("#filtered-stock").on( 'keyup', 'input', function (){
    	$(this).change();
    });

    $("#filtered-stock").on( 'change', 'input,select', function () {
        table
            .column( $(this).data('index') )
            .search( this.value )
            .draw();
    } );*/

    $("body").on("click",".delete-action", function(){
        if (confirm("Are you sure?")) {
            return true;
        } else {
            return false;
        }
    });

    $('#reservation').daterangepicker();
    $('select.select2').select2();
});

function printContent(id){

    str=document.getElementById(id).innerHTML

    newwin=window.open('','printwin','left=0,top=0,width=625,hieght=600px')

    newwin.document.write('<HTML>\n<HEAD>\n')

    newwin.document.write('<TITLE>Print Page</TITLE>\n')

    newwin.document.write('<script>\n')

    newwin.document.write('function chkstate(){\n')

    newwin.document.write('if(document.readyState=="complete"){\n')

    newwin.document.write('window.close()\n')

    newwin.document.write('}\n')

    newwin.document.write('else{\n')

    newwin.document.write('setTimeout("chkstate()",2000)\n')

    newwin.document.write('}\n')

    newwin.document.write('}\n')

    newwin.document.write('function print_win(){\n')

    newwin.document.write('window.print();\n')

    newwin.document.write('chkstate();\n')

    newwin.document.write('}\n')

    newwin.document.write('<\/script>\n')

    newwin.document.write('<link rel="stylesheet" href="/assets/main.css"></HEAD>\n')

    newwin.document.write('<BODY onload="print_win()">\n')

    newwin.document.write(str)

    newwin.document.write('</BODY>\n')

    newwin.document.write('</HTML>\n')

    newwin.document.close()


}
