-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 19 2019 г., 16:44
-- Версия сервера: 5.6.29
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mmrs_new`
--

-- --------------------------------------------------------

--
-- Структура таблицы `prescription_forms`
--

CREATE TABLE IF NOT EXISTS `prescription_forms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prescription_forms`
--

INSERT INTO `prescription_forms` (`id`, `name`) VALUES
(1, 'App'),
(2, 'Tab'),
(3, 'Puff'),
(4, 'Drop'),
(5, 'Supp'),
(6, 'ML'),
(7, 'Micro'),
(8, 'MG'),
(9, 'Cap'),
(10, 'Inj'),
(11, 'Unit');

-- --------------------------------------------------------

--
-- Структура таблицы `prescription_frequency`
--

CREATE TABLE IF NOT EXISTS `prescription_frequency` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prescription_frequency`
--

INSERT INTO `prescription_frequency` (`id`, `name`, `sort`) VALUES
(1, 'Prn', 1),
(2, 'Once', 2),
(3, 'Twice', 3),
(4, 'TiD', 5),
(5, 'QiD', 6),
(6, 'Night', 7),
(7, '3 times wk', 8),
(8, '4 hourly', 9),
(9, '2 times wk', 10),
(10, '6 Hourly', 4),
(11, 'Once wk', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `prescription_route`
--

CREATE TABLE IF NOT EXISTS `prescription_route` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prescription_route`
--

INSERT INTO `prescription_route` (`id`, `name`) VALUES
(1, 'PO'),
(4, 'IM'),
(5, 'SC'),
(6, 'IV'),
(7, 'PR'),
(8, 'Topical'),
(9, 'NEB'),
(10, 'MDI'),
(11, 'Nasal'),
(12, 'Ears'),
(13, 'Eyes');

-- --------------------------------------------------------

--
-- Структура таблицы `prescription_use`
--

CREATE TABLE IF NOT EXISTS `prescription_use` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `sort` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prescription_use`
--

INSERT INTO `prescription_use` (`id`, `name`, `sort`) VALUES
(1, 'After food', 1),
(2, 'Before food', 2),
(3, 'face', 7),
(4, 'Body', 3),
(5, 'diaper area', 6),
(6, 'night', 14),
(7, 'morning', 13),
(8, 'as needed', 5),
(9, 'acute use', 4),
(10, 'feet', 8),
(11, 'genitals', 9),
(12, 'hands', 10),
(13, 'maintanance use', 11),
(14, 'mix saline', 12),
(15, 'step 1', 15),
(16, 'step 2', 16),
(17, 'step 3', 17);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `prescription_forms`
--
ALTER TABLE `prescription_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prescription_frequency`
--
ALTER TABLE `prescription_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prescription_route`
--
ALTER TABLE `prescription_route`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prescription_use`
--
ALTER TABLE `prescription_use`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `prescription_forms`
--
ALTER TABLE `prescription_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `prescription_frequency`
--
ALTER TABLE `prescription_frequency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `prescription_use`
--
ALTER TABLE `prescription_use`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
